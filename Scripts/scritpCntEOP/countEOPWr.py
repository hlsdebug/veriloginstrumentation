import sys
#inputfile = open('adpcm.v')

inputfile = open(sys.argv[1])
#outputfile = open('adpcm_cnt.v', 'w')
outputfile = open(sys.argv[1]+".cnt", 'w')

for line in inputfile:
	outputfile.writelines(line)
	if "eop_" in line:
		break

print line

eop_lines = []

for line in inputfile:
	if "ap_return" in line:
		break
	else:
		if "_vld" in line:
			eop_lines.append(line)
		outputfile.writelines(line)

splitted_lines = []

for eop_line in eop_lines:
	splitted_lines.append(eop_line.split('.')[1].split('(')[0] + "_sig + ")
	
outputfile.write("wire [31:0]sum_valids = ")
outputfile.writelines(splitted_lines)
outputfile.write(";\n")

outputfile.writelines("DONE")

for line in inputfile:
	outputfile.writelines(line)


inputfile.close()
outputfile.close()
