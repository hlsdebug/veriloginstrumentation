// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
dfsin_main dfsin_main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_157_main_result_159(eop_157_main_result_159_sig) ,	// output [31:0] eop_157_main_result_159_sig
	.eop_157_main_result_159_ap_vld(eop_157_main_result_159_ap_vld_sig) ,	// output  eop_157_main_result_159_ap_vld_sig
	.eop_50_diff_54(eop_50_diff_54_sig) ,	// output [63:0] eop_50_diff_54_sig
	.eop_50_diff_54_ap_vld(eop_50_diff_54_ap_vld_sig) ,	// output  eop_50_diff_54_ap_vld_sig
	.eop_49_app_54(eop_49_app_54_sig) ,	// output [63:0] eop_49_app_54_sig
	.eop_49_app_54_ap_vld(eop_49_app_54_ap_vld_sig) ,	// output  eop_49_app_54_ap_vld_sig
	.eop_52_inc_55(eop_52_inc_55_sig) ,	// output [31:0] eop_52_inc_55_sig
	.eop_52_inc_55_ap_vld(eop_52_inc_55_ap_vld_sig) ,	// output  eop_52_inc_55_ap_vld_sig
	.eop_473_aSig_475(eop_473_aSig_475_sig) ,	// output [63:0] eop_473_aSig_475_sig
	.eop_473_aSig_475_ap_vld(eop_473_aSig_475_ap_vld_sig) ,	// output  eop_473_aSig_475_ap_vld_sig
	.eop_472_aExp_476(eop_472_aExp_476_sig) ,	// output [31:0] eop_472_aExp_476_sig
	.eop_472_aExp_476_ap_vld(eop_472_aExp_476_ap_vld_sig) ,	// output  eop_472_aExp_476_ap_vld_sig
	.eop_471_aSign_477(eop_471_aSign_477_sig) ,	// output [31:0] eop_471_aSign_477_sig
	.eop_471_aSign_477_ap_vld(eop_471_aSign_477_ap_vld_sig) ,	// output  eop_471_aSign_477_ap_vld_sig
	.eop_473_bSig_478(eop_473_bSig_478_sig) ,	// output [63:0] eop_473_bSig_478_sig
	.eop_473_bSig_478_ap_vld(eop_473_bSig_478_ap_vld_sig) ,	// output  eop_473_bSig_478_ap_vld_sig
	.eop_472_bExp_479(eop_472_bExp_479_sig) ,	// output [31:0] eop_472_bExp_479_sig
	.eop_472_bExp_479_ap_vld(eop_472_bExp_479_ap_vld_sig) ,	// output  eop_472_bExp_479_ap_vld_sig
	.eop_471_bSign_480(eop_471_bSign_480_sig) ,	// output [31:0] eop_471_bSign_480_sig
	.eop_471_bSign_480_ap_vld(eop_471_bSign_480_ap_vld_sig) ,	// output  eop_471_bSign_480_ap_vld_sig
	.eop_471_zSign_481(eop_471_zSign_481_sig) ,	// output [31:0] eop_471_zSign_481_sig
	.eop_471_zSign_481_ap_vld(eop_471_zSign_481_ap_vld_sig) ,	// output  eop_471_zSign_481_ap_vld_sig
	.eop_124_shiftCount_126(eop_124_shiftCount_126_sig) ,	// output [31:0] eop_124_shiftCount_126_sig
	.eop_124_shiftCount_126_ap_vld(eop_124_shiftCount_126_ap_vld_sig) ,	// output  eop_124_shiftCount_126_ap_vld_sig
	.eop_472_zExp_516(eop_472_zExp_516_sig) ,	// output [31:0] eop_472_zExp_516_sig
	.eop_472_zExp_516_ap_vld(eop_472_zExp_516_ap_vld_sig) ,	// output  eop_472_zExp_516_ap_vld_sig
	.eop_473_aSig_517(eop_473_aSig_517_sig) ,	// output [63:0] eop_473_aSig_517_sig
	.eop_473_aSig_517_ap_vld(eop_473_aSig_517_ap_vld_sig) ,	// output  eop_473_aSig_517_ap_vld_sig
	.eop_473_bSig_518(eop_473_bSig_518_sig) ,	// output [63:0] eop_473_bSig_518_sig
	.eop_473_bSig_518_ap_vld(eop_473_bSig_518_ap_vld_sig) ,	// output  eop_473_bSig_518_ap_vld_sig
	.eop_472_zExp_524(eop_472_zExp_524_sig) ,	// output [31:0] eop_472_zExp_524_sig
	.eop_472_zExp_524_ap_vld(eop_472_zExp_524_ap_vld_sig) ,	// output  eop_472_zExp_524_ap_vld_sig
	.eop_176_roundingMode_180(eop_176_roundingMode_180_sig) ,	// output [31:0] eop_176_roundingMode_180_sig
	.eop_176_roundingMode_180_ap_vld(eop_176_roundingMode_180_ap_vld_sig) ,	// output  eop_176_roundingMode_180_ap_vld_sig
	.eop_177_roundNearestEven_181(eop_177_roundNearestEven_181_sig) ,	// output [31:0] eop_177_roundNearestEven_181_sig
	.eop_177_roundNearestEven_181_ap_vld(eop_177_roundNearestEven_181_ap_vld_sig) ,	// output  eop_177_roundNearestEven_181_ap_vld_sig
	.eop_178_roundIncrement_182(eop_178_roundIncrement_182_sig) ,	// output [31:0] eop_178_roundIncrement_182_sig
	.eop_178_roundIncrement_182_ap_vld(eop_178_roundIncrement_182_ap_vld_sig) ,	// output  eop_178_roundIncrement_182_ap_vld_sig
	.eop_178_roundBits_204(eop_178_roundBits_204_sig) ,	// output [31:0] eop_178_roundBits_204_sig
	.eop_178_roundBits_204_ap_vld(eop_178_roundBits_204_ap_vld_sig) ,	// output  eop_178_roundBits_204_ap_vld_sig
	.eop_177_isTiny_215(eop_177_isTiny_215_sig) ,	// output [31:0] eop_177_isTiny_215_sig
	.eop_177_isTiny_215_ap_vld(eop_177_isTiny_215_ap_vld_sig) ,	// output  eop_177_isTiny_215_ap_vld_sig
	.eop_174_zExp_219(eop_174_zExp_219_sig) ,	// output [31:0] eop_174_zExp_219_sig
	.eop_174_zExp_219_ap_vld(eop_174_zExp_219_ap_vld_sig) ,	// output  eop_174_zExp_219_ap_vld_sig
	.eop_178_roundBits_220(eop_178_roundBits_220_sig) ,	// output [31:0] eop_178_roundBits_220_sig
	.eop_178_roundBits_220_ap_vld(eop_178_roundBits_220_ap_vld_sig) ,	// output  eop_178_roundBits_220_ap_vld_sig
	.eop_174_zSig_227(eop_174_zSig_227_sig) ,	// output [63:0] eop_174_zSig_227_sig
	.eop_174_zSig_227_ap_vld(eop_174_zSig_227_ap_vld_sig) ,	// output  eop_174_zSig_227_ap_vld_sig
	.eop_174_zExp_230(eop_174_zExp_230_sig) ,	// output [31:0] eop_174_zExp_230_sig
	.eop_174_zExp_230_ap_vld(eop_174_zExp_230_ap_vld_sig) ,	// output  eop_174_zExp_230_ap_vld_sig
	.eop_51_m_rad2_56(eop_51_m_rad2_56_sig) ,	// output [63:0] eop_51_m_rad2_56_sig
	.eop_51_m_rad2_56_ap_vld(eop_51_m_rad2_56_ap_vld_sig) ,	// output  eop_51_m_rad2_56_ap_vld_sig
	.eop_263_zSign_270(eop_263_zSign_270_sig) ,	// output [31:0] eop_263_zSign_270_sig
	.eop_263_zSign_270_ap_vld(eop_263_zSign_270_ap_vld_sig) ,	// output  eop_263_zSign_270_ap_vld_sig
	.eop_264_absA_271(eop_264_absA_271_sig) ,	// output [31:0] eop_264_absA_271_sig
	.eop_264_absA_271_ap_vld(eop_264_absA_271_ap_vld_sig) ,	// output  eop_264_absA_271_ap_vld_sig
	.eop_265_shiftCount_272(eop_265_shiftCount_272_sig) ,	// output [31:0] eop_265_shiftCount_272_sig
	.eop_265_shiftCount_272_ap_vld(eop_265_shiftCount_272_ap_vld_sig) ,	// output  eop_265_shiftCount_272_ap_vld_sig
	.eop_266_zSig_273(eop_266_zSig_273_sig) ,	// output [63:0] eop_266_zSig_273_sig
	.eop_266_zSig_273_ap_vld(eop_266_zSig_273_ap_vld_sig) ,	// output  eop_266_zSig_273_ap_vld_sig
	.eop_541_aSig_544(eop_541_aSig_544_sig) ,	// output [63:0] eop_541_aSig_544_sig
	.eop_541_aSig_544_ap_vld(eop_541_aSig_544_ap_vld_sig) ,	// output  eop_541_aSig_544_ap_vld_sig
	.eop_540_aExp_545(eop_540_aExp_545_sig) ,	// output [31:0] eop_540_aExp_545_sig
	.eop_540_aExp_545_ap_vld(eop_540_aExp_545_ap_vld_sig) ,	// output  eop_540_aExp_545_ap_vld_sig
	.eop_539_aSign_546(eop_539_aSign_546_sig) ,	// output [31:0] eop_539_aSign_546_sig
	.eop_539_aSign_546_ap_vld(eop_539_aSign_546_ap_vld_sig) ,	// output  eop_539_aSign_546_ap_vld_sig
	.eop_541_bSig_547(eop_541_bSig_547_sig) ,	// output [63:0] eop_541_bSig_547_sig
	.eop_541_bSig_547_ap_vld(eop_541_bSig_547_ap_vld_sig) ,	// output  eop_541_bSig_547_ap_vld_sig
	.eop_540_bExp_548(eop_540_bExp_548_sig) ,	// output [31:0] eop_540_bExp_548_sig
	.eop_540_bExp_548_ap_vld(eop_540_bExp_548_ap_vld_sig) ,	// output  eop_540_bExp_548_ap_vld_sig
	.eop_539_bSign_549(eop_539_bSign_549_sig) ,	// output [31:0] eop_539_bSign_549_sig
	.eop_539_bSign_549_ap_vld(eop_539_bSign_549_ap_vld_sig) ,	// output  eop_539_bSign_549_ap_vld_sig
	.eop_539_zSign_550(eop_539_zSign_550_sig) ,	// output [31:0] eop_539_zSign_550_sig
	.eop_539_zSign_550_ap_vld(eop_539_zSign_550_ap_vld_sig) ,	// output  eop_539_zSign_550_ap_vld_sig
	.eop_540_zExp_590(eop_540_zExp_590_sig) ,	// output [31:0] eop_540_zExp_590_sig
	.eop_540_zExp_590_ap_vld(eop_540_zExp_590_ap_vld_sig) ,	// output  eop_540_zExp_590_ap_vld_sig
	.eop_541_aSig_591(eop_541_aSig_591_sig) ,	// output [63:0] eop_541_aSig_591_sig
	.eop_541_aSig_591_ap_vld(eop_541_aSig_591_ap_vld_sig) ,	// output  eop_541_aSig_591_ap_vld_sig
	.eop_541_bSig_592(eop_541_bSig_592_sig) ,	// output [63:0] eop_541_bSig_592_sig
	.eop_541_bSig_592_ap_vld(eop_541_bSig_592_ap_vld_sig) ,	// output  eop_541_bSig_592_ap_vld_sig
	.eop_540_zExp_596(eop_540_zExp_596_sig) ,	// output [31:0] eop_540_zExp_596_sig
	.eop_540_zExp_596_ap_vld(eop_540_zExp_596_ap_vld_sig) ,	// output  eop_540_zExp_596_ap_vld_sig
	.eop_541_zSig_598(eop_541_zSig_598_sig) ,	// output [63:0] eop_541_zSig_598_sig
	.eop_541_zSig_598_ap_vld(eop_541_zSig_598_ap_vld_sig) ,	// output  eop_541_zSig_598_ap_vld_sig
	.eop_541_zSig_605(eop_541_zSig_605_sig) ,	// output [63:0] eop_541_zSig_605_sig
	.eop_541_zSig_605_ap_vld(eop_541_zSig_605_ap_vld_sig) ,	// output  eop_541_zSig_605_ap_vld_sig
	.eop_50_diff_59(eop_50_diff_59_sig) ,	// output [63:0] eop_50_diff_59_sig
	.eop_50_diff_59_ap_vld(eop_50_diff_59_ap_vld_sig) ,	// output  eop_50_diff_59_ap_vld_sig
	.eop_451_aSign_453(eop_451_aSign_453_sig) ,	// output [31:0] eop_451_aSign_453_sig
	.eop_451_aSign_453_ap_vld(eop_451_aSign_453_ap_vld_sig) ,	// output  eop_451_aSign_453_ap_vld_sig
	.eop_451_bSign_454(eop_451_bSign_454_sig) ,	// output [31:0] eop_451_bSign_454_sig
	.eop_451_bSign_454_ap_vld(eop_451_bSign_454_ap_vld_sig) ,	// output  eop_451_bSign_454_ap_vld_sig
	.eop_290_aSig_293(eop_290_aSig_293_sig) ,	// output [63:0] eop_290_aSig_293_sig
	.eop_290_aSig_293_ap_vld(eop_290_aSig_293_ap_vld_sig) ,	// output  eop_290_aSig_293_ap_vld_sig
	.eop_289_aExp_294(eop_289_aExp_294_sig) ,	// output [31:0] eop_289_aExp_294_sig
	.eop_289_aExp_294_ap_vld(eop_289_aExp_294_ap_vld_sig) ,	// output  eop_289_aExp_294_ap_vld_sig
	.eop_290_bSig_295(eop_290_bSig_295_sig) ,	// output [63:0] eop_290_bSig_295_sig
	.eop_290_bSig_295_ap_vld(eop_290_bSig_295_ap_vld_sig) ,	// output  eop_290_bSig_295_ap_vld_sig
	.eop_289_bExp_296(eop_289_bExp_296_sig) ,	// output [31:0] eop_289_bExp_296_sig
	.eop_289_bExp_296_ap_vld(eop_289_bExp_296_ap_vld_sig) ,	// output  eop_289_bExp_296_ap_vld_sig
	.eop_291_expDiff_297(eop_291_expDiff_297_sig) ,	// output [31:0] eop_291_expDiff_297_sig
	.eop_291_expDiff_297_ap_vld(eop_291_expDiff_297_ap_vld_sig) ,	// output  eop_291_expDiff_297_ap_vld_sig
	.eop_291_expDiff_309(eop_291_expDiff_309_sig) ,	// output [31:0] eop_291_expDiff_309_sig
	.eop_291_expDiff_309_ap_vld(eop_291_expDiff_309_ap_vld_sig) ,	// output  eop_291_expDiff_309_ap_vld_sig
	.eop_289_zExp_313(eop_289_zExp_313_sig) ,	// output [31:0] eop_289_zExp_313_sig
	.eop_289_zExp_313_ap_vld(eop_289_zExp_313_ap_vld_sig) ,	// output  eop_289_zExp_313_ap_vld_sig
	.eop_291_expDiff_324(eop_291_expDiff_324_sig) ,	// output [31:0] eop_291_expDiff_324_sig
	.eop_291_expDiff_324_ap_vld(eop_291_expDiff_324_ap_vld_sig) ,	// output  eop_291_expDiff_324_ap_vld_sig
	.eop_289_zExp_330(eop_289_zExp_330_sig) ,	// output [31:0] eop_289_zExp_330_sig
	.eop_289_zExp_330_ap_vld(eop_289_zExp_330_ap_vld_sig) ,	// output  eop_289_zExp_330_ap_vld_sig
	.eop_290_zSig_342(eop_290_zSig_342_sig) ,	// output [63:0] eop_290_zSig_342_sig
	.eop_290_zSig_342_ap_vld(eop_290_zSig_342_ap_vld_sig) ,	// output  eop_290_zSig_342_ap_vld_sig
	.eop_289_zExp_343(eop_289_zExp_343_sig) ,	// output [31:0] eop_289_zExp_343_sig
	.eop_289_zExp_343_ap_vld(eop_289_zExp_343_ap_vld_sig) ,	// output  eop_289_zExp_343_ap_vld_sig
	.eop_290_zSig_347(eop_290_zSig_347_sig) ,	// output [63:0] eop_290_zSig_347_sig
	.eop_290_zSig_347_ap_vld(eop_290_zSig_347_ap_vld_sig) ,	// output  eop_290_zSig_347_ap_vld_sig
	.eop_289_zExp_348(eop_289_zExp_348_sig) ,	// output [31:0] eop_289_zExp_348_sig
	.eop_289_zExp_348_ap_vld(eop_289_zExp_348_ap_vld_sig) ,	// output  eop_289_zExp_348_ap_vld_sig
	.eop_290_zSig_351(eop_290_zSig_351_sig) ,	// output [63:0] eop_290_zSig_351_sig
	.eop_290_zSig_351_ap_vld(eop_290_zSig_351_ap_vld_sig) ,	// output  eop_290_zSig_351_ap_vld_sig
	.eop_289_zExp_352(eop_289_zExp_352_sig) ,	// output [31:0] eop_289_zExp_352_sig
	.eop_289_zExp_352_ap_vld(eop_289_zExp_352_ap_vld_sig) ,	// output  eop_289_zExp_352_ap_vld_sig
	.eop_371_aSig_374(eop_371_aSig_374_sig) ,	// output [63:0] eop_371_aSig_374_sig
	.eop_371_aSig_374_ap_vld(eop_371_aSig_374_ap_vld_sig) ,	// output  eop_371_aSig_374_ap_vld_sig
	.eop_370_aExp_375(eop_370_aExp_375_sig) ,	// output [31:0] eop_370_aExp_375_sig
	.eop_370_aExp_375_ap_vld(eop_370_aExp_375_ap_vld_sig) ,	// output  eop_370_aExp_375_ap_vld_sig
	.eop_371_bSig_376(eop_371_bSig_376_sig) ,	// output [63:0] eop_371_bSig_376_sig
	.eop_371_bSig_376_ap_vld(eop_371_bSig_376_ap_vld_sig) ,	// output  eop_371_bSig_376_ap_vld_sig
	.eop_370_bExp_377(eop_370_bExp_377_sig) ,	// output [31:0] eop_370_bExp_377_sig
	.eop_370_bExp_377_ap_vld(eop_370_bExp_377_ap_vld_sig) ,	// output  eop_370_bExp_377_ap_vld_sig
	.eop_372_expDiff_378(eop_372_expDiff_378_sig) ,	// output [31:0] eop_372_expDiff_378_sig
	.eop_372_expDiff_378_ap_vld(eop_372_expDiff_378_ap_vld_sig) ,	// output  eop_372_expDiff_378_ap_vld_sig
	.eop_370_aExp_394(eop_370_aExp_394_sig) ,	// output [31:0] eop_370_aExp_394_sig
	.eop_370_aExp_394_ap_vld(eop_370_aExp_394_ap_vld_sig) ,	// output  eop_370_aExp_394_ap_vld_sig
	.eop_370_bExp_395(eop_370_bExp_395_sig) ,	// output [31:0] eop_370_bExp_395_sig
	.eop_370_bExp_395_ap_vld(eop_370_bExp_395_ap_vld_sig) ,	// output  eop_370_bExp_395_ap_vld_sig
	.eop_372_expDiff_410(eop_372_expDiff_410_sig) ,	// output [31:0] eop_372_expDiff_410_sig
	.eop_372_expDiff_410_ap_vld(eop_372_expDiff_410_ap_vld_sig) ,	// output  eop_372_expDiff_410_ap_vld_sig
	.eop_371_zSig_416(eop_371_zSig_416_sig) ,	// output [63:0] eop_371_zSig_416_sig
	.eop_371_zSig_416_ap_vld(eop_371_zSig_416_ap_vld_sig) ,	// output  eop_371_zSig_416_ap_vld_sig
	.eop_370_zExp_417(eop_370_zExp_417_sig) ,	// output [31:0] eop_370_zExp_417_sig
	.eop_370_zExp_417_ap_vld(eop_370_zExp_417_ap_vld_sig) ,	// output  eop_370_zExp_417_ap_vld_sig
	.eop_372_expDiff_428(eop_372_expDiff_428_sig) ,	// output [31:0] eop_372_expDiff_428_sig
	.eop_372_expDiff_428_ap_vld(eop_372_expDiff_428_ap_vld_sig) ,	// output  eop_372_expDiff_428_ap_vld_sig
	.eop_371_zSig_434(eop_371_zSig_434_sig) ,	// output [63:0] eop_371_zSig_434_sig
	.eop_371_zSig_434_ap_vld(eop_371_zSig_434_ap_vld_sig) ,	// output  eop_371_zSig_434_ap_vld_sig
	.eop_370_zExp_435(eop_370_zExp_435_sig) ,	// output [31:0] eop_370_zExp_435_sig
	.eop_370_zExp_435_ap_vld(eop_370_zExp_435_ap_vld_sig) ,	// output  eop_370_zExp_435_ap_vld_sig
	.eop_370_zExp_437(eop_370_zExp_437_sig) ,	// output [31:0] eop_370_zExp_437_sig
	.eop_370_zExp_437_ap_vld(eop_370_zExp_437_ap_vld_sig) ,	// output  eop_370_zExp_437_ap_vld_sig
	.eop_247_shiftCount_249(eop_247_shiftCount_249_sig) ,	// output [31:0] eop_247_shiftCount_249_sig
	.eop_247_shiftCount_249_ap_vld(eop_247_shiftCount_249_ap_vld_sig) ,	// output  eop_247_shiftCount_249_ap_vld_sig
	.eop_49_app_61(eop_49_app_61_sig) ,	// output [63:0] eop_49_app_61_sig
	.eop_49_app_61_ap_vld(eop_49_app_61_ap_vld_sig) ,	// output  eop_49_app_61_ap_vld_sig
	.eop_52_inc_62(eop_52_inc_62_sig) ,	// output [31:0] eop_52_inc_62_sig
	.eop_52_inc_62_ap_vld(eop_52_inc_62_ap_vld_sig) ,	// output  eop_52_inc_62_ap_vld_sig
	.eop_624_aSign_632(eop_624_aSign_632_sig) ,	// output [31:0] eop_624_aSign_632_sig
	.eop_624_aSign_632_ap_vld(eop_624_aSign_632_ap_vld_sig) ,	// output  eop_624_aSign_632_ap_vld_sig
	.eop_624_bSign_633(eop_624_bSign_633_sig) ,	// output [31:0] eop_624_bSign_633_sig
	.eop_624_bSign_633_ap_vld(eop_624_bSign_633_ap_vld_sig) ,	// output  eop_624_bSign_633_ap_vld_sig
	.eop_162_result_163(eop_162_result_163_sig) ,	// output [63:0] eop_162_result_163_sig
	.eop_162_result_163_ap_vld(eop_162_result_163_ap_vld_sig) ,	// output  eop_162_result_163_ap_vld_sig
	.eop_157_main_result_164(eop_157_main_result_164_sig) ,	// output [31:0] eop_157_main_result_164_sig
	.eop_157_main_result_164_ap_vld(eop_157_main_result_164_ap_vld_sig) ,	// output  eop_157_main_result_164_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 82;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_157_main_result_159_ap_vld_sig;
valid_regs[1] <= eop_50_diff_54_ap_vld_sig;
valid_regs[2] <= eop_49_app_54_ap_vld_sig;
valid_regs[3] <= eop_52_inc_55_ap_vld_sig;
valid_regs[4] <= eop_473_aSig_475_ap_vld_sig;
valid_regs[5] <= eop_472_aExp_476_ap_vld_sig;
valid_regs[6] <= eop_471_aSign_477_ap_vld_sig;
valid_regs[7] <= eop_473_bSig_478_ap_vld_sig;
valid_regs[8] <= eop_472_bExp_479_ap_vld_sig;
valid_regs[9] <= eop_471_bSign_480_ap_vld_sig;
valid_regs[10] <= eop_471_zSign_481_ap_vld_sig;
valid_regs[11] <= eop_124_shiftCount_126_ap_vld_sig;
valid_regs[12] <= eop_472_zExp_516_ap_vld_sig;
valid_regs[13] <= eop_473_aSig_517_ap_vld_sig;
valid_regs[14] <= eop_473_bSig_518_ap_vld_sig;
valid_regs[15] <= eop_472_zExp_524_ap_vld_sig;
valid_regs[16] <= eop_176_roundingMode_180_ap_vld_sig;
valid_regs[17] <= eop_177_roundNearestEven_181_ap_vld_sig;
valid_regs[18] <= eop_178_roundIncrement_182_ap_vld_sig;
valid_regs[19] <= eop_178_roundBits_204_ap_vld_sig;
valid_regs[20] <= eop_177_isTiny_215_ap_vld_sig;
valid_regs[21] <= eop_174_zExp_219_ap_vld_sig;
valid_regs[22] <= eop_178_roundBits_220_ap_vld_sig;
valid_regs[23] <= eop_174_zSig_227_ap_vld_sig;
valid_regs[24] <= eop_174_zExp_230_ap_vld_sig;
valid_regs[25] <= eop_51_m_rad2_56_ap_vld_sig;
valid_regs[26] <= eop_263_zSign_270_ap_vld_sig;
valid_regs[27] <= eop_264_absA_271_ap_vld_sig;
valid_regs[28] <= eop_265_shiftCount_272_ap_vld_sig;
valid_regs[29] <= eop_266_zSig_273_ap_vld_sig;
valid_regs[30] <= eop_541_aSig_544_ap_vld_sig;
valid_regs[31] <= eop_540_aExp_545_ap_vld_sig;
valid_regs[32] <= eop_539_aSign_546_ap_vld_sig;
valid_regs[33] <= eop_541_bSig_547_ap_vld_sig;
valid_regs[34] <= eop_540_bExp_548_ap_vld_sig;
valid_regs[35] <= eop_539_bSign_549_ap_vld_sig;
valid_regs[36] <= eop_539_zSign_550_ap_vld_sig;
valid_regs[37] <= eop_540_zExp_590_ap_vld_sig;
valid_regs[38] <= eop_541_aSig_591_ap_vld_sig;
valid_regs[39] <= eop_541_bSig_592_ap_vld_sig;
valid_regs[40] <= eop_540_zExp_596_ap_vld_sig;
valid_regs[41] <= eop_541_zSig_598_ap_vld_sig;
valid_regs[42] <= eop_541_zSig_605_ap_vld_sig;
valid_regs[43] <= eop_50_diff_59_ap_vld_sig;
valid_regs[44] <= eop_451_aSign_453_ap_vld_sig;
valid_regs[45] <= eop_451_bSign_454_ap_vld_sig;
valid_regs[46] <= eop_290_aSig_293_ap_vld_sig;
valid_regs[47] <= eop_289_aExp_294_ap_vld_sig;
valid_regs[48] <= eop_290_bSig_295_ap_vld_sig;
valid_regs[49] <= eop_289_bExp_296_ap_vld_sig;
valid_regs[50] <= eop_291_expDiff_297_ap_vld_sig;
valid_regs[51] <= eop_291_expDiff_309_ap_vld_sig;
valid_regs[52] <= eop_289_zExp_313_ap_vld_sig;
valid_regs[53] <= eop_291_expDiff_324_ap_vld_sig;
valid_regs[54] <= eop_289_zExp_330_ap_vld_sig;
valid_regs[55] <= eop_290_zSig_342_ap_vld_sig;
valid_regs[56] <= eop_289_zExp_343_ap_vld_sig;
valid_regs[57] <= eop_290_zSig_347_ap_vld_sig;
valid_regs[58] <= eop_289_zExp_348_ap_vld_sig;
valid_regs[59] <= eop_290_zSig_351_ap_vld_sig;
valid_regs[60] <= eop_289_zExp_352_ap_vld_sig;
valid_regs[61] <= eop_371_aSig_374_ap_vld_sig;
valid_regs[62] <= eop_370_aExp_375_ap_vld_sig;
valid_regs[63] <= eop_371_bSig_376_ap_vld_sig;
valid_regs[64] <= eop_370_bExp_377_ap_vld_sig;
valid_regs[65] <= eop_372_expDiff_378_ap_vld_sig;
valid_regs[66] <= eop_370_aExp_394_ap_vld_sig;
valid_regs[67] <= eop_370_bExp_395_ap_vld_sig;
valid_regs[68] <= eop_372_expDiff_410_ap_vld_sig;
valid_regs[69] <= eop_371_zSig_416_ap_vld_sig;
valid_regs[70] <= eop_370_zExp_417_ap_vld_sig;
valid_regs[71] <= eop_372_expDiff_428_ap_vld_sig;
valid_regs[72] <= eop_371_zSig_434_ap_vld_sig;
valid_regs[73] <= eop_370_zExp_435_ap_vld_sig;
valid_regs[74] <= eop_370_zExp_437_ap_vld_sig;
valid_regs[75] <= eop_247_shiftCount_249_ap_vld_sig;
valid_regs[76] <= eop_49_app_61_ap_vld_sig;
valid_regs[77] <= eop_52_inc_62_ap_vld_sig;
valid_regs[78] <= eop_624_aSign_632_ap_vld_sig;
valid_regs[79] <= eop_624_bSign_633_ap_vld_sig;
valid_regs[80] <= eop_162_result_163_ap_vld_sig;
valid_regs[81] <= eop_157_main_result_164_ap_vld_sig;


end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
