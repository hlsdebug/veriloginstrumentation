// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
gsm_main gsm_main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_94_main_result_97(eop_94_main_result_97_sig) ,	// output [31:0] eop_94_main_result_97_sig
	.eop_94_main_result_97_ap_vld(eop_94_main_result_97_ap_vld_sig) ,	// output  eop_94_main_result_97_ap_vld_sig
	.eop_49_smax_56(eop_49_smax_56_sig) ,	// output [15:0] eop_49_smax_56_sig
	.eop_49_smax_56_ap_vld(eop_49_smax_56_ap_vld_sig) ,	// output  eop_49_smax_56_ap_vld_sig
	.eop_48_temp_59(eop_48_temp_59_sig) ,	// output [15:0] eop_48_temp_59_sig
	.eop_48_temp_59_ap_vld(eop_48_temp_59_ap_vld_sig) ,	// output  eop_48_temp_59_ap_vld_sig
	.eop_49_smax_61(eop_49_smax_61_sig) ,	// output [15:0] eop_49_smax_61_sig
	.eop_49_smax_61_ap_vld(eop_49_smax_61_ap_vld_sig) ,	// output  eop_49_smax_61_ap_vld_sig
	.eop_50_scalauto_67(eop_50_scalauto_67_sig) ,	// output [15:0] eop_50_scalauto_67_sig
	.eop_50_scalauto_67_ap_vld(eop_50_scalauto_67_ap_vld_sig) ,	// output  eop_50_scalauto_67_ap_vld_sig
	.eop_92_a_117(eop_92_a_117_sig) ,	// output [31:0] eop_92_a_117_sig
	.eop_92_a_117_ap_vld(eop_92_a_117_ap_vld_sig) ,	// output  eop_92_a_117_ap_vld_sig
	.eop_50_scalauto_69(eop_50_scalauto_69_sig) ,	// output [15:0] eop_50_scalauto_69_sig
	.eop_50_scalauto_69_ap_vld(eop_50_scalauto_69_ap_vld_sig) ,	// output  eop_50_scalauto_69_ap_vld_sig
	.eop_50_n_73(eop_50_n_73_sig) ,	// output [15:0] eop_50_n_73_sig
	.eop_50_n_73_ap_vld(eop_50_n_73_ap_vld_sig) ,	// output  eop_50_n_73_ap_vld_sig
	.eop_55_prod_60(eop_55_prod_60_sig) ,	// output [31:0] eop_55_prod_60_sig
	.eop_55_prod_60_ap_vld(eop_55_prod_60_ap_vld_sig) ,	// output  eop_55_prod_60_ap_vld_sig
	.eop_52_sl_82(eop_52_sl_82_sig) ,	// output [15:0] eop_52_sl_82_sig
	.eop_52_sl_82_ap_vld(eop_52_sl_82_ap_vld_sig) ,	// output  eop_52_sl_82_ap_vld_sig
	.eop_52_sl_91(eop_52_sl_91_sig) ,	// output [15:0] eop_52_sl_91_sig
	.eop_52_sl_91_ap_vld(eop_52_sl_91_ap_vld_sig) ,	// output  eop_52_sl_91_ap_vld_sig
	.eop_52_sl_94(eop_52_sl_94_sig) ,	// output [15:0] eop_52_sl_94_sig
	.eop_52_sl_94_ap_vld(eop_52_sl_94_ap_vld_sig) ,	// output  eop_52_sl_94_ap_vld_sig
	.eop_52_sl_98(eop_52_sl_98_sig) ,	// output [15:0] eop_52_sl_98_sig
	.eop_52_sl_98_ap_vld(eop_52_sl_98_ap_vld_sig) ,	// output  eop_52_sl_98_ap_vld_sig
	.eop_52_sl_103(eop_52_sl_103_sig) ,	// output [15:0] eop_52_sl_103_sig
	.eop_52_sl_103_ap_vld(eop_52_sl_103_ap_vld_sig) ,	// output  eop_52_sl_103_ap_vld_sig
	.eop_52_sl_109(eop_52_sl_109_sig) ,	// output [15:0] eop_52_sl_109_sig
	.eop_52_sl_109_ap_vld(eop_52_sl_109_ap_vld_sig) ,	// output  eop_52_sl_109_ap_vld_sig
	.eop_52_sl_116(eop_52_sl_116_sig) ,	// output [15:0] eop_52_sl_116_sig
	.eop_52_sl_116_ap_vld(eop_52_sl_116_ap_vld_sig) ,	// output  eop_52_sl_116_ap_vld_sig
	.eop_52_sl_124(eop_52_sl_124_sig) ,	// output [15:0] eop_52_sl_124_sig
	.eop_52_sl_124_ap_vld(eop_52_sl_124_ap_vld_sig) ,	// output  eop_52_sl_124_ap_vld_sig
	.eop_52_sl_137(eop_52_sl_137_sig) ,	// output [15:0] eop_52_sl_137_sig
	.eop_52_sl_137_ap_vld(eop_52_sl_137_ap_vld_sig) ,	// output  eop_52_sl_137_ap_vld_sig
	.eop_168_temp_183(eop_168_temp_183_sig) ,	// output [15:0] eop_168_temp_183_sig
	.eop_168_temp_183_ap_vld(eop_168_temp_183_ap_vld_sig) ,	// output  eop_168_temp_183_ap_vld_sig
	.eop_168_temp_200(eop_168_temp_200_sig) ,	// output [15:0] eop_168_temp_200_sig
	.eop_168_temp_200_ap_vld(eop_168_temp_200_ap_vld_sig) ,	// output  eop_168_temp_200_ap_vld_sig
	.eop_168_temp_201(eop_168_temp_201_sig) ,	// output [15:0] eop_168_temp_201_sig
	.eop_168_temp_201_ap_vld(eop_168_temp_201_ap_vld_sig) ,	// output  eop_168_temp_201_ap_vld_sig
	.eop_129_L_num_134(eop_129_L_num_134_sig) ,	// output [31:0] eop_129_L_num_134_sig
	.eop_129_L_num_134_ap_vld(eop_129_L_num_134_ap_vld_sig) ,	// output  eop_129_L_num_134_ap_vld_sig
	.eop_130_L_denum_135(eop_130_L_denum_135_sig) ,	// output [31:0] eop_130_L_denum_135_sig
	.eop_130_L_denum_135_ap_vld(eop_130_L_denum_135_ap_vld_sig) ,	// output  eop_130_L_denum_135_ap_vld_sig
	.eop_131_div_136(eop_131_div_136_sig) ,	// output [15:0] eop_131_div_136_sig
	.eop_131_div_136_ap_vld(eop_131_div_136_ap_vld_sig) ,	// output  eop_131_div_136_ap_vld_sig
	.eop_132_k_137(eop_132_k_137_sig) ,	// output [31:0] eop_132_k_137_sig
	.eop_132_k_137_ap_vld(eop_132_k_137_ap_vld_sig) ,	// output  eop_132_k_137_ap_vld_sig
	.eop_131_div_154(eop_131_div_154_sig) ,	// output [15:0] eop_131_div_154_sig
	.eop_131_div_154_ap_vld(eop_131_div_154_ap_vld_sig) ,	// output  eop_131_div_154_ap_vld_sig
	.eop_132_k_146(eop_132_k_146_sig) ,	// output [31:0] eop_132_k_146_sig
	.eop_132_k_146_ap_vld(eop_132_k_146_ap_vld_sig) ,	// output  eop_132_k_146_ap_vld_sig
	.eop_168_temp_218(eop_168_temp_218_sig) ,	// output [15:0] eop_168_temp_218_sig
	.eop_168_temp_218_ap_vld(eop_168_temp_218_ap_vld_sig) ,	// output  eop_168_temp_218_ap_vld_sig
	.eop_38_sum_39(eop_38_sum_39_sig) ,	// output [31:0] eop_38_sum_39_sig
	.eop_38_sum_39_ap_vld(eop_38_sum_39_ap_vld_sig) ,	// output  eop_38_sum_39_ap_vld_sig
	.eop_168_temp_223(eop_168_temp_223_sig) ,	// output [15:0] eop_168_temp_223_sig
	.eop_168_temp_223_ap_vld(eop_168_temp_223_ap_vld_sig) ,	// output  eop_168_temp_223_ap_vld_sig
	.eop_168_temp_226(eop_168_temp_226_sig) ,	// output [15:0] eop_168_temp_226_sig
	.eop_168_temp_226_ap_vld(eop_168_temp_226_ap_vld_sig) ,	// output  eop_168_temp_226_ap_vld_sig
	.eop_244_temp_253(eop_244_temp_253_sig) ,	// output [15:0] eop_244_temp_253_sig
	.eop_244_temp_253_ap_vld(eop_244_temp_253_ap_vld_sig) ,	// output  eop_244_temp_253_ap_vld_sig
	.eop_244_temp_254(eop_244_temp_254_sig) ,	// output [15:0] eop_244_temp_254_sig
	.eop_244_temp_254_ap_vld(eop_244_temp_254_ap_vld_sig) ,	// output  eop_244_temp_254_ap_vld_sig
	.eop_245_i_250(eop_245_i_250_sig) ,	// output [31:0] eop_245_i_250_sig
	.eop_245_i_250_ap_vld(eop_245_i_250_ap_vld_sig) ,	// output  eop_245_i_250_ap_vld_sig
	.eop_279_temp_300(eop_279_temp_300_sig) ,	// output [15:0] eop_279_temp_300_sig
	.eop_279_temp_300_ap_vld(eop_279_temp_300_ap_vld_sig) ,	// output  eop_279_temp_300_ap_vld_sig
	.eop_279_temp_301(eop_279_temp_301_sig) ,	// output [15:0] eop_279_temp_301_sig
	.eop_279_temp_301_ap_vld(eop_279_temp_301_ap_vld_sig) ,	// output  eop_279_temp_301_ap_vld_sig
	.eop_279_temp_302(eop_279_temp_302_sig) ,	// output [15:0] eop_279_temp_302_sig
	.eop_279_temp_302_ap_vld(eop_279_temp_302_ap_vld_sig) ,	// output  eop_279_temp_302_ap_vld_sig
	.eop_279_temp_303(eop_279_temp_303_sig) ,	// output [15:0] eop_279_temp_303_sig
	.eop_279_temp_303_ap_vld(eop_279_temp_303_ap_vld_sig) ,	// output  eop_279_temp_303_ap_vld_sig
	.eop_279_temp_305(eop_279_temp_305_sig) ,	// output [15:0] eop_279_temp_305_sig
	.eop_279_temp_305_ap_vld(eop_279_temp_305_ap_vld_sig) ,	// output  eop_279_temp_305_ap_vld_sig
	.eop_279_temp_306(eop_279_temp_306_sig) ,	// output [15:0] eop_279_temp_306_sig
	.eop_279_temp_306_ap_vld(eop_279_temp_306_ap_vld_sig) ,	// output  eop_279_temp_306_ap_vld_sig
	.eop_279_temp_307(eop_279_temp_307_sig) ,	// output [15:0] eop_279_temp_307_sig
	.eop_279_temp_307_ap_vld(eop_279_temp_307_ap_vld_sig) ,	// output  eop_279_temp_307_ap_vld_sig
	.eop_279_temp_308(eop_279_temp_308_sig) ,	// output [15:0] eop_279_temp_308_sig
	.eop_279_temp_308_ap_vld(eop_279_temp_308_ap_vld_sig) ,	// output  eop_279_temp_308_ap_vld_sig
	.eop_94_main_result_105(eop_94_main_result_105_sig) ,	// output [31:0] eop_94_main_result_105_sig
	.eop_94_main_result_105_ap_vld(eop_94_main_result_105_ap_vld_sig) ,	// output  eop_94_main_result_105_ap_vld_sig
	.eop_94_main_result_107(eop_94_main_result_107_sig) ,	// output [31:0] eop_94_main_result_107_sig
	.eop_94_main_result_107_ap_vld(eop_94_main_result_107_ap_vld_sig) ,	// output  eop_94_main_result_107_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 44;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_94_main_result_97_ap_vld_sig;
valid_regs[1] <= eop_49_smax_56_ap_vld_sig;
valid_regs[2] <= eop_48_temp_59_ap_vld_sig;
valid_regs[3] <= eop_49_smax_61_ap_vld_sig;
valid_regs[4] <= eop_50_scalauto_67_ap_vld_sig;
valid_regs[5] <= eop_92_a_117_ap_vld_sig;
valid_regs[6] <= eop_50_scalauto_69_ap_vld_sig;
valid_regs[7] <= eop_50_n_73_ap_vld_sig;
valid_regs[8] <= eop_55_prod_60_ap_vld_sig;
valid_regs[9] <= eop_52_sl_82_ap_vld_sig;
valid_regs[10] <= eop_52_sl_91_ap_vld_sig;
valid_regs[11] <= eop_52_sl_94_ap_vld_sig;
valid_regs[12] <= eop_52_sl_98_ap_vld_sig;
valid_regs[13] <= eop_52_sl_103_ap_vld_sig;
valid_regs[14] <= eop_52_sl_109_ap_vld_sig;
valid_regs[15] <= eop_52_sl_116_ap_vld_sig;
valid_regs[16] <= eop_52_sl_124_ap_vld_sig;
valid_regs[17] <= eop_52_sl_137_ap_vld_sig;
valid_regs[18] <= eop_168_temp_183_ap_vld_sig;
valid_regs[19] <= eop_168_temp_200_ap_vld_sig;
valid_regs[20] <= eop_168_temp_201_ap_vld_sig;
valid_regs[21] <= eop_129_L_num_134_ap_vld_sig;
valid_regs[22] <= eop_130_L_denum_135_ap_vld_sig;
valid_regs[23] <= eop_131_div_136_ap_vld_sig;
valid_regs[24] <= eop_132_k_137_ap_vld_sig;
valid_regs[25] <= eop_131_div_154_ap_vld_sig;
valid_regs[26] <= eop_132_k_146_ap_vld_sig;
valid_regs[27] <= eop_168_temp_218_ap_vld_sig;
valid_regs[28] <= eop_38_sum_39_ap_vld_sig;
valid_regs[29] <= eop_168_temp_223_ap_vld_sig;
valid_regs[30] <= eop_168_temp_226_ap_vld_sig;
valid_regs[31] <= eop_244_temp_253_ap_vld_sig;
valid_regs[32] <= eop_244_temp_254_ap_vld_sig;
valid_regs[33] <= eop_245_i_250_ap_vld_sig;
valid_regs[34] <= eop_279_temp_300_ap_vld_sig;
valid_regs[35] <= eop_279_temp_301_ap_vld_sig;
valid_regs[36] <= eop_279_temp_302_ap_vld_sig;
valid_regs[37] <= eop_279_temp_303_ap_vld_sig;
valid_regs[38] <= eop_279_temp_305_ap_vld_sig;
valid_regs[39] <= eop_279_temp_306_ap_vld_sig;
valid_regs[40] <= eop_279_temp_307_ap_vld_sig;
valid_regs[41] <= eop_279_temp_308_ap_vld_sig;
valid_regs[42] <= eop_94_main_result_105_ap_vld_sig;
valid_regs[43] <= eop_94_main_result_107_ap_vld_sig;



end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
