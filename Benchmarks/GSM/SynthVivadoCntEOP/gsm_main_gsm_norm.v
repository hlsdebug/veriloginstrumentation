// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.3
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module gsm_main_gsm_norm (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        a,
        eop_92_a_117,
        eop_92_a_117_ap_vld,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 2'b1;
parameter    ap_ST_st2_fsm_1 = 2'b10;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv6_0 = 6'b000000;
parameter    ap_const_lv32_1F = 32'b11111;
parameter    ap_const_lv32_C0000001 = 32'b11000000000000000000000000000001;
parameter    ap_const_lv32_FFFFFFFF = 32'b11111111111111111111111111111111;
parameter    ap_const_lv32_10 = 32'b10000;
parameter    ap_const_lv16_0 = 16'b0000000000000000;
parameter    ap_const_lv32_18 = 32'b11000;
parameter    ap_const_lv8_0 = 8'b00000000;
parameter    ap_const_lv32_17 = 32'b10111;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_F = 32'b1111;
parameter    ap_const_lv4_F = 4'b1111;
parameter    ap_const_lv4_7 = 4'b111;
parameter    ap_const_lv5_F = 5'b1111;
parameter    ap_const_lv5_17 = 5'b10111;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [31:0] a;
output  [31:0] eop_92_a_117;
output   eop_92_a_117_ap_vld;
output  [5:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg eop_92_a_117_ap_vld;
reg[5:0] ap_return;
(* fsm_encoding = "none" *) reg   [1:0] ap_CS_fsm = 2'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_20;
reg   [7:0] bitoff_address0;
reg    bitoff_ce0;
wire   [3:0] bitoff_q0;
wire   [0:0] tmp_fu_124_p3;
reg   [0:0] tmp_reg_300;
wire   [0:0] tmp_1_fu_132_p2;
reg   [0:0] tmp_1_reg_304;
wire   [0:0] icmp_fu_160_p2;
reg   [0:0] icmp_reg_308;
wire   [0:0] icmp5_fu_176_p2;
reg   [0:0] icmp5_reg_312;
wire   [0:0] icmp8_fu_222_p2;
reg   [0:0] icmp8_reg_326;
wire   [31:0] a_assign_fu_138_p2;
reg   [31:0] p_s_phi_fu_100_p4;
wire  signed [5:0] tmp_cast_fu_254_p1;
reg   [5:0] p_0_phi_fu_111_p10;
reg   [5:0] p_0_reg_107;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_111;
wire   [5:0] tmp_13_cast_fu_265_p1;
wire   [5:0] tmp_22_cast_fu_280_p1;
wire   [5:0] tmp_17_cast_fu_295_p1;
wire   [63:0] tmp_8_fu_192_p1;
wire   [63:0] tmp_5_fu_207_p1;
wire   [63:0] tmp_11_fu_238_p1;
wire   [63:0] tmp_2_fu_243_p1;
wire   [15:0] tmp_4_fu_150_p4;
wire   [7:0] tmp_13_fu_166_p4;
wire   [7:0] tmp_7_fu_182_p4;
wire   [7:0] tmp_2_cast_fu_197_p4;
wire   [7:0] tmp_6_fu_212_p4;
wire   [7:0] tmp_18_cast_fu_228_p4;
wire   [7:0] tmp_3_fu_146_p1;
wire   [3:0] tmp_s_fu_248_p2;
wire   [3:0] tmp_9_fu_259_p2;
wire   [4:0] tmp_21_cast_fu_270_p1;
wire   [4:0] tmp_12_fu_274_p2;
wire   [4:0] tmp_16_cast_fu_285_p1;
wire   [4:0] tmp_10_fu_289_p2;
reg   [5:0] ap_return_preg = 6'b000000;
reg   [1:0] ap_NS_fsm;
reg    ap_sig_bdd_65;
reg    ap_sig_bdd_72;
reg    ap_sig_bdd_86;
reg    ap_sig_bdd_93;
reg    ap_sig_bdd_122;
reg    ap_sig_bdd_128;
reg    ap_sig_bdd_137;
reg    ap_sig_bdd_143;
reg    ap_sig_bdd_98;


gsm_main_gsm_norm_bitoff #(
    .DataWidth( 4 ),
    .AddressRange( 256 ),
    .AddressWidth( 8 ))
bitoff_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( bitoff_address0 ),
    .ce0( bitoff_ce0 ),
    .q0( bitoff_q0 )
);



/// the current state (ap_CS_fsm) of the state machine. ///
always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

/// ap_return_preg assign process. ///
always @ (posedge ap_clk) begin : ap_ret_ap_return_preg
    if (ap_rst == 1'b1) begin
        ap_return_preg <= ap_const_lv6_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
            ap_return_preg <= p_0_phi_fu_111_p10;
        end
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_fu_124_p3 == ap_const_lv1_0) & ~(tmp_1_fu_132_p2 == ap_const_lv1_0))) begin
        p_0_reg_107 <= ap_const_lv6_0;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (((tmp_reg_300 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp8_reg_326)) | ((tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp8_reg_326))))) begin
        p_0_reg_107 <= tmp_17_cast_fu_295_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (((tmp_reg_300 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp8_reg_326)) | ((tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp8_reg_326))))) begin
        p_0_reg_107 <= tmp_22_cast_fu_280_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (((tmp_reg_300 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp5_reg_312)) | ((ap_const_lv1_0 == icmp_reg_308) & (tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp5_reg_312))))) begin
        p_0_reg_107 <= tmp_13_cast_fu_265_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (((tmp_reg_300 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp5_reg_312)) | ((ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp5_reg_312) & (tmp_1_reg_304 == ap_const_lv1_0))))) begin
        p_0_reg_107 <= tmp_cast_fu_254_p1;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2))))) begin
        icmp5_reg_312 <= icmp5_fu_176_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2))))) begin
        icmp8_reg_326 <= icmp8_fu_222_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((tmp_fu_124_p3 == ap_const_lv1_0) | (tmp_1_fu_132_p2 == ap_const_lv1_0)))) begin
        icmp_reg_308 <= icmp_fu_160_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_fu_124_p3 == ap_const_lv1_0))) begin
        tmp_1_reg_304 <= tmp_1_fu_132_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        tmp_reg_300 <= a[ap_const_lv32_1F];
    end
end

/// ap_done assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

/// ap_idle assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

/// ap_ready assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

/// ap_return assign process. ///
always @ (p_0_phi_fu_111_p10 or ap_sig_cseq_ST_st2_fsm_1 or ap_return_preg) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        ap_return = p_0_phi_fu_111_p10;
    end else begin
        ap_return = ap_return_preg;
    end
end

/// ap_sig_cseq_ST_st1_fsm_0 assign process. ///
always @ (ap_sig_bdd_20) begin
    if (ap_sig_bdd_20) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st2_fsm_1 assign process. ///
always @ (ap_sig_bdd_111) begin
    if (ap_sig_bdd_111) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

/// bitoff_address0 assign process. ///
always @ (ap_sig_cseq_ST_st1_fsm_0 or tmp_8_fu_192_p1 or tmp_5_fu_207_p1 or tmp_11_fu_238_p1 or tmp_2_fu_243_p1 or ap_sig_bdd_65 or ap_sig_bdd_72 or ap_sig_bdd_86 or ap_sig_bdd_93) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) begin
        if (ap_sig_bdd_93) begin
            bitoff_address0 = tmp_2_fu_243_p1;
        end else if (ap_sig_bdd_86) begin
            bitoff_address0 = tmp_11_fu_238_p1;
        end else if (ap_sig_bdd_72) begin
            bitoff_address0 = tmp_5_fu_207_p1;
        end else if (ap_sig_bdd_65) begin
            bitoff_address0 = tmp_8_fu_192_p1;
        end else begin
            bitoff_address0 = 'bx;
        end
    end else begin
        bitoff_address0 = 'bx;
    end
end

/// bitoff_ce0 assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or tmp_fu_124_p3 or tmp_1_fu_132_p2 or icmp_fu_160_p2 or icmp5_fu_176_p2 or icmp8_fu_222_p2) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp5_fu_176_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp5_fu_176_p2)))) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp5_fu_176_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp5_fu_176_p2)))) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp8_fu_222_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp8_fu_222_p2)))) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (((tmp_fu_124_p3 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp8_fu_222_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp8_fu_222_p2)))))) begin
        bitoff_ce0 = ap_const_logic_1;
    end else begin
        bitoff_ce0 = ap_const_logic_0;
    end
end

/// eop_92_a_117_ap_vld assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or tmp_fu_124_p3 or tmp_1_fu_132_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_fu_124_p3 == ap_const_lv1_0) & (tmp_1_fu_132_p2 == ap_const_lv1_0))) begin
        eop_92_a_117_ap_vld = ap_const_logic_1;
    end else begin
        eop_92_a_117_ap_vld = ap_const_logic_0;
    end
end

/// p_0_phi_fu_111_p10 assign process. ///
always @ (tmp_cast_fu_254_p1 or p_0_reg_107 or ap_sig_cseq_ST_st2_fsm_1 or tmp_13_cast_fu_265_p1 or tmp_22_cast_fu_280_p1 or tmp_17_cast_fu_295_p1 or ap_sig_bdd_122 or ap_sig_bdd_128 or ap_sig_bdd_137 or ap_sig_bdd_143) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        if (ap_sig_bdd_143) begin
            p_0_phi_fu_111_p10 = tmp_17_cast_fu_295_p1;
        end else if (ap_sig_bdd_137) begin
            p_0_phi_fu_111_p10 = tmp_22_cast_fu_280_p1;
        end else if (ap_sig_bdd_128) begin
            p_0_phi_fu_111_p10 = tmp_13_cast_fu_265_p1;
        end else if (ap_sig_bdd_122) begin
            p_0_phi_fu_111_p10 = tmp_cast_fu_254_p1;
        end else begin
            p_0_phi_fu_111_p10 = p_0_reg_107;
        end
    end else begin
        p_0_phi_fu_111_p10 = p_0_reg_107;
    end
end

/// p_s_phi_fu_100_p4 assign process. ///
always @ (ap_sig_cseq_ST_st1_fsm_0 or a or tmp_fu_124_p3 or a_assign_fu_138_p2 or ap_sig_bdd_98) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) begin
        if ((tmp_fu_124_p3 == ap_const_lv1_0)) begin
            p_s_phi_fu_100_p4 = a;
        end else if (ap_sig_bdd_98) begin
            p_s_phi_fu_100_p4 = a_assign_fu_138_p2;
        end else begin
            p_s_phi_fu_100_p4 = 'bx;
        end
    end else begin
        p_s_phi_fu_100_p4 = 'bx;
    end
end
/// the next state (ap_NS_fsm) of the state machine. ///
always @ (ap_start or ap_CS_fsm) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign a_assign_fu_138_p2 = (a ^ ap_const_lv32_FFFFFFFF);

/// ap_sig_bdd_111 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_111 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

/// ap_sig_bdd_122 assign process. ///
always @ (tmp_reg_300 or tmp_1_reg_304 or icmp_reg_308 or icmp5_reg_312) begin
    ap_sig_bdd_122 = (((tmp_reg_300 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp5_reg_312)) | ((ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp5_reg_312) & (tmp_1_reg_304 == ap_const_lv1_0)));
end

/// ap_sig_bdd_128 assign process. ///
always @ (tmp_reg_300 or tmp_1_reg_304 or icmp_reg_308 or icmp5_reg_312) begin
    ap_sig_bdd_128 = (((tmp_reg_300 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp5_reg_312)) | ((ap_const_lv1_0 == icmp_reg_308) & (tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp5_reg_312)));
end

/// ap_sig_bdd_137 assign process. ///
always @ (tmp_reg_300 or tmp_1_reg_304 or icmp_reg_308 or icmp8_reg_326) begin
    ap_sig_bdd_137 = (((tmp_reg_300 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp8_reg_326)) | ((tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & (ap_const_lv1_0 == icmp8_reg_326)));
end

/// ap_sig_bdd_143 assign process. ///
always @ (tmp_reg_300 or tmp_1_reg_304 or icmp_reg_308 or icmp8_reg_326) begin
    ap_sig_bdd_143 = (((tmp_reg_300 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp8_reg_326)) | ((tmp_1_reg_304 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_reg_308) & ~(ap_const_lv1_0 == icmp8_reg_326)));
end

/// ap_sig_bdd_20 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_20 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end

/// ap_sig_bdd_65 assign process. ///
always @ (tmp_fu_124_p3 or tmp_1_fu_132_p2 or icmp_fu_160_p2 or icmp5_fu_176_p2) begin
    ap_sig_bdd_65 = (((tmp_fu_124_p3 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp5_fu_176_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp5_fu_176_p2)));
end

/// ap_sig_bdd_72 assign process. ///
always @ (tmp_fu_124_p3 or tmp_1_fu_132_p2 or icmp_fu_160_p2 or icmp5_fu_176_p2) begin
    ap_sig_bdd_72 = (((tmp_fu_124_p3 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp5_fu_176_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp5_fu_176_p2)));
end

/// ap_sig_bdd_86 assign process. ///
always @ (tmp_fu_124_p3 or tmp_1_fu_132_p2 or icmp_fu_160_p2 or icmp8_fu_222_p2) begin
    ap_sig_bdd_86 = (((tmp_fu_124_p3 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp8_fu_222_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & (ap_const_lv1_0 == icmp8_fu_222_p2)));
end

/// ap_sig_bdd_93 assign process. ///
always @ (tmp_fu_124_p3 or tmp_1_fu_132_p2 or icmp_fu_160_p2 or icmp8_fu_222_p2) begin
    ap_sig_bdd_93 = (((tmp_fu_124_p3 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp8_fu_222_p2)) | ((tmp_1_fu_132_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == icmp_fu_160_p2) & ~(ap_const_lv1_0 == icmp8_fu_222_p2)));
end

/// ap_sig_bdd_98 assign process. ///
always @ (tmp_fu_124_p3 or tmp_1_fu_132_p2) begin
    ap_sig_bdd_98 = (~(tmp_fu_124_p3 == ap_const_lv1_0) & (tmp_1_fu_132_p2 == ap_const_lv1_0));
end
assign eop_92_a_117 = (a ^ ap_const_lv32_FFFFFFFF);
assign icmp5_fu_176_p2 = (tmp_13_fu_166_p4 == ap_const_lv8_0? 1'b1: 1'b0);
assign icmp8_fu_222_p2 = (tmp_6_fu_212_p4 == ap_const_lv8_0? 1'b1: 1'b0);
assign icmp_fu_160_p2 = (tmp_4_fu_150_p4 == ap_const_lv16_0? 1'b1: 1'b0);
assign tmp_10_fu_289_p2 = ($signed(tmp_16_cast_fu_285_p1) + $signed(ap_const_lv5_17));
assign tmp_11_fu_238_p1 = tmp_18_cast_fu_228_p4;
assign tmp_12_fu_274_p2 = (tmp_21_cast_fu_270_p1 + ap_const_lv5_F);
assign tmp_13_cast_fu_265_p1 = tmp_9_fu_259_p2;
assign tmp_13_fu_166_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_1F : ap_const_lv32_18]}};
assign tmp_16_cast_fu_285_p1 = bitoff_q0;
assign tmp_17_cast_fu_295_p1 = tmp_10_fu_289_p2;
assign tmp_18_cast_fu_228_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_F : ap_const_lv32_8]}};
assign tmp_1_fu_132_p2 = ($signed(a) < $signed(32'b11000000000000000000000000000001)? 1'b1: 1'b0);
assign tmp_21_cast_fu_270_p1 = bitoff_q0;
assign tmp_22_cast_fu_280_p1 = tmp_12_fu_274_p2;
assign tmp_2_cast_fu_197_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_17 : ap_const_lv32_10]}};
assign tmp_2_fu_243_p1 = tmp_3_fu_146_p1;
assign tmp_3_fu_146_p1 = p_s_phi_fu_100_p4[7:0];
assign tmp_4_fu_150_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_1F : ap_const_lv32_10]}};
assign tmp_5_fu_207_p1 = tmp_2_cast_fu_197_p4;
assign tmp_6_fu_212_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_F : ap_const_lv32_8]}};
assign tmp_7_fu_182_p4 = {{p_s_phi_fu_100_p4[ap_const_lv32_1F : ap_const_lv32_18]}};
assign tmp_8_fu_192_p1 = tmp_7_fu_182_p4;
assign tmp_9_fu_259_p2 = (bitoff_q0 + ap_const_lv4_7);
assign tmp_cast_fu_254_p1 = $signed(tmp_s_fu_248_p2);
assign tmp_fu_124_p3 = a[ap_const_lv32_1F];
assign tmp_s_fu_248_p2 = ($signed(bitoff_q0) + $signed(ap_const_lv4_F));


endmodule //gsm_main_gsm_norm

