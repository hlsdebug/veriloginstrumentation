// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [31:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	reg idle = 1;
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	always @ (posedge start or posedge done) begin
		if (start)
			idle <= 1'b0;
		else
			idle <= 1'b1;
	end
	
main main_inst
(
	.clk(clk) ,	// input  clk_sig
	.clk2x() ,	// input  clk2x_sig
	.clk1x_follower() ,	// input  clk1x_follower_sig
	.reset(reset) ,	// input  reset_sig
	.start(start) ,	// input  start_sig
	.finish(done) ,	// output  finish_sig
	.memory_controller_waitrequest(memory_controller_waitrequest_sig) ,	// input  memory_controller_waitrequest_sig
	.return_val(ap_return_sig) ,	// output [31:0] return_val_sig
	.eop_127_main_result_130(eop_127_main_result_130_sig) ,	// output [31:0] eop_127_main_result_130_sig
	.eop_127_main_result_130_valid(eop_127_main_result_130_valid_sig) ,	// output  eop_127_main_result_130_valid_sig
	.eop_129_x1_134(eop_129_x1_134_sig) ,	// output [63:0] eop_129_x1_134_sig
	.eop_129_x1_134_valid(eop_129_x1_134_valid_sig) ,	// output  eop_129_x1_134_valid_sig
	.eop_129_x2_135(eop_129_x2_135_sig) ,	// output [63:0] eop_129_x2_135_sig
	.eop_129_x2_135_valid(eop_129_x2_135_valid_sig) ,	// output  eop_129_x2_135_valid_sig
	.eop_246_aSig_249(eop_246_aSig_249_sig) ,	// output [63:0] eop_246_aSig_249_sig
	.eop_246_aSig_249_valid(eop_246_aSig_249_valid_sig) ,	// output  eop_246_aSig_249_valid_sig
	.eop_245_aExp_250(eop_245_aExp_250_sig) ,	// output [31:0] eop_245_aExp_250_sig
	.eop_245_aExp_250_valid(eop_245_aExp_250_valid_sig) ,	// output  eop_245_aExp_250_valid_sig
	.eop_244_aSign_251(eop_244_aSign_251_sig) ,	// output [31:0] eop_244_aSign_251_sig
	.eop_244_aSign_251_valid(eop_244_aSign_251_valid_sig) ,	// output  eop_244_aSign_251_valid_sig
	.eop_246_bSig_252(eop_246_bSig_252_sig) ,	// output [63:0] eop_246_bSig_252_sig
	.eop_246_bSig_252_valid(eop_246_bSig_252_valid_sig) ,	// output  eop_246_bSig_252_valid_sig
	.eop_245_bExp_253(eop_245_bExp_253_sig) ,	// output [31:0] eop_245_bExp_253_sig
	.eop_245_bExp_253_valid(eop_245_bExp_253_valid_sig) ,	// output  eop_245_bExp_253_valid_sig
	.eop_244_bSign_254(eop_244_bSign_254_sig) ,	// output [31:0] eop_244_bSign_254_sig
	.eop_244_bSign_254_valid(eop_244_bSign_254_valid_sig) ,	// output  eop_244_bSign_254_valid_sig
	.eop_244_zSign_255(eop_244_zSign_255_sig) ,	// output [31:0] eop_244_zSign_255_sig
	.eop_244_zSign_255_valid(eop_244_zSign_255_valid_sig) ,	// output  eop_244_zSign_255_valid_sig
	.eop_124_shiftCount_126(eop_124_shiftCount_126_sig) ,	// output [31:0] eop_124_shiftCount_126_sig
	.eop_124_shiftCount_126_valid(eop_124_shiftCount_126_valid_sig) ,	// output  eop_124_shiftCount_126_valid_sig
	.eop_245_zExp_295(eop_245_zExp_295_sig) ,	// output [31:0] eop_245_zExp_295_sig
	.eop_245_zExp_295_valid(eop_245_zExp_295_valid_sig) ,	// output  eop_245_zExp_295_valid_sig
	.eop_246_aSig_296(eop_246_aSig_296_sig) ,	// output [63:0] eop_246_aSig_296_sig
	.eop_246_aSig_296_valid(eop_246_aSig_296_valid_sig) ,	// output  eop_246_aSig_296_valid_sig
	.eop_246_bSig_297(eop_246_bSig_297_sig) ,	// output [63:0] eop_246_bSig_297_sig
	.eop_246_bSig_297_valid(eop_246_bSig_297_valid_sig) ,	// output  eop_246_bSig_297_valid_sig
	.eop_245_zExp_301(eop_245_zExp_301_sig) ,	// output [31:0] eop_245_zExp_301_sig
	.eop_245_zExp_301_valid(eop_245_zExp_301_valid_sig) ,	// output  eop_245_zExp_301_valid_sig
	.eop_246_zSig_303(eop_246_zSig_303_sig) ,	// output [63:0] eop_246_zSig_303_sig
	.eop_246_zSig_303_valid(eop_246_zSig_303_valid_sig) ,	// output  eop_246_zSig_303_valid_sig
	.eop_246_zSig_310(eop_246_zSig_310_sig) ,	// output [63:0] eop_246_zSig_310_sig
	.eop_246_zSig_310_valid(eop_246_zSig_310_valid_sig) ,	// output  eop_246_zSig_310_valid_sig
	.eop_176_roundingMode_180(eop_176_roundingMode_180_sig) ,	// output [31:0] eop_176_roundingMode_180_sig
	.eop_176_roundingMode_180_valid(eop_176_roundingMode_180_valid_sig) ,	// output  eop_176_roundingMode_180_valid_sig
	.eop_177_roundNearestEven_181(eop_177_roundNearestEven_181_sig) ,	// output [31:0] eop_177_roundNearestEven_181_sig
	.eop_177_roundNearestEven_181_valid(eop_177_roundNearestEven_181_valid_sig) ,	// output  eop_177_roundNearestEven_181_valid_sig
	.eop_178_roundIncrement_182(eop_178_roundIncrement_182_sig) ,	// output [31:0] eop_178_roundIncrement_182_sig
	.eop_178_roundIncrement_182_valid(eop_178_roundIncrement_182_valid_sig) ,	// output  eop_178_roundIncrement_182_valid_sig
	.eop_178_roundBits_204(eop_178_roundBits_204_sig) ,	// output [31:0] eop_178_roundBits_204_sig
	.eop_178_roundBits_204_valid(eop_178_roundBits_204_valid_sig) ,	// output  eop_178_roundBits_204_valid_sig
	.eop_177_isTiny_215(eop_177_isTiny_215_sig) ,	// output [31:0] eop_177_isTiny_215_sig
	.eop_177_isTiny_215_valid(eop_177_isTiny_215_valid_sig) ,	// output  eop_177_isTiny_215_valid_sig
	.eop_174_zExp_219(eop_174_zExp_219_sig) ,	// output [31:0] eop_174_zExp_219_sig
	.eop_174_zExp_219_valid(eop_174_zExp_219_valid_sig) ,	// output  eop_174_zExp_219_valid_sig
	.eop_178_roundBits_220(eop_178_roundBits_220_sig) ,	// output [31:0] eop_178_roundBits_220_sig
	.eop_178_roundBits_220_valid(eop_178_roundBits_220_valid_sig) ,	// output  eop_178_roundBits_220_valid_sig
	.eop_174_zSig_227(eop_174_zSig_227_sig) ,	// output [63:0] eop_174_zSig_227_sig
	.eop_174_zSig_227_valid(eop_174_zSig_227_valid_sig) ,	// output  eop_174_zSig_227_valid_sig
	.eop_174_zExp_230(eop_174_zExp_230_sig) ,	// output [31:0] eop_174_zExp_230_sig
	.eop_174_zExp_230_valid(eop_174_zExp_230_valid_sig) ,	// output  eop_174_zExp_230_valid_sig
	.eop_133_result_136(eop_133_result_136_sig) ,	// output [63:0] eop_133_result_136_sig
	.eop_133_result_136_valid(eop_133_result_136_valid_sig) ,	// output  eop_133_result_136_valid_sig
	.eop_127_main_result_137(eop_127_main_result_137_sig) ,	// output [31:0] eop_127_main_result_137_sig
	.eop_127_main_result_137_valid(eop_127_main_result_137_valid_sig) 	// output  eop_127_main_result_137_valid_sig
);



parameter N = 28;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
	valid_regs[0] <= eop_127_main_result_130_valid_sig;
	valid_regs[1] <= eop_129_x1_134_valid_sig;
	valid_regs[2] <= eop_129_x2_135_valid_sig;
	valid_regs[3] <= eop_246_aSig_249_valid_sig;
	valid_regs[4] <= eop_245_aExp_250_valid_sig;
	valid_regs[5] <= eop_244_aSign_251_valid_sig;
	valid_regs[6] <= eop_246_bSig_252_valid_sig;
	valid_regs[7] <= eop_245_bExp_253_valid_sig;
	valid_regs[8] <= eop_244_bSign_254_valid_sig;
	valid_regs[9] <= eop_244_zSign_255_valid_sig;
	valid_regs[10] <= eop_124_shiftCount_126_valid_sig;
	valid_regs[11] <= eop_245_zExp_295_valid_sig;
	valid_regs[12] <= eop_246_aSig_296_valid_sig;
	valid_regs[13] <= eop_246_bSig_297_valid_sig;
	valid_regs[14] <= eop_245_zExp_301_valid_sig;
	valid_regs[15] <= eop_246_zSig_303_valid_sig;
	valid_regs[16] <= eop_246_zSig_310_valid_sig;
	valid_regs[17] <= eop_176_roundingMode_180_valid_sig;
	valid_regs[18] <= eop_177_roundNearestEven_181_valid_sig;
	valid_regs[19] <= eop_178_roundIncrement_182_valid_sig;
	valid_regs[20] <= eop_178_roundBits_204_valid_sig;
	valid_regs[21] <= eop_177_isTiny_215_valid_sig;
	valid_regs[22] <= eop_174_zExp_219_valid_sig;
	valid_regs[23] <= eop_178_roundBits_220_valid_sig;
	valid_regs[24] <= eop_174_zSig_227_valid_sig;
	valid_regs[25] <= eop_174_zExp_230_valid_sig;
	valid_regs[26] <= eop_133_result_136_valid_sig;
	valid_regs[27] <= eop_127_main_result_137_valid_sig;


end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
