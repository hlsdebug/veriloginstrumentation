// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.


// Generated by Quartus II 64-Bit Version 13.0 (Build Build 232 06/12/2013)
// Created on Fri Nov 13 17:04:19 2015

main main_inst
(
	.clk(clk_sig) ,	// input  clk_sig
	.clk2x(clk2x_sig) ,	// input  clk2x_sig
	.clk1x_follower(clk1x_follower_sig) ,	// input  clk1x_follower_sig
	.reset(reset_sig) ,	// input  reset_sig
	.start(start_sig) ,	// input  start_sig
	.finish(finish_sig) ,	// output  finish_sig
	.memory_controller_waitrequest(memory_controller_waitrequest_sig) ,	// input  memory_controller_waitrequest_sig
	.return_val(return_val_sig) ,	// output [31:0] return_val_sig
	.eop_127_main_result_130(eop_127_main_result_130_sig) ,	// output [31:0] eop_127_main_result_130_sig
	.eop_127_main_result_130_valid(eop_127_main_result_130_valid_sig) ,	// output  eop_127_main_result_130_valid_sig
	.eop_129_x1_134(eop_129_x1_134_sig) ,	// output [63:0] eop_129_x1_134_sig
	.eop_129_x1_134_valid(eop_129_x1_134_valid_sig) ,	// output  eop_129_x1_134_valid_sig
	.eop_129_x2_135(eop_129_x2_135_sig) ,	// output [63:0] eop_129_x2_135_sig
	.eop_129_x2_135_valid(eop_129_x2_135_valid_sig) ,	// output  eop_129_x2_135_valid_sig
	.eop_246_aSig_249(eop_246_aSig_249_sig) ,	// output [63:0] eop_246_aSig_249_sig
	.eop_246_aSig_249_valid(eop_246_aSig_249_valid_sig) ,	// output  eop_246_aSig_249_valid_sig
	.eop_245_aExp_250(eop_245_aExp_250_sig) ,	// output [31:0] eop_245_aExp_250_sig
	.eop_245_aExp_250_valid(eop_245_aExp_250_valid_sig) ,	// output  eop_245_aExp_250_valid_sig
	.eop_244_aSign_251(eop_244_aSign_251_sig) ,	// output [31:0] eop_244_aSign_251_sig
	.eop_244_aSign_251_valid(eop_244_aSign_251_valid_sig) ,	// output  eop_244_aSign_251_valid_sig
	.eop_246_bSig_252(eop_246_bSig_252_sig) ,	// output [63:0] eop_246_bSig_252_sig
	.eop_246_bSig_252_valid(eop_246_bSig_252_valid_sig) ,	// output  eop_246_bSig_252_valid_sig
	.eop_245_bExp_253(eop_245_bExp_253_sig) ,	// output [31:0] eop_245_bExp_253_sig
	.eop_245_bExp_253_valid(eop_245_bExp_253_valid_sig) ,	// output  eop_245_bExp_253_valid_sig
	.eop_244_bSign_254(eop_244_bSign_254_sig) ,	// output [31:0] eop_244_bSign_254_sig
	.eop_244_bSign_254_valid(eop_244_bSign_254_valid_sig) ,	// output  eop_244_bSign_254_valid_sig
	.eop_244_zSign_255(eop_244_zSign_255_sig) ,	// output [31:0] eop_244_zSign_255_sig
	.eop_244_zSign_255_valid(eop_244_zSign_255_valid_sig) ,	// output  eop_244_zSign_255_valid_sig
	.eop_124_shiftCount_126(eop_124_shiftCount_126_sig) ,	// output [31:0] eop_124_shiftCount_126_sig
	.eop_124_shiftCount_126_valid(eop_124_shiftCount_126_valid_sig) ,	// output  eop_124_shiftCount_126_valid_sig
	.eop_245_zExp_295(eop_245_zExp_295_sig) ,	// output [31:0] eop_245_zExp_295_sig
	.eop_245_zExp_295_valid(eop_245_zExp_295_valid_sig) ,	// output  eop_245_zExp_295_valid_sig
	.eop_246_aSig_296(eop_246_aSig_296_sig) ,	// output [63:0] eop_246_aSig_296_sig
	.eop_246_aSig_296_valid(eop_246_aSig_296_valid_sig) ,	// output  eop_246_aSig_296_valid_sig
	.eop_246_bSig_297(eop_246_bSig_297_sig) ,	// output [63:0] eop_246_bSig_297_sig
	.eop_246_bSig_297_valid(eop_246_bSig_297_valid_sig) ,	// output  eop_246_bSig_297_valid_sig
	.eop_245_zExp_301(eop_245_zExp_301_sig) ,	// output [31:0] eop_245_zExp_301_sig
	.eop_245_zExp_301_valid(eop_245_zExp_301_valid_sig) ,	// output  eop_245_zExp_301_valid_sig
	.eop_246_zSig_303(eop_246_zSig_303_sig) ,	// output [63:0] eop_246_zSig_303_sig
	.eop_246_zSig_303_valid(eop_246_zSig_303_valid_sig) ,	// output  eop_246_zSig_303_valid_sig
	.eop_246_zSig_310(eop_246_zSig_310_sig) ,	// output [63:0] eop_246_zSig_310_sig
	.eop_246_zSig_310_valid(eop_246_zSig_310_valid_sig) ,	// output  eop_246_zSig_310_valid_sig
	.eop_176_roundingMode_180(eop_176_roundingMode_180_sig) ,	// output [31:0] eop_176_roundingMode_180_sig
	.eop_176_roundingMode_180_valid(eop_176_roundingMode_180_valid_sig) ,	// output  eop_176_roundingMode_180_valid_sig
	.eop_177_roundNearestEven_181(eop_177_roundNearestEven_181_sig) ,	// output [31:0] eop_177_roundNearestEven_181_sig
	.eop_177_roundNearestEven_181_valid(eop_177_roundNearestEven_181_valid_sig) ,	// output  eop_177_roundNearestEven_181_valid_sig
	.eop_178_roundIncrement_182(eop_178_roundIncrement_182_sig) ,	// output [31:0] eop_178_roundIncrement_182_sig
	.eop_178_roundIncrement_182_valid(eop_178_roundIncrement_182_valid_sig) ,	// output  eop_178_roundIncrement_182_valid_sig
	.eop_178_roundBits_204(eop_178_roundBits_204_sig) ,	// output [31:0] eop_178_roundBits_204_sig
	.eop_178_roundBits_204_valid(eop_178_roundBits_204_valid_sig) ,	// output  eop_178_roundBits_204_valid_sig
	.eop_177_isTiny_215(eop_177_isTiny_215_sig) ,	// output [31:0] eop_177_isTiny_215_sig
	.eop_177_isTiny_215_valid(eop_177_isTiny_215_valid_sig) ,	// output  eop_177_isTiny_215_valid_sig
	.eop_174_zExp_219(eop_174_zExp_219_sig) ,	// output [31:0] eop_174_zExp_219_sig
	.eop_174_zExp_219_valid(eop_174_zExp_219_valid_sig) ,	// output  eop_174_zExp_219_valid_sig
	.eop_178_roundBits_220(eop_178_roundBits_220_sig) ,	// output [31:0] eop_178_roundBits_220_sig
	.eop_178_roundBits_220_valid(eop_178_roundBits_220_valid_sig) ,	// output  eop_178_roundBits_220_valid_sig
	.eop_174_zSig_227(eop_174_zSig_227_sig) ,	// output [63:0] eop_174_zSig_227_sig
	.eop_174_zSig_227_valid(eop_174_zSig_227_valid_sig) ,	// output  eop_174_zSig_227_valid_sig
	.eop_174_zExp_230(eop_174_zExp_230_sig) ,	// output [31:0] eop_174_zExp_230_sig
	.eop_174_zExp_230_valid(eop_174_zExp_230_valid_sig) ,	// output  eop_174_zExp_230_valid_sig
	.eop_133_result_136(eop_133_result_136_sig) ,	// output [63:0] eop_133_result_136_sig
	.eop_133_result_136_valid(eop_133_result_136_valid_sig) ,	// output  eop_133_result_136_valid_sig
	.eop_127_main_result_137(eop_127_main_result_137_sig) ,	// output [31:0] eop_127_main_result_137_sig
	.eop_127_main_result_137_valid(eop_127_main_result_137_valid_sig) 	// output  eop_127_main_result_137_valid_sig
);

defparam main_inst.LEGUP_0 = 'b00000000;
defparam main_inst.LEGUP_F_main_BB__0_1 = 'b00000001;
defparam main_inst.LEGUP_F_main_BB__0_2 = 'b00000010;
defparam main_inst.LEGUP_F_main_BB__1_3 = 'b00000011;
defparam main_inst.LEGUP_F_main_BB__1_4 = 'b00000100;
defparam main_inst.LEGUP_F_main_BB__1_5 = 'b00000101;
defparam main_inst.LEGUP_F_main_BB__19_6 = 'b00000110;
defparam main_inst.LEGUP_F_main_BB__23_7 = 'b00000111;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1i16ithread_8 = 'b00001000;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1i16i_9 = 'b00001001;
defparam main_inst.LEGUP_F_main_BB__27_10 = 'b00001010;
defparam main_inst.LEGUP_F_main_BB__27_11 = 'b00001011;
defparam main_inst.LEGUP_F_main_BB__30_12 = 'b00001100;
defparam main_inst.LEGUP_F_main_BB__32_13 = 'b00001101;
defparam main_inst.LEGUP_F_main_BB__36_14 = 'b00001110;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1i11ithread_15 = 'b00001111;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1i11i_16 = 'b00010000;
defparam main_inst.LEGUP_F_main_BB__40_17 = 'b00010001;
defparam main_inst.LEGUP_F_main_BB__40_18 = 'b00010010;
defparam main_inst.LEGUP_F_main_BB__43_19 = 'b00010011;
defparam main_inst.LEGUP_F_main_BB__46_20 = 'b00010100;
defparam main_inst.LEGUP_F_main_BB__47_21 = 'b00010101;
defparam main_inst.LEGUP_F_main_BB__51_22 = 'b00010110;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1iithread_23 = 'b00010111;
defparam main_inst.LEGUP_F_main_BB_float64_is_signaling_nanexit1ii_24 = 'b00011000;
defparam main_inst.LEGUP_F_main_BB__55_25 = 'b00011001;
defparam main_inst.LEGUP_F_main_BB__55_26 = 'b00011010;
defparam main_inst.LEGUP_F_main_BB__58_27 = 'b00011011;
defparam main_inst.LEGUP_F_main_BB__60_28 = 'b00011100;
defparam main_inst.LEGUP_F_main_BB__64_29 = 'b00011101;
defparam main_inst.LEGUP_F_main_BB__68_30 = 'b00011110;
defparam main_inst.LEGUP_F_main_BB__71_31 = 'b00011111;
defparam main_inst.LEGUP_F_main_BB__77_32 = 'b00100000;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexit10i_33 = 'b00100001;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexit10i_34 = 'b00100010;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexit10i_35 = 'b00100011;
defparam main_inst.LEGUP_F_main_BB__88_36 = 'b00100100;
defparam main_inst.LEGUP_F_main_BB__92_37 = 'b00100101;
defparam main_inst.LEGUP_F_main_BB__96_38 = 'b00100110;
defparam main_inst.LEGUP_F_main_BB__98_39 = 'b00100111;
defparam main_inst.LEGUP_F_main_BB__104_40 = 'b00101000;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexiti_41 = 'b00101001;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexiti_42 = 'b00101010;
defparam main_inst.LEGUP_F_main_BB_normalizeFloat64Subnormalexiti_43 = 'b00101011;
defparam main_inst.LEGUP_F_main_BB__115_44 = 'b00101100;
defparam main_inst.LEGUP_F_main_BB__115_45 = 'b00101101;
defparam main_inst.LEGUP_F_main_BB__126_46 = 'b00101110;
defparam main_inst.LEGUP_F_main_BB__126_47 = 'b00101111;
defparam main_inst.LEGUP_F_main_BB__129_48 = 'b00110000;
defparam main_inst.LEGUP_F_main_BB_estimateDiv128To64exitthreadi_49 = 'b00110001;
defparam main_inst.LEGUP_F_main_BB_estimateDiv128To64exitthreadi_50 = 'b00110010;
defparam main_inst.LEGUP_F_main_BB__132_51 = 'b00110011;
defparam main_inst.LEGUP_F_main_BB__136_52 = 'b00110100;
defparam main_inst.LEGUP_F_main_BB__136_53 = 'b00110101;
defparam main_inst.LEGUP_F_main_BB__136_54 = 'b00110110;
defparam main_inst.LEGUP_F_main_BB__136_55 = 'b00110111;
defparam main_inst.LEGUP_F_main_BB__136_56 = 'b00111000;
defparam main_inst.LEGUP_F_main_BB__136_57 = 'b00111001;
defparam main_inst.LEGUP_F_main_BB__136_58 = 'b00111010;
defparam main_inst.LEGUP_F_main_BB__136_59 = 'b00111011;
defparam main_inst.LEGUP_F_main_BB__136_60 = 'b00111100;
defparam main_inst.LEGUP_F_main_BB__136_61 = 'b00111101;
defparam main_inst.LEGUP_F_main_BB__136_62 = 'b00111110;
defparam main_inst.LEGUP_F_main_BB__136_63 = 'b00111111;
defparam main_inst.LEGUP_F_main_BB__136_64 = 'b01000000;
defparam main_inst.LEGUP_F_main_BB__136_65 = 'b01000001;
defparam main_inst.LEGUP_F_main_BB__136_66 = 'b01000010;
defparam main_inst.LEGUP_F_main_BB__136_67 = 'b01000011;
defparam main_inst.LEGUP_F_main_BB__136_68 = 'b01000100;
defparam main_inst.LEGUP_F_main_BB__136_69 = 'b01000101;
defparam main_inst.LEGUP_F_main_BB__136_70 = 'b01000110;
defparam main_inst.LEGUP_F_main_BB__136_71 = 'b01000111;
defparam main_inst.LEGUP_F_main_BB__136_72 = 'b01001000;
defparam main_inst.LEGUP_F_main_BB__136_73 = 'b01001001;
defparam main_inst.LEGUP_F_main_BB__136_74 = 'b01001010;
defparam main_inst.LEGUP_F_main_BB__136_75 = 'b01001011;
defparam main_inst.LEGUP_F_main_BB__136_76 = 'b01001100;
defparam main_inst.LEGUP_F_main_BB__136_77 = 'b01001101;
defparam main_inst.LEGUP_F_main_BB__136_78 = 'b01001110;
defparam main_inst.LEGUP_F_main_BB__136_79 = 'b01001111;
defparam main_inst.LEGUP_F_main_BB__136_80 = 'b01010000;
defparam main_inst.LEGUP_F_main_BB__136_81 = 'b01010001;
defparam main_inst.LEGUP_F_main_BB__136_82 = 'b01010010;
defparam main_inst.LEGUP_F_main_BB__136_83 = 'b01010011;
defparam main_inst.LEGUP_F_main_BB__136_84 = 'b01010100;
defparam main_inst.LEGUP_F_main_BB__136_85 = 'b01010101;
defparam main_inst.LEGUP_F_main_BB__136_86 = 'b01010110;
defparam main_inst.LEGUP_F_main_BB__136_87 = 'b01010111;
defparam main_inst.LEGUP_F_main_BB__136_88 = 'b01011000;
defparam main_inst.LEGUP_F_main_BB__136_89 = 'b01011001;
defparam main_inst.LEGUP_F_main_BB__136_90 = 'b01011010;
defparam main_inst.LEGUP_F_main_BB__136_91 = 'b01011011;
defparam main_inst.LEGUP_F_main_BB__136_92 = 'b01011100;
defparam main_inst.LEGUP_F_main_BB__136_93 = 'b01011101;
defparam main_inst.LEGUP_F_main_BB__136_94 = 'b01011110;
defparam main_inst.LEGUP_F_main_BB__136_95 = 'b01011111;
defparam main_inst.LEGUP_F_main_BB__136_96 = 'b01100000;
defparam main_inst.LEGUP_F_main_BB__136_97 = 'b01100001;
defparam main_inst.LEGUP_F_main_BB__136_98 = 'b01100010;
defparam main_inst.LEGUP_F_main_BB__136_99 = 'b01100011;
defparam main_inst.LEGUP_F_main_BB__136_100 = 'b01100100;
defparam main_inst.LEGUP_F_main_BB__136_101 = 'b01100101;
defparam main_inst.LEGUP_F_main_BB__136_102 = 'b01100110;
defparam main_inst.LEGUP_F_main_BB__136_103 = 'b01100111;
defparam main_inst.LEGUP_F_main_BB__136_104 = 'b01101000;
defparam main_inst.LEGUP_F_main_BB__136_105 = 'b01101001;
defparam main_inst.LEGUP_F_main_BB__136_106 = 'b01101010;
defparam main_inst.LEGUP_F_main_BB__136_107 = 'b01101011;
defparam main_inst.LEGUP_F_main_BB__136_108 = 'b01101100;
defparam main_inst.LEGUP_F_main_BB__136_109 = 'b01101101;
defparam main_inst.LEGUP_F_main_BB__136_110 = 'b01101110;
defparam main_inst.LEGUP_F_main_BB__136_111 = 'b01101111;
defparam main_inst.LEGUP_F_main_BB__136_112 = 'b01110000;
defparam main_inst.LEGUP_F_main_BB__136_113 = 'b01110001;
defparam main_inst.LEGUP_F_main_BB__136_114 = 'b01110010;
defparam main_inst.LEGUP_F_main_BB__136_115 = 'b01110011;
defparam main_inst.LEGUP_F_main_BB__136_116 = 'b01110100;
defparam main_inst.LEGUP_F_main_BB__139_117 = 'b01110101;
defparam main_inst.LEGUP_F_main_BB__139_118 = 'b01110110;
defparam main_inst.LEGUP_F_main_BB__139_119 = 'b01110111;
defparam main_inst.LEGUP_F_main_BB__139_120 = 'b01111000;
defparam main_inst.LEGUP_F_main_BB_lrphii_121 = 'b01111001;
defparam main_inst.LEGUP_F_main_BB__153_122 = 'b01111010;
defparam main_inst.LEGUP_F_main_BB__153_123 = 'b01111011;
defparam main_inst.LEGUP_F_main_BB__153_124 = 'b01111100;
defparam main_inst.LEGUP_F_main_BB__crit_edgeiiloopexit_125 = 'b01111101;
defparam main_inst.LEGUP_F_main_BB__crit_edgeii_126 = 'b01111110;
defparam main_inst.LEGUP_F_main_BB__165_127 = 'b01111111;
defparam main_inst.LEGUP_F_main_BB__165_128 = 'b10000000;
defparam main_inst.LEGUP_F_main_BB__165_129 = 'b10000001;
defparam main_inst.LEGUP_F_main_BB__165_130 = 'b10000010;
defparam main_inst.LEGUP_F_main_BB__165_131 = 'b10000011;
defparam main_inst.LEGUP_F_main_BB__165_132 = 'b10000100;
defparam main_inst.LEGUP_F_main_BB__165_133 = 'b10000101;
defparam main_inst.LEGUP_F_main_BB__165_134 = 'b10000110;
defparam main_inst.LEGUP_F_main_BB__165_135 = 'b10000111;
defparam main_inst.LEGUP_F_main_BB__165_136 = 'b10001000;
defparam main_inst.LEGUP_F_main_BB__165_137 = 'b10001001;
defparam main_inst.LEGUP_F_main_BB__165_138 = 'b10001010;
defparam main_inst.LEGUP_F_main_BB__165_139 = 'b10001011;
defparam main_inst.LEGUP_F_main_BB__165_140 = 'b10001100;
defparam main_inst.LEGUP_F_main_BB__165_141 = 'b10001101;
defparam main_inst.LEGUP_F_main_BB__165_142 = 'b10001110;
defparam main_inst.LEGUP_F_main_BB__165_143 = 'b10001111;
defparam main_inst.LEGUP_F_main_BB__165_144 = 'b10010000;
defparam main_inst.LEGUP_F_main_BB__165_145 = 'b10010001;
defparam main_inst.LEGUP_F_main_BB__165_146 = 'b10010010;
defparam main_inst.LEGUP_F_main_BB__165_147 = 'b10010011;
defparam main_inst.LEGUP_F_main_BB__165_148 = 'b10010100;
defparam main_inst.LEGUP_F_main_BB__165_149 = 'b10010101;
defparam main_inst.LEGUP_F_main_BB__165_150 = 'b10010110;
defparam main_inst.LEGUP_F_main_BB__165_151 = 'b10010111;
defparam main_inst.LEGUP_F_main_BB__165_152 = 'b10011000;
defparam main_inst.LEGUP_F_main_BB__165_153 = 'b10011001;
defparam main_inst.LEGUP_F_main_BB__165_154 = 'b10011010;
defparam main_inst.LEGUP_F_main_BB__165_155 = 'b10011011;
defparam main_inst.LEGUP_F_main_BB__165_156 = 'b10011100;
defparam main_inst.LEGUP_F_main_BB__165_157 = 'b10011101;
defparam main_inst.LEGUP_F_main_BB__165_158 = 'b10011110;
defparam main_inst.LEGUP_F_main_BB__165_159 = 'b10011111;
defparam main_inst.LEGUP_F_main_BB__165_160 = 'b10100000;
defparam main_inst.LEGUP_F_main_BB__165_161 = 'b10100001;
defparam main_inst.LEGUP_F_main_BB__165_162 = 'b10100010;
defparam main_inst.LEGUP_F_main_BB__165_163 = 'b10100011;
defparam main_inst.LEGUP_F_main_BB__165_164 = 'b10100100;
defparam main_inst.LEGUP_F_main_BB__165_165 = 'b10100101;
defparam main_inst.LEGUP_F_main_BB__165_166 = 'b10100110;
defparam main_inst.LEGUP_F_main_BB__165_167 = 'b10100111;
defparam main_inst.LEGUP_F_main_BB__165_168 = 'b10101000;
defparam main_inst.LEGUP_F_main_BB__165_169 = 'b10101001;
defparam main_inst.LEGUP_F_main_BB__165_170 = 'b10101010;
defparam main_inst.LEGUP_F_main_BB__165_171 = 'b10101011;
defparam main_inst.LEGUP_F_main_BB__165_172 = 'b10101100;
defparam main_inst.LEGUP_F_main_BB__165_173 = 'b10101101;
defparam main_inst.LEGUP_F_main_BB__165_174 = 'b10101110;
defparam main_inst.LEGUP_F_main_BB__165_175 = 'b10101111;
defparam main_inst.LEGUP_F_main_BB__165_176 = 'b10110000;
defparam main_inst.LEGUP_F_main_BB__165_177 = 'b10110001;
defparam main_inst.LEGUP_F_main_BB__165_178 = 'b10110010;
defparam main_inst.LEGUP_F_main_BB__165_179 = 'b10110011;
defparam main_inst.LEGUP_F_main_BB__165_180 = 'b10110100;
defparam main_inst.LEGUP_F_main_BB__165_181 = 'b10110101;
defparam main_inst.LEGUP_F_main_BB__165_182 = 'b10110110;
defparam main_inst.LEGUP_F_main_BB__165_183 = 'b10110111;
defparam main_inst.LEGUP_F_main_BB__165_184 = 'b10111000;
defparam main_inst.LEGUP_F_main_BB__165_185 = 'b10111001;
defparam main_inst.LEGUP_F_main_BB__165_186 = 'b10111010;
defparam main_inst.LEGUP_F_main_BB__165_187 = 'b10111011;
defparam main_inst.LEGUP_F_main_BB__165_188 = 'b10111100;
defparam main_inst.LEGUP_F_main_BB__165_189 = 'b10111101;
defparam main_inst.LEGUP_F_main_BB__165_190 = 'b10111110;
defparam main_inst.LEGUP_F_main_BB__165_191 = 'b10111111;
defparam main_inst.LEGUP_F_main_BB_estimateDiv128To64exiti_192 = 'b11000000;
defparam main_inst.LEGUP_F_main_BB_estimateDiv128To64exiti_193 = 'b11000001;
defparam main_inst.LEGUP_F_main_BB__171_194 = 'b11000010;
defparam main_inst.LEGUP_F_main_BB__171_195 = 'b11000011;
defparam main_inst.LEGUP_F_main_BB__171_196 = 'b11000100;
defparam main_inst.LEGUP_F_main_BB__171_197 = 'b11000101;
defparam main_inst.LEGUP_F_main_BB__171_198 = 'b11000110;
defparam main_inst.LEGUP_F_main_BB__171_199 = 'b11000111;
defparam main_inst.LEGUP_F_main_BB_lrphipreheader_200 = 'b11001000;
defparam main_inst.LEGUP_F_main_BB_lrphi_201 = 'b11001001;
defparam main_inst.LEGUP_F_main_BB_lrphi_202 = 'b11001010;
defparam main_inst.LEGUP_F_main_BB_lrphi_203 = 'b11001011;
defparam main_inst.LEGUP_F_main_BB__crit_edgeiloopexit_204 = 'b11001100;
defparam main_inst.LEGUP_F_main_BB__crit_edgei_205 = 'b11001101;
defparam main_inst.LEGUP_F_main_BB__197_206 = 'b11001110;
defparam main_inst.LEGUP_F_main_BB__197_207 = 'b11001111;
defparam main_inst.LEGUP_F_main_BB__201_208 = 'b11010000;
defparam main_inst.LEGUP_F_main_BB__203_209 = 'b11010001;
defparam main_inst.LEGUP_F_main_BB__205_210 = 'b11010010;
defparam main_inst.LEGUP_F_main_BB__205_211 = 'b11010011;
defparam main_inst.LEGUP_F_main_BB__208_212 = 'b11010100;
defparam main_inst.LEGUP_F_main_BB__211_213 = 'b11010101;
defparam main_inst.LEGUP_F_main_BB__213_214 = 'b11010110;
defparam main_inst.LEGUP_F_main_BB__213_215 = 'b11010111;
defparam main_inst.LEGUP_F_main_BB__215_216 = 'b11011000;
defparam main_inst.LEGUP_F_main_BB__225_217 = 'b11011001;
defparam main_inst.LEGUP_F_main_BB_shift64RightJammingexitii_218 = 'b11011010;
defparam main_inst.LEGUP_F_main_BB_shift64RightJammingexitii_219 = 'b11011011;
defparam main_inst.LEGUP_F_main_BB_threadii_220 = 'b11011100;
defparam main_inst.LEGUP_F_main_BB_threadii_221 = 'b11011101;
defparam main_inst.LEGUP_F_main_BB__237_222 = 'b11011110;
defparam main_inst.LEGUP_F_main_BB__237_223 = 'b11011111;
defparam main_inst.LEGUP_F_main_BB__238_224 = 'b11100000;
defparam main_inst.LEGUP_F_main_BB_float64_divexit_225 = 'b11100001;
defparam main_inst.LEGUP_F_main_BB_float64_divexit_226 = 'b11100010;
defparam main_inst.LEGUP_F_main_BB_float64_divexit_227 = 'b11100011;
defparam main_inst.LEGUP_F_main_BB__253_228 = 'b11100100;
defparam main_inst.LEGUP_F_main_BB__256_229 = 'b11100101;
defparam main_inst.LEGUP_F_main_BB__258_230 = 'b11100110;
defparam main_inst.LEGUP_F_main_BB__260_231 = 'b11100111;
defparam main_inst.tag_offset = 'b000000000;
//defparam main_inst.tag_addr_offset = ;
