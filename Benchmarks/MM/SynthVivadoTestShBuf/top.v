// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   

   output [7:0] LEDG;
    input UART_RXD;
    output UART_TXD;    
	wire clk = CLOCK_50;



   wire 	reset = ~KEY[0];
   wire 	start = ~KEY[1];
   wire [31:0] 	return_val;
   reg  [31:0] 	return_val_reg;
   wire 	finish;
   wire [3:0]	state;

   hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
   hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
   hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
   hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
   hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
   hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
   hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
   hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
   
    
    wire [15:0] dbg_active_instance;
    wire [15:0] dbg_current_state;
    wire [7:0] hlsd_state;
	always @ (*) begin
		hex7 <= 4'b0;
		hex6 <= 4'b0;
		hex5 <= 4'b0;
		hex4 <= 4'b0;
		hex3 <= 4'b0;
		hex2 <= 4'b0;
		hex1 <= 4'b0;
		hex0 <= 4'b0;
		if (SW[0] == 1'b0) begin
			hex7 <= return_val_reg[31:28];
			hex6 <= return_val_reg[27:24];
			hex5 <= return_val_reg[23:20];
			hex4 <= return_val_reg[19:16];
			hex3 <= return_val_reg[15:12];
			hex2 <= return_val_reg[11:8];
			hex1 <= return_val_reg[7:4];
			hex0 <= return_val_reg[3:0];
		end
		else if (SW[0] == 1'b1) begin
			hex7 <= dbg_active_instance[11:8];
			hex6 <= dbg_active_instance[7:4];
			hex5 <= dbg_active_instance[3:0];
			hex4 <= dbg_current_state[11:8];
			hex3 <= dbg_current_state[7:4];
			hex2 <= dbg_current_state[3:0];
			hex1 <= hlsd_state[7:4];
			hex0 <= hlsd_state[3:0];
		end
	end

	always @(posedge clk) begin
		if (reset)
			return_val_reg <= 0;
		else if (finish) begin
			return_val_reg <= return_val;
		end
	end


wire BUFFER_we;
wire BUFFER_we_dbg;
wire [6:0]BUFFER_address0;
wire [6:0]BUFFER_address0_dbg;
wire BUFFER_ce0;
wire BUFFER_ce0_dbg;
wire clk_debug;
wire [63:0] BUFFER_q0;

main main_inst
(
	//.ap_clk(clk_debug) ,
	.ap_clk(clk) ,
	.ap_rst(reset) ,
	//.ap_start(1'b1) ,
	.ap_start(start) ,
	.ap_done() ,
	.ap_idle() ,
	.ap_ready() ,
	.ap_return(return_val) ,
);


endmodule
