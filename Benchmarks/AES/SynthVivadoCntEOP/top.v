// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			finish;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg finish_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge finish_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (finish_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			finish_reg <= 0;
		else if (finish)
			finish_reg <= 1;
		else
			finish_reg <=finish_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (finish) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
main main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(finish) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_65_main_result_123(eop_65_main_result_123_sig) ,	// output [31:0] eop_65_main_result_123_sig
	.eop_65_main_result_123_ap_vld(eop_65_main_result_123_ap_vld_sig) ,	// output  eop_65_main_result_123_ap_vld_sig
	.eop_81_nk_87(eop_81_nk_87_sig) ,	// output [31:0] eop_81_nk_87_sig
	.eop_81_nk_87_ap_vld(eop_81_nk_87_ap_vld_sig) ,	// output  eop_81_nk_87_ap_vld_sig
	.eop_81_nb_88(eop_81_nb_88_sig) ,	// output [31:0] eop_81_nb_88_sig
	.eop_81_nb_88_ap_vld(eop_81_nb_88_ap_vld_sig) ,	// output  eop_81_nb_88_ap_vld_sig
	.eop_81_round_89(eop_81_round_89_sig) ,	// output [31:0] eop_81_round_89_sig
	.eop_81_round_89_ap_vld(eop_81_round_89_ap_vld_sig) ,	// output  eop_81_round_89_ap_vld_sig
	.eop_70_round_85(eop_70_round_85_sig) ,	// output [31:0] eop_70_round_85_sig
	.eop_70_round_85_ap_vld(eop_70_round_85_ap_vld_sig) ,	// output  eop_70_round_85_ap_vld_sig
	.eop_69_nb_86(eop_69_nb_86_sig) ,	// output [31:0] eop_69_nb_86_sig
	.eop_69_nb_86_ap_vld(eop_69_nb_86_ap_vld_sig) ,	// output  eop_69_nb_86_ap_vld_sig
	.eop_517_nb_524(eop_517_nb_524_sig) ,	// output [31:0] eop_517_nb_524_sig
	.eop_517_nb_524_ap_vld(eop_517_nb_524_ap_vld_sig) ,	// output  eop_517_nb_524_ap_vld_sig
	.eop_138_temp_143(eop_138_temp_143_sig) ,	// output [31:0] eop_138_temp_143_sig
	.eop_138_temp_143_ap_vld(eop_138_temp_143_ap_vld_sig) ,	// output  eop_138_temp_143_ap_vld_sig
	.eop_138_temp_149(eop_138_temp_149_sig) ,	// output [31:0] eop_138_temp_149_sig
	.eop_138_temp_149_ap_vld(eop_138_temp_149_ap_vld_sig) ,	// output  eop_138_temp_149_ap_vld_sig
	.eop_138_temp_152(eop_138_temp_152_sig) ,	// output [31:0] eop_138_temp_152_sig
	.eop_138_temp_152_ap_vld(eop_138_temp_152_ap_vld_sig) ,	// output  eop_138_temp_152_ap_vld_sig
	.eop_138_temp_156(eop_138_temp_156_sig) ,	// output [31:0] eop_138_temp_156_sig
	.eop_138_temp_156_ap_vld(eop_138_temp_156_ap_vld_sig) ,	// output  eop_138_temp_156_ap_vld_sig
	.eop_138_temp_168(eop_138_temp_168_sig) ,	// output [31:0] eop_138_temp_168_sig
	.eop_138_temp_168_ap_vld(eop_138_temp_168_ap_vld_sig) ,	// output  eop_138_temp_168_ap_vld_sig
	.eop_138_temp_176(eop_138_temp_176_sig) ,	// output [31:0] eop_138_temp_176_sig
	.eop_138_temp_176_ap_vld(eop_138_temp_176_ap_vld_sig) ,	// output  eop_138_temp_176_ap_vld_sig
	.eop_138_temp_180(eop_138_temp_180_sig) ,	// output [31:0] eop_138_temp_180_sig
	.eop_138_temp_180_ap_vld(eop_138_temp_180_ap_vld_sig) ,	// output  eop_138_temp_180_ap_vld_sig
	.eop_138_temp_185(eop_138_temp_185_sig) ,	// output [31:0] eop_138_temp_185_sig
	.eop_138_temp_185_ap_vld(eop_138_temp_185_ap_vld_sig) ,	// output  eop_138_temp_185_ap_vld_sig
	.eop_138_temp_188(eop_138_temp_188_sig) ,	// output [31:0] eop_138_temp_188_sig
	.eop_138_temp_188_ap_vld(eop_138_temp_188_ap_vld_sig) ,	// output  eop_138_temp_188_ap_vld_sig
	.eop_138_temp_191(eop_138_temp_191_sig) ,	// output [31:0] eop_138_temp_191_sig
	.eop_138_temp_191_ap_vld(eop_138_temp_191_ap_vld_sig) ,	// output  eop_138_temp_191_ap_vld_sig
	.eop_138_temp_203(eop_138_temp_203_sig) ,	// output [31:0] eop_138_temp_203_sig
	.eop_138_temp_203_ap_vld(eop_138_temp_203_ap_vld_sig) ,	// output  eop_138_temp_203_ap_vld_sig
	.eop_138_temp_213(eop_138_temp_213_sig) ,	// output [31:0] eop_138_temp_213_sig
	.eop_138_temp_213_ap_vld(eop_138_temp_213_ap_vld_sig) ,	// output  eop_138_temp_213_ap_vld_sig
	.eop_138_temp_223(eop_138_temp_223_sig) ,	// output [31:0] eop_138_temp_223_sig
	.eop_138_temp_223_ap_vld(eop_138_temp_223_ap_vld_sig) ,	// output  eop_138_temp_223_ap_vld_sig
	.eop_138_temp_226(eop_138_temp_226_sig) ,	// output [31:0] eop_138_temp_226_sig
	.eop_138_temp_226_ap_vld(eop_138_temp_226_ap_vld_sig) ,	// output  eop_138_temp_226_ap_vld_sig
	.eop_138_temp_229(eop_138_temp_229_sig) ,	// output [31:0] eop_138_temp_229_sig
	.eop_138_temp_229_ap_vld(eop_138_temp_229_ap_vld_sig) ,	// output  eop_138_temp_229_ap_vld_sig
	.eop_138_temp_232(eop_138_temp_232_sig) ,	// output [31:0] eop_138_temp_232_sig
	.eop_138_temp_232_ap_vld(eop_138_temp_232_ap_vld_sig) ,	// output  eop_138_temp_232_ap_vld_sig
	.eop_373_x_380(eop_373_x_380_sig) ,	// output [31:0] eop_373_x_380_sig
	.eop_373_x_380_ap_vld(eop_373_x_380_ap_vld_sig) ,	// output  eop_373_x_380_ap_vld_sig
	.eop_373_x_392(eop_373_x_392_sig) ,	// output [31:0] eop_373_x_392_sig
	.eop_373_x_392_ap_vld(eop_373_x_392_ap_vld_sig) ,	// output  eop_373_x_392_ap_vld_sig
	.eop_373_x_404(eop_373_x_404_sig) ,	// output [31:0] eop_373_x_404_sig
	.eop_373_x_404_ap_vld(eop_373_x_404_ap_vld_sig) ,	// output  eop_373_x_404_ap_vld_sig
	.eop_373_x_416(eop_373_x_416_sig) ,	// output [31:0] eop_373_x_416_sig
	.eop_373_x_416_ap_vld(eop_373_x_416_ap_vld_sig) ,	// output  eop_373_x_416_ap_vld_sig
	.eop_65_main_result_130(eop_65_main_result_130_sig) ,	// output [31:0] eop_65_main_result_130_sig
	.eop_65_main_result_130_ap_vld(eop_65_main_result_130_ap_vld_sig) ,	// output  eop_65_main_result_130_ap_vld_sig
	.eop_70_round_84(eop_70_round_84_sig) ,	// output [31:0] eop_70_round_84_sig
	.eop_70_round_84_ap_vld(eop_70_round_84_ap_vld_sig) ,	// output  eop_70_round_84_ap_vld_sig
	.eop_69_nb_85(eop_69_nb_85_sig) ,	// output [31:0] eop_69_nb_85_sig
	.eop_69_nb_85_ap_vld(eop_69_nb_85_ap_vld_sig) ,	// output  eop_69_nb_85_ap_vld_sig
	.eop_258_temp_263(eop_258_temp_263_sig) ,	// output [31:0] eop_258_temp_263_sig
	.eop_258_temp_263_ap_vld(eop_258_temp_263_ap_vld_sig) ,	// output  eop_258_temp_263_ap_vld_sig
	.eop_258_temp_269(eop_258_temp_269_sig) ,	// output [31:0] eop_258_temp_269_sig
	.eop_258_temp_269_ap_vld(eop_258_temp_269_ap_vld_sig) ,	// output  eop_258_temp_269_ap_vld_sig
	.eop_258_temp_272(eop_258_temp_272_sig) ,	// output [31:0] eop_258_temp_272_sig
	.eop_258_temp_272_ap_vld(eop_258_temp_272_ap_vld_sig) ,	// output  eop_258_temp_272_ap_vld_sig
	.eop_258_temp_276(eop_258_temp_276_sig) ,	// output [31:0] eop_258_temp_276_sig
	.eop_258_temp_276_ap_vld(eop_258_temp_276_ap_vld_sig) ,	// output  eop_258_temp_276_ap_vld_sig
	.eop_258_temp_288(eop_258_temp_288_sig) ,	// output [31:0] eop_258_temp_288_sig
	.eop_258_temp_288_ap_vld(eop_258_temp_288_ap_vld_sig) ,	// output  eop_258_temp_288_ap_vld_sig
	.eop_258_temp_296(eop_258_temp_296_sig) ,	// output [31:0] eop_258_temp_296_sig
	.eop_258_temp_296_ap_vld(eop_258_temp_296_ap_vld_sig) ,	// output  eop_258_temp_296_ap_vld_sig
	.eop_258_temp_300(eop_258_temp_300_sig) ,	// output [31:0] eop_258_temp_300_sig
	.eop_258_temp_300_ap_vld(eop_258_temp_300_ap_vld_sig) ,	// output  eop_258_temp_300_ap_vld_sig
	.eop_258_temp_305(eop_258_temp_305_sig) ,	// output [31:0] eop_258_temp_305_sig
	.eop_258_temp_305_ap_vld(eop_258_temp_305_ap_vld_sig) ,	// output  eop_258_temp_305_ap_vld_sig
	.eop_258_temp_308(eop_258_temp_308_sig) ,	// output [31:0] eop_258_temp_308_sig
	.eop_258_temp_308_ap_vld(eop_258_temp_308_ap_vld_sig) ,	// output  eop_258_temp_308_ap_vld_sig
	.eop_258_temp_311(eop_258_temp_311_sig) ,	// output [31:0] eop_258_temp_311_sig
	.eop_258_temp_311_ap_vld(eop_258_temp_311_ap_vld_sig) ,	// output  eop_258_temp_311_ap_vld_sig
	.eop_258_temp_323(eop_258_temp_323_sig) ,	// output [31:0] eop_258_temp_323_sig
	.eop_258_temp_323_ap_vld(eop_258_temp_323_ap_vld_sig) ,	// output  eop_258_temp_323_ap_vld_sig
	.eop_258_temp_333(eop_258_temp_333_sig) ,	// output [31:0] eop_258_temp_333_sig
	.eop_258_temp_333_ap_vld(eop_258_temp_333_ap_vld_sig) ,	// output  eop_258_temp_333_ap_vld_sig
	.eop_258_temp_343(eop_258_temp_343_sig) ,	// output [31:0] eop_258_temp_343_sig
	.eop_258_temp_343_ap_vld(eop_258_temp_343_ap_vld_sig) ,	// output  eop_258_temp_343_ap_vld_sig
	.eop_258_temp_346(eop_258_temp_346_sig) ,	// output [31:0] eop_258_temp_346_sig
	.eop_258_temp_346_ap_vld(eop_258_temp_346_ap_vld_sig) ,	// output  eop_258_temp_346_ap_vld_sig
	.eop_258_temp_349(eop_258_temp_349_sig) ,	// output [31:0] eop_258_temp_349_sig
	.eop_258_temp_349_ap_vld(eop_258_temp_349_ap_vld_sig) ,	// output  eop_258_temp_349_ap_vld_sig
	.eop_258_temp_352(eop_258_temp_352_sig) ,	// output [31:0] eop_258_temp_352_sig
	.eop_258_temp_352_ap_vld(eop_258_temp_352_ap_vld_sig) ,	// output  eop_258_temp_352_ap_vld_sig
	.eop_440_x_452(eop_440_x_452_sig) ,	// output [31:0] eop_440_x_452_sig
	.eop_440_x_452_ap_vld(eop_440_x_452_ap_vld_sig) ,	// output  eop_440_x_452_ap_vld_sig
	.eop_440_x_456(eop_440_x_456_sig) ,	// output [31:0] eop_440_x_456_sig
	.eop_440_x_456_ap_vld(eop_440_x_456_ap_vld_sig) ,	// output  eop_440_x_456_ap_vld_sig
	.eop_440_x_460(eop_440_x_460_sig) ,	// output [31:0] eop_440_x_460_sig
	.eop_440_x_460_ap_vld(eop_440_x_460_ap_vld_sig) ,	// output  eop_440_x_460_ap_vld_sig
	.eop_440_x_465(eop_440_x_465_sig) ,	// output [31:0] eop_440_x_465_sig
	.eop_440_x_465_ap_vld(eop_440_x_465_ap_vld_sig) ,	// output  eop_440_x_465_ap_vld_sig
	.eop_440_x_468(eop_440_x_468_sig) ,	// output [31:0] eop_440_x_468_sig
	.eop_440_x_468_ap_vld(eop_440_x_468_ap_vld_sig) ,	// output  eop_440_x_468_ap_vld_sig
	.eop_440_x_472(eop_440_x_472_sig) ,	// output [31:0] eop_440_x_472_sig
	.eop_440_x_472_ap_vld(eop_440_x_472_ap_vld_sig) ,	// output  eop_440_x_472_ap_vld_sig
	.eop_440_x_478(eop_440_x_478_sig) ,	// output [31:0] eop_440_x_478_sig
	.eop_440_x_478_ap_vld(eop_440_x_478_ap_vld_sig) ,	// output  eop_440_x_478_ap_vld_sig
	.eop_440_x_482(eop_440_x_482_sig) ,	// output [31:0] eop_440_x_482_sig
	.eop_440_x_482_ap_vld(eop_440_x_482_ap_vld_sig) ,	// output  eop_440_x_482_ap_vld_sig
	.eop_440_x_485(eop_440_x_485_sig) ,	// output [31:0] eop_440_x_485_sig
	.eop_440_x_485_ap_vld(eop_440_x_485_ap_vld_sig) ,	// output  eop_440_x_485_ap_vld_sig
	.eop_440_x_491(eop_440_x_491_sig) ,	// output [31:0] eop_440_x_491_sig
	.eop_440_x_491_ap_vld(eop_440_x_491_ap_vld_sig) ,	// output  eop_440_x_491_ap_vld_sig
	.eop_440_x_494(eop_440_x_494_sig) ,	// output [31:0] eop_440_x_494_sig
	.eop_440_x_494_ap_vld(eop_440_x_494_ap_vld_sig) ,	// output  eop_440_x_494_ap_vld_sig
	.eop_440_x_497(eop_440_x_497_sig) ,	// output [31:0] eop_440_x_497_sig
	.eop_440_x_497_ap_vld(eop_440_x_497_ap_vld_sig) ,	// output  eop_440_x_497_ap_vld_sig
	.eop_65_main_result_136(eop_65_main_result_136_sig) ,	// output [31:0] eop_65_main_result_136_sig
	.eop_65_main_result_136_ap_vld(eop_65_main_result_136_ap_vld_sig) ,	// output  eop_65_main_result_136_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 60;

reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_65_main_result_123_sig;
valid_regs[1] <= eop_65_main_result_123_ap_vld_sig;
valid_regs[2] <= eop_81_nk_87_ap_vld_sig;
valid_regs[3] <= eop_81_nb_88_ap_vld_sig;
valid_regs[4] <= eop_81_round_89_ap_vld_sig;
valid_regs[5] <= eop_70_round_85_ap_vld_sig;
valid_regs[6] <= eop_69_nb_86_ap_vld_sig;
valid_regs[7] <= eop_517_nb_524_ap_vld_sig;
valid_regs[8] <= eop_138_temp_143_ap_vld_sig;
valid_regs[9] <= eop_138_temp_149_ap_vld_sig;
valid_regs[10] <= eop_138_temp_152_ap_vld_sig;
valid_regs[11] <= eop_138_temp_156_ap_vld_sig;
valid_regs[12] <= eop_138_temp_168_ap_vld_sig;
valid_regs[13] <= eop_138_temp_176_ap_vld_sig;
valid_regs[14] <= eop_138_temp_180_ap_vld_sig;
valid_regs[15] <= eop_138_temp_185_ap_vld_sig;
valid_regs[16] <= eop_138_temp_188_ap_vld_sig;
valid_regs[17] <= eop_138_temp_191_ap_vld_sig;
valid_regs[18] <= eop_138_temp_203_ap_vld_sig;
valid_regs[19] <= eop_138_temp_213_ap_vld_sig;
valid_regs[20] <= eop_138_temp_223_ap_vld_sig;
valid_regs[21] <= eop_138_temp_226_ap_vld_sig;
valid_regs[22] <= eop_138_temp_229_ap_vld_sig;
valid_regs[23] <= eop_138_temp_232_ap_vld_sig;
valid_regs[24] <= eop_373_x_380_ap_vld_sig;
valid_regs[25] <= eop_373_x_392_ap_vld_sig;
valid_regs[26] <= eop_373_x_404_ap_vld_sig;
valid_regs[27] <= eop_373_x_416_ap_vld_sig;
valid_regs[28] <= eop_65_main_result_130_ap_vld_sig;
valid_regs[29] <= eop_70_round_84_ap_vld_sig;
valid_regs[30] <= eop_69_nb_85_ap_vld_sig;
valid_regs[31] <= eop_258_temp_263_ap_vld_sig;
valid_regs[32] <= eop_258_temp_269_ap_vld_sig;
valid_regs[33] <= eop_258_temp_272_ap_vld_sig;
valid_regs[34] <= eop_258_temp_276_ap_vld_sig;
valid_regs[35] <= eop_258_temp_288_ap_vld_sig;
valid_regs[36] <= eop_258_temp_296_ap_vld_sig;
valid_regs[37] <= eop_258_temp_300_ap_vld_sig;
valid_regs[38] <= eop_258_temp_305_ap_vld_sig;
valid_regs[39] <= eop_258_temp_308_ap_vld_sig;
valid_regs[40] <= eop_258_temp_311_ap_vld_sig;
valid_regs[41] <= eop_258_temp_323_ap_vld_sig;
valid_regs[42] <= eop_258_temp_333_ap_vld_sig;
valid_regs[43] <= eop_258_temp_343_ap_vld_sig;
valid_regs[44] <= eop_258_temp_346_ap_vld_sig;
valid_regs[45] <= eop_258_temp_349_ap_vld_sig;
valid_regs[46] <= eop_258_temp_352_ap_vld_sig;
valid_regs[47] <= eop_440_x_452_ap_vld_sig;
valid_regs[48] <= eop_440_x_456_ap_vld_sig;
valid_regs[49] <= eop_440_x_460_ap_vld_sig;
valid_regs[50] <= eop_440_x_465_ap_vld_sig;
valid_regs[51] <= eop_440_x_468_ap_vld_sig;
valid_regs[52] <= eop_440_x_472_ap_vld_sig;
valid_regs[53] <= eop_440_x_478_ap_vld_sig;
valid_regs[54] <= eop_440_x_482_ap_vld_sig;
valid_regs[55] <= eop_440_x_485_ap_vld_sig;
valid_regs[56] <= eop_440_x_491_ap_vld_sig;
valid_regs[57] <= eop_440_x_494_ap_vld_sig;
valid_regs[58] <= eop_440_x_497_ap_vld_sig;
valid_regs[59] <= eop_65_main_result_136_ap_vld_sig;

end

integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
