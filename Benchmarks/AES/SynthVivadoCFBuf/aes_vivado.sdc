create_clock -name CLOCK_50 -period 20 [get_ports CLOCK_50]
create_generated_clock -divide_by 250 -source [get_ports CLOCK_50] -name out_clk [get_registers out_clk]
#create_generated_clock -divide_by 2 -source [get_ports CLOCK_50] -name CLOCK_25 [get_registers CLOCK_25]

#create_clock -name CLOCK_27 -period 27MHz [get_ports CLOCK_27]
