// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
sha_main sha_main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_44_main_result_45(eop_44_main_result_45_sig) ,	// output [31:0] eop_44_main_result_45_sig
	.eop_44_main_result_45_ap_vld(eop_44_main_result_45_ap_vld_sig) ,	// output  eop_44_main_result_45_ap_vld_sig
	.eop_33_sha_info_count_lo_151(eop_33_sha_info_count_lo_151_sig) ,	// output [31:0] eop_33_sha_info_count_lo_151_sig
	.eop_33_sha_info_count_lo_151_ap_vld(eop_33_sha_info_count_lo_151_ap_vld_sig) ,	// output  eop_33_sha_info_count_lo_151_ap_vld_sig
	.eop_33_sha_info_count_hi_152(eop_33_sha_info_count_hi_152_sig) ,	// output [31:0] eop_33_sha_info_count_hi_152_sig
	.eop_33_sha_info_count_hi_152_ap_vld(eop_33_sha_info_count_hi_152_ap_vld_sig) ,	// output  eop_33_sha_info_count_hi_152_ap_vld_sig
	.eop_208_i_214(eop_208_i_214_sig) ,	// output [31:0] eop_208_i_214_sig
	.eop_208_i_214_ap_vld(eop_208_i_214_ap_vld_sig) ,	// output  eop_208_i_214_ap_vld_sig
	.eop_33_sha_info_count_hi_162(eop_33_sha_info_count_hi_162_sig) ,	// output [31:0] eop_33_sha_info_count_hi_162_sig
	.eop_33_sha_info_count_hi_162_ap_vld(eop_33_sha_info_count_hi_162_ap_vld_sig) ,	// output  eop_33_sha_info_count_hi_162_ap_vld_sig
	.eop_33_sha_info_count_lo_164(eop_33_sha_info_count_lo_164_sig) ,	// output [31:0] eop_33_sha_info_count_lo_164_sig
	.eop_33_sha_info_count_lo_164_ap_vld(eop_33_sha_info_count_lo_164_ap_vld_sig) ,	// output  eop_33_sha_info_count_lo_164_ap_vld_sig
	.eop_33_sha_info_count_hi_165(eop_33_sha_info_count_hi_165_sig) ,	// output [31:0] eop_33_sha_info_count_hi_165_sig
	.eop_33_sha_info_count_hi_165_ap_vld(eop_33_sha_info_count_hi_165_ap_vld_sig) ,	// output  eop_33_sha_info_count_hi_165_ap_vld_sig
	.eop_78_m_79(eop_78_m_79_sig) ,	// output [31:0] eop_78_m_79_sig
	.eop_78_m_79_ap_vld(eop_78_m_79_ap_vld_sig) ,	// output  eop_78_m_79_ap_vld_sig
	.eop_77_tmp_85(eop_77_tmp_85_sig) ,	// output [31:0] eop_77_tmp_85_sig
	.eop_77_tmp_85_ap_vld(eop_77_tmp_85_ap_vld_sig) ,	// output  eop_77_tmp_85_ap_vld_sig
	.eop_78_m_83(eop_78_m_83_sig) ,	// output [31:0] eop_78_m_83_sig
	.eop_78_m_83_ap_vld(eop_78_m_83_ap_vld_sig) ,	// output  eop_78_m_83_ap_vld_sig
	.eop_101_A_111(eop_101_A_111_sig) ,	// output [31:0] eop_101_A_111_sig
	.eop_101_A_111_ap_vld(eop_101_A_111_ap_vld_sig) ,	// output  eop_101_A_111_ap_vld_sig
	.eop_101_B_112(eop_101_B_112_sig) ,	// output [31:0] eop_101_B_112_sig
	.eop_101_B_112_ap_vld(eop_101_B_112_ap_vld_sig) ,	// output  eop_101_B_112_ap_vld_sig
	.eop_101_C_113(eop_101_C_113_sig) ,	// output [31:0] eop_101_C_113_sig
	.eop_101_C_113_ap_vld(eop_101_C_113_ap_vld_sig) ,	// output  eop_101_C_113_ap_vld_sig
	.eop_101_D_114(eop_101_D_114_sig) ,	// output [31:0] eop_101_D_114_sig
	.eop_101_D_114_ap_vld(eop_101_D_114_ap_vld_sig) ,	// output  eop_101_D_114_ap_vld_sig
	.eop_101_E_115(eop_101_E_115_sig) ,	// output [31:0] eop_101_E_115_sig
	.eop_101_E_115_ap_vld(eop_101_E_115_ap_vld_sig) ,	// output  eop_101_E_115_ap_vld_sig
	.eop_101_temp_119(eop_101_temp_119_sig) ,	// output [31:0] eop_101_temp_119_sig
	.eop_101_temp_119_ap_vld(eop_101_temp_119_ap_vld_sig) ,	// output  eop_101_temp_119_ap_vld_sig
	.eop_101_E_119(eop_101_E_119_sig) ,	// output [31:0] eop_101_E_119_sig
	.eop_101_E_119_ap_vld(eop_101_E_119_ap_vld_sig) ,	// output  eop_101_E_119_ap_vld_sig
	.eop_101_D_119(eop_101_D_119_sig) ,	// output [31:0] eop_101_D_119_sig
	.eop_101_D_119_ap_vld(eop_101_D_119_ap_vld_sig) ,	// output  eop_101_D_119_ap_vld_sig
	.eop_101_C_119(eop_101_C_119_sig) ,	// output [31:0] eop_101_C_119_sig
	.eop_101_C_119_ap_vld(eop_101_C_119_ap_vld_sig) ,	// output  eop_101_C_119_ap_vld_sig
	.eop_101_B_119(eop_101_B_119_sig) ,	// output [31:0] eop_101_B_119_sig
	.eop_101_B_119_ap_vld(eop_101_B_119_ap_vld_sig) ,	// output  eop_101_B_119_ap_vld_sig
	.eop_101_A_119(eop_101_A_119_sig) ,	// output [31:0] eop_101_A_119_sig
	.eop_101_A_119_ap_vld(eop_101_A_119_ap_vld_sig) ,	// output  eop_101_A_119_ap_vld_sig
	.eop_101_temp_123(eop_101_temp_123_sig) ,	// output [31:0] eop_101_temp_123_sig
	.eop_101_temp_123_ap_vld(eop_101_temp_123_ap_vld_sig) ,	// output  eop_101_temp_123_ap_vld_sig
	.eop_101_E_123(eop_101_E_123_sig) ,	// output [31:0] eop_101_E_123_sig
	.eop_101_E_123_ap_vld(eop_101_E_123_ap_vld_sig) ,	// output  eop_101_E_123_ap_vld_sig
	.eop_101_D_123(eop_101_D_123_sig) ,	// output [31:0] eop_101_D_123_sig
	.eop_101_D_123_ap_vld(eop_101_D_123_ap_vld_sig) ,	// output  eop_101_D_123_ap_vld_sig
	.eop_101_C_123(eop_101_C_123_sig) ,	// output [31:0] eop_101_C_123_sig
	.eop_101_C_123_ap_vld(eop_101_C_123_ap_vld_sig) ,	// output  eop_101_C_123_ap_vld_sig
	.eop_101_B_123(eop_101_B_123_sig) ,	// output [31:0] eop_101_B_123_sig
	.eop_101_B_123_ap_vld(eop_101_B_123_ap_vld_sig) ,	// output  eop_101_B_123_ap_vld_sig
	.eop_101_A_123(eop_101_A_123_sig) ,	// output [31:0] eop_101_A_123_sig
	.eop_101_A_123_ap_vld(eop_101_A_123_ap_vld_sig) ,	// output  eop_101_A_123_ap_vld_sig
	.eop_101_temp_127(eop_101_temp_127_sig) ,	// output [31:0] eop_101_temp_127_sig
	.eop_101_temp_127_ap_vld(eop_101_temp_127_ap_vld_sig) ,	// output  eop_101_temp_127_ap_vld_sig
	.eop_101_E_127(eop_101_E_127_sig) ,	// output [31:0] eop_101_E_127_sig
	.eop_101_E_127_ap_vld(eop_101_E_127_ap_vld_sig) ,	// output  eop_101_E_127_ap_vld_sig
	.eop_101_D_127(eop_101_D_127_sig) ,	// output [31:0] eop_101_D_127_sig
	.eop_101_D_127_ap_vld(eop_101_D_127_ap_vld_sig) ,	// output  eop_101_D_127_ap_vld_sig
	.eop_101_C_127(eop_101_C_127_sig) ,	// output [31:0] eop_101_C_127_sig
	.eop_101_C_127_ap_vld(eop_101_C_127_ap_vld_sig) ,	// output  eop_101_C_127_ap_vld_sig
	.eop_101_B_127(eop_101_B_127_sig) ,	// output [31:0] eop_101_B_127_sig
	.eop_101_B_127_ap_vld(eop_101_B_127_ap_vld_sig) ,	// output  eop_101_B_127_ap_vld_sig
	.eop_101_A_127(eop_101_A_127_sig) ,	// output [31:0] eop_101_A_127_sig
	.eop_101_A_127_ap_vld(eop_101_A_127_ap_vld_sig) ,	// output  eop_101_A_127_ap_vld_sig
	.eop_101_temp_131(eop_101_temp_131_sig) ,	// output [31:0] eop_101_temp_131_sig
	.eop_101_temp_131_ap_vld(eop_101_temp_131_ap_vld_sig) ,	// output  eop_101_temp_131_ap_vld_sig
	.eop_101_E_131(eop_101_E_131_sig) ,	// output [31:0] eop_101_E_131_sig
	.eop_101_E_131_ap_vld(eop_101_E_131_ap_vld_sig) ,	// output  eop_101_E_131_ap_vld_sig
	.eop_101_D_131(eop_101_D_131_sig) ,	// output [31:0] eop_101_D_131_sig
	.eop_101_D_131_ap_vld(eop_101_D_131_ap_vld_sig) ,	// output  eop_101_D_131_ap_vld_sig
	.eop_101_C_131(eop_101_C_131_sig) ,	// output [31:0] eop_101_C_131_sig
	.eop_101_C_131_ap_vld(eop_101_C_131_ap_vld_sig) ,	// output  eop_101_C_131_ap_vld_sig
	.eop_101_B_131(eop_101_B_131_sig) ,	// output [31:0] eop_101_B_131_sig
	.eop_101_B_131_ap_vld(eop_101_B_131_ap_vld_sig) ,	// output  eop_101_B_131_ap_vld_sig
	.eop_101_A_131(eop_101_A_131_sig) ,	// output [31:0] eop_101_A_131_sig
	.eop_101_A_131_ap_vld(eop_101_A_131_ap_vld_sig) ,	// output  eop_101_A_131_ap_vld_sig
	.eop_182_lo_bit_count_185(eop_182_lo_bit_count_185_sig) ,	// output [31:0] eop_182_lo_bit_count_185_sig
	.eop_182_lo_bit_count_185_ap_vld(eop_182_lo_bit_count_185_ap_vld_sig) ,	// output  eop_182_lo_bit_count_185_ap_vld_sig
	.eop_183_hi_bit_count_186(eop_183_hi_bit_count_186_sig) ,	// output [31:0] eop_183_hi_bit_count_186_sig
	.eop_183_hi_bit_count_186_ap_vld(eop_183_hi_bit_count_186_ap_vld_sig) ,	// output  eop_183_hi_bit_count_186_ap_vld_sig
	.eop_181_count_187(eop_181_count_187_sig) ,	// output [31:0] eop_181_count_187_sig
	.eop_181_count_187_ap_vld(eop_181_count_187_ap_vld_sig) ,	// output  eop_181_count_187_ap_vld_sig
	.eop_181_count_188(eop_181_count_188_sig) ,	// output [31:0] eop_181_count_188_sig
	.eop_181_count_188_ap_vld(eop_181_count_188_ap_vld_sig) ,	// output  eop_181_count_188_ap_vld_sig
	.eop_57_m_59(eop_57_m_59_sig) ,	// output [31:0] eop_57_m_59_sig
	.eop_57_m_59_ap_vld(eop_57_m_59_ap_vld_sig) ,	// output  eop_57_m_59_ap_vld_sig
	.eop_55_uc_60(eop_55_uc_60_sig) ,	// output [31:0] eop_55_uc_60_sig
	.eop_55_uc_60_ap_vld(eop_55_uc_60_ap_vld_sig) ,	// output  eop_55_uc_60_ap_vld_sig
	.eop_53_e_62(eop_53_e_62_sig) ,	// output [31:0] eop_53_e_62_sig
	.eop_53_e_62_ap_vld(eop_53_e_62_ap_vld_sig) ,	// output  eop_53_e_62_ap_vld_sig
	.eop_57_m_66(eop_57_m_66_sig) ,	// output [31:0] eop_57_m_66_sig
	.eop_57_m_66_ap_vld(eop_57_m_66_ap_vld_sig) ,	// output  eop_57_m_66_ap_vld_sig
	.eop_44_main_result_51(eop_44_main_result_51_sig) ,	// output [31:0] eop_44_main_result_51_sig
	.eop_44_main_result_51_ap_vld(eop_44_main_result_51_ap_vld_sig) ,	// output  eop_44_main_result_51_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 48;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_44_main_result_45_ap_vld_sig;
valid_regs[1] <= eop_33_sha_info_count_lo_151_ap_vld_sig;
valid_regs[2] <= eop_33_sha_info_count_hi_152_ap_vld_sig;
valid_regs[3] <= eop_208_i_214_ap_vld_sig;
valid_regs[4] <= eop_33_sha_info_count_hi_162_ap_vld_sig;
valid_regs[5] <= eop_33_sha_info_count_lo_164_ap_vld_sig;
valid_regs[6] <= eop_33_sha_info_count_hi_165_ap_vld_sig;
valid_regs[7] <= eop_78_m_79_ap_vld_sig;
valid_regs[8] <= eop_77_tmp_85_ap_vld_sig;
valid_regs[9] <= eop_78_m_83_ap_vld_sig;
valid_regs[10] <= eop_101_A_111_ap_vld_sig;
valid_regs[11] <= eop_101_B_112_ap_vld_sig;
valid_regs[12] <= eop_101_C_113_ap_vld_sig;
valid_regs[13] <= eop_101_D_114_ap_vld_sig;
valid_regs[14] <= eop_101_E_115_ap_vld_sig;
valid_regs[15] <= eop_101_temp_119_ap_vld_sig;
valid_regs[16] <= eop_101_E_119_ap_vld_sig;
valid_regs[17] <= eop_101_D_119_ap_vld_sig;
valid_regs[18] <= eop_101_C_119_ap_vld_sig;
valid_regs[19] <= eop_101_B_119_ap_vld_sig;
valid_regs[20] <= eop_101_A_119_ap_vld_sig;
valid_regs[21] <= eop_101_temp_123_ap_vld_sig;
valid_regs[22] <= eop_101_E_123_ap_vld_sig;
valid_regs[23] <= eop_101_D_123_ap_vld_sig;
valid_regs[24] <= eop_101_C_123_ap_vld_sig;
valid_regs[25] <= eop_101_B_123_ap_vld_sig;
valid_regs[26] <= eop_101_A_123_ap_vld_sig;
valid_regs[27] <= eop_101_temp_127_ap_vld_sig;
valid_regs[28] <= eop_101_E_127_ap_vld_sig;
valid_regs[29] <= eop_101_D_127_ap_vld_sig;
valid_regs[30] <= eop_101_C_127_ap_vld_sig;
valid_regs[31] <= eop_101_B_127_ap_vld_sig;
valid_regs[32] <= eop_101_A_127_ap_vld_sig;
valid_regs[33] <= eop_101_temp_131_ap_vld_sig;
valid_regs[34] <= eop_101_E_131_ap_vld_sig;
valid_regs[35] <= eop_101_D_131_ap_vld_sig;
valid_regs[36] <= eop_101_C_131_ap_vld_sig;
valid_regs[37] <= eop_101_B_131_ap_vld_sig;
valid_regs[38] <= eop_101_A_131_ap_vld_sig;
valid_regs[39] <= eop_182_lo_bit_count_185_ap_vld_sig;
valid_regs[40] <= eop_183_hi_bit_count_186_ap_vld_sig;
valid_regs[41] <= eop_181_count_187_ap_vld_sig;
valid_regs[42] <= eop_181_count_188_ap_vld_sig;
valid_regs[43] <= eop_57_m_59_ap_vld_sig;
valid_regs[44] <= eop_55_uc_60_ap_vld_sig;
valid_regs[45] <= eop_53_e_62_ap_vld_sig;
valid_regs[46] <= eop_57_m_66_ap_vld_sig;
valid_regs[47] <= eop_44_main_result_51_ap_vld_sig;



end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
