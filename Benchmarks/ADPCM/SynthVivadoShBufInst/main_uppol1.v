// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.3
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module main_uppol1 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        al1,
        apl2,
        plt,
        plt1,
        bufIndex_i,
        bufIndex_o,
        bufIndex_o_ap_vld,
        TRACEBUFFER_address0,
        TRACEBUFFER_ce0,
        TRACEBUFFER_we0,
        TRACEBUFFER_d0,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 10'b1;
parameter    ap_ST_st2_fsm_1 = 10'b10;
parameter    ap_ST_st3_fsm_2 = 10'b100;
parameter    ap_ST_st4_fsm_3 = 10'b1000;
parameter    ap_ST_st5_fsm_4 = 10'b10000;
parameter    ap_ST_st6_fsm_5 = 10'b100000;
parameter    ap_ST_st7_fsm_6 = 10'b1000000;
parameter    ap_ST_st8_fsm_7 = 10'b10000000;
parameter    ap_ST_st9_fsm_8 = 10'b100000000;
parameter    ap_ST_st10_fsm_9 = 10'b1000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_5 = 32'b101;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_9 = 32'b1001;
parameter    ap_const_lv32_2DA = 32'b1011011010;
parameter    ap_const_lv32_2DF = 32'b1011011111;
parameter    ap_const_lv32_2E3 = 32'b1011100011;
parameter    ap_const_lv32_64 = 32'b1100100;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv32_1F = 32'b11111;
parameter    ap_const_lv25_C0 = 25'b11000000;
parameter    ap_const_lv25_1FFFF40 = 25'b1111111111111111101000000;
parameter    ap_const_lv32_3C00 = 32'b11110000000000;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [31:0] al1;
input  [31:0] apl2;
input  [31:0] plt;
input  [31:0] plt1;
input  [31:0] bufIndex_i;
output  [31:0] bufIndex_o;
output   bufIndex_o_ap_vld;
output  [6:0] TRACEBUFFER_address0;
output   TRACEBUFFER_ce0;
output   TRACEBUFFER_we0;
output  [31:0] TRACEBUFFER_d0;
output  [31:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[31:0] bufIndex_o;
reg bufIndex_o_ap_vld;
reg[6:0] TRACEBUFFER_address0;
reg TRACEBUFFER_ce0;
reg TRACEBUFFER_we0;
reg[31:0] TRACEBUFFER_d0;
reg[31:0] ap_return;
(* fsm_encoding = "none" *) reg   [9:0] ap_CS_fsm = 10'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_28;
wire   [31:0] p_i_fu_207_p3;
reg   [31:0] p_i_reg_447;
reg    ap_sig_cseq_ST_st5_fsm_4;
reg    ap_sig_bdd_62;
reg    ap_sig_cseq_ST_st6_fsm_5;
reg    ap_sig_bdd_71;
wire  signed [24:0] wd2_cast_fu_247_p1;
reg  signed [24:0] wd2_cast_reg_457;
wire   [0:0] tmp_20_fu_251_p3;
reg   [0:0] tmp_20_reg_463;
wire   [31:0] tmp_i4_fu_273_p2;
reg   [31:0] tmp_i4_reg_467;
wire   [24:0] apl1_fu_284_p2;
reg    ap_sig_cseq_ST_st7_fsm_6;
reg    ap_sig_bdd_86;
wire   [24:0] apl1_3_fu_299_p2;
wire   [31:0] bufIndex_load_i_fu_314_p2;
reg   [31:0] bufIndex_load_i_reg_483;
wire  signed [31:0] apl_cast_fu_320_p1;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_104;
wire   [31:0] wd3_fu_324_p2;
reg   [31:0] wd3_reg_494;
wire   [31:0] tmp_i2_18_fu_342_p2;
reg   [31:0] tmp_i2_18_reg_501;
wire   [0:0] tmp_8_fu_359_p2;
reg   [0:0] tmp_8_reg_507;
wire   [0:0] tmp_i5_fu_365_p2;
reg   [0:0] tmp_i5_reg_511;
wire   [31:0] apl1_4_fu_395_p2;
reg   [31:0] apl1_4_reg_516;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_124;
wire   [0:0] tmp_9_fu_400_p2;
reg   [0:0] tmp_9_reg_522;
wire   [31:0] grp_fu_182_p3;
reg   [24:0] apl_reg_138;
wire   [31:0] tmp_i3_19_fu_377_p2;
reg   [31:0] bufIndex_load_i1_reg_147;
reg   [31:0] apl1_1_phi_fu_160_p4;
reg   [31:0] apl1_1_reg_157;
reg   [31:0] apl1_2_phi_fu_170_p4;
reg   [31:0] apl1_2_reg_167;
reg    ap_sig_cseq_ST_st10_fsm_9;
reg    ap_sig_bdd_150;
wire  signed [63:0] tmp_11_i_fu_215_p1;
wire  signed [63:0] tmp_11_i5_fu_279_p1;
wire  signed [63:0] tmp_11_i1_fu_294_p1;
wire  signed [63:0] tmp_11_i3_fu_309_p1;
wire  signed [63:0] tmp_11_i4_fu_354_p1;
wire  signed [63:0] tmp_11_i6_fu_390_p1;
wire  signed [63:0] tmp_11_i7_fu_432_p1;
wire   [31:0] tmp_i5_20_fu_420_p2;
wire  signed [31:0] apl1_cast_fu_289_p1;
wire  signed [31:0] apl1_3_cast_fu_304_p1;
wire   [0:0] grp_fu_177_p2;
wire   [0:0] tmp_i_fu_201_p2;
wire   [31:0] tmp_19_fu_225_p2;
wire   [31:0] tmp_fu_231_p2;
wire   [23:0] tmp_15_fu_237_p4;
wire   [31:0] grp_fu_191_p2;
wire   [31:0] tmp_i_16_fu_220_p2;
wire   [0:0] tmp_i2_fu_259_p2;
wire   [31:0] p_i3_fu_265_p3;
wire   [0:0] tmp_i4_17_fu_330_p2;
wire   [31:0] p_i4_fu_335_p3;
wire   [31:0] p_i5_fu_371_p3;
wire   [0:0] tmp_i6_fu_406_p2;
wire   [31:0] p_i6_fu_412_p3;
wire    grp_fu_191_ce;
reg   [31:0] ap_return_preg = 32'b00000000000000000000000000000000;
reg   [9:0] ap_NS_fsm;


main_mul_32s_32s_32_6 #(
    .ID( 1 ),
    .NUM_STAGE( 6 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
main_mul_32s_32s_32_6_U76(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( plt1 ),
    .din1( plt ),
    .ce( grp_fu_191_ce ),
    .dout( grp_fu_191_p2 )
);



/// the current state (ap_CS_fsm) of the state machine. ///
always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

/// ap_return_preg assign process. ///
always @ (posedge ap_clk) begin : ap_ret_ap_return_preg
    if (ap_rst == 1'b1) begin
        ap_return_preg <= ap_const_lv32_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
            ap_return_preg <= apl1_2_phi_fu_170_p4;
        end
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & (ap_const_lv1_0 == tmp_8_fu_359_p2))) begin
        apl1_1_reg_157 <= apl_cast_fu_320_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507))) begin
        apl1_1_reg_157 <= wd3_reg_494;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & (ap_const_lv1_0 == tmp_9_fu_400_p2))) begin
        apl1_2_reg_167 <= apl1_1_phi_fu_160_p4;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) & ~(ap_const_lv1_0 == tmp_9_reg_522))) begin
        apl1_2_reg_167 <= apl1_4_reg_516;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        if (~(tmp_20_reg_463 == ap_const_lv1_0)) begin
            apl_reg_138 <= apl1_3_fu_299_p2;
        end else if ((tmp_20_reg_463 == ap_const_lv1_0)) begin
            apl_reg_138 <= apl1_fu_284_p2;
        end
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & (ap_const_lv1_0 == tmp_8_fu_359_p2))) begin
        bufIndex_load_i1_reg_147 <= tmp_i2_18_fu_342_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507))) begin
        bufIndex_load_i1_reg_147 <= tmp_i3_19_fu_377_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        apl1_4_reg_516 <= apl1_4_fu_395_p2;
        tmp_9_reg_522 <= tmp_9_fu_400_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        bufIndex_load_i_reg_483 <= bufIndex_load_i_fu_314_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        p_i_reg_447 <= p_i_fu_207_p3;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        tmp_20_reg_463 <= grp_fu_191_p2[ap_const_lv32_1F];
        tmp_i4_reg_467 <= tmp_i4_fu_273_p2;
        wd2_cast_reg_457 <= wd2_cast_fu_247_p1;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        tmp_8_reg_507 <= tmp_8_fu_359_p2;
        tmp_i2_18_reg_501 <= tmp_i2_18_fu_342_p2;
        wd3_reg_494 <= wd3_fu_324_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & ~(ap_const_lv1_0 == tmp_8_fu_359_p2))) begin
        tmp_i5_reg_511 <= tmp_i5_fu_365_p2;
    end
end

/// TRACEBUFFER_address0 assign process. ///
always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_20_fu_251_p3 or tmp_20_reg_463 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st10_fsm_9 or tmp_11_i_fu_215_p1 or tmp_11_i5_fu_279_p1 or tmp_11_i1_fu_294_p1 or tmp_11_i3_fu_309_p1 or tmp_11_i4_fu_354_p1 or tmp_11_i6_fu_390_p1 or tmp_11_i7_fu_432_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        TRACEBUFFER_address0 = tmp_11_i7_fu_432_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        TRACEBUFFER_address0 = tmp_11_i6_fu_390_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        TRACEBUFFER_address0 = tmp_11_i4_fu_354_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(tmp_20_reg_463 == ap_const_lv1_0))) begin
        TRACEBUFFER_address0 = tmp_11_i3_fu_309_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & (tmp_20_reg_463 == ap_const_lv1_0))) begin
        TRACEBUFFER_address0 = tmp_11_i1_fu_294_p1;
    end else if ((((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_20_fu_251_p3 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & ~(tmp_20_fu_251_p3 == ap_const_lv1_0)))) begin
        TRACEBUFFER_address0 = tmp_11_i5_fu_279_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_address0 = tmp_11_i_fu_215_p1;
    end else begin
        TRACEBUFFER_address0 = 'bx;
    end
end

/// TRACEBUFFER_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_20_fu_251_p3 or tmp_20_reg_463 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & (tmp_20_reg_463 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(tmp_20_reg_463 == ap_const_lv1_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_20_fu_251_p3 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & ~(tmp_20_fu_251_p3 == ap_const_lv1_0)))) begin
        TRACEBUFFER_ce0 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_ce0 = ap_const_logic_0;
    end
end

/// TRACEBUFFER_d0 assign process. ///
always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_20_fu_251_p3 or tmp_20_reg_463 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or wd3_fu_324_p2 or wd3_reg_494 or apl1_4_reg_516 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st10_fsm_9 or apl1_cast_fu_289_p1 or apl1_3_cast_fu_304_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        TRACEBUFFER_d0 = apl1_4_reg_516;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        TRACEBUFFER_d0 = wd3_reg_494;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        TRACEBUFFER_d0 = wd3_fu_324_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(tmp_20_reg_463 == ap_const_lv1_0))) begin
        TRACEBUFFER_d0 = apl1_3_cast_fu_304_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & (tmp_20_reg_463 == ap_const_lv1_0))) begin
        TRACEBUFFER_d0 = apl1_cast_fu_289_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & ~(tmp_20_fu_251_p3 == ap_const_lv1_0))) begin
        TRACEBUFFER_d0 = ap_const_lv32_2E3;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_20_fu_251_p3 == ap_const_lv1_0))) begin
        TRACEBUFFER_d0 = ap_const_lv32_2DF;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_d0 = ap_const_lv32_2DA;
    end else begin
        TRACEBUFFER_d0 = 'bx;
    end
end

/// TRACEBUFFER_we0 assign process. ///
always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_20_fu_251_p3 or tmp_20_reg_463 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or tmp_8_reg_507 or ap_sig_cseq_ST_st9_fsm_8 or tmp_9_reg_522 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & (tmp_20_reg_463 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(tmp_20_reg_463 == ap_const_lv1_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) | ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) & ~(ap_const_lv1_0 == tmp_9_reg_522)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_20_fu_251_p3 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & ~(tmp_20_fu_251_p3 == ap_const_lv1_0)))) begin
        TRACEBUFFER_we0 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_we0 = ap_const_logic_0;
    end
end

/// ap_done assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

/// ap_idle assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

/// ap_ready assign process. ///
always @ (ap_sig_cseq_ST_st10_fsm_9) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

/// ap_return assign process. ///
always @ (apl1_2_phi_fu_170_p4 or ap_sig_cseq_ST_st10_fsm_9 or ap_return_preg) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        ap_return = apl1_2_phi_fu_170_p4;
    end else begin
        ap_return = ap_return_preg;
    end
end

/// ap_sig_cseq_ST_st10_fsm_9 assign process. ///
always @ (ap_sig_bdd_150) begin
    if (ap_sig_bdd_150) begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st1_fsm_0 assign process. ///
always @ (ap_sig_bdd_28) begin
    if (ap_sig_bdd_28) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st5_fsm_4 assign process. ///
always @ (ap_sig_bdd_62) begin
    if (ap_sig_bdd_62) begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st6_fsm_5 assign process. ///
always @ (ap_sig_bdd_71) begin
    if (ap_sig_bdd_71) begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st7_fsm_6 assign process. ///
always @ (ap_sig_bdd_86) begin
    if (ap_sig_bdd_86) begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st8_fsm_7 assign process. ///
always @ (ap_sig_bdd_104) begin
    if (ap_sig_bdd_104) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st9_fsm_8 assign process. ///
always @ (ap_sig_bdd_124) begin
    if (ap_sig_bdd_124) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

/// apl1_1_phi_fu_160_p4 assign process. ///
always @ (wd3_reg_494 or tmp_8_reg_507 or ap_sig_cseq_ST_st9_fsm_8 or apl1_1_reg_157) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507))) begin
        apl1_1_phi_fu_160_p4 = wd3_reg_494;
    end else begin
        apl1_1_phi_fu_160_p4 = apl1_1_reg_157;
    end
end

/// apl1_2_phi_fu_170_p4 assign process. ///
always @ (apl1_4_reg_516 or tmp_9_reg_522 or apl1_2_reg_167 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) & ~(ap_const_lv1_0 == tmp_9_reg_522))) begin
        apl1_2_phi_fu_170_p4 = apl1_4_reg_516;
    end else begin
        apl1_2_phi_fu_170_p4 = apl1_2_reg_167;
    end
end

/// bufIndex_o assign process. ///
always @ (bufIndex_i or ap_sig_cseq_ST_st8_fsm_7 or tmp_i2_18_fu_342_p2 or tmp_8_reg_507 or ap_sig_cseq_ST_st9_fsm_8 or tmp_9_reg_522 or tmp_i3_19_fu_377_p2 or ap_sig_cseq_ST_st10_fsm_9 or tmp_i5_20_fu_420_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) & ~(ap_const_lv1_0 == tmp_9_reg_522))) begin
        bufIndex_o = tmp_i5_20_fu_420_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507))) begin
        bufIndex_o = tmp_i3_19_fu_377_p2;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        bufIndex_o = tmp_i2_18_fu_342_p2;
    end else begin
        bufIndex_o = bufIndex_i;
    end
end

/// bufIndex_o_ap_vld assign process. ///
always @ (ap_sig_cseq_ST_st8_fsm_7 or tmp_8_reg_507 or ap_sig_cseq_ST_st9_fsm_8 or tmp_9_reg_522 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) | ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) & ~(ap_const_lv1_0 == tmp_8_reg_507)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) & ~(ap_const_lv1_0 == tmp_9_reg_522)))) begin
        bufIndex_o_ap_vld = ap_const_logic_1;
    end else begin
        bufIndex_o_ap_vld = ap_const_logic_0;
    end
end
/// the next state (ap_NS_fsm) of the state machine. ///
always @ (ap_start or ap_CS_fsm) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            ap_NS_fsm = ap_ST_st3_fsm_2;
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            ap_NS_fsm = ap_ST_st5_fsm_4;
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


/// ap_sig_bdd_104 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_104 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end

/// ap_sig_bdd_124 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_124 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end

/// ap_sig_bdd_150 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_150 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_9]);
end

/// ap_sig_bdd_28 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_28 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end

/// ap_sig_bdd_62 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_62 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end

/// ap_sig_bdd_71 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_71 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_5]);
end

/// ap_sig_bdd_86 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_86 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_6]);
end
assign apl1_3_cast_fu_304_p1 = $signed(apl1_3_fu_299_p2);
assign apl1_3_fu_299_p2 = ($signed(wd2_cast_reg_457) + $signed(ap_const_lv25_1FFFF40));
assign apl1_4_fu_395_p2 = (ap_const_lv32_0 - wd3_reg_494);
assign apl1_cast_fu_289_p1 = $signed(apl1_fu_284_p2);
assign apl1_fu_284_p2 = ($signed(wd2_cast_reg_457) + $signed(ap_const_lv25_C0));
assign apl_cast_fu_320_p1 = $signed(apl_reg_138);
assign bufIndex_load_i_fu_314_p2 = (grp_fu_182_p3 + ap_const_lv32_1);
assign grp_fu_177_p2 = (tmp_i4_reg_467 == ap_const_lv32_64? 1'b1: 1'b0);
assign grp_fu_182_p3 = ((grp_fu_177_p2[0:0] === 1'b1) ? ap_const_lv32_0 : tmp_i4_reg_467);
assign grp_fu_191_ce = ap_const_logic_1;
assign p_i3_fu_265_p3 = ((tmp_i2_fu_259_p2[0:0] === 1'b1) ? ap_const_lv32_0 : tmp_i_16_fu_220_p2);
assign p_i4_fu_335_p3 = ((tmp_i4_17_fu_330_p2[0:0] === 1'b1) ? ap_const_lv32_0 : bufIndex_load_i_reg_483);
assign p_i5_fu_371_p3 = ((tmp_i5_reg_511[0:0] === 1'b1) ? ap_const_lv32_0 : tmp_i2_18_reg_501);
assign p_i6_fu_412_p3 = ((tmp_i6_fu_406_p2[0:0] === 1'b1) ? ap_const_lv32_0 : bufIndex_load_i1_reg_147);
assign p_i_fu_207_p3 = ((tmp_i_fu_201_p2[0:0] === 1'b1) ? ap_const_lv32_0 : bufIndex_i);
assign tmp_11_i1_fu_294_p1 = $signed(grp_fu_182_p3);
assign tmp_11_i3_fu_309_p1 = $signed(grp_fu_182_p3);
assign tmp_11_i4_fu_354_p1 = $signed(p_i4_fu_335_p3);
assign tmp_11_i5_fu_279_p1 = $signed(p_i3_fu_265_p3);
assign tmp_11_i6_fu_390_p1 = $signed(p_i5_fu_371_p3);
assign tmp_11_i7_fu_432_p1 = $signed(p_i6_fu_412_p3);
assign tmp_11_i_fu_215_p1 = $signed(p_i_fu_207_p3);
assign tmp_15_fu_237_p4 = {{tmp_fu_231_p2[ap_const_lv32_1F : ap_const_lv32_8]}};
assign tmp_19_fu_225_p2 = al1 << ap_const_lv32_8;
assign tmp_20_fu_251_p3 = grp_fu_191_p2[ap_const_lv32_1F];
assign tmp_8_fu_359_p2 = ($signed(apl_cast_fu_320_p1) > $signed(wd3_fu_324_p2)? 1'b1: 1'b0);
assign tmp_9_fu_400_p2 = ($signed(apl1_1_phi_fu_160_p4) < $signed(apl1_4_fu_395_p2)? 1'b1: 1'b0);
assign tmp_fu_231_p2 = (tmp_19_fu_225_p2 - al1);
assign tmp_i2_18_fu_342_p2 = (p_i4_fu_335_p3 + ap_const_lv32_1);
assign tmp_i2_fu_259_p2 = (tmp_i_16_fu_220_p2 == ap_const_lv32_64? 1'b1: 1'b0);
assign tmp_i3_19_fu_377_p2 = (p_i5_fu_371_p3 + ap_const_lv32_1);
assign tmp_i4_17_fu_330_p2 = (bufIndex_load_i_reg_483 == ap_const_lv32_64? 1'b1: 1'b0);
assign tmp_i4_fu_273_p2 = (ap_const_lv32_1 + p_i3_fu_265_p3);
assign tmp_i5_20_fu_420_p2 = (p_i6_fu_412_p3 + ap_const_lv32_1);
assign tmp_i5_fu_365_p2 = (tmp_i2_18_fu_342_p2 == ap_const_lv32_64? 1'b1: 1'b0);
assign tmp_i6_fu_406_p2 = (bufIndex_load_i1_reg_147 == ap_const_lv32_64? 1'b1: 1'b0);
assign tmp_i_16_fu_220_p2 = (ap_const_lv32_1 + p_i_reg_447);
assign tmp_i_fu_201_p2 = (bufIndex_i == ap_const_lv32_64? 1'b1: 1'b0);
assign wd2_cast_fu_247_p1 = $signed(tmp_15_fu_237_p4);
assign wd3_fu_324_p2 = (ap_const_lv32_3C00 - apl2);


endmodule //main_uppol1

