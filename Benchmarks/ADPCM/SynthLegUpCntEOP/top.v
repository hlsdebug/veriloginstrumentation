// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd250);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW[0])
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
main main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_873_main_result_875(eop_873_main_result_875_sig) ,	// output [31:0] eop_873_main_result_875_sig
	.eop_873_main_result_875_ap_vld(eop_873_main_result_875_ap_vld_sig) ,	// output  eop_873_main_result_875_ap_vld_sig
	.eop_202_dec_detl_544(eop_202_dec_detl_544_sig) ,	// output [31:0] eop_202_dec_detl_544_sig
	.eop_202_dec_detl_544_ap_vld(eop_202_dec_detl_544_ap_vld_sig) ,	// output  eop_202_dec_detl_544_ap_vld_sig
	.eop_154_detl_544(eop_154_detl_544_sig) ,	// output [31:0] eop_154_detl_544_sig
	.eop_154_detl_544_ap_vld(eop_154_detl_544_ap_vld_sig) ,	// output  eop_154_detl_544_ap_vld_sig
	.eop_202_dec_deth_545(eop_202_dec_deth_545_sig) ,	// output [31:0] eop_202_dec_deth_545_sig
	.eop_202_dec_deth_545_ap_vld(eop_202_dec_deth_545_ap_vld_sig) ,	// output  eop_202_dec_deth_545_ap_vld_sig
	.eop_175_deth_545(eop_175_deth_545_sig) ,	// output [31:0] eop_175_deth_545_sig
	.eop_175_deth_545_ap_vld(eop_175_deth_545_ap_vld_sig) ,	// output  eop_175_deth_545_ap_vld_sig
	.eop_144_rlt2_546(eop_144_rlt2_546_sig) ,	// output [31:0] eop_144_rlt2_546_sig
	.eop_144_rlt2_546_ap_vld(eop_144_rlt2_546_ap_vld_sig) ,	// output  eop_144_rlt2_546_ap_vld_sig
	.eop_144_rlt1_546(eop_144_rlt1_546_sig) ,	// output [31:0] eop_144_rlt1_546_sig
	.eop_144_rlt1_546_ap_vld(eop_144_rlt1_546_ap_vld_sig) ,	// output  eop_144_rlt1_546_ap_vld_sig
	.eop_142_plt2_546(eop_142_plt2_546_sig) ,	// output [31:0] eop_142_plt2_546_sig
	.eop_142_plt2_546_ap_vld(eop_142_plt2_546_ap_vld_sig) ,	// output  eop_142_plt2_546_ap_vld_sig
	.eop_142_plt1_546(eop_142_plt1_546_sig) ,	// output [31:0] eop_142_plt1_546_sig
	.eop_142_plt1_546_ap_vld(eop_142_plt1_546_ap_vld_sig) ,	// output  eop_142_plt1_546_ap_vld_sig
	.eop_141_al2_546(eop_141_al2_546_sig) ,	// output [31:0] eop_141_al2_546_sig
	.eop_141_al2_546_ap_vld(eop_141_al2_546_ap_vld_sig) ,	// output  eop_141_al2_546_ap_vld_sig
	.eop_141_al1_546(eop_141_al1_546_sig) ,	// output [31:0] eop_141_al1_546_sig
	.eop_141_al1_546_ap_vld(eop_141_al1_546_ap_vld_sig) ,	// output  eop_141_al1_546_ap_vld_sig
	.eop_140_nbl_546(eop_140_nbl_546_sig) ,	// output [31:0] eop_140_nbl_546_sig
	.eop_140_nbl_546_ap_vld(eop_140_nbl_546_ap_vld_sig) ,	// output  eop_140_nbl_546_ap_vld_sig
	.eop_198_rh2_547(eop_198_rh2_547_sig) ,	// output [31:0] eop_198_rh2_547_sig
	.eop_198_rh2_547_ap_vld(eop_198_rh2_547_ap_vld_sig) ,	// output  eop_198_rh2_547_ap_vld_sig
	.eop_198_rh1_547(eop_198_rh1_547_sig) ,	// output [31:0] eop_198_rh1_547_sig
	.eop_198_rh1_547_ap_vld(eop_198_rh1_547_ap_vld_sig) ,	// output  eop_198_rh1_547_ap_vld_sig
	.eop_197_ph2_547(eop_197_ph2_547_sig) ,	// output [31:0] eop_197_ph2_547_sig
	.eop_197_ph2_547_ap_vld(eop_197_ph2_547_ap_vld_sig) ,	// output  eop_197_ph2_547_ap_vld_sig
	.eop_197_ph1_547(eop_197_ph1_547_sig) ,	// output [31:0] eop_197_ph1_547_sig
	.eop_197_ph1_547_ap_vld(eop_197_ph1_547_ap_vld_sig) ,	// output  eop_197_ph1_547_ap_vld_sig
	.eop_196_ah2_547(eop_196_ah2_547_sig) ,	// output [31:0] eop_196_ah2_547_sig
	.eop_196_ah2_547_ap_vld(eop_196_ah2_547_ap_vld_sig) ,	// output  eop_196_ah2_547_ap_vld_sig
	.eop_196_ah1_547(eop_196_ah1_547_sig) ,	// output [31:0] eop_196_ah1_547_sig
	.eop_196_ah1_547_ap_vld(eop_196_ah1_547_ap_vld_sig) ,	// output  eop_196_ah1_547_ap_vld_sig
	.eop_189_nbh_547(eop_189_nbh_547_sig) ,	// output [31:0] eop_189_nbh_547_sig
	.eop_189_nbh_547_ap_vld(eop_189_nbh_547_ap_vld_sig) ,	// output  eop_189_nbh_547_ap_vld_sig
	.eop_210_dec_rlt2_548(eop_210_dec_rlt2_548_sig) ,	// output [31:0] eop_210_dec_rlt2_548_sig
	.eop_210_dec_rlt2_548_ap_vld(eop_210_dec_rlt2_548_ap_vld_sig) ,	// output  eop_210_dec_rlt2_548_ap_vld_sig
	.eop_210_dec_rlt1_548(eop_210_dec_rlt1_548_sig) ,	// output [31:0] eop_210_dec_rlt1_548_sig
	.eop_210_dec_rlt1_548_ap_vld(eop_210_dec_rlt1_548_ap_vld_sig) ,	// output  eop_210_dec_rlt1_548_ap_vld_sig
	.eop_208_dec_plt2_548(eop_208_dec_plt2_548_sig) ,	// output [31:0] eop_208_dec_plt2_548_sig
	.eop_208_dec_plt2_548_ap_vld(eop_208_dec_plt2_548_ap_vld_sig) ,	// output  eop_208_dec_plt2_548_ap_vld_sig
	.eop_208_dec_plt1_548(eop_208_dec_plt1_548_sig) ,	// output [31:0] eop_208_dec_plt1_548_sig
	.eop_208_dec_plt1_548_ap_vld(eop_208_dec_plt1_548_ap_vld_sig) ,	// output  eop_208_dec_plt1_548_ap_vld_sig
	.eop_211_dec_al2_548(eop_211_dec_al2_548_sig) ,	// output [31:0] eop_211_dec_al2_548_sig
	.eop_211_dec_al2_548_ap_vld(eop_211_dec_al2_548_ap_vld_sig) ,	// output  eop_211_dec_al2_548_ap_vld_sig
	.eop_211_dec_al1_548(eop_211_dec_al1_548_sig) ,	// output [31:0] eop_211_dec_al1_548_sig
	.eop_211_dec_al1_548_ap_vld(eop_211_dec_al1_548_ap_vld_sig) ,	// output  eop_211_dec_al1_548_ap_vld_sig
	.eop_213_dec_nbl_548(eop_213_dec_nbl_548_sig) ,	// output [31:0] eop_213_dec_nbl_548_sig
	.eop_213_dec_nbl_548_ap_vld(eop_213_dec_nbl_548_ap_vld_sig) ,	// output  eop_213_dec_nbl_548_ap_vld_sig
	.eop_222_dec_rh2_549(eop_222_dec_rh2_549_sig) ,	// output [31:0] eop_222_dec_rh2_549_sig
	.eop_222_dec_rh2_549_ap_vld(eop_222_dec_rh2_549_ap_vld_sig) ,	// output  eop_222_dec_rh2_549_ap_vld_sig
	.eop_222_dec_rh1_549(eop_222_dec_rh1_549_sig) ,	// output [31:0] eop_222_dec_rh1_549_sig
	.eop_222_dec_rh1_549_ap_vld(eop_222_dec_rh1_549_ap_vld_sig) ,	// output  eop_222_dec_rh1_549_ap_vld_sig
	.eop_228_dec_ph2_549(eop_228_dec_ph2_549_sig) ,	// output [31:0] eop_228_dec_ph2_549_sig
	.eop_228_dec_ph2_549_ap_vld(eop_228_dec_ph2_549_ap_vld_sig) ,	// output  eop_228_dec_ph2_549_ap_vld_sig
	.eop_228_dec_ph1_549(eop_228_dec_ph1_549_sig) ,	// output [31:0] eop_228_dec_ph1_549_sig
	.eop_228_dec_ph1_549_ap_vld(eop_228_dec_ph1_549_ap_vld_sig) ,	// output  eop_228_dec_ph1_549_ap_vld_sig
	.eop_223_dec_ah2_549(eop_223_dec_ah2_549_sig) ,	// output [31:0] eop_223_dec_ah2_549_sig
	.eop_223_dec_ah2_549_ap_vld(eop_223_dec_ah2_549_ap_vld_sig) ,	// output  eop_223_dec_ah2_549_ap_vld_sig
	.eop_223_dec_ah1_549(eop_223_dec_ah1_549_sig) ,	// output [31:0] eop_223_dec_ah1_549_sig
	.eop_223_dec_ah1_549_ap_vld(eop_223_dec_ah1_549_ap_vld_sig) ,	// output  eop_223_dec_ah1_549_ap_vld_sig
	.eop_213_dec_nbh_549(eop_213_dec_nbh_549_sig) ,	// output [31:0] eop_213_dec_nbh_549_sig
	.eop_213_dec_nbh_549_ap_vld(eop_213_dec_nbh_549_ap_vld_sig) ,	// output  eop_213_dec_nbh_549_ap_vld_sig
	.eop_253_xa_259(eop_253_xa_259_sig) ,	// output [31:0] eop_253_xa_259_sig
	.eop_253_xa_259_ap_vld(eop_253_xa_259_ap_vld_sig) ,	// output  eop_253_xa_259_ap_vld_sig
	.eop_253_xb_260(eop_253_xb_260_sig) ,	// output [31:0] eop_253_xb_260_sig
	.eop_253_xb_260_ap_vld(eop_253_xb_260_ap_vld_sig) ,	// output  eop_253_xb_260_ap_vld_sig
	.eop_253_xa_264(eop_253_xa_264_sig) ,	// output [31:0] eop_253_xa_264_sig
	.eop_253_xa_264_ap_vld(eop_253_xa_264_ap_vld_sig) ,	// output  eop_253_xa_264_ap_vld_sig
	.eop_253_xb_265(eop_253_xb_265_sig) ,	// output [31:0] eop_253_xb_265_sig
	.eop_253_xb_265_ap_vld(eop_253_xb_265_ap_vld_sig) ,	// output  eop_253_xb_265_ap_vld_sig
	.eop_253_xa_268(eop_253_xa_268_sig) ,	// output [31:0] eop_253_xa_268_sig
	.eop_253_xa_268_ap_vld(eop_253_xa_268_ap_vld_sig) ,	// output  eop_253_xa_268_ap_vld_sig
	.eop_253_xb_269(eop_253_xb_269_sig) ,	// output [31:0] eop_253_xb_269_sig
	.eop_253_xb_269_ap_vld(eop_253_xb_269_ap_vld_sig) ,	// output  eop_253_xb_269_ap_vld_sig
	.eop_93_xl_279(eop_93_xl_279_sig) ,	// output [31:0] eop_93_xl_279_sig
	.eop_93_xl_279_ap_vld(eop_93_xl_279_ap_vld_sig) ,	// output  eop_93_xl_279_ap_vld_sig
	.eop_93_xh_280(eop_93_xh_280_sig) ,	// output [31:0] eop_93_xh_280_sig
	.eop_93_xh_280_ap_vld(eop_93_xh_280_ap_vld_sig) ,	// output  eop_93_xh_280_ap_vld_sig
	.eop_584_zl_585(eop_584_zl_585_sig) ,	// output [31:0] eop_584_zl_585_sig
	.eop_584_zl_585_ap_vld(eop_584_zl_585_ap_vld_sig) ,	// output  eop_584_zl_585_ap_vld_sig
	.eop_584_zl_587(eop_584_zl_587_sig) ,	// output [31:0] eop_584_zl_587_sig
	.eop_584_zl_587_ap_vld(eop_584_zl_587_ap_vld_sig) ,	// output  eop_584_zl_587_ap_vld_sig
	.eop_105_szl_287(eop_105_szl_287_sig) ,	// output [31:0] eop_105_szl_287_sig
	.eop_105_szl_287_ap_vld(eop_105_szl_287_ap_vld_sig) ,	// output  eop_105_szl_287_ap_vld_sig
	.eop_598_pl_599(eop_598_pl_599_sig) ,	// output [31:0] eop_598_pl_599_sig
	.eop_598_pl_599_ap_vld(eop_598_pl_599_ap_vld_sig) ,	// output  eop_598_pl_599_ap_vld_sig
	.eop_598_pl_600(eop_598_pl_600_sig) ,	// output [31:0] eop_598_pl_600_sig
	.eop_598_pl_600_ap_vld(eop_598_pl_600_ap_vld_sig) ,	// output  eop_598_pl_600_ap_vld_sig
	.eop_598_pl2_601(eop_598_pl2_601_sig) ,	// output [31:0] eop_598_pl2_601_sig
	.eop_598_pl2_601_ap_vld(eop_598_pl2_601_ap_vld_sig) ,	// output  eop_598_pl2_601_ap_vld_sig
	.eop_598_pl_602(eop_598_pl_602_sig) ,	// output [31:0] eop_598_pl_602_sig
	.eop_598_pl_602_ap_vld(eop_598_pl_602_ap_vld_sig) ,	// output  eop_598_pl_602_ap_vld_sig
	.eop_105_spl_290(eop_105_spl_290_sig) ,	// output [31:0] eop_105_spl_290_sig
	.eop_105_spl_290_ap_vld(eop_105_spl_290_ap_vld_sig) ,	// output  eop_105_spl_290_ap_vld_sig
	.eop_105_sl_293(eop_105_sl_293_sig) ,	// output [31:0] eop_105_sl_293_sig
	.eop_105_sl_293_ap_vld(eop_105_sl_293_ap_vld_sig) ,	// output  eop_105_sl_293_ap_vld_sig
	.eop_105_el_294(eop_105_el_294_sig) ,	// output [31:0] eop_105_el_294_sig
	.eop_105_el_294_ap_vld(eop_105_el_294_ap_vld_sig) ,	// output  eop_105_el_294_ap_vld_sig
	.eop_238_m_241(eop_238_m_241_sig) ,	// output [31:0] eop_238_m_241_sig
	.eop_238_m_241_ap_vld(eop_238_m_241_ap_vld_sig) ,	// output  eop_238_m_241_ap_vld_sig
	.eop_238_m_243(eop_238_m_243_sig) ,	// output [31:0] eop_238_m_243_sig
	.eop_238_m_243_ap_vld(eop_238_m_243_ap_vld_sig) ,	// output  eop_238_m_243_ap_vld_sig
	.eop_611_wd_614(eop_611_wd_614_sig) ,	// output [31:0] eop_611_wd_614_sig
	.eop_611_wd_614_ap_vld(eop_611_wd_614_ap_vld_sig) ,	// output  eop_611_wd_614_ap_vld_sig
	.eop_611_decis_618(eop_611_decis_618_sig) ,	// output [31:0] eop_611_decis_618_sig
	.eop_611_decis_618_ap_vld(eop_611_decis_618_ap_vld_sig) ,	// output  eop_611_decis_618_ap_vld_sig
	.eop_610_ril_624(eop_610_ril_624_sig) ,	// output [31:0] eop_610_ril_624_sig
	.eop_610_ril_624_ap_vld(eop_610_ril_624_ap_vld_sig) ,	// output  eop_610_ril_624_ap_vld_sig
	.eop_610_ril_626(eop_610_ril_626_sig) ,	// output [31:0] eop_610_ril_626_sig
	.eop_610_ril_626_ap_vld(eop_610_ril_626_ap_vld_sig) ,	// output  eop_610_ril_626_ap_vld_sig
	.eop_105_il_297(eop_105_il_297_sig) ,	// output [31:0] eop_105_il_297_sig
	.eop_105_il_297_ap_vld(eop_105_il_297_ap_vld_sig) ,	// output  eop_105_il_297_ap_vld_sig
	.eop_143_dlt_301(eop_143_dlt_301_sig) ,	// output [31:0] eop_143_dlt_301_sig
	.eop_143_dlt_301_ap_vld(eop_143_dlt_301_ap_vld_sig) ,	// output  eop_143_dlt_301_ap_vld_sig
	.eop_636_wd_637(eop_636_wd_637_sig) ,	// output [31:0] eop_636_wd_637_sig
	.eop_636_wd_637_ap_vld(eop_636_wd_637_ap_vld_sig) ,	// output  eop_636_wd_637_ap_vld_sig
	.eop_634_nbl_638(eop_634_nbl_638_sig) ,	// output [31:0] eop_634_nbl_638_sig
	.eop_634_nbl_638_ap_vld(eop_634_nbl_638_ap_vld_sig) ,	// output  eop_634_nbl_638_ap_vld_sig
	.eop_634_nbl_640(eop_634_nbl_640_sig) ,	// output [31:0] eop_634_nbl_640_sig
	.eop_634_nbl_640_ap_vld(eop_634_nbl_640_ap_vld_sig) ,	// output  eop_634_nbl_640_ap_vld_sig
	.eop_634_nbl_642(eop_634_nbl_642_sig) ,	// output [31:0] eop_634_nbl_642_sig
	.eop_634_nbl_642_ap_vld(eop_634_nbl_642_ap_vld_sig) ,	// output  eop_634_nbl_642_ap_vld_sig
	.eop_140_nbl_304(eop_140_nbl_304_sig) ,	// output [31:0] eop_140_nbl_304_sig
	.eop_140_nbl_304_ap_vld(eop_140_nbl_304_ap_vld_sig) ,	// output  eop_140_nbl_304_ap_vld_sig
	.eop_653_wd1_653(eop_653_wd1_653_sig) ,	// output [31:0] eop_653_wd1_653_sig
	.eop_653_wd1_653_ap_vld(eop_653_wd1_653_ap_vld_sig) ,	// output  eop_653_wd1_653_ap_vld_sig
	.eop_654_wd2_654(eop_654_wd2_654_sig) ,	// output [31:0] eop_654_wd2_654_sig
	.eop_654_wd2_654_ap_vld(eop_654_wd2_654_ap_vld_sig) ,	// output  eop_654_wd2_654_ap_vld_sig
	.eop_655_wd3_655(eop_655_wd3_655_sig) ,	// output [31:0] eop_655_wd3_655_sig
	.eop_655_wd3_655_ap_vld(eop_655_wd3_655_ap_vld_sig) ,	// output  eop_655_wd3_655_ap_vld_sig
	.eop_154_detl_308(eop_154_detl_308_sig) ,	// output [31:0] eop_154_detl_308_sig
	.eop_154_detl_308_ap_vld(eop_154_detl_308_ap_vld_sig) ,	// output  eop_154_detl_308_ap_vld_sig
	.eop_142_plt_311(eop_142_plt_311_sig) ,	// output [31:0] eop_142_plt_311_sig
	.eop_142_plt_311_ap_vld(eop_142_plt_311_ap_vld_sig) ,	// output  eop_142_plt_311_ap_vld_sig
	.eop_665_wd2_679(eop_665_wd2_679_sig) ,	// output [31:0] eop_665_wd2_679_sig
	.eop_665_wd2_679_ap_vld(eop_665_wd2_679_ap_vld_sig) ,	// output  eop_665_wd2_679_ap_vld_sig
	.eop_665_wd2_681(eop_665_wd2_681_sig) ,	// output [31:0] eop_665_wd2_681_sig
	.eop_665_wd2_681_ap_vld(eop_665_wd2_681_ap_vld_sig) ,	// output  eop_665_wd2_681_ap_vld_sig
	.eop_665_wd3_682(eop_665_wd3_682_sig) ,	// output [31:0] eop_665_wd3_682_sig
	.eop_665_wd3_682_ap_vld(eop_665_wd3_682_ap_vld_sig) ,	// output  eop_665_wd3_682_ap_vld_sig
	.eop_700_wd2_702(eop_700_wd2_702_sig) ,	// output [31:0] eop_700_wd2_702_sig
	.eop_700_wd2_702_ap_vld(eop_700_wd2_702_ap_vld_sig) ,	// output  eop_700_wd2_702_ap_vld_sig
	.eop_700_wd2_704(eop_700_wd2_704_sig) ,	// output [31:0] eop_700_wd2_704_sig
	.eop_700_wd2_704_ap_vld(eop_700_wd2_704_ap_vld_sig) ,	// output  eop_700_wd2_704_ap_vld_sig
	.eop_700_wd2_705(eop_700_wd2_705_sig) ,	// output [31:0] eop_700_wd2_705_sig
	.eop_700_wd2_705_ap_vld(eop_700_wd2_705_ap_vld_sig) ,	// output  eop_700_wd2_705_ap_vld_sig
	.eop_700_wd4_708(eop_700_wd4_708_sig) ,	// output [31:0] eop_700_wd4_708_sig
	.eop_700_wd4_708_ap_vld(eop_700_wd4_708_ap_vld_sig) ,	// output  eop_700_wd4_708_ap_vld_sig
	.eop_700_wd4_712(eop_700_wd4_712_sig) ,	// output [31:0] eop_700_wd4_712_sig
	.eop_700_wd4_712_ap_vld(eop_700_wd4_712_ap_vld_sig) ,	// output  eop_700_wd4_712_ap_vld_sig
	.eop_701_apl2_714(eop_701_apl2_714_sig) ,	// output [31:0] eop_701_apl2_714_sig
	.eop_701_apl2_714_ap_vld(eop_701_apl2_714_ap_vld_sig) ,	// output  eop_701_apl2_714_ap_vld_sig
	.eop_701_apl2_718(eop_701_apl2_718_sig) ,	// output [31:0] eop_701_apl2_718_sig
	.eop_701_apl2_718_ap_vld(eop_701_apl2_718_ap_vld_sig) ,	// output  eop_701_apl2_718_ap_vld_sig
	.eop_701_apl2_720(eop_701_apl2_720_sig) ,	// output [31:0] eop_701_apl2_720_sig
	.eop_701_apl2_720_ap_vld(eop_701_apl2_720_ap_vld_sig) ,	// output  eop_701_apl2_720_ap_vld_sig
	.eop_141_al2_321(eop_141_al2_321_sig) ,	// output [31:0] eop_141_al2_321_sig
	.eop_141_al2_321_ap_vld(eop_141_al2_321_ap_vld_sig) ,	// output  eop_141_al2_321_ap_vld_sig
	.eop_730_wd2_732(eop_730_wd2_732_sig) ,	// output [31:0] eop_730_wd2_732_sig
	.eop_730_wd2_732_ap_vld(eop_730_wd2_732_ap_vld_sig) ,	// output  eop_730_wd2_732_ap_vld_sig
	.eop_731_apl1_735(eop_731_apl1_735_sig) ,	// output [31:0] eop_731_apl1_735_sig
	.eop_731_apl1_735_ap_vld(eop_731_apl1_735_ap_vld_sig) ,	// output  eop_731_apl1_735_ap_vld_sig
	.eop_731_apl1_739(eop_731_apl1_739_sig) ,	// output [31:0] eop_731_apl1_739_sig
	.eop_731_apl1_739_ap_vld(eop_731_apl1_739_ap_vld_sig) ,	// output  eop_731_apl1_739_ap_vld_sig
	.eop_731_wd3_742(eop_731_wd3_742_sig) ,	// output [31:0] eop_731_wd3_742_sig
	.eop_731_wd3_742_ap_vld(eop_731_wd3_742_ap_vld_sig) ,	// output  eop_731_wd3_742_ap_vld_sig
	.eop_731_apl1_744(eop_731_apl1_744_sig) ,	// output [31:0] eop_731_apl1_744_sig
	.eop_731_apl1_744_ap_vld(eop_731_apl1_744_ap_vld_sig) ,	// output  eop_731_apl1_744_ap_vld_sig
	.eop_731_apl1_746(eop_731_apl1_746_sig) ,	// output [31:0] eop_731_apl1_746_sig
	.eop_731_apl1_746_ap_vld(eop_731_apl1_746_ap_vld_sig) ,	// output  eop_731_apl1_746_ap_vld_sig
	.eop_141_al1_325(eop_141_al1_325_sig) ,	// output [31:0] eop_141_al1_325_sig
	.eop_141_al1_325_ap_vld(eop_141_al1_325_ap_vld_sig) ,	// output  eop_141_al1_325_ap_vld_sig
	.eop_144_rlt_328(eop_144_rlt_328_sig) ,	// output [31:0] eop_144_rlt_328_sig
	.eop_144_rlt_328_ap_vld(eop_144_rlt_328_ap_vld_sig) ,	// output  eop_144_rlt_328_ap_vld_sig
	.eop_144_rlt2_331(eop_144_rlt2_331_sig) ,	// output [31:0] eop_144_rlt2_331_sig
	.eop_144_rlt2_331_ap_vld(eop_144_rlt2_331_ap_vld_sig) ,	// output  eop_144_rlt2_331_ap_vld_sig
	.eop_144_rlt1_332(eop_144_rlt1_332_sig) ,	// output [31:0] eop_144_rlt1_332_sig
	.eop_144_rlt1_332_ap_vld(eop_144_rlt1_332_ap_vld_sig) ,	// output  eop_144_rlt1_332_ap_vld_sig
	.eop_142_plt2_333(eop_142_plt2_333_sig) ,	// output [31:0] eop_142_plt2_333_sig
	.eop_142_plt2_333_ap_vld(eop_142_plt2_333_ap_vld_sig) ,	// output  eop_142_plt2_333_ap_vld_sig
	.eop_142_plt1_334(eop_142_plt1_334_sig) ,	// output [31:0] eop_142_plt1_334_sig
	.eop_142_plt1_334_ap_vld(eop_142_plt1_334_ap_vld_sig) ,	// output  eop_142_plt1_334_ap_vld_sig
	.eop_189_szh_338(eop_189_szh_338_sig) ,	// output [31:0] eop_189_szh_338_sig
	.eop_189_szh_338_ap_vld(eop_189_szh_338_ap_vld_sig) ,	// output  eop_189_szh_338_ap_vld_sig
	.eop_190_sph_340(eop_190_sph_340_sig) ,	// output [31:0] eop_190_sph_340_sig
	.eop_190_sph_340_ap_vld(eop_190_sph_340_ap_vld_sig) ,	// output  eop_190_sph_340_ap_vld_sig
	.eop_176_sh_343(eop_176_sh_343_sig) ,	// output [31:0] eop_176_sh_343_sig
	.eop_176_sh_343_ap_vld(eop_176_sh_343_ap_vld_sig) ,	// output  eop_176_sh_343_ap_vld_sig
	.eop_177_eh_345(eop_177_eh_345_sig) ,	// output [31:0] eop_177_eh_345_sig
	.eop_177_eh_345_ap_vld(eop_177_eh_345_ap_vld_sig) ,	// output  eop_177_eh_345_ap_vld_sig
	.eop_188_ih_351(eop_188_ih_351_sig) ,	// output [31:0] eop_188_ih_351_sig
	.eop_188_ih_351_ap_vld(eop_188_ih_351_ap_vld_sig) ,	// output  eop_188_ih_351_ap_vld_sig
	.eop_188_ih_355(eop_188_ih_355_sig) ,	// output [31:0] eop_188_ih_355_sig
	.eop_188_ih_355_ap_vld(eop_188_ih_355_ap_vld_sig) ,	// output  eop_188_ih_355_ap_vld_sig
	.eop_254_decis_357(eop_254_decis_357_sig) ,	// output [31:0] eop_254_decis_357_sig
	.eop_254_decis_357_ap_vld(eop_254_decis_357_ap_vld_sig) ,	// output  eop_254_decis_357_ap_vld_sig
	.eop_188_ih_359(eop_188_ih_359_sig) ,	// output [31:0] eop_188_ih_359_sig
	.eop_188_ih_359_ap_vld(eop_188_ih_359_ap_vld_sig) ,	// output  eop_188_ih_359_ap_vld_sig
	.eop_188_dh_362(eop_188_dh_362_sig) ,	// output [31:0] eop_188_dh_362_sig
	.eop_188_dh_362_ap_vld(eop_188_dh_362_ap_vld_sig) ,	// output  eop_188_dh_362_ap_vld_sig
	.eop_756_wd_756(eop_756_wd_756_sig) ,	// output [31:0] eop_756_wd_756_sig
	.eop_756_wd_756_ap_vld(eop_756_wd_756_ap_vld_sig) ,	// output  eop_756_wd_756_ap_vld_sig
	.eop_754_nbh_761(eop_754_nbh_761_sig) ,	// output [31:0] eop_754_nbh_761_sig
	.eop_754_nbh_761_ap_vld(eop_754_nbh_761_ap_vld_sig) ,	// output  eop_754_nbh_761_ap_vld_sig
	.eop_754_nbh_763(eop_754_nbh_763_sig) ,	// output [31:0] eop_754_nbh_763_sig
	.eop_754_nbh_763_ap_vld(eop_754_nbh_763_ap_vld_sig) ,	// output  eop_754_nbh_763_ap_vld_sig
	.eop_754_nbh_765(eop_754_nbh_765_sig) ,	// output [31:0] eop_754_nbh_765_sig
	.eop_754_nbh_765_ap_vld(eop_754_nbh_765_ap_vld_sig) ,	// output  eop_754_nbh_765_ap_vld_sig
	.eop_189_nbh_365(eop_189_nbh_365_sig) ,	// output [31:0] eop_189_nbh_365_sig
	.eop_189_nbh_365_ap_vld(eop_189_nbh_365_ap_vld_sig) ,	// output  eop_189_nbh_365_ap_vld_sig
	.eop_175_deth_368(eop_175_deth_368_sig) ,	// output [31:0] eop_175_deth_368_sig
	.eop_175_deth_368_ap_vld(eop_175_deth_368_ap_vld_sig) ,	// output  eop_175_deth_368_ap_vld_sig
	.eop_190_ph_371(eop_190_ph_371_sig) ,	// output [31:0] eop_190_ph_371_sig
	.eop_190_ph_371_ap_vld(eop_190_ph_371_ap_vld_sig) ,	// output  eop_190_ph_371_ap_vld_sig
	.eop_196_ah2_380(eop_196_ah2_380_sig) ,	// output [31:0] eop_196_ah2_380_sig
	.eop_196_ah2_380_ap_vld(eop_196_ah2_380_ap_vld_sig) ,	// output  eop_196_ah2_380_ap_vld_sig
	.eop_196_ah1_383(eop_196_ah1_383_sig) ,	// output [31:0] eop_196_ah1_383_sig
	.eop_196_ah1_383_ap_vld(eop_196_ah1_383_ap_vld_sig) ,	// output  eop_196_ah1_383_ap_vld_sig
	.eop_190_yh_386(eop_190_yh_386_sig) ,	// output [31:0] eop_190_yh_386_sig
	.eop_190_yh_386_ap_vld(eop_190_yh_386_ap_vld_sig) ,	// output  eop_190_yh_386_ap_vld_sig
	.eop_198_rh2_389(eop_198_rh2_389_sig) ,	// output [31:0] eop_198_rh2_389_sig
	.eop_198_rh2_389_ap_vld(eop_198_rh2_389_ap_vld_sig) ,	// output  eop_198_rh2_389_ap_vld_sig
	.eop_198_rh1_390(eop_198_rh1_390_sig) ,	// output [31:0] eop_198_rh1_390_sig
	.eop_198_rh1_390_ap_vld(eop_198_rh1_390_ap_vld_sig) ,	// output  eop_198_rh1_390_ap_vld_sig
	.eop_197_ph2_391(eop_197_ph2_391_sig) ,	// output [31:0] eop_197_ph2_391_sig
	.eop_197_ph2_391_ap_vld(eop_197_ph2_391_ap_vld_sig) ,	// output  eop_197_ph2_391_ap_vld_sig
	.eop_197_ph1_392(eop_197_ph1_392_sig) ,	// output [31:0] eop_197_ph1_392_sig
	.eop_197_ph1_392_ap_vld(eop_197_ph1_392_ap_vld_sig) ,	// output  eop_197_ph1_392_ap_vld_sig
	.eop_201_ilr_409(eop_201_ilr_409_sig) ,	// output [31:0] eop_201_ilr_409_sig
	.eop_201_ilr_409_ap_vld(eop_201_ilr_409_ap_vld_sig) ,	// output  eop_201_ilr_409_ap_vld_sig
	.eop_188_ih_410(eop_188_ih_410_sig) ,	// output [31:0] eop_188_ih_410_sig
	.eop_188_ih_410_ap_vld(eop_188_ih_410_ap_vld_sig) ,	// output  eop_188_ih_410_ap_vld_sig
	.eop_209_dec_szl_415(eop_209_dec_szl_415_sig) ,	// output [31:0] eop_209_dec_szl_415_sig
	.eop_209_dec_szl_415_ap_vld(eop_209_dec_szl_415_ap_vld_sig) ,	// output  eop_209_dec_szl_415_ap_vld_sig
	.eop_209_dec_spl_418(eop_209_dec_spl_418_sig) ,	// output [31:0] eop_209_dec_spl_418_sig
	.eop_209_dec_spl_418_ap_vld(eop_209_dec_spl_418_ap_vld_sig) ,	// output  eop_209_dec_spl_418_ap_vld_sig
	.eop_209_dec_sl_420(eop_209_dec_sl_420_sig) ,	// output [31:0] eop_209_dec_sl_420_sig
	.eop_209_dec_sl_420_ap_vld(eop_209_dec_sl_420_ap_vld_sig) ,	// output  eop_209_dec_sl_420_ap_vld_sig
	.eop_202_dec_dlt_423(eop_202_dec_dlt_423_sig) ,	// output [31:0] eop_202_dec_dlt_423_sig
	.eop_202_dec_dlt_423_ap_vld(eop_202_dec_dlt_423_ap_vld_sig) ,	// output  eop_202_dec_dlt_423_ap_vld_sig
	.eop_212_dl_426(eop_212_dl_426_sig) ,	// output [31:0] eop_212_dl_426_sig
	.eop_212_dl_426_ap_vld(eop_212_dl_426_ap_vld_sig) ,	// output  eop_212_dl_426_ap_vld_sig
	.eop_201_rl_428(eop_201_rl_428_sig) ,	// output [31:0] eop_201_rl_428_sig
	.eop_201_rl_428_ap_vld(eop_201_rl_428_ap_vld_sig) ,	// output  eop_201_rl_428_ap_vld_sig
	.eop_213_dec_nbl_431(eop_213_dec_nbl_431_sig) ,	// output [31:0] eop_213_dec_nbl_431_sig
	.eop_213_dec_nbl_431_ap_vld(eop_213_dec_nbl_431_ap_vld_sig) ,	// output  eop_213_dec_nbl_431_ap_vld_sig
	.eop_202_dec_detl_434(eop_202_dec_detl_434_sig) ,	// output [31:0] eop_202_dec_detl_434_sig
	.eop_202_dec_detl_434_ap_vld(eop_202_dec_detl_434_ap_vld_sig) ,	// output  eop_202_dec_detl_434_ap_vld_sig
	.eop_208_dec_plt_438(eop_208_dec_plt_438_sig) ,	// output [31:0] eop_208_dec_plt_438_sig
	.eop_208_dec_plt_438_ap_vld(eop_208_dec_plt_438_ap_vld_sig) ,	// output  eop_208_dec_plt_438_ap_vld_sig
	.eop_211_dec_al2_444(eop_211_dec_al2_444_sig) ,	// output [31:0] eop_211_dec_al2_444_sig
	.eop_211_dec_al2_444_ap_vld(eop_211_dec_al2_444_ap_vld_sig) ,	// output  eop_211_dec_al2_444_ap_vld_sig
	.eop_211_dec_al1_447(eop_211_dec_al1_447_sig) ,	// output [31:0] eop_211_dec_al1_447_sig
	.eop_211_dec_al1_447_ap_vld(eop_211_dec_al1_447_ap_vld_sig) ,	// output  eop_211_dec_al1_447_ap_vld_sig
	.eop_210_dec_rlt_450(eop_210_dec_rlt_450_sig) ,	// output [31:0] eop_210_dec_rlt_450_sig
	.eop_210_dec_rlt_450_ap_vld(eop_210_dec_rlt_450_ap_vld_sig) ,	// output  eop_210_dec_rlt_450_ap_vld_sig
	.eop_210_dec_rlt2_453(eop_210_dec_rlt2_453_sig) ,	// output [31:0] eop_210_dec_rlt2_453_sig
	.eop_210_dec_rlt2_453_ap_vld(eop_210_dec_rlt2_453_ap_vld_sig) ,	// output  eop_210_dec_rlt2_453_ap_vld_sig
	.eop_210_dec_rlt1_454(eop_210_dec_rlt1_454_sig) ,	// output [31:0] eop_210_dec_rlt1_454_sig
	.eop_210_dec_rlt1_454_ap_vld(eop_210_dec_rlt1_454_ap_vld_sig) ,	// output  eop_210_dec_rlt1_454_ap_vld_sig
	.eop_208_dec_plt2_455(eop_208_dec_plt2_455_sig) ,	// output [31:0] eop_208_dec_plt2_455_sig
	.eop_208_dec_plt2_455_ap_vld(eop_208_dec_plt2_455_ap_vld_sig) ,	// output  eop_208_dec_plt2_455_ap_vld_sig
	.eop_208_dec_plt1_456(eop_208_dec_plt1_456_sig) ,	// output [31:0] eop_208_dec_plt1_456_sig
	.eop_208_dec_plt1_456_ap_vld(eop_208_dec_plt1_456_ap_vld_sig) ,	// output  eop_208_dec_plt1_456_ap_vld_sig
	.eop_220_dec_szh_461(eop_220_dec_szh_461_sig) ,	// output [31:0] eop_220_dec_szh_461_sig
	.eop_220_dec_szh_461_ap_vld(eop_220_dec_szh_461_ap_vld_sig) ,	// output  eop_220_dec_szh_461_ap_vld_sig
	.eop_224_dec_sph_464(eop_224_dec_sph_464_sig) ,	// output [31:0] eop_224_dec_sph_464_sig
	.eop_224_dec_sph_464_ap_vld(eop_224_dec_sph_464_ap_vld_sig) ,	// output  eop_224_dec_sph_464_ap_vld_sig
	.eop_226_dec_sh_467(eop_226_dec_sh_467_sig) ,	// output [31:0] eop_226_dec_sh_467_sig
	.eop_226_dec_sh_467_ap_vld(eop_226_dec_sh_467_ap_vld_sig) ,	// output  eop_226_dec_sh_467_ap_vld_sig
	.eop_213_dec_dh_470(eop_213_dec_dh_470_sig) ,	// output [31:0] eop_213_dec_dh_470_sig
	.eop_213_dec_dh_470_ap_vld(eop_213_dec_dh_470_ap_vld_sig) ,	// output  eop_213_dec_dh_470_ap_vld_sig
	.eop_213_dec_nbh_473(eop_213_dec_nbh_473_sig) ,	// output [31:0] eop_213_dec_nbh_473_sig
	.eop_213_dec_nbh_473_ap_vld(eop_213_dec_nbh_473_ap_vld_sig) ,	// output  eop_213_dec_nbh_473_ap_vld_sig
	.eop_202_dec_deth_476(eop_202_dec_deth_476_sig) ,	// output [31:0] eop_202_dec_deth_476_sig
	.eop_202_dec_deth_476_ap_vld(eop_202_dec_deth_476_ap_vld_sig) ,	// output  eop_202_dec_deth_476_ap_vld_sig
	.eop_224_dec_ph_479(eop_224_dec_ph_479_sig) ,	// output [31:0] eop_224_dec_ph_479_sig
	.eop_224_dec_ph_479_ap_vld(eop_224_dec_ph_479_ap_vld_sig) ,	// output  eop_224_dec_ph_479_ap_vld_sig
	.eop_223_dec_ah2_485(eop_223_dec_ah2_485_sig) ,	// output [31:0] eop_223_dec_ah2_485_sig
	.eop_223_dec_ah2_485_ap_vld(eop_223_dec_ah2_485_ap_vld_sig) ,	// output  eop_223_dec_ah2_485_ap_vld_sig
	.eop_223_dec_ah1_488(eop_223_dec_ah1_488_sig) ,	// output [31:0] eop_223_dec_ah1_488_sig
	.eop_223_dec_ah1_488_ap_vld(eop_223_dec_ah1_488_ap_vld_sig) ,	// output  eop_223_dec_ah1_488_ap_vld_sig
	.eop_190_rh_491(eop_190_rh_491_sig) ,	// output [31:0] eop_190_rh_491_sig
	.eop_190_rh_491_ap_vld(eop_190_rh_491_ap_vld_sig) ,	// output  eop_190_rh_491_ap_vld_sig
	.eop_222_dec_rh2_494(eop_222_dec_rh2_494_sig) ,	// output [31:0] eop_222_dec_rh2_494_sig
	.eop_222_dec_rh2_494_ap_vld(eop_222_dec_rh2_494_ap_vld_sig) ,	// output  eop_222_dec_rh2_494_ap_vld_sig
	.eop_222_dec_rh1_495(eop_222_dec_rh1_495_sig) ,	// output [31:0] eop_222_dec_rh1_495_sig
	.eop_222_dec_rh1_495_ap_vld(eop_222_dec_rh1_495_ap_vld_sig) ,	// output  eop_222_dec_rh1_495_ap_vld_sig
	.eop_228_dec_ph2_496(eop_228_dec_ph2_496_sig) ,	// output [31:0] eop_228_dec_ph2_496_sig
	.eop_228_dec_ph2_496_ap_vld(eop_228_dec_ph2_496_ap_vld_sig) ,	// output  eop_228_dec_ph2_496_ap_vld_sig
	.eop_228_dec_ph1_497(eop_228_dec_ph1_497_sig) ,	// output [31:0] eop_228_dec_ph1_497_sig
	.eop_228_dec_ph1_497_ap_vld(eop_228_dec_ph1_497_ap_vld_sig) ,	// output  eop_228_dec_ph1_497_ap_vld_sig
	.eop_101_xd_502(eop_101_xd_502_sig) ,	// output [31:0] eop_101_xd_502_sig
	.eop_101_xd_502_ap_vld(eop_101_xd_502_ap_vld_sig) ,	// output  eop_101_xd_502_ap_vld_sig
	.eop_101_xs_503(eop_101_xs_503_sig) ,	// output [31:0] eop_101_xs_503_sig
	.eop_101_xs_503_ap_vld(eop_101_xs_503_ap_vld_sig) ,	// output  eop_101_xs_503_ap_vld_sig
	.eop_404_xa1_509(eop_404_xa1_509_sig) ,	// output [31:0] eop_404_xa1_509_sig
	.eop_404_xa1_509_ap_vld(eop_404_xa1_509_ap_vld_sig) ,	// output  eop_404_xa1_509_ap_vld_sig
	.eop_404_xa2_510(eop_404_xa2_510_sig) ,	// output [31:0] eop_404_xa2_510_sig
	.eop_404_xa2_510_ap_vld(eop_404_xa2_510_ap_vld_sig) ,	// output  eop_404_xa2_510_ap_vld_sig
	.eop_404_xa1_514(eop_404_xa1_514_sig) ,	// output [31:0] eop_404_xa1_514_sig
	.eop_404_xa1_514_ap_vld(eop_404_xa1_514_ap_vld_sig) ,	// output  eop_404_xa1_514_ap_vld_sig
	.eop_404_xa2_515(eop_404_xa2_515_sig) ,	// output [31:0] eop_404_xa2_515_sig
	.eop_404_xa2_515_ap_vld(eop_404_xa2_515_ap_vld_sig) ,	// output  eop_404_xa2_515_ap_vld_sig
	.eop_404_xa1_518(eop_404_xa1_518_sig) ,	// output [31:0] eop_404_xa1_518_sig
	.eop_404_xa1_518_ap_vld(eop_404_xa1_518_ap_vld_sig) ,	// output  eop_404_xa1_518_ap_vld_sig
	.eop_404_xa2_519(eop_404_xa2_519_sig) ,	// output [31:0] eop_404_xa2_519_sig
	.eop_404_xa2_519_ap_vld(eop_404_xa2_519_ap_vld_sig) ,	// output  eop_404_xa2_519_ap_vld_sig
	.eop_99_xout1_522(eop_99_xout1_522_sig) ,	// output [31:0] eop_99_xout1_522_sig
	.eop_99_xout1_522_ap_vld(eop_99_xout1_522_ap_vld_sig) ,	// output  eop_99_xout1_522_ap_vld_sig
	.eop_99_xout2_523(eop_99_xout2_523_sig) ,	// output [31:0] eop_99_xout2_523_sig
	.eop_99_xout2_523_ap_vld(eop_99_xout2_523_ap_vld_sig) ,	// output  eop_99_xout2_523_ap_vld_sig
	.eop_873_main_result_881(eop_873_main_result_881_sig) ,	// output [31:0] eop_873_main_result_881_sig
	.eop_873_main_result_881_ap_vld(eop_873_main_result_881_ap_vld_sig) ,	// output  eop_873_main_result_881_ap_vld_sig
	.eop_873_main_result_888(eop_873_main_result_888_sig) ,	// output [31:0] eop_873_main_result_888_sig
	.eop_873_main_result_888_ap_vld(eop_873_main_result_888_ap_vld_sig),	// output  eop_873_main_result_888_ap_vld_sig
	.ap_return(ap_return_sig)
);
parameter N = 160;

reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <=  eop_873_main_result_875_ap_vld_sig;
valid_regs[1] <=  eop_202_dec_detl_544_ap_vld_sig;
valid_regs[2] <=  eop_154_detl_544_ap_vld_sig;
valid_regs[3] <=  eop_202_dec_deth_545_ap_vld_sig;
valid_regs[4] <=  eop_175_deth_545_ap_vld_sig;
valid_regs[5] <=  eop_144_rlt2_546_ap_vld_sig;
valid_regs[6] <=  eop_144_rlt1_546_ap_vld_sig;
valid_regs[7] <=  eop_142_plt2_546_ap_vld_sig;
valid_regs[8] <=  eop_142_plt1_546_ap_vld_sig;
valid_regs[9] <=  eop_141_al2_546_ap_vld_sig;
valid_regs[10] <= eop_141_al1_546_ap_vld_sig;
valid_regs[11] <= eop_140_nbl_546_ap_vld_sig;
valid_regs[12] <= eop_198_rh2_547_ap_vld_sig;
valid_regs[13] <= eop_198_rh1_547_ap_vld_sig;
valid_regs[14] <= eop_197_ph2_547_ap_vld_sig;
valid_regs[15] <= eop_197_ph1_547_ap_vld_sig;
valid_regs[16] <= eop_196_ah2_547_ap_vld_sig;
valid_regs[17] <= eop_196_ah1_547_ap_vld_sig;
valid_regs[18] <= eop_189_nbh_547_ap_vld_sig;
valid_regs[19] <= eop_210_dec_rlt2_548_ap_vld_sig;
valid_regs[20] <= eop_210_dec_rlt1_548_ap_vld_sig;
valid_regs[21] <= eop_208_dec_plt2_548_ap_vld_sig;
valid_regs[22] <= eop_208_dec_plt1_548_ap_vld_sig;
valid_regs[23] <= eop_211_dec_al2_548_ap_vld_sig;
valid_regs[24] <= eop_211_dec_al1_548_ap_vld_sig;
valid_regs[25] <= eop_213_dec_nbl_548_ap_vld_sig;
valid_regs[26] <= eop_222_dec_rh2_549_ap_vld_sig;
valid_regs[27] <= eop_222_dec_rh1_549_ap_vld_sig;
valid_regs[28] <= eop_228_dec_ph2_549_ap_vld_sig;
valid_regs[29] <= eop_228_dec_ph1_549_ap_vld_sig;
valid_regs[30] <= eop_223_dec_ah2_549_ap_vld_sig;
valid_regs[31] <= eop_223_dec_ah1_549_ap_vld_sig;
valid_regs[32] <= eop_213_dec_nbh_549_ap_vld_sig;
valid_regs[33] <= eop_253_xa_259_ap_vld_sig;
valid_regs[34] <= eop_253_xb_260_ap_vld_sig;
valid_regs[35] <= eop_253_xa_264_ap_vld_sig;
valid_regs[36] <= eop_253_xb_265_ap_vld_sig;
valid_regs[37] <= eop_253_xa_268_ap_vld_sig;
valid_regs[38] <= eop_253_xb_269_ap_vld_sig;
valid_regs[39] <= eop_93_xl_279_ap_vld_sig;
valid_regs[40] <= eop_93_xh_280_ap_vld_sig;
valid_regs[41] <= eop_584_zl_585_ap_vld_sig;
valid_regs[42] <= eop_584_zl_587_ap_vld_sig;
valid_regs[43] <= eop_105_szl_287_ap_vld_sig;
valid_regs[44] <= eop_598_pl_599_ap_vld_sig;
valid_regs[45] <= eop_598_pl_600_ap_vld_sig;
valid_regs[46] <= eop_598_pl2_601_ap_vld_sig;
valid_regs[47] <= eop_598_pl_602_ap_vld_sig;
valid_regs[48] <= eop_105_spl_290_ap_vld_sig;
valid_regs[49] <= eop_105_sl_293_ap_vld_sig;
valid_regs[50] <= eop_105_el_294_ap_vld_sig;
valid_regs[51] <= eop_238_m_241_ap_vld_sig;
valid_regs[52] <= eop_238_m_243_ap_vld_sig;
valid_regs[53] <= eop_611_wd_614_ap_vld_sig;
valid_regs[54] <= eop_611_decis_618_ap_vld_sig;
valid_regs[55] <= eop_610_ril_624_ap_vld_sig;
valid_regs[56] <= eop_610_ril_626_ap_vld_sig;
valid_regs[57] <= eop_105_il_297_ap_vld_sig;
valid_regs[58] <= eop_143_dlt_301_ap_vld_sig;
valid_regs[59] <= eop_636_wd_637_ap_vld_sig;
valid_regs[60] <= eop_634_nbl_638_ap_vld_sig;
valid_regs[61] <= eop_634_nbl_640_ap_vld_sig;
valid_regs[62] <= eop_634_nbl_642_ap_vld_sig;
valid_regs[63] <= eop_140_nbl_304_ap_vld_sig;
valid_regs[64] <= eop_653_wd1_653_ap_vld_sig;
valid_regs[65] <= eop_654_wd2_654_ap_vld_sig;
valid_regs[66] <= eop_655_wd3_655_ap_vld_sig;
valid_regs[67] <= eop_154_detl_308_ap_vld_sig;
valid_regs[68] <= eop_142_plt_311_ap_vld_sig;
valid_regs[69] <= eop_665_wd2_679_ap_vld_sig;
valid_regs[70] <= eop_665_wd2_681_ap_vld_sig;
valid_regs[71] <= eop_665_wd3_682_ap_vld_sig;
valid_regs[72] <= eop_700_wd2_702_ap_vld_sig;
valid_regs[73] <= eop_700_wd2_704_ap_vld_sig;
valid_regs[74] <= eop_700_wd2_705_ap_vld_sig;
valid_regs[75] <= eop_700_wd4_708_ap_vld_sig;
valid_regs[76] <= eop_700_wd4_712_ap_vld_sig;
valid_regs[77] <= eop_701_apl2_714_ap_vld_sig;
valid_regs[78] <= eop_701_apl2_718_ap_vld_sig;
valid_regs[79] <= eop_701_apl2_720_ap_vld_sig;
valid_regs[80] <= eop_141_al2_321_ap_vld_sig;
valid_regs[81] <= eop_730_wd2_732_ap_vld_sig;
valid_regs[82] <= eop_731_apl1_735_ap_vld_sig;
valid_regs[83] <= eop_731_apl1_739_ap_vld_sig;
valid_regs[84] <= eop_731_wd3_742_ap_vld_sig;
valid_regs[85] <= eop_731_apl1_744_ap_vld_sig;
valid_regs[86] <= eop_731_apl1_746_ap_vld_sig;
valid_regs[87] <= eop_141_al1_325_ap_vld_sig;
valid_regs[88] <= eop_144_rlt_328_ap_vld_sig;
valid_regs[89] <= eop_144_rlt2_331_ap_vld_sig;
valid_regs[90] <= eop_144_rlt1_332_ap_vld_sig;
valid_regs[91] <= eop_142_plt2_333_ap_vld_sig;
valid_regs[92] <= eop_142_plt1_334_ap_vld_sig;
valid_regs[93] <= eop_189_szh_338_ap_vld_sig;
valid_regs[94] <= eop_190_sph_340_ap_vld_sig;
valid_regs[95] <= eop_176_sh_343_ap_vld_sig;
valid_regs[96] <= eop_177_eh_345_ap_vld_sig;
valid_regs[97] <= eop_188_ih_351_ap_vld_sig;
valid_regs[98] <= eop_188_ih_355_ap_vld_sig;
valid_regs[99] <= eop_254_decis_357_ap_vld_sig;
valid_regs[100] <= eop_188_ih_359_ap_vld_sig;
valid_regs[101] <= eop_188_dh_362_ap_vld_sig;
valid_regs[102] <= eop_756_wd_756_ap_vld_sig;
valid_regs[103] <= eop_754_nbh_761_ap_vld_sig;
valid_regs[104] <= eop_754_nbh_763_ap_vld_sig;
valid_regs[105] <= eop_754_nbh_765_ap_vld_sig;
valid_regs[106] <= eop_189_nbh_365_ap_vld_sig;
valid_regs[107] <= eop_175_deth_368_ap_vld_sig;
valid_regs[108] <= eop_190_ph_371_ap_vld_sig;
valid_regs[109] <= eop_196_ah2_380_ap_vld_sig;
valid_regs[110] <= eop_196_ah1_383_ap_vld_sig;
valid_regs[111] <= eop_190_yh_386_ap_vld_sig;
valid_regs[112] <= eop_198_rh2_389_ap_vld_sig;
valid_regs[113] <= eop_198_rh1_390_ap_vld_sig;
valid_regs[114] <= eop_197_ph2_391_ap_vld_sig;
valid_regs[115] <= eop_197_ph1_392_ap_vld_sig;
valid_regs[116] <= eop_201_ilr_409_ap_vld_sig;
valid_regs[117] <= eop_188_ih_410_ap_vld_sig;
valid_regs[118] <= eop_209_dec_szl_415_ap_vld_sig;
valid_regs[119] <= eop_209_dec_spl_418_ap_vld_sig;
valid_regs[120] <= eop_209_dec_sl_420_ap_vld_sig;
valid_regs[121] <= eop_202_dec_dlt_423_ap_vld_sig;
valid_regs[122] <= eop_212_dl_426_ap_vld_sig;
valid_regs[123] <= eop_201_rl_428_ap_vld_sig;
valid_regs[124] <= eop_213_dec_nbl_431_ap_vld_sig;
valid_regs[125] <= eop_202_dec_detl_434_ap_vld_sig;
valid_regs[126] <= eop_208_dec_plt_438_ap_vld_sig;
valid_regs[127] <= eop_211_dec_al2_444_ap_vld_sig;
valid_regs[128] <= eop_211_dec_al1_447_ap_vld_sig;
valid_regs[129] <= eop_210_dec_rlt_450_ap_vld_sig;
valid_regs[130] <= eop_210_dec_rlt2_453_ap_vld_sig;
valid_regs[131] <= eop_210_dec_rlt1_454_ap_vld_sig;
valid_regs[132] <= eop_208_dec_plt2_455_ap_vld_sig;
valid_regs[133] <= eop_208_dec_plt1_456_ap_vld_sig;
valid_regs[134] <= eop_220_dec_szh_461_ap_vld_sig;
valid_regs[135] <= eop_224_dec_sph_464_ap_vld_sig;
valid_regs[136] <= eop_226_dec_sh_467_ap_vld_sig;
valid_regs[137] <= eop_213_dec_dh_470_ap_vld_sig;
valid_regs[138] <= eop_213_dec_nbh_473_ap_vld_sig;
valid_regs[139] <= eop_202_dec_deth_476_ap_vld_sig;
valid_regs[140] <= eop_224_dec_ph_479_ap_vld_sig;
valid_regs[141] <= eop_223_dec_ah2_485_ap_vld_sig;
valid_regs[142] <= eop_223_dec_ah1_488_ap_vld_sig;
valid_regs[143] <= eop_190_rh_491_ap_vld_sig;
valid_regs[144] <= eop_222_dec_rh2_494_ap_vld_sig;
valid_regs[145] <= eop_222_dec_rh1_495_ap_vld_sig;
valid_regs[146] <= eop_228_dec_ph2_496_ap_vld_sig;
valid_regs[147] <= eop_228_dec_ph1_497_ap_vld_sig;
valid_regs[148] <= eop_101_xd_502_ap_vld_sig;
valid_regs[149] <= eop_101_xs_503_ap_vld_sig;
valid_regs[150] <= eop_404_xa1_509_ap_vld_sig;
valid_regs[151] <= eop_404_xa2_510_ap_vld_sig;
valid_regs[152] <= eop_404_xa1_514_ap_vld_sig;
valid_regs[153] <= eop_404_xa2_515_ap_vld_sig;
valid_regs[154] <= eop_404_xa1_518_ap_vld_sig;
valid_regs[155] <= eop_404_xa2_519_ap_vld_sig;
valid_regs[156] <= eop_99_xout1_522_ap_vld_sig;
valid_regs[157] <= eop_99_xout2_523_ap_vld_sig;
valid_regs[158] <= eop_873_main_result_881_ap_vld_sig;
valid_regs[159] <= eop_873_main_result_888_ap_vld_sig;

end

integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
