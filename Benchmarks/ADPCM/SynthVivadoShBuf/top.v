// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   

   output [7:0] LEDG;
    input UART_RXD;
    output UART_TXD;    
	wire clk = CLOCK_50;

	wire 	reset = ~KEY[0];
   wire 	start = ~KEY[1];
   wire [31:0] 	ap_return_sig;
   reg  [31:0] 	return_val_reg = 0;
   wire 	finish;

   hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
   hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
   hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
   hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
   hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
   hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
   hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
   hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
   
    
	reg [31:0]cycle_cnt_reg = 0;
	always @ (*) begin
			hex7 <= KEY[2]?cycle_cnt_reg[31:28]:return_val_reg[31:28];
			hex6 <= KEY[2]?cycle_cnt_reg[27:24]:return_val_reg[27:24];
			hex5 <= KEY[2]?cycle_cnt_reg[23:20]:return_val_reg[23:20];
			hex4 <= KEY[2]?cycle_cnt_reg[19:16]:return_val_reg[19:16];
			hex3 <= KEY[2]?cycle_cnt_reg[15:12]:return_val_reg[15:12];
			hex2 <= KEY[2]?cycle_cnt_reg[11:8]:return_val_reg[11:8];
			hex1 <= KEY[2]?cycle_cnt_reg[7:4]:return_val_reg[7:4];
			hex0 <= KEY[2]?cycle_cnt_reg[3:0]:return_val_reg[3:0];
	end

	always @(posedge clk) begin
		if (reset)
			return_val_reg <= 0;
		else if (finish) begin
			return_val_reg <= ap_return_sig;
		end
	end

	reg [31:0]cycle_cnt = 0;
	wire idle;
	reg finish_reg;
	always @ (posedge clk) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge finish_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (finish_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk) begin
		if (reset)
			finish_reg <= 0;
		else if (finish)
			finish_reg <= 1;
		else
			finish_reg <=finish_reg;
	end
	
main main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(finish) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.ap_return(ap_return_sig)
);

endmodule
