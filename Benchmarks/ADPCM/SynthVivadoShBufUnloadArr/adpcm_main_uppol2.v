// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module adpcm_main_uppol2 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        al1,
        al2,
        plt,
        plt1,
        plt2,
        bufIndex_i,
        bufIndex_o,
        bufIndex_o_ap_vld,
        TRACEBUFFER_address0,
        TRACEBUFFER_ce0,
        TRACEBUFFER_we0,
        TRACEBUFFER_d0,
        TRACEBUFFER_address1,
        TRACEBUFFER_ce1,
        TRACEBUFFER_we1,
        TRACEBUFFER_d1,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 12'b1;
parameter    ap_ST_st2_fsm_1 = 12'b10;
parameter    ap_ST_st3_fsm_2 = 12'b100;
parameter    ap_ST_st4_fsm_3 = 12'b1000;
parameter    ap_ST_st5_fsm_4 = 12'b10000;
parameter    ap_ST_st6_fsm_5 = 12'b100000;
parameter    ap_ST_st7_fsm_6 = 12'b1000000;
parameter    ap_ST_st8_fsm_7 = 12'b10000000;
parameter    ap_ST_st9_fsm_8 = 12'b100000000;
parameter    ap_ST_st10_fsm_9 = 12'b1000000000;
parameter    ap_ST_st11_fsm_10 = 12'b10000000000;
parameter    ap_ST_st12_fsm_11 = 12'b100000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_5 = 32'b101;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv32_A = 32'b1010;
parameter    ap_const_lv32_FFFFD000 = 32'b11111111111111111101000000000000;
parameter    ap_const_lv32_B = 32'b1011;
parameter    ap_const_lv32_9 = 32'b1001;
parameter    ap_const_lv64_FFFFFFFFFFFFD000 = 64'b1111111111111111111111111111111111111111111111111101000000000000;
parameter    ap_const_lv8_3 = 8'b11;
parameter    ap_const_lv32_1E = 32'b11110;
parameter    ap_const_lv32_1F = 32'b11111;
parameter    ap_const_lv42_14200000000 = 42'b10100001000000000000000000000000000000000;
parameter    ap_const_lv8_1 = 8'b1;
parameter    ap_const_lv9_142 = 9'b101000010;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv8_2 = 8'b10;
parameter    ap_const_lv32_3F = 32'b111111;
parameter    ap_const_lv35_0 = 35'b00000000000000000000000000000000000;
parameter    ap_const_lv32_20 = 32'b100000;
parameter    ap_const_lv32_22 = 32'b100010;
parameter    ap_const_lv42_14600000000 = 42'b10100011000000000000000000000000000000000;
parameter    ap_const_lv9_146 = 9'b101000110;
parameter    ap_const_lv8_4 = 8'b100;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv64_FFFFFFFFFFFFFFFF = 64'b1111111111111111111111111111111111111111111111111111111111111111;
parameter    ap_const_lv64_14700000000 = 64'b10100011100000000000000000000000000000000;
parameter    ap_const_lv9_147 = 9'b101000111;
parameter    ap_const_lv28_80 = 28'b10000000;
parameter    ap_const_lv32_1B = 32'b11011;
parameter    ap_const_lv64_14B00000000 = 64'b10100101100000000000000000000000000000000;
parameter    ap_const_lv9_14B = 9'b101001011;
parameter    ap_const_lv28_FFFFF80 = 28'b1111111111111111111110000000;
parameter    ap_const_lv64_14D00000000 = 64'b10100110100000000000000000000000000000000;
parameter    ap_const_lv9_14D = 9'b101001101;
parameter    ap_const_lv7_0 = 7'b0000000;
parameter    ap_const_lv32_26 = 32'b100110;
parameter    ap_const_lv8_5 = 8'b101;
parameter    ap_const_lv42_14F00000000 = 42'b10100111100000000000000000000000000000000;
parameter    ap_const_lv32_3000 = 32'b11000000000000;
parameter    ap_const_lv8_6 = 8'b110;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [31:0] al1;
input  [31:0] al2;
input  [31:0] plt;
input  [31:0] plt1;
input  [31:0] plt2;
input  [7:0] bufIndex_i;
output  [7:0] bufIndex_o;
output   bufIndex_o_ap_vld;
output  [7:0] TRACEBUFFER_address0;
output   TRACEBUFFER_ce0;
output   TRACEBUFFER_we0;
output  [63:0] TRACEBUFFER_d0;
output  [7:0] TRACEBUFFER_address1;
output   TRACEBUFFER_ce1;
output   TRACEBUFFER_we1;
output  [63:0] TRACEBUFFER_d1;
output  [31:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[7:0] bufIndex_o;
reg bufIndex_o_ap_vld;
reg[7:0] TRACEBUFFER_address0;
reg TRACEBUFFER_ce0;
reg TRACEBUFFER_we0;
reg[63:0] TRACEBUFFER_d0;
reg[7:0] TRACEBUFFER_address1;
reg TRACEBUFFER_ce1;
reg TRACEBUFFER_we1;
reg[63:0] TRACEBUFFER_d1;
reg[31:0] ap_return;
(* fsm_encoding = "none" *) reg   [11:0] ap_CS_fsm = 12'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_30;
wire  signed [63:0] tmp_s_fu_286_p1;
reg  signed [63:0] tmp_s_reg_750;
reg    ap_sig_cseq_ST_st5_fsm_4;
reg    ap_sig_bdd_69;
reg   [7:0] bufIndex_load_reg_766;
reg    ap_sig_cseq_ST_st6_fsm_5;
reg    ap_sig_bdd_80;
wire  signed [34:0] wd2_cast_fu_371_p1;
wire   [7:0] tmp_78_i_fu_375_p2;
wire   [34:0] wd2_1_fu_388_p2;
wire   [0:0] tmp_8_fu_380_p3;
wire   [7:0] tmp_78_i1_fu_446_p2;
reg   [27:0] tmp_5_reg_807;
reg    ap_sig_cseq_ST_st7_fsm_6;
reg    ap_sig_bdd_103;
wire   [31:0] apl2_fu_672_p2;
reg   [31:0] apl2_reg_817;
reg    ap_sig_cseq_ST_st11_fsm_10;
reg    ap_sig_bdd_112;
wire   [7:0] tmp_82_i_fu_678_p2;
reg   [7:0] tmp_82_i_reg_824;
reg   [7:0] bufIndex_load_i_reg_242;
reg   [34:0] wd_reg_252;
wire   [27:0] wd4_fu_538_p2;
reg   [27:0] wd1_phi_fu_264_p4;
wire   [0:0] tmp_12_fu_530_p3;
wire   [27:0] wd4_1_fu_588_p2;
reg   [31:0] apl2_1_phi_fu_273_p4;
reg    ap_sig_cseq_ST_st12_fsm_11;
reg    ap_sig_bdd_139;
wire   [0:0] tmp_15_fu_728_p2;
wire   [31:0] p_s_fu_720_p3;
wire   [63:0] tmp_75_i_fu_339_p1;
wire   [63:0] tmp_79_i_fu_359_p1;
wire   [63:0] tmp_75_i7_fu_428_p1;
wire   [63:0] tmp_79_i1_fu_451_p1;
wire   [63:0] tmp_75_i1_fu_492_p1;
wire   [63:0] tmp_79_i2_fu_519_p1;
reg    ap_sig_cseq_ST_st10_fsm_9;
reg    ap_sig_bdd_164;
wire   [63:0] tmp_75_i2_fu_565_p1;
wire   [63:0] tmp_79_i3_fu_583_p1;
wire   [63:0] tmp_75_i3_fu_615_p1;
wire   [63:0] tmp_79_i4_fu_633_p1;
wire   [63:0] tmp_83_i_fu_710_p1;
wire   [63:0] tmp_83_i9_fu_746_p1;
wire   [7:0] tmp_82_i9_fu_734_p2;
wire  signed [63:0] tmp_73_i_cast_fu_324_p1;
wire  signed [63:0] tmp_73_i4_cast_fu_418_p1;
wire   [63:0] tmp_73_i1_cast_cast_fu_483_p3;
wire   [63:0] tmp_73_i2_cast_cast_fu_556_p3;
wire   [63:0] tmp_73_i3_cast_cast_fu_606_p3;
wire  signed [63:0] tmp_81_i_cast_fu_705_p1;
wire   [63:0] tmp_77_i_cast_fu_354_p1;
wire   [63:0] tmp_77_i1_cast_fu_441_p1;
wire   [63:0] tmp_77_i2_cast_fu_514_p1;
wire   [63:0] tmp_77_i3_cast_fu_578_p1;
wire   [63:0] tmp_77_i4_cast_fu_628_p1;
wire   [1:0] tmp_1_fu_304_p4;
wire  signed [41:0] tmp_72_i_cast_fu_314_p1;
wire   [41:0] tmp_73_i_fu_318_p2;
wire   [29:0] tmp_fu_300_p1;
wire   [40:0] tmp_77_i_fu_344_p4;
wire   [7:0] tmp_74_i_fu_333_p2;
wire   [33:0] wd2_fu_364_p3;
wire   [63:0] grp_fu_294_p2;
wire   [2:0] tmp_3_fu_398_p4;
wire  signed [41:0] tmp_72_i3_cast_fu_408_p1;
wire   [41:0] tmp_73_i4_fu_412_p2;
wire   [31:0] tmp_9_fu_394_p1;
wire   [40:0] tmp_77_i1_fu_433_p3;
wire   [7:0] tmp_74_i6_fu_423_p2;
wire  signed [31:0] grp_fu_460_p1;
wire   [0:0] tmp_11_fu_475_p3;
wire  signed [31:0] wd2_2_cast_fu_497_p1;
wire   [40:0] tmp_77_i2_fu_506_p3;
wire   [7:0] tmp_74_i1_fu_500_p2;
wire   [63:0] grp_fu_460_p2;
wire   [0:0] tmp_17_fu_548_p3;
wire   [7:0] tmp_78_i2_fu_524_p2;
wire  signed [31:0] wd4_cast_fu_544_p1;
wire   [40:0] tmp_77_i3_fu_570_p3;
wire   [7:0] grp_fu_280_p2;
wire   [0:0] tmp_13_fu_598_p3;
wire  signed [31:0] wd4_1_cast_fu_594_p1;
wire   [40:0] tmp_77_i4_fu_620_p3;
wire   [38:0] p_shl_fu_645_p3;
wire   [39:0] p_shl_cast_fu_652_p1;
wire  signed [39:0] tmp_20_cast7_fu_642_p1;
wire   [39:0] tmp_10_fu_656_p2;
wire  signed [31:0] wd1_cast_cast_fu_638_p1;
wire   [31:0] tmp_22_cast_cast_fu_662_p4;
wire  signed [41:0] tmp_80_i_cast_fu_696_p1;
wire   [41:0] tmp_81_i_fu_699_p2;
wire   [7:0] bufIndex_load_i1_fu_690_p2;
wire   [0:0] tmp_14_fu_715_p2;
wire    grp_fu_294_ce;
wire    grp_fu_460_ce;
reg   [31:0] ap_return_preg = 32'b00000000000000000000000000000000;
reg   [11:0] ap_NS_fsm;


adpcm_main_mul_32s_32s_64_6 #(
    .ID( 1 ),
    .NUM_STAGE( 6 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 64 ))
adpcm_main_mul_32s_32s_64_6_U68(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( plt ),
    .din1( plt1 ),
    .ce( grp_fu_294_ce ),
    .dout( grp_fu_294_p2 )
);

adpcm_main_mul_32s_32s_64_6 #(
    .ID( 1 ),
    .NUM_STAGE( 6 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 64 ))
adpcm_main_mul_32s_32s_64_6_U69(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( plt2 ),
    .din1( grp_fu_460_p1 ),
    .ce( grp_fu_460_ce ),
    .dout( grp_fu_460_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_return_preg
    if (ap_rst == 1'b1) begin
        ap_return_preg <= ap_const_lv32_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
            ap_return_preg <= apl2_1_phi_fu_273_p4;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        if (~(tmp_8_fu_380_p3 == ap_const_lv1_0)) begin
            bufIndex_load_i_reg_242 <= tmp_78_i_fu_375_p2;
        end else if ((tmp_8_fu_380_p3 == ap_const_lv1_0)) begin
            bufIndex_load_i_reg_242 <= tmp_78_i1_fu_446_p2;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        if (~(tmp_8_fu_380_p3 == ap_const_lv1_0)) begin
                        wd_reg_252[34 : 2] <= wd2_cast_fu_371_p1[34 : 2];
        end else if ((tmp_8_fu_380_p3 == ap_const_lv1_0)) begin
                        wd_reg_252[34 : 2] <= wd2_1_fu_388_p2[34 : 2];
        end
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10)) begin
        apl2_reg_817 <= apl2_fu_672_p2;
        tmp_82_i_reg_824 <= tmp_82_i_fu_678_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        bufIndex_load_reg_766 <= bufIndex_i;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        tmp_5_reg_807 <= {{wd_reg_252[ap_const_lv32_22 : ap_const_lv32_7]}};
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        tmp_s_reg_750 <= tmp_s_fu_286_p1;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or tmp_75_i_fu_339_p1 or tmp_75_i7_fu_428_p1 or tmp_75_i1_fu_492_p1 or tmp_75_i2_fu_565_p1 or tmp_75_i3_fu_615_p1 or tmp_83_i_fu_710_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        TRACEBUFFER_address0 = tmp_83_i_fu_710_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_address0 = tmp_75_i3_fu_615_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_address0 = tmp_75_i2_fu_565_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        TRACEBUFFER_address0 = tmp_75_i1_fu_492_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        TRACEBUFFER_address0 = tmp_75_i7_fu_428_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_address0 = tmp_75_i_fu_339_p1;
    end else begin
        TRACEBUFFER_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or tmp_79_i_fu_359_p1 or tmp_79_i1_fu_451_p1 or tmp_79_i2_fu_519_p1 or ap_sig_cseq_ST_st10_fsm_9 or tmp_79_i3_fu_583_p1 or tmp_79_i4_fu_633_p1 or tmp_83_i9_fu_746_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        TRACEBUFFER_address1 = tmp_83_i9_fu_746_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_address1 = tmp_79_i4_fu_633_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_address1 = tmp_79_i3_fu_583_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        TRACEBUFFER_address1 = tmp_79_i2_fu_519_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        TRACEBUFFER_address1 = tmp_79_i1_fu_451_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_address1 = tmp_79_i_fu_359_p1;
    end else begin
        TRACEBUFFER_address1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | (ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) | (ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3)) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        TRACEBUFFER_ce0 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | (ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3)) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9))) begin
        TRACEBUFFER_ce1 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_ce1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or tmp_73_i_cast_fu_324_p1 or tmp_73_i4_cast_fu_418_p1 or tmp_73_i1_cast_cast_fu_483_p3 or tmp_73_i2_cast_cast_fu_556_p3 or tmp_73_i3_cast_cast_fu_606_p3 or tmp_81_i_cast_fu_705_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        TRACEBUFFER_d0 = tmp_81_i_cast_fu_705_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_d0 = tmp_73_i3_cast_cast_fu_606_p3;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_d0 = tmp_73_i2_cast_cast_fu_556_p3;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        TRACEBUFFER_d0 = tmp_73_i1_cast_cast_fu_483_p3;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        TRACEBUFFER_d0 = tmp_73_i4_cast_fu_418_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_d0 = tmp_73_i_cast_fu_324_p1;
    end else begin
        TRACEBUFFER_d0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or ap_sig_cseq_ST_st10_fsm_9 or tmp_77_i_cast_fu_354_p1 or tmp_77_i1_cast_fu_441_p1 or tmp_77_i2_cast_fu_514_p1 or tmp_77_i3_cast_fu_578_p1 or tmp_77_i4_cast_fu_628_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        TRACEBUFFER_d1 = ap_const_lv64_FFFFFFFFFFFFD000;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_d1 = tmp_77_i4_cast_fu_628_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3))) begin
        TRACEBUFFER_d1 = tmp_77_i3_cast_fu_578_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        TRACEBUFFER_d1 = tmp_77_i2_cast_fu_514_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        TRACEBUFFER_d1 = tmp_77_i1_cast_fu_441_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        TRACEBUFFER_d1 = tmp_77_i_cast_fu_354_p1;
    end else begin
        TRACEBUFFER_d1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_8_fu_380_p3 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_8_fu_380_p3 == ap_const_lv1_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3)) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        TRACEBUFFER_we0 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st6_fsm_5 or tmp_8_fu_380_p3 or ap_sig_cseq_ST_st11_fsm_10 or tmp_12_fu_530_p3 or ap_sig_cseq_ST_st12_fsm_11 or tmp_15_fu_728_p2 or ap_sig_cseq_ST_st10_fsm_9) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) & (tmp_8_fu_380_p3 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & (ap_const_lv1_0 == tmp_12_fu_530_p3)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) & ~(ap_const_lv1_0 == tmp_12_fu_530_p3)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11) & ~(ap_const_lv1_0 == tmp_15_fu_728_p2)) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9))) begin
        TRACEBUFFER_we1 = ap_const_logic_1;
    end else begin
        TRACEBUFFER_we1 = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st12_fsm_11) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st12_fsm_11) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (apl2_1_phi_fu_273_p4 or ap_sig_cseq_ST_st12_fsm_11 or ap_return_preg) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        ap_return = apl2_1_phi_fu_273_p4;
    end else begin
        ap_return = ap_return_preg;
    end
end

always @ (ap_sig_bdd_164) begin
    if (ap_sig_bdd_164) begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_112) begin
    if (ap_sig_bdd_112) begin
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_139) begin
    if (ap_sig_bdd_139) begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_30) begin
    if (ap_sig_bdd_30) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_69) begin
    if (ap_sig_bdd_69) begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_80) begin
    if (ap_sig_bdd_80) begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_103) begin
    if (ap_sig_bdd_103) begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st12_fsm_11 or tmp_15_fu_728_p2 or p_s_fu_720_p3) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        if ((ap_const_lv1_0 == tmp_15_fu_728_p2)) begin
            apl2_1_phi_fu_273_p4 = p_s_fu_720_p3;
        end else if (~(ap_const_lv1_0 == tmp_15_fu_728_p2)) begin
            apl2_1_phi_fu_273_p4 = ap_const_lv32_FFFFD000;
        end else begin
            apl2_1_phi_fu_273_p4 = 'bx;
        end
    end else begin
        apl2_1_phi_fu_273_p4 = 'bx;
    end
end

always @ (bufIndex_i or ap_sig_cseq_ST_st11_fsm_10 or tmp_82_i_fu_678_p2 or ap_sig_cseq_ST_st12_fsm_11 or tmp_15_fu_728_p2 or tmp_82_i9_fu_734_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11) & ~(ap_const_lv1_0 == tmp_15_fu_728_p2))) begin
        bufIndex_o = tmp_82_i9_fu_734_p2;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10)) begin
        bufIndex_o = tmp_82_i_fu_678_p2;
    end else begin
        bufIndex_o = bufIndex_i;
    end
end

always @ (ap_sig_cseq_ST_st11_fsm_10 or ap_sig_cseq_ST_st12_fsm_11 or tmp_15_fu_728_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) | ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11) & ~(ap_const_lv1_0 == tmp_15_fu_728_p2)))) begin
        bufIndex_o_ap_vld = ap_const_logic_1;
    end else begin
        bufIndex_o_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st11_fsm_10 or wd4_fu_538_p2 or tmp_12_fu_530_p3 or wd4_1_fu_588_p2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10)) begin
        if (~(ap_const_lv1_0 == tmp_12_fu_530_p3)) begin
            wd1_phi_fu_264_p4 = wd4_1_fu_588_p2;
        end else if ((ap_const_lv1_0 == tmp_12_fu_530_p3)) begin
            wd1_phi_fu_264_p4 = wd4_fu_538_p2;
        end else begin
            wd1_phi_fu_264_p4 = 'bx;
        end
    end else begin
        wd1_phi_fu_264_p4 = 'bx;
    end
end
always @ (ap_start or ap_CS_fsm) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            ap_NS_fsm = ap_ST_st3_fsm_2;
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            ap_NS_fsm = ap_ST_st5_fsm_4;
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end



always @ (ap_CS_fsm) begin
    ap_sig_bdd_103 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_6]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_112 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_A]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_139 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_B]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_164 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_9]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_30 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_69 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_80 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_5]);
end

assign apl2_fu_672_p2 = ($signed(wd1_cast_cast_fu_638_p1) + $signed(tmp_22_cast_cast_fu_662_p4));

assign bufIndex_load_i1_fu_690_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_4);

assign grp_fu_280_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_3);

assign grp_fu_294_ce = ap_const_logic_1;

assign grp_fu_460_ce = ap_const_logic_1;

assign grp_fu_460_p1 = tmp_s_reg_750;

assign p_s_fu_720_p3 = ((tmp_14_fu_715_p2[0:0] === 1'b1) ? ap_const_lv32_3000 : apl2_reg_817);

assign p_shl_cast_fu_652_p1 = p_shl_fu_645_p3;

assign p_shl_fu_645_p3 = {{al2}, {ap_const_lv7_0}};

assign tmp_10_fu_656_p2 = ($signed(p_shl_cast_fu_652_p1) - $signed(tmp_20_cast7_fu_642_p1));

assign tmp_11_fu_475_p3 = wd_reg_252[ap_const_lv32_22];

assign tmp_12_fu_530_p3 = grp_fu_460_p2[ap_const_lv32_3F];

assign tmp_13_fu_598_p3 = wd4_1_fu_588_p2[ap_const_lv32_1B];

assign tmp_14_fu_715_p2 = ($signed(apl2_reg_817) > $signed(32'b11000000000000)? 1'b1: 1'b0);

assign tmp_15_fu_728_p2 = ($signed(p_s_fu_720_p3) < $signed(32'b11111111111111111101000000000000)? 1'b1: 1'b0);

assign tmp_17_fu_548_p3 = wd4_fu_538_p2[ap_const_lv32_1B];

assign tmp_1_fu_304_p4 = {{al1[ap_const_lv32_1F : ap_const_lv32_1E]}};

assign tmp_20_cast7_fu_642_p1 = $signed(al2);

assign tmp_22_cast_cast_fu_662_p4 = {{tmp_10_fu_656_p2[ap_const_lv32_26 : ap_const_lv32_7]}};

assign tmp_3_fu_398_p4 = {{wd2_1_fu_388_p2[ap_const_lv32_22 : ap_const_lv32_20]}};

assign tmp_72_i3_cast_fu_408_p1 = $signed(tmp_3_fu_398_p4);

assign tmp_72_i_cast_fu_314_p1 = $signed(tmp_1_fu_304_p4);

assign tmp_73_i1_cast_cast_fu_483_p3 = ((tmp_11_fu_475_p3[0:0] === 1'b1) ? ap_const_lv64_FFFFFFFFFFFFFFFF : ap_const_lv64_14700000000);

assign tmp_73_i2_cast_cast_fu_556_p3 = ((tmp_17_fu_548_p3[0:0] === 1'b1) ? ap_const_lv64_FFFFFFFFFFFFFFFF : ap_const_lv64_14B00000000);

assign tmp_73_i3_cast_cast_fu_606_p3 = ((tmp_13_fu_598_p3[0:0] === 1'b1) ? ap_const_lv64_FFFFFFFFFFFFFFFF : ap_const_lv64_14D00000000);

assign tmp_73_i4_cast_fu_418_p1 = $signed(tmp_73_i4_fu_412_p2);

assign tmp_73_i4_fu_412_p2 = (tmp_72_i3_cast_fu_408_p1 | ap_const_lv42_14600000000);

assign tmp_73_i_cast_fu_324_p1 = $signed(tmp_73_i_fu_318_p2);

assign tmp_73_i_fu_318_p2 = (tmp_72_i_cast_fu_314_p1 | ap_const_lv42_14200000000);

assign tmp_74_i1_fu_500_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_1);

assign tmp_74_i6_fu_423_p2 = (ap_const_lv8_3 + bufIndex_load_reg_766);

assign tmp_74_i_fu_333_p2 = (ap_const_lv8_1 + bufIndex_i);

assign tmp_75_i1_fu_492_p1 = bufIndex_load_i_reg_242;

assign tmp_75_i2_fu_565_p1 = tmp_78_i2_fu_524_p2;

assign tmp_75_i3_fu_615_p1 = tmp_78_i2_fu_524_p2;

assign tmp_75_i7_fu_428_p1 = tmp_78_i_fu_375_p2;

assign tmp_75_i_fu_339_p1 = bufIndex_i;

assign tmp_77_i1_cast_fu_441_p1 = tmp_77_i1_fu_433_p3;

assign tmp_77_i1_fu_433_p3 = {{ap_const_lv9_146}, {tmp_9_fu_394_p1}};

assign tmp_77_i2_cast_fu_514_p1 = tmp_77_i2_fu_506_p3;

assign tmp_77_i2_fu_506_p3 = {{ap_const_lv9_147}, {wd2_2_cast_fu_497_p1}};

assign tmp_77_i3_cast_fu_578_p1 = tmp_77_i3_fu_570_p3;

assign tmp_77_i3_fu_570_p3 = {{ap_const_lv9_14B}, {wd4_cast_fu_544_p1}};

assign tmp_77_i4_cast_fu_628_p1 = tmp_77_i4_fu_620_p3;

assign tmp_77_i4_fu_620_p3 = {{ap_const_lv9_14D}, {wd4_1_cast_fu_594_p1}};

assign tmp_77_i_cast_fu_354_p1 = tmp_77_i_fu_344_p4;

assign tmp_77_i_fu_344_p4 = {{{{ap_const_lv9_142}, {tmp_fu_300_p1}}}, {ap_const_lv2_0}};

assign tmp_78_i1_fu_446_p2 = (ap_const_lv8_4 + bufIndex_load_reg_766);

assign tmp_78_i2_fu_524_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_2);

assign tmp_78_i_fu_375_p2 = (ap_const_lv8_2 + bufIndex_load_reg_766);

assign tmp_79_i1_fu_451_p1 = tmp_74_i6_fu_423_p2;

assign tmp_79_i2_fu_519_p1 = tmp_74_i1_fu_500_p2;

assign tmp_79_i3_fu_583_p1 = grp_fu_280_p2;

assign tmp_79_i4_fu_633_p1 = grp_fu_280_p2;

assign tmp_79_i_fu_359_p1 = tmp_74_i_fu_333_p2;

assign tmp_80_i_cast_fu_696_p1 = $signed(apl2_reg_817);

assign tmp_81_i_cast_fu_705_p1 = $signed(tmp_81_i_fu_699_p2);

assign tmp_81_i_fu_699_p2 = (tmp_80_i_cast_fu_696_p1 | ap_const_lv42_14F00000000);

assign tmp_82_i9_fu_734_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_6);

assign tmp_82_i_fu_678_p2 = (bufIndex_load_i_reg_242 + ap_const_lv8_5);

assign tmp_83_i9_fu_746_p1 = tmp_82_i_reg_824;

assign tmp_83_i_fu_710_p1 = bufIndex_load_i1_fu_690_p2;

assign tmp_8_fu_380_p3 = grp_fu_294_p2[ap_const_lv32_3F];

assign tmp_9_fu_394_p1 = wd2_1_fu_388_p2[31:0];

assign tmp_fu_300_p1 = al1[29:0];

assign tmp_s_fu_286_p1 = $signed(plt);

assign wd1_cast_cast_fu_638_p1 = $signed(wd1_phi_fu_264_p4);

assign wd2_1_fu_388_p2 = ($signed(ap_const_lv35_0) - $signed(wd2_cast_fu_371_p1));

assign wd2_2_cast_fu_497_p1 = $signed(tmp_5_reg_807);

assign wd2_cast_fu_371_p1 = $signed(wd2_fu_364_p3);

assign wd2_fu_364_p3 = {{al1}, {ap_const_lv2_0}};

assign wd4_1_cast_fu_594_p1 = $signed(wd4_1_fu_588_p2);

assign wd4_1_fu_588_p2 = ($signed(tmp_5_reg_807) + $signed(ap_const_lv28_FFFFF80));

assign wd4_cast_fu_544_p1 = $signed(wd4_fu_538_p2);

assign wd4_fu_538_p2 = (tmp_5_reg_807 + ap_const_lv28_80);
always @ (posedge ap_clk) begin
    wd_reg_252[1:0] <= 2'b00;
end



endmodule //adpcm_main_uppol2

