#Creating simulation wave:
#wave.do



vsim -voptargs=+acc work.de2

add wave -position insertpoint  \
sim:/de2/main_inst/TRACEBUFFER_U/address0 \
sim:/de2/main_inst/TRACEBUFFER_U/we0 \
sim:/de2/main_inst/TRACEBUFFER_U/d0 \
sim:/de2/main_inst/TRACEBUFFER_U/address1 \
sim:/de2/main_inst/TRACEBUFFER_U/we1 \
sim:/de2/main_inst/TRACEBUFFER_U/d1
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/TRACEBUFFER_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/result_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/compressed_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/tqmf_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/accumc_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/accumd_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/delay_bpl_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/delay_dltx_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/delay_dhx_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/delay_bph_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/dec_del_bpl_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/dec_del_dltx_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/dec_del_bph_address0
add wave -position insertpoint  \
sim:/de2/main_inst/grp_adpcm_main_traceUnload_fu_532/dec_del_dhx_address0

add wave -position insertpoint  \
sim:/de2/CLOCK_50 \
sim:/de2/reset \
sim:/de2/start \
sim:/de2/finish \
sim:/de2/traceOut \
sim:/de2/return_val


#Add wave tp debug trace
#add wave -position insertpoint  sim:/de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/tmp_146_reg_3648

#Write configuration to file
#write format wave -window .main_pane.wave.interior.cs.body.pw.wf D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/simulation/modelsim/wave.do