`timescale 1 ns / 1 ns
module debug_manager
(
	clk,
	reset,
	TRACEBUFFER_address,
	//TRACEBUFFER_address_a,
	//TRACEBUFFER_address_b,
	TRACEBUFFER_out,
	//TRACEBUFFER_out_a,
	//TRACEBUFFER_out_b,
	memory_controller_enable,
	//memory_controller_enable_a,
	//memory_controller_enable_b,
	memory_controller_write_enable,
	//memory_controller_write_enable_a,
	//memory_controller_write_enable_b,

	clk_debug,
	memory_controller_enable_debug,
	//memory_controller_enable_a_debug,
	//memory_controller_enable_b_debug,
	TRACEBUFFER_address_debug,
	//TRACEBUFFER_address_a_debug,
	//TRACEBUFFER_address_b_debug,
	memory_controller_write_enable_debug,
	//memory_controller_write_enable_a_debug,
	//memory_controller_write_enable_b_debug,

	uart_tx,
	uart_rx

);
		localparam STATES = 8;
		localparam STATE_BITS = $clog2(STATES-1);
		reg [(STATE_BITS-1):0] state;
		localparam [(STATE_BITS-1):0] 
			S_START = 0,
			S_LISTENING = 1,
			S_UART_READ_BYTE = 2,
			S_UART_WAIT_FOR_READ = 3,
			S_UART_READ_BYTE_DONE = 4,
			S_PROCESS_MSG = 5;

		//TODO: Create all states
		
		parameter FPGA_VENDOR = "Altera";
		parameter DATA_SIZE = 32;
		localparam DATA_BYTES = DATA_SIZE/8;
		localparam BYTE_CNTR_BITS = $clog2(DATA_BYTES);
		
		localparam [7:0]
        MSG_RUN = 1, 
        MSG_RESET = 2;

		input clk;
		input	reset;
		input	[6:0] TRACEBUFFER_address;
		//input	[6:0] TRACEBUFFER_address_a;
		//input	[6:0] TRACEBUFFER_address_b;
		input [(DATA_SIZE-1):0] TRACEBUFFER_out;
		//input [31:0] TRACEBUFFER_out_a;
		//input [31:0] TRACEBUFFER_out_b;
		input	memory_controller_enable;
		//input	memory_controller_enable_a;
		//input	memory_controller_enable_b;
		input	memory_controller_write_enable;
		//input	memory_controller_write_enable_a;
		//input	memory_controller_write_enable_b;
		input uart_rx;

		output clk_debug;
		output memory_controller_enable_debug;
		//output memory_controller_enable_a_debug;
		//output memory_controller_enable_b_debug;
		output [6:0] TRACEBUFFER_address_debug;
		//output [6:0] TRACEBUFFER_address_a_debug;
		//output [6:0] TRACEBUFFER_address_b_debug;
		output memory_controller_write_enable_debug;
		//output memory_controller_write_enable_a_debug;
		//output memory_controller_write_enable_b_debug;
		output uart_tx;



		//TODO: Trace Address signals are replaced with counter 

		wire	uart_read;
		wire	[7:0]	uart_readdata;
		wire	uart_readdone;
		wire	uart_dataavail;
		wire	uart_write;
		wire	[7:0]	uart_writedata;
		wire	uart_writespace;  

		assign uart_read = (state == S_UART_READ_BYTE);
		
		uart_control
				#(
				    .FPGA_VENDOR(FPGA_VENDOR)
				) 
				uart_control_inst (
				.clk(clk),
				.reset(reset),
				.read(uart_read), 
				.readdata(uart_readdata),
				.readdone(uart_readdone),
				.dataavail(uart_dataavail),
				.write(uart_write),
				.writedata(uart_writedata),
				.writespace(uart_writespace),
				.RX(uart_rx),
				.TX(uart_tx)
		);

		reg run = 0 ;

		always @ (posedge clk or posedge reset) begin
			if (reset) 
				state <= S_START;
			else 
				case (state)
					S_START:
						state <= S_LISTENING;

					/* Uart Receive */
					S_LISTENING: 
						if (uart_dataavail)
							state <= S_UART_READ_BYTE;
					S_UART_READ_BYTE:
							state <= S_UART_WAIT_FOR_READ;
					S_UART_WAIT_FOR_READ:
						if (uart_readdone)
							state <= S_UART_READ_BYTE_DONE;
					S_UART_READ_BYTE_DONE:
						state <= S_PROCESS_MSG;

					S_PROCESS_MSG: begin
						 state <= S_LISTENING;
						 case (uart_readdata)
							  MSG_RUN:
										run <= 1'b1;
							  MSG_RESET:
										run <= 1'b0;
						 endcase
					end				
				endcase
			end


    clockbuf    clockbuf_inst (
      .ena ( run | reset ),  // User circuit is a synchronous reset, so we need to enable the program clock during reset.
      .inclk ( clk ),
      .outclk ( clk_debug )
    );
	 
	 
	 localparam [(STATE_BITS-1):0]
		S_STANDBY = 0,
		S_READING = 1,
		S_WAIT_SEND_BYTE = 2,
		S_SEND_BYTE = 3,
		S_NEXT_BYTE = 4,
		S_INCR_ADD = 5;
		
	reg [(STATE_BITS-1):0] read_state = S_STANDBY;
	 
	 reg [6:0] read_addr;
	 //reg [6:0] read_addr_a;
	 //reg [6:0] read_addr_b;
	 reg [(DATA_SIZE-1):0] wr_data;
	 //reg [63:0] wr_data; //64 was used {} concat a and b
	 reg [(BYTE_CNTR_BITS-1):0] byte_cntr;
	 
	 assign uart_writedata = wr_data[7:0];
	 
	 assign uart_write = (read_state == S_SEND_BYTE);
	 
	 always @ (posedge clk or posedge reset) begin
	 
		if (reset) begin
				read_state <= S_STANDBY;
				read_addr <= 0;
				//read_addr_a <= 0;
				//read_addr_b <= 0;
			end
		else
			case (read_state)
				S_STANDBY:
					if (run) begin
							read_state <= S_READING;
							read_addr <= 0;
							//read_addr_a <= 0;
							//read_addr_b <= 0;
						end
				S_READING: begin
						read_state <= S_WAIT_SEND_BYTE;
						wr_data <= TRACEBUFFER_out;				
						//wr_data <= {TRACEBUFFER_out_a,TRACEBUFFER_out_b};				
					end
				S_WAIT_SEND_BYTE: begin
						if (uart_writespace) begin
							read_state <= S_SEND_BYTE;
						end
					end
				S_SEND_BYTE:
					read_state <= S_NEXT_BYTE;
				S_NEXT_BYTE: 
						if (byte_cntr<(DATA_BYTES-1)) begin
								wr_data <= wr_data>>8;
								read_state <= S_WAIT_SEND_BYTE;
								byte_cntr <= byte_cntr + 1'b1;
							end
						else
							read_state <= S_INCR_ADD;
				S_INCR_ADD: begin
					read_addr <= read_addr + 1'b1;
					//read_addr_a <= read_addr_a + 1'b1;
					//read_addr_b <= read_addr_b + 1'b1;
					read_state <= S_READING;
				end	
			endcase
	 end
	 

		assign memory_controller_enable_debug = run ? 1'b1 : memory_controller_enable;
		//assign memory_controller_enable_a_debug = run ? 1'b1 : memory_controller_enable_a;
		//assign memory_controller_enable_b_debug = run ? 1'b1 : memory_controller_enable_b;
		assign TRACEBUFFER_address_debug = run ? read_addr : TRACEBUFFER_address;
		//assign TRACEBUFFER_address_a_debug = run ? read_addr_a : TRACEBUFFER_address_a;
		//assign TRACEBUFFER_address_b_debug = run ? read_addr_b : TRACEBUFFER_address_b;
		assign memory_controller_write_enable_debug =	run ? 1'b0 :memory_controller_write_enable;
		//assign memory_controller_write_enable_a_debug =	run ? 1'b0 :memory_controller_write_enable_a;
		//assign memory_controller_write_enable_b_debug = run ? 1'b0 :memory_controller_write_enable_b;
	 
	 
endmodule

