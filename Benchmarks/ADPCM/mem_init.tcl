
#TO RUN FROM MODELSIM USE:
#> source /PATH/../mem_init.tcl

restart

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_wl_code_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/wl_code_table_U/adpcm_main_encode_wl_code_table_rom_U/ram
mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_wl_code_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/wl_code_table_U/adpcm_main_encode_wl_code_table_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_quantl_decis_levl_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/grp_adpcm_main_quantl_fu_1027/decis_levl_U/adpcm_main_quantl_decis_levl_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_quantl_quant26bt_neg_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/grp_adpcm_main_quantl_fu_1027/quant26bt_neg_U/adpcm_main_quantl_quant26bt_neg_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_quantl_quant26bt_pos_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/grp_adpcm_main_quantl_fu_1027/quant26bt_pos_U/adpcm_main_quantl_quant26bt_pos_rom_U/ram


mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_test_compressed_rom.mem -format hex /de2/main_inst/test_compressed_U/adpcm_main_test_compressed_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_adpcm_test_data_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/test_data_U/adpcm_main_adpcm_test_data_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_test_result_rom.mem -format hex /de2/main_inst/test_result_U/adpcm_main_test_result_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_decode_qq6_code6_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/qq6_code6_table_U/adpcm_main_decode_qq6_code6_table_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_h_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/h_U/adpcm_main_encode_h_rom_U/ram
mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_h_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/h_U/adpcm_main_encode_h_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_ilb_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/ilb_table_U/adpcm_main_encode_ilb_table_rom_U/ram
mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_ilb_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/ilb_table_U/adpcm_main_encode_ilb_table_rom_U/ram

mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_qq4_code4_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_decode_fu_448/qq4_code4_table_U/adpcm_main_encode_qq4_code4_table_rom_U/ram
mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_encode_qq4_code4_table_rom.mem -format hex /de2/main_inst/grp_adpcm_main_adpcm_fu_342/grp_adpcm_main_encode_fu_343/qq4_code4_table_U/adpcm_main_encode_qq4_code4_table_rom_U/ram

#Init to 0
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_accumc_ram.mem -format hex /de2/main_inst/accumc_U/adpcm_main_accumc_ram_U/ram
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_compressed_ram.mem -format hex /de2/main_inst/compressed_U/adpcm_main_compressed_ram_U/ram 
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_delay_bpl_ram.mem -format hex /de2/main_inst/delay_bpl_U/adpcm_main_delay_bpl_ram_U/ram 
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_delay_dltx_ram.mem -format hex /de2/main_inst/delay_dltx_U/adpcm_main_delay_dltx_ram_U/ram 
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_result_ram.mem -format hex /de2/main_inst/result_U/adpcm_main_result_ram_U/ram 
#mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_tqmf_ram.mem -format hex /de2/main_inst/tqmf_U/adpcm_main_tqmf_ram_U/ram
mem load -i D:/Jose/VerilogInst/Benchmarks/ADPCM/SynthVivadoShBufUnloadArr/adpcm_main_TRACEBUFFER_ram.mem -format hex /de2/main_inst/TRACEBUFFER_U/adpcm_main_TRACEBUFFER_ram_U/ram 


force -freeze sim:/de2/CLOCK_50 1 0, 0 {5 ps} -r 10
force -freeze sim:/de2/reset 1 0
force -freeze sim:/de2/start 0 0

run 8ns

force -freeze sim:/de2/reset 0 0

run 10ns

force -freeze sim:/de2/start 1 0

run 411.9ns

mem save -o TRACEBUFFER.mem -f binary -noaddress /de2/main_inst/TRACEBUFFER_U/adpcm_main_TRACEBUFFER_ram_U/ram