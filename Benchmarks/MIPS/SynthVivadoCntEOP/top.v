// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
mips_main mips_main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_102_Hi_102(eop_102_Hi_102_sig) ,	// output [31:0] eop_102_Hi_102_sig
	.eop_102_Hi_102_ap_vld(eop_102_Hi_102_ap_vld_sig) ,	// output  eop_102_Hi_102_ap_vld_sig
	.eop_103_Lo_103(eop_103_Lo_103_sig) ,	// output [31:0] eop_103_Lo_103_sig
	.eop_103_Lo_103_ap_vld(eop_103_Lo_103_ap_vld_sig) ,	// output  eop_103_Lo_103_ap_vld_sig
	.eop_104_pc_104(eop_104_pc_104_sig) ,	// output [31:0] eop_104_pc_104_sig
	.eop_104_pc_104_ap_vld(eop_104_pc_104_ap_vld_sig) ,	// output  eop_104_pc_104_ap_vld_sig
	.eop_121_n_inst_123(eop_121_n_inst_123_sig) ,	// output [31:0] eop_121_n_inst_123_sig
	.eop_121_n_inst_123_ap_vld(eop_121_n_inst_123_ap_vld_sig) ,	// output  eop_121_n_inst_123_ap_vld_sig
	.eop_38_main_result_124(eop_38_main_result_124_sig) ,	// output [31:0] eop_38_main_result_124_sig
	.eop_38_main_result_124_ap_vld(eop_38_main_result_124_ap_vld_sig) ,	// output  eop_38_main_result_124_ap_vld_sig
	.eop_104_pc_137(eop_104_pc_137_sig) ,	// output [31:0] eop_104_pc_137_sig
	.eop_104_pc_137_ap_vld(eop_104_pc_137_ap_vld_sig) ,	// output  eop_104_pc_137_ap_vld_sig
	.eop_114_funct_149(eop_114_funct_149_sig) ,	// output [31:0] eop_114_funct_149_sig
	.eop_114_funct_149_ap_vld(eop_114_funct_149_ap_vld_sig) ,	// output  eop_114_funct_149_ap_vld_sig
	.eop_113_shamt_150(eop_113_shamt_150_sig) ,	// output [31:0] eop_113_shamt_150_sig
	.eop_113_shamt_150_ap_vld(eop_113_shamt_150_ap_vld_sig) ,	// output  eop_113_shamt_150_ap_vld_sig
	.eop_112_rd_151(eop_112_rd_151_sig) ,	// output [31:0] eop_112_rd_151_sig
	.eop_112_rd_151_ap_vld(eop_112_rd_151_ap_vld_sig) ,	// output  eop_112_rd_151_ap_vld_sig
	.eop_111_rt_152(eop_111_rt_152_sig) ,	// output [31:0] eop_111_rt_152_sig
	.eop_111_rt_152_ap_vld(eop_111_rt_152_ap_vld_sig) ,	// output  eop_111_rt_152_ap_vld_sig
	.eop_110_rs_153(eop_110_rs_153_sig) ,	// output [31:0] eop_110_rs_153_sig
	.eop_110_rs_153_ap_vld(eop_110_rs_153_ap_vld_sig) ,	// output  eop_110_rs_153_ap_vld_sig
	.eop_100_hilo_166(eop_100_hilo_166_sig) ,	// output [63:0] eop_100_hilo_166_sig
	.eop_100_hilo_166_ap_vld(eop_100_hilo_166_ap_vld_sig) ,	// output  eop_100_hilo_166_ap_vld_sig
	.eop_103_Lo_167(eop_103_Lo_167_sig) ,	// output [31:0] eop_103_Lo_167_sig
	.eop_103_Lo_167_ap_vld(eop_103_Lo_167_ap_vld_sig) ,	// output  eop_103_Lo_167_ap_vld_sig
	.eop_102_Hi_168(eop_102_Hi_168_sig) ,	// output [31:0] eop_102_Hi_168_sig
	.eop_102_Hi_168_ap_vld(eop_102_Hi_168_ap_vld_sig) ,	// output  eop_102_Hi_168_ap_vld_sig
	.eop_100_hilo_171(eop_100_hilo_171_sig) ,	// output [63:0] eop_100_hilo_171_sig
	.eop_100_hilo_171_ap_vld(eop_100_hilo_171_ap_vld_sig) ,	// output  eop_100_hilo_171_ap_vld_sig
	.eop_103_Lo_174(eop_103_Lo_174_sig) ,	// output [31:0] eop_103_Lo_174_sig
	.eop_103_Lo_174_ap_vld(eop_103_Lo_174_ap_vld_sig) ,	// output  eop_103_Lo_174_ap_vld_sig
	.eop_102_Hi_175(eop_102_Hi_175_sig) ,	// output [31:0] eop_102_Hi_175_sig
	.eop_102_Hi_175_ap_vld(eop_102_Hi_175_ap_vld_sig) ,	// output  eop_102_Hi_175_ap_vld_sig
	.eop_104_pc_215(eop_104_pc_215_sig) ,	// output [31:0] eop_104_pc_215_sig
	.eop_104_pc_215_ap_vld(eop_104_pc_215_ap_vld_sig) ,	// output  eop_104_pc_215_ap_vld_sig
	.eop_104_pc_218(eop_104_pc_218_sig) ,	// output [31:0] eop_104_pc_218_sig
	.eop_104_pc_218_ap_vld(eop_104_pc_218_ap_vld_sig) ,	// output  eop_104_pc_218_ap_vld_sig
	.eop_116_tgtadr_224(eop_116_tgtadr_224_sig) ,	// output [31:0] eop_116_tgtadr_224_sig
	.eop_116_tgtadr_224_ap_vld(eop_116_tgtadr_224_ap_vld_sig) ,	// output  eop_116_tgtadr_224_ap_vld_sig
	.eop_104_pc_225(eop_104_pc_225_sig) ,	// output [31:0] eop_104_pc_225_sig
	.eop_104_pc_225_ap_vld(eop_104_pc_225_ap_vld_sig) ,	// output  eop_104_pc_225_ap_vld_sig
	.eop_116_tgtadr_228(eop_116_tgtadr_228_sig) ,	// output [31:0] eop_116_tgtadr_228_sig
	.eop_116_tgtadr_228_ap_vld(eop_116_tgtadr_228_ap_vld_sig) ,	// output  eop_116_tgtadr_228_ap_vld_sig
	.eop_104_pc_230(eop_104_pc_230_sig) ,	// output [31:0] eop_104_pc_230_sig
	.eop_104_pc_230_ap_vld(eop_104_pc_230_ap_vld_sig) ,	// output  eop_104_pc_230_ap_vld_sig
	.eop_115_address_235(eop_115_address_235_sig) ,	// output [15:0] eop_115_address_235_sig
	.eop_115_address_235_ap_vld(eop_115_address_235_ap_vld_sig) ,	// output  eop_115_address_235_ap_vld_sig
	.eop_111_rt_236(eop_111_rt_236_sig) ,	// output [31:0] eop_111_rt_236_sig
	.eop_111_rt_236_ap_vld(eop_111_rt_236_ap_vld_sig) ,	// output  eop_111_rt_236_ap_vld_sig
	.eop_110_rs_237(eop_110_rs_237_sig) ,	// output [31:0] eop_110_rs_237_sig
	.eop_110_rs_237_ap_vld(eop_110_rs_237_ap_vld_sig) ,	// output  eop_110_rs_237_ap_vld_sig
	.eop_104_pc_267(eop_104_pc_267_sig) ,	// output [31:0] eop_104_pc_267_sig
	.eop_104_pc_267_ap_vld(eop_104_pc_267_ap_vld_sig) ,	// output  eop_104_pc_267_ap_vld_sig
	.eop_104_pc_271(eop_104_pc_271_sig) ,	// output [31:0] eop_104_pc_271_sig
	.eop_104_pc_271_ap_vld(eop_104_pc_271_ap_vld_sig) ,	// output  eop_104_pc_271_ap_vld_sig
	.eop_104_pc_275(eop_104_pc_275_sig) ,	// output [31:0] eop_104_pc_275_sig
	.eop_104_pc_275_ap_vld(eop_104_pc_275_ap_vld_sig) ,	// output  eop_104_pc_275_ap_vld_sig
	.eop_104_pc_287(eop_104_pc_287_sig) ,	// output [31:0] eop_104_pc_287_sig
	.eop_104_pc_287_ap_vld(eop_104_pc_287_ap_vld_sig) ,	// output  eop_104_pc_287_ap_vld_sig
	.eop_121_n_inst_293(eop_121_n_inst_293_sig) ,	// output [31:0] eop_121_n_inst_293_sig
	.eop_121_n_inst_293_ap_vld(eop_121_n_inst_293_ap_vld_sig) ,	// output  eop_121_n_inst_293_ap_vld_sig
	.eop_109_op_144(eop_109_op_144_sig) ,	// output [31:0] eop_109_op_144_sig
	.eop_109_op_144_ap_vld(eop_109_op_144_ap_vld_sig) ,	// output  eop_109_op_144_ap_vld_sig
	.eop_104_pc_142(eop_104_pc_142_sig) ,	// output [31:0] eop_104_pc_142_sig
	.eop_104_pc_142_ap_vld(eop_104_pc_142_ap_vld_sig) ,	// output  eop_104_pc_142_ap_vld_sig
	.eop_108_ins_141(eop_108_ins_141_sig) ,	// output [31:0] eop_108_ins_141_sig
	.eop_108_ins_141_ap_vld(eop_108_ins_141_ap_vld_sig) ,	// output  eop_108_ins_141_ap_vld_sig
	.eop_38_main_result_297(eop_38_main_result_297_sig) ,	// output [31:0] eop_38_main_result_297_sig
	.eop_38_main_result_297_ap_vld(eop_38_main_result_297_ap_vld_sig) ,	// output  eop_38_main_result_297_ap_vld_sig
	.eop_38_main_result_300(eop_38_main_result_300_sig) ,	// output [31:0] eop_38_main_result_300_sig
	.eop_38_main_result_300_ap_vld(eop_38_main_result_300_ap_vld_sig) ,	// output  eop_38_main_result_300_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 36;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_102_Hi_102_ap_vld_sig;
valid_regs[1] <= eop_103_Lo_103_ap_vld_sig;
valid_regs[2] <= eop_104_pc_104_ap_vld_sig;
valid_regs[3] <= eop_121_n_inst_123_ap_vld_sig;
valid_regs[4] <= eop_38_main_result_124_ap_vld_sig;
valid_regs[5] <= eop_104_pc_137_ap_vld_sig;
valid_regs[6] <= eop_114_funct_149_ap_vld_sig;
valid_regs[7] <= eop_113_shamt_150_ap_vld_sig;
valid_regs[8] <= eop_112_rd_151_ap_vld_sig;
valid_regs[9] <= eop_111_rt_152_ap_vld_sig;
valid_regs[10] <= eop_110_rs_153_ap_vld_sig;
valid_regs[11] <= eop_100_hilo_166_ap_vld_sig;
valid_regs[12] <= eop_103_Lo_167_ap_vld_sig;
valid_regs[13] <= eop_102_Hi_168_ap_vld_sig;
valid_regs[14] <= eop_100_hilo_171_ap_vld_sig;
valid_regs[15] <= eop_103_Lo_174_ap_vld_sig;
valid_regs[16] <= eop_102_Hi_175_ap_vld_sig;
valid_regs[17] <= eop_104_pc_215_ap_vld_sig;
valid_regs[18] <= eop_104_pc_218_ap_vld_sig;
valid_regs[19] <= eop_116_tgtadr_224_ap_vld_sig;
valid_regs[20] <= eop_104_pc_225_ap_vld_sig;
valid_regs[21] <= eop_116_tgtadr_228_ap_vld_sig;
valid_regs[22] <= eop_104_pc_230_ap_vld_sig;
valid_regs[23] <= eop_115_address_235_ap_vld_sig;
valid_regs[24] <= eop_111_rt_236_ap_vld_sig;
valid_regs[25] <= eop_110_rs_237_ap_vld_sig;
valid_regs[26] <= eop_104_pc_267_ap_vld_sig;
valid_regs[27] <= eop_104_pc_271_ap_vld_sig;
valid_regs[28] <= eop_104_pc_275_ap_vld_sig;
valid_regs[29] <= eop_104_pc_287_ap_vld_sig;
valid_regs[30] <= eop_121_n_inst_293_ap_vld_sig;
valid_regs[31] <= eop_109_op_144_ap_vld_sig;
valid_regs[32] <= eop_104_pc_142_ap_vld_sig;
valid_regs[33] <= eop_108_ins_141_ap_vld_sig;
valid_regs[34] <= eop_38_main_result_297_ap_vld_sig;
valid_regs[35] <= eop_38_main_result_300_ap_vld_sig;


end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
