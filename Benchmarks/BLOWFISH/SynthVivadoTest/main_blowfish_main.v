// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.3
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module main_blowfish_main (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        key_P_address0,
        key_P_ce0,
        key_P_we0,
        key_P_d0,
        key_P_q0,
        key_P_address1,
        key_P_ce1,
        key_P_we1,
        key_P_d1,
        key_P_q1,
        key_S_address0,
        key_S_ce0,
        key_S_we0,
        key_S_d0,
        key_S_q0,
        key_S_address1,
        key_S_ce1,
        key_S_we1,
        key_S_d1,
        key_S_q1,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 9'b1;
parameter    ap_ST_st2_fsm_1 = 9'b10;
parameter    ap_ST_st3_fsm_2 = 9'b100;
parameter    ap_ST_st4_fsm_3 = 9'b1000;
parameter    ap_ST_st5_fsm_4 = 9'b10000;
parameter    ap_ST_st6_fsm_5 = 9'b100000;
parameter    ap_ST_st7_fsm_6 = 9'b1000000;
parameter    ap_ST_st8_fsm_7 = 9'b10000000;
parameter    ap_ST_st9_fsm_8 = 9'b100000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_5 = 32'b101;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv8_0 = 8'b00000000;
parameter    ap_const_lv4_8 = 4'b1000;
parameter    ap_const_lv4_1 = 4'b1;
parameter    ap_const_lv32_1450 = 32'b1010001010000;
parameter    ap_const_lv32_FFFFFFFF = 32'b11111111111111111111111111111111;
parameter    ap_const_lv32_FFFFEBAF = 32'b11111111111111111110101110101111;
parameter    ap_const_lv32_FFFFFFD7 = 32'b11111111111111111111111111010111;
parameter    ap_const_lv7_57 = 7'b1010111;
parameter    ap_const_lv7_7F = 7'b1111111;
parameter    ap_const_lv5_0 = 5'b00000;
parameter    ap_const_lv10_0 = 10'b0000000000;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
output  [4:0] key_P_address0;
output   key_P_ce0;
output   key_P_we0;
output  [31:0] key_P_d0;
input  [31:0] key_P_q0;
output  [4:0] key_P_address1;
output   key_P_ce1;
output   key_P_we1;
output  [31:0] key_P_d1;
input  [31:0] key_P_q1;
output  [9:0] key_S_address0;
output   key_S_ce0;
output   key_S_we0;
output  [31:0] key_S_d0;
input  [31:0] key_S_q0;
output  [9:0] key_S_address1;
output   key_S_ce1;
output   key_S_we1;
output  [31:0] key_S_d1;
input  [31:0] key_S_q1;
output  [31:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[4:0] key_P_address0;
reg key_P_ce0;
reg[4:0] key_P_address1;
reg key_P_ce1;
reg[9:0] key_S_address0;
reg key_S_ce0;
reg[9:0] key_S_address1;
reg key_S_ce1;
(* fsm_encoding = "none" *) reg   [8:0] ap_CS_fsm = 9'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_27;
wire   [2:0] ukey_address0;
reg    ukey_ce0;
wire   [7:0] ukey_q0;
wire   [2:0] ukey_address1;
reg    ukey_ce1;
wire   [7:0] ukey_q1;
wire   [12:0] in_key_address0;
reg    in_key_ce0;
wire   [6:0] in_key_q0;
wire   [12:0] out_key_address0;
reg    out_key_ce0;
wire   [7:0] out_key_q0;
wire   [3:0] i_1_fu_252_p2;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_91;
wire   [6:0] i_3_fu_308_p2;
reg   [6:0] i_3_reg_425;
reg    ap_sig_cseq_ST_st4_fsm_3;
reg    ap_sig_bdd_100;
wire   [0:0] tmp_2_fu_268_p2;
wire   [31:0] k_2_fu_314_p2;
reg   [31:0] k_2_reg_430;
wire   [31:0] tmp_s_fu_326_p2;
reg   [31:0] tmp_s_reg_436;
wire   [31:0] tmp_8_fu_337_p2;
reg   [31:0] tmp_8_reg_445;
reg    ap_sig_cseq_ST_st5_fsm_4;
reg    ap_sig_bdd_118;
wire   [31:0] tmp_1_fu_343_p2;
reg   [31:0] tmp_1_reg_450;
wire   [0:0] exitcond2_fu_332_p2;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_ap_return;
reg   [31:0] num_1_reg_460;
reg    ap_sig_cseq_ST_st7_fsm_6;
reg    ap_sig_bdd_134;
wire    grp_main_BF_cfb64_encrypt_fu_229_ap_done;
wire   [31:0] j_1_fu_364_p2;
reg   [31:0] j_1_reg_468;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_147;
wire   [0:0] exitcond_fu_359_p2;
wire   [31:0] tmp_11_fu_375_p2;
reg   [31:0] tmp_11_reg_478;
reg   [5:0] indata_address0;
reg    indata_ce0;
reg    indata_we0;
wire   [6:0] indata_d0;
wire   [6:0] indata_q0;
reg   [5:0] outdata_address0;
reg    outdata_ce0;
reg    outdata_we0;
wire   [7:0] outdata_d0;
wire   [7:0] outdata_q0;
reg   [2:0] ivec_address0;
reg    ivec_ce0;
reg    ivec_we0;
reg   [7:0] ivec_d0;
wire   [7:0] ivec_q0;
wire   [2:0] ivec_address1;
reg    ivec_ce1;
reg    ivec_we1;
wire   [7:0] ivec_d1;
wire   [7:0] ivec_q1;
wire    grp_main_BF_set_key_fu_215_ap_start;
wire    grp_main_BF_set_key_fu_215_ap_done;
wire    grp_main_BF_set_key_fu_215_ap_idle;
wire    grp_main_BF_set_key_fu_215_ap_ready;
wire   [2:0] grp_main_BF_set_key_fu_215_data_address0;
wire    grp_main_BF_set_key_fu_215_data_ce0;
wire   [7:0] grp_main_BF_set_key_fu_215_data_q0;
wire   [2:0] grp_main_BF_set_key_fu_215_data_address1;
wire    grp_main_BF_set_key_fu_215_data_ce1;
wire   [7:0] grp_main_BF_set_key_fu_215_data_q1;
wire    grp_main_BF_cfb64_encrypt_fu_229_ap_start;
wire    grp_main_BF_cfb64_encrypt_fu_229_ap_idle;
wire    grp_main_BF_cfb64_encrypt_fu_229_ap_ready;
wire   [5:0] grp_main_BF_cfb64_encrypt_fu_229_in_r_address0;
wire    grp_main_BF_cfb64_encrypt_fu_229_in_r_ce0;
wire   [6:0] grp_main_BF_cfb64_encrypt_fu_229_in_r_q0;
wire   [5:0] grp_main_BF_cfb64_encrypt_fu_229_out_r_address0;
wire    grp_main_BF_cfb64_encrypt_fu_229_out_r_ce0;
wire    grp_main_BF_cfb64_encrypt_fu_229_out_r_we0;
wire   [7:0] grp_main_BF_cfb64_encrypt_fu_229_out_r_d0;
wire   [6:0] grp_main_BF_cfb64_encrypt_fu_229_length_r;
wire   [2:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_address0;
wire    grp_main_BF_cfb64_encrypt_fu_229_ivec_ce0;
wire    grp_main_BF_cfb64_encrypt_fu_229_ivec_we0;
wire   [7:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_d0;
wire   [7:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_q0;
wire   [2:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_address1;
wire    grp_main_BF_cfb64_encrypt_fu_229_ivec_ce1;
wire    grp_main_BF_cfb64_encrypt_fu_229_ivec_we1;
wire   [7:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_d1;
wire   [7:0] grp_main_BF_cfb64_encrypt_fu_229_ivec_q1;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_num_read;
wire   [9:0] grp_main_BF_cfb64_encrypt_fu_229_key_S_address0;
wire    grp_main_BF_cfb64_encrypt_fu_229_key_S_ce0;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_key_S_q0;
wire   [9:0] grp_main_BF_cfb64_encrypt_fu_229_key_S_address1;
wire    grp_main_BF_cfb64_encrypt_fu_229_key_S_ce1;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_key_S_q1;
wire   [4:0] grp_main_BF_cfb64_encrypt_fu_229_key_P_address0;
wire    grp_main_BF_cfb64_encrypt_fu_229_key_P_ce0;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_key_P_q0;
wire   [4:0] grp_main_BF_cfb64_encrypt_fu_229_key_P_address1;
wire    grp_main_BF_cfb64_encrypt_fu_229_key_P_ce1;
wire   [31:0] grp_main_BF_cfb64_encrypt_fu_229_key_P_q1;
reg   [3:0] i_reg_125;
wire   [0:0] exitcond1_fu_246_p2;
reg   [31:0] num_reg_136;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_257;
reg   [31:0] l_reg_148;
reg   [31:0] k_reg_160;
reg   [31:0] k_1_reg_172;
reg    ap_sig_cseq_ST_st6_fsm_5;
reg    ap_sig_bdd_275;
reg   [31:0] i_2_reg_182;
reg   [31:0] l_1_reg_194;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_286;
reg   [31:0] j_reg_204;
reg    grp_main_BF_set_key_fu_215_ap_start_ap_start_reg = 1'b0;
reg    grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg = 1'b0;
wire   [63:0] tmp_fu_258_p1;
wire  signed [63:0] tmp_7_fu_349_p1;
wire  signed [63:0] tmp_4_fu_354_p1;
wire  signed [63:0] tmp_10_fu_370_p1;
wire  signed [63:0] tmp_12_fu_381_p1;
reg   [31:0] check_fu_62;
wire   [31:0] check_1_fu_396_p2;
wire   [31:0] tmp_5_fu_280_p2;
wire   [0:0] tmp_6_fu_290_p2;
wire   [6:0] tmp_15_fu_286_p1;
wire   [6:0] umax_fu_296_p3;
wire   [31:0] tmp_3_fu_274_p2;
wire  signed [31:0] umax_cast_fu_304_p1;
wire   [31:0] tmp_9_fu_320_p2;
wire   [0:0] tmp_13_fu_386_p2;
wire   [31:0] tmp_14_fu_392_p1;
reg   [8:0] ap_NS_fsm;


main_blowfish_main_ukey #(
    .DataWidth( 8 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
ukey_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( ukey_address0 ),
    .ce0( ukey_ce0 ),
    .q0( ukey_q0 ),
    .address1( ukey_address1 ),
    .ce1( ukey_ce1 ),
    .q1( ukey_q1 )
);

main_blowfish_main_in_key #(
    .DataWidth( 7 ),
    .AddressRange( 5200 ),
    .AddressWidth( 13 ))
in_key_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( in_key_address0 ),
    .ce0( in_key_ce0 ),
    .q0( in_key_q0 )
);

main_blowfish_main_out_key #(
    .DataWidth( 8 ),
    .AddressRange( 5200 ),
    .AddressWidth( 13 ))
out_key_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( out_key_address0 ),
    .ce0( out_key_ce0 ),
    .q0( out_key_q0 )
);

main_blowfish_main_indata #(
    .DataWidth( 7 ),
    .AddressRange( 40 ),
    .AddressWidth( 6 ))
indata_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( indata_address0 ),
    .ce0( indata_ce0 ),
    .we0( indata_we0 ),
    .d0( indata_d0 ),
    .q0( indata_q0 )
);

main_blowfish_main_outdata #(
    .DataWidth( 8 ),
    .AddressRange( 40 ),
    .AddressWidth( 6 ))
outdata_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( outdata_address0 ),
    .ce0( outdata_ce0 ),
    .we0( outdata_we0 ),
    .d0( outdata_d0 ),
    .q0( outdata_q0 )
);

main_blowfish_main_ivec #(
    .DataWidth( 8 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
ivec_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( ivec_address0 ),
    .ce0( ivec_ce0 ),
    .we0( ivec_we0 ),
    .d0( ivec_d0 ),
    .q0( ivec_q0 ),
    .address1( ivec_address1 ),
    .ce1( ivec_ce1 ),
    .we1( ivec_we1 ),
    .d1( ivec_d1 ),
    .q1( ivec_q1 )
);

main_BF_set_key grp_main_BF_set_key_fu_215(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_main_BF_set_key_fu_215_ap_start ),
    .ap_done( grp_main_BF_set_key_fu_215_ap_done ),
    .ap_idle( grp_main_BF_set_key_fu_215_ap_idle ),
    .ap_ready( grp_main_BF_set_key_fu_215_ap_ready ),
    .data_address0( grp_main_BF_set_key_fu_215_data_address0 ),
    .data_ce0( grp_main_BF_set_key_fu_215_data_ce0 ),
    .data_q0( grp_main_BF_set_key_fu_215_data_q0 ),
    .data_address1( grp_main_BF_set_key_fu_215_data_address1 ),
    .data_ce1( grp_main_BF_set_key_fu_215_data_ce1 ),
    .data_q1( grp_main_BF_set_key_fu_215_data_q1 )
);

main_BF_cfb64_encrypt grp_main_BF_cfb64_encrypt_fu_229(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_main_BF_cfb64_encrypt_fu_229_ap_start ),
    .ap_done( grp_main_BF_cfb64_encrypt_fu_229_ap_done ),
    .ap_idle( grp_main_BF_cfb64_encrypt_fu_229_ap_idle ),
    .ap_ready( grp_main_BF_cfb64_encrypt_fu_229_ap_ready ),
    .in_r_address0( grp_main_BF_cfb64_encrypt_fu_229_in_r_address0 ),
    .in_r_ce0( grp_main_BF_cfb64_encrypt_fu_229_in_r_ce0 ),
    .in_r_q0( grp_main_BF_cfb64_encrypt_fu_229_in_r_q0 ),
    .out_r_address0( grp_main_BF_cfb64_encrypt_fu_229_out_r_address0 ),
    .out_r_ce0( grp_main_BF_cfb64_encrypt_fu_229_out_r_ce0 ),
    .out_r_we0( grp_main_BF_cfb64_encrypt_fu_229_out_r_we0 ),
    .out_r_d0( grp_main_BF_cfb64_encrypt_fu_229_out_r_d0 ),
    .length_r( grp_main_BF_cfb64_encrypt_fu_229_length_r ),
    .ivec_address0( grp_main_BF_cfb64_encrypt_fu_229_ivec_address0 ),
    .ivec_ce0( grp_main_BF_cfb64_encrypt_fu_229_ivec_ce0 ),
    .ivec_we0( grp_main_BF_cfb64_encrypt_fu_229_ivec_we0 ),
    .ivec_d0( grp_main_BF_cfb64_encrypt_fu_229_ivec_d0 ),
    .ivec_q0( grp_main_BF_cfb64_encrypt_fu_229_ivec_q0 ),
    .ivec_address1( grp_main_BF_cfb64_encrypt_fu_229_ivec_address1 ),
    .ivec_ce1( grp_main_BF_cfb64_encrypt_fu_229_ivec_ce1 ),
    .ivec_we1( grp_main_BF_cfb64_encrypt_fu_229_ivec_we1 ),
    .ivec_d1( grp_main_BF_cfb64_encrypt_fu_229_ivec_d1 ),
    .ivec_q1( grp_main_BF_cfb64_encrypt_fu_229_ivec_q1 ),
    .num_read( grp_main_BF_cfb64_encrypt_fu_229_num_read ),
    .key_S_address0( grp_main_BF_cfb64_encrypt_fu_229_key_S_address0 ),
    .key_S_ce0( grp_main_BF_cfb64_encrypt_fu_229_key_S_ce0 ),
    .key_S_q0( grp_main_BF_cfb64_encrypt_fu_229_key_S_q0 ),
    .key_S_address1( grp_main_BF_cfb64_encrypt_fu_229_key_S_address1 ),
    .key_S_ce1( grp_main_BF_cfb64_encrypt_fu_229_key_S_ce1 ),
    .key_S_q1( grp_main_BF_cfb64_encrypt_fu_229_key_S_q1 ),
    .key_P_address0( grp_main_BF_cfb64_encrypt_fu_229_key_P_address0 ),
    .key_P_ce0( grp_main_BF_cfb64_encrypt_fu_229_key_P_ce0 ),
    .key_P_q0( grp_main_BF_cfb64_encrypt_fu_229_key_P_q0 ),
    .key_P_address1( grp_main_BF_cfb64_encrypt_fu_229_key_P_address1 ),
    .key_P_ce1( grp_main_BF_cfb64_encrypt_fu_229_key_P_ce1 ),
    .key_P_q1( grp_main_BF_cfb64_encrypt_fu_229_key_P_q1 ),
    .ap_return( grp_main_BF_cfb64_encrypt_fu_229_ap_return )
);



/// the current state (ap_CS_fsm) of the state machine. ///
always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

/// grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg assign process. ///
always @ (posedge ap_clk) begin : ap_ret_grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) & ~(ap_const_lv1_0 == exitcond2_fu_332_p2))) begin
            grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_main_BF_cfb64_encrypt_fu_229_ap_ready)) begin
            grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

/// grp_main_BF_set_key_fu_215_ap_start_ap_start_reg assign process. ///
always @ (posedge ap_clk) begin : ap_ret_grp_main_BF_set_key_fu_215_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_main_BF_set_key_fu_215_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(ap_const_lv1_0 == exitcond1_fu_246_p2))) begin
            grp_main_BF_set_key_fu_215_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_main_BF_set_key_fu_215_ap_ready)) begin
            grp_main_BF_set_key_fu_215_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        check_fu_62 <= check_1_fu_396_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(ap_const_lv1_0 == exitcond1_fu_246_p2))) begin
        check_fu_62 <= ap_const_lv32_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(tmp_2_fu_268_p2 == ap_const_lv1_0))) begin
        i_2_reg_182 <= ap_const_lv32_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        i_2_reg_182 <= tmp_8_reg_445;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (ap_const_lv1_0 == exitcond1_fu_246_p2))) begin
        i_reg_125 <= i_1_fu_252_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        i_reg_125 <= ap_const_lv4_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        j_reg_204 <= j_1_reg_468;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(ap_const_logic_0 == grp_main_BF_cfb64_encrypt_fu_229_ap_done))) begin
        j_reg_204 <= ap_const_lv32_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(tmp_2_fu_268_p2 == ap_const_lv1_0))) begin
        k_1_reg_172 <= k_reg_160;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        k_1_reg_172 <= tmp_1_reg_450;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & ~(ap_const_lv1_0 == exitcond_fu_359_p2))) begin
        k_reg_160 <= k_2_reg_430;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ~(ap_const_logic_0 == grp_main_BF_set_key_fu_215_ap_done))) begin
        k_reg_160 <= ap_const_lv32_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        l_1_reg_194 <= tmp_11_reg_478;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(ap_const_logic_0 == grp_main_BF_cfb64_encrypt_fu_229_ap_done))) begin
        l_1_reg_194 <= l_reg_148;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & ~(ap_const_lv1_0 == exitcond_fu_359_p2))) begin
        l_reg_148 <= tmp_s_reg_436;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ~(ap_const_logic_0 == grp_main_BF_set_key_fu_215_ap_done))) begin
        l_reg_148 <= ap_const_lv32_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & ~(ap_const_lv1_0 == exitcond_fu_359_p2))) begin
        num_reg_136 <= num_1_reg_460;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ~(ap_const_logic_0 == grp_main_BF_set_key_fu_215_ap_done))) begin
        num_reg_136 <= ap_const_lv32_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(tmp_2_fu_268_p2 == ap_const_lv1_0))) begin
        i_3_reg_425 <= i_3_fu_308_p2;
        k_2_reg_430 <= k_2_fu_314_p2;
        tmp_s_reg_436 <= tmp_s_fu_326_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        j_1_reg_468 <= j_1_fu_364_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) & ~(ap_const_logic_0 == grp_main_BF_cfb64_encrypt_fu_229_ap_done))) begin
        num_1_reg_460 <= grp_main_BF_cfb64_encrypt_fu_229_ap_return;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7) & (ap_const_lv1_0 == exitcond_fu_359_p2))) begin
        tmp_11_reg_478 <= tmp_11_fu_375_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) & (ap_const_lv1_0 == exitcond2_fu_332_p2))) begin
        tmp_1_reg_450 <= tmp_1_fu_343_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        tmp_8_reg_445 <= tmp_8_fu_337_p2;
    end
end

/// ap_done assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st4_fsm_3 or tmp_2_fu_268_p2) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & (tmp_2_fu_268_p2 == ap_const_lv1_0)))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

/// ap_idle assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

/// ap_ready assign process. ///
always @ (ap_sig_cseq_ST_st4_fsm_3 or tmp_2_fu_268_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & (tmp_2_fu_268_p2 == ap_const_lv1_0))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st1_fsm_0 assign process. ///
always @ (ap_sig_bdd_27) begin
    if (ap_sig_bdd_27) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st2_fsm_1 assign process. ///
always @ (ap_sig_bdd_91) begin
    if (ap_sig_bdd_91) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st3_fsm_2 assign process. ///
always @ (ap_sig_bdd_257) begin
    if (ap_sig_bdd_257) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st4_fsm_3 assign process. ///
always @ (ap_sig_bdd_100) begin
    if (ap_sig_bdd_100) begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st5_fsm_4 assign process. ///
always @ (ap_sig_bdd_118) begin
    if (ap_sig_bdd_118) begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st6_fsm_5 assign process. ///
always @ (ap_sig_bdd_275) begin
    if (ap_sig_bdd_275) begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st7_fsm_6 assign process. ///
always @ (ap_sig_bdd_134) begin
    if (ap_sig_bdd_134) begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st8_fsm_7 assign process. ///
always @ (ap_sig_bdd_147) begin
    if (ap_sig_bdd_147) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st9_fsm_8 assign process. ///
always @ (ap_sig_bdd_286) begin
    if (ap_sig_bdd_286) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

/// in_key_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st5_fsm_4) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        in_key_ce0 = ap_const_logic_1;
    end else begin
        in_key_ce0 = ap_const_logic_0;
    end
end

/// indata_address0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_in_r_address0 or ap_sig_cseq_ST_st6_fsm_5 or tmp_4_fu_354_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        indata_address0 = tmp_4_fu_354_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        indata_address0 = grp_main_BF_cfb64_encrypt_fu_229_in_r_address0;
    end else begin
        indata_address0 = 'bx;
    end
end

/// indata_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_in_r_ce0 or ap_sig_cseq_ST_st6_fsm_5) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        indata_ce0 = ap_const_logic_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        indata_ce0 = grp_main_BF_cfb64_encrypt_fu_229_in_r_ce0;
    end else begin
        indata_ce0 = ap_const_logic_0;
    end
end

/// indata_we0 assign process. ///
always @ (ap_sig_cseq_ST_st6_fsm_5) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        indata_we0 = ap_const_logic_1;
    end else begin
        indata_we0 = ap_const_logic_0;
    end
end

/// ivec_address0 assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_address0 or tmp_fu_258_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        ivec_address0 = tmp_fu_258_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_address0 = grp_main_BF_cfb64_encrypt_fu_229_ivec_address0;
    end else begin
        ivec_address0 = 'bx;
    end
end

/// ivec_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_ce0) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        ivec_ce0 = ap_const_logic_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_ce0 = grp_main_BF_cfb64_encrypt_fu_229_ivec_ce0;
    end else begin
        ivec_ce0 = ap_const_logic_0;
    end
end

/// ivec_ce1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_ce1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_ce1 = grp_main_BF_cfb64_encrypt_fu_229_ivec_ce1;
    end else begin
        ivec_ce1 = ap_const_logic_0;
    end
end

/// ivec_d0 assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_d0) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        ivec_d0 = ap_const_lv8_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_d0 = grp_main_BF_cfb64_encrypt_fu_229_ivec_d0;
    end else begin
        ivec_d0 = 'bx;
    end
end

/// ivec_we0 assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_we0 or exitcond1_fu_246_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (ap_const_lv1_0 == exitcond1_fu_246_p2))) begin
        ivec_we0 = ap_const_logic_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_we0 = grp_main_BF_cfb64_encrypt_fu_229_ivec_we0;
    end else begin
        ivec_we0 = ap_const_logic_0;
    end
end

/// ivec_we1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_ivec_we1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        ivec_we1 = grp_main_BF_cfb64_encrypt_fu_229_ivec_we1;
    end else begin
        ivec_we1 = ap_const_logic_0;
    end
end

/// key_P_address0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_P_address0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_P_address0 = grp_main_BF_cfb64_encrypt_fu_229_key_P_address0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_P_address0 = ap_const_lv5_0;
    end else begin
        key_P_address0 = 'bx;
    end
end

/// key_P_address1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_P_address1 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_P_address1 = grp_main_BF_cfb64_encrypt_fu_229_key_P_address1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_P_address1 = ap_const_lv5_0;
    end else begin
        key_P_address1 = 'bx;
    end
end

/// key_P_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_P_ce0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_P_ce0 = grp_main_BF_cfb64_encrypt_fu_229_key_P_ce0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_P_ce0 = ap_const_logic_0;
    end else begin
        key_P_ce0 = ap_const_logic_0;
    end
end

/// key_P_ce1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_P_ce1 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_P_ce1 = grp_main_BF_cfb64_encrypt_fu_229_key_P_ce1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_P_ce1 = ap_const_logic_0;
    end else begin
        key_P_ce1 = ap_const_logic_0;
    end
end

/// key_S_address0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_S_address0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_S_address0 = grp_main_BF_cfb64_encrypt_fu_229_key_S_address0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_S_address0 = ap_const_lv10_0;
    end else begin
        key_S_address0 = 'bx;
    end
end

/// key_S_address1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_S_address1 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_S_address1 = grp_main_BF_cfb64_encrypt_fu_229_key_S_address1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_S_address1 = ap_const_lv10_0;
    end else begin
        key_S_address1 = 'bx;
    end
end

/// key_S_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_S_ce0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_S_ce0 = grp_main_BF_cfb64_encrypt_fu_229_key_S_ce0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_S_ce0 = ap_const_logic_0;
    end else begin
        key_S_ce0 = ap_const_logic_0;
    end
end

/// key_S_ce1 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_key_S_ce1 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        key_S_ce1 = grp_main_BF_cfb64_encrypt_fu_229_key_S_ce1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        key_S_ce1 = ap_const_logic_0;
    end else begin
        key_S_ce1 = ap_const_logic_0;
    end
end

/// out_key_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st8_fsm_7) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        out_key_ce0 = ap_const_logic_1;
    end else begin
        out_key_ce0 = ap_const_logic_0;
    end
end

/// outdata_address0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or grp_main_BF_cfb64_encrypt_fu_229_out_r_address0 or tmp_10_fu_370_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        outdata_address0 = tmp_10_fu_370_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        outdata_address0 = grp_main_BF_cfb64_encrypt_fu_229_out_r_address0;
    end else begin
        outdata_address0 = 'bx;
    end
end

/// outdata_ce0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st8_fsm_7 or grp_main_BF_cfb64_encrypt_fu_229_out_r_ce0) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        outdata_ce0 = ap_const_logic_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        outdata_ce0 = grp_main_BF_cfb64_encrypt_fu_229_out_r_ce0;
    end else begin
        outdata_ce0 = ap_const_logic_0;
    end
end

/// outdata_we0 assign process. ///
always @ (ap_sig_cseq_ST_st7_fsm_6 or grp_main_BF_cfb64_encrypt_fu_229_out_r_we0) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        outdata_we0 = grp_main_BF_cfb64_encrypt_fu_229_out_r_we0;
    end else begin
        outdata_we0 = ap_const_logic_0;
    end
end

/// ukey_ce0 assign process. ///
always @ (grp_main_BF_set_key_fu_215_data_ce0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        ukey_ce0 = grp_main_BF_set_key_fu_215_data_ce0;
    end else begin
        ukey_ce0 = ap_const_logic_0;
    end
end

/// ukey_ce1 assign process. ///
always @ (grp_main_BF_set_key_fu_215_data_ce1 or ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        ukey_ce1 = grp_main_BF_set_key_fu_215_data_ce1;
    end else begin
        ukey_ce1 = ap_const_logic_0;
    end
end
/// the next state (ap_NS_fsm) of the state machine. ///
always @ (ap_start or ap_CS_fsm or tmp_2_fu_268_p2 or exitcond2_fu_332_p2 or grp_main_BF_cfb64_encrypt_fu_229_ap_done or exitcond_fu_359_p2 or grp_main_BF_set_key_fu_215_ap_done or exitcond1_fu_246_p2) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if ((ap_const_lv1_0 == exitcond1_fu_246_p2)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end
        end
        ap_ST_st3_fsm_2 : 
        begin
            if (~(ap_const_logic_0 == grp_main_BF_set_key_fu_215_ap_done)) begin
                ap_NS_fsm = ap_ST_st4_fsm_3;
            end else begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end
        end
        ap_ST_st4_fsm_3 : 
        begin
            if ((tmp_2_fu_268_p2 == ap_const_lv1_0)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st5_fsm_4;
            end
        end
        ap_ST_st5_fsm_4 : 
        begin
            if (~(ap_const_lv1_0 == exitcond2_fu_332_p2)) begin
                ap_NS_fsm = ap_ST_st7_fsm_6;
            end else begin
                ap_NS_fsm = ap_ST_st6_fsm_5;
            end
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st5_fsm_4;
        end
        ap_ST_st7_fsm_6 : 
        begin
            if (~(ap_const_logic_0 == grp_main_BF_cfb64_encrypt_fu_229_ap_done)) begin
                ap_NS_fsm = ap_ST_st8_fsm_7;
            end else begin
                ap_NS_fsm = ap_ST_st7_fsm_6;
            end
        end
        ap_ST_st8_fsm_7 : 
        begin
            if ((ap_const_lv1_0 == exitcond_fu_359_p2)) begin
                ap_NS_fsm = ap_ST_st9_fsm_8;
            end else begin
                ap_NS_fsm = ap_ST_st4_fsm_3;
            end
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_return = check_fu_62;

/// ap_sig_bdd_100 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_100 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end

/// ap_sig_bdd_118 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_118 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end

/// ap_sig_bdd_134 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_134 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_6]);
end

/// ap_sig_bdd_147 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_147 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end

/// ap_sig_bdd_257 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_257 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end

/// ap_sig_bdd_27 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_27 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end

/// ap_sig_bdd_275 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_275 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_5]);
end

/// ap_sig_bdd_286 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_286 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end

/// ap_sig_bdd_91 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_91 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end
assign check_1_fu_396_p2 = (tmp_14_fu_392_p1 + check_fu_62);
assign exitcond1_fu_246_p2 = (i_reg_125 == ap_const_lv4_8? 1'b1: 1'b0);
assign exitcond2_fu_332_p2 = (k_1_reg_172 == k_2_reg_430? 1'b1: 1'b0);
assign exitcond_fu_359_p2 = (l_1_reg_194 == tmp_s_reg_436? 1'b1: 1'b0);
assign grp_main_BF_cfb64_encrypt_fu_229_ap_start = grp_main_BF_cfb64_encrypt_fu_229_ap_start_ap_start_reg;
assign grp_main_BF_cfb64_encrypt_fu_229_in_r_q0 = indata_q0;
assign grp_main_BF_cfb64_encrypt_fu_229_ivec_q0 = ivec_q0;
assign grp_main_BF_cfb64_encrypt_fu_229_ivec_q1 = ivec_q1;
assign grp_main_BF_cfb64_encrypt_fu_229_key_P_q0 = key_P_q0;
assign grp_main_BF_cfb64_encrypt_fu_229_key_P_q1 = key_P_q1;
assign grp_main_BF_cfb64_encrypt_fu_229_key_S_q0 = key_S_q0;
assign grp_main_BF_cfb64_encrypt_fu_229_key_S_q1 = key_S_q1;
assign grp_main_BF_cfb64_encrypt_fu_229_length_r = i_3_reg_425;
assign grp_main_BF_cfb64_encrypt_fu_229_num_read = num_reg_136;
assign grp_main_BF_set_key_fu_215_ap_start = grp_main_BF_set_key_fu_215_ap_start_ap_start_reg;
assign grp_main_BF_set_key_fu_215_data_q0 = ukey_q0;
assign grp_main_BF_set_key_fu_215_data_q1 = ukey_q1;
assign i_1_fu_252_p2 = (i_reg_125 + ap_const_lv4_1);
assign i_3_fu_308_p2 = (umax_fu_296_p3 ^ ap_const_lv7_7F);
assign in_key_address0 = tmp_7_fu_349_p1;
assign indata_d0 = in_key_q0;
assign ivec_address1 = grp_main_BF_cfb64_encrypt_fu_229_ivec_address1;
assign ivec_d1 = grp_main_BF_cfb64_encrypt_fu_229_ivec_d1;
assign j_1_fu_364_p2 = (j_reg_204 + ap_const_lv32_1);
assign k_2_fu_314_p2 = ($signed(tmp_3_fu_274_p2) - $signed(umax_cast_fu_304_p1));
assign key_P_d0 = ap_const_lv32_0;
assign key_P_d1 = ap_const_lv32_0;
assign key_P_we0 = ap_const_logic_0;
assign key_P_we1 = ap_const_logic_0;
assign key_S_d0 = ap_const_lv32_0;
assign key_S_d1 = ap_const_lv32_0;
assign key_S_we0 = ap_const_logic_0;
assign key_S_we1 = ap_const_logic_0;
assign out_key_address0 = tmp_12_fu_381_p1;
assign outdata_d0 = grp_main_BF_cfb64_encrypt_fu_229_out_r_d0;
assign tmp_10_fu_370_p1 = $signed(j_reg_204);
assign tmp_11_fu_375_p2 = (l_1_reg_194 + ap_const_lv32_1);
assign tmp_12_fu_381_p1 = $signed(l_1_reg_194);
assign tmp_13_fu_386_p2 = (outdata_q0 == out_key_q0? 1'b1: 1'b0);
assign tmp_14_fu_392_p1 = tmp_13_fu_386_p2;
assign tmp_15_fu_286_p1 = tmp_5_fu_280_p2[6:0];
assign tmp_1_fu_343_p2 = (k_1_reg_172 + ap_const_lv32_1);
assign tmp_2_fu_268_p2 = ($signed(k_reg_160) < $signed(32'b1010001010000)? 1'b1: 1'b0);
assign tmp_3_fu_274_p2 = ($signed(ap_const_lv32_FFFFFFFF) + $signed(k_reg_160));
assign tmp_4_fu_354_p1 = $signed(i_2_reg_182);
assign tmp_5_fu_280_p2 = ($signed(ap_const_lv32_FFFFEBAF) + $signed(k_reg_160));
assign tmp_6_fu_290_p2 = (tmp_5_fu_280_p2 > ap_const_lv32_FFFFFFD7? 1'b1: 1'b0);
assign tmp_7_fu_349_p1 = $signed(k_1_reg_172);
assign tmp_8_fu_337_p2 = (i_2_reg_182 + ap_const_lv32_1);
assign tmp_9_fu_320_p2 = ($signed(ap_const_lv32_FFFFFFFF) + $signed(l_reg_148));
assign tmp_fu_258_p1 = i_reg_125;
assign tmp_s_fu_326_p2 = ($signed(tmp_9_fu_320_p2) - $signed(umax_cast_fu_304_p1));
assign ukey_address0 = grp_main_BF_set_key_fu_215_data_address0;
assign ukey_address1 = grp_main_BF_set_key_fu_215_data_address1;
assign umax_cast_fu_304_p1 = $signed(umax_fu_296_p3);
assign umax_fu_296_p3 = ((tmp_6_fu_290_p2[0:0] === 1'b1) ? tmp_15_fu_286_p1 : ap_const_lv7_57);


endmodule //main_blowfish_main

