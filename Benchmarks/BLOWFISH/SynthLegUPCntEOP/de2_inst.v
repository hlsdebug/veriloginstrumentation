// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.


// Generated by Quartus II 64-Bit Version 13.0 (Build Build 232 06/12/2013)
// Created on Mon Oct 26 18:41:58 2015

de2 de2_inst
(
	.CLOCK_50(CLOCK_50_sig) ,	// input  CLOCK_50_sig
	.KEY(KEY_sig) ,	// input [3:0] KEY_sig
	.SW(SW_sig) ,	// input [17:0] SW_sig
	.HEX0(HEX0_sig) ,	// output [6:0] HEX0_sig
	.HEX1(HEX1_sig) ,	// output [6:0] HEX1_sig
	.HEX2(HEX2_sig) ,	// output [6:0] HEX2_sig
	.HEX3(HEX3_sig) ,	// output [6:0] HEX3_sig
	.HEX4(HEX4_sig) ,	// output [6:0] HEX4_sig
	.HEX5(HEX5_sig) ,	// output [6:0] HEX5_sig
	.HEX6(HEX6_sig) ,	// output [6:0] HEX6_sig
	.HEX7(HEX7_sig) ,	// output [6:0] HEX7_sig
	.LEDG(LEDG_sig) ,	// output [7:0] LEDG_sig
	.UART_RXD(UART_RXD_sig) ,	// input  UART_RXD_sig
	.UART_TXD(UART_TXD_sig) 	// output  UART_TXD_sig
);

defparam de2_inst.s_WAIT = 'b001;
defparam de2_inst.s_START = 'b010;
defparam de2_inst.s_EXE = 'b011;
defparam de2_inst.s_DONE = 'b100;
