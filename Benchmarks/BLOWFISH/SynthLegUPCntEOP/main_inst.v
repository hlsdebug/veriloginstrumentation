// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.


// Generated by Quartus II 64-Bit Version 13.0 (Build Build 232 06/12/2013)
// Created on Mon Oct 26 18:49:07 2015

main main_inst
(
	.clk(clk_sig) ,	// input  clk_sig
	.clk2x(clk2x_sig) ,	// input  clk2x_sig
	.clk1x_follower(clk1x_follower_sig) ,	// input  clk1x_follower_sig
	.reset(reset_sig) ,	// input  reset_sig
	.start(start_sig) ,	// input  start_sig
	.finish(finish_sig) ,	// output  finish_sig
	.memory_controller_waitrequest(memory_controller_waitrequest_sig) ,	// input  memory_controller_waitrequest_sig
	.memory_controller_enable_a(memory_controller_enable_a_sig) ,	// output  memory_controller_enable_a_sig
	.memory_controller_address_a(memory_controller_address_a_sig) ,	// output [31:0] memory_controller_address_a_sig
	.memory_controller_write_enable_a(memory_controller_write_enable_a_sig) ,	// output  memory_controller_write_enable_a_sig
	.memory_controller_in_a(memory_controller_in_a_sig) ,	// output [63:0] memory_controller_in_a_sig
	.memory_controller_size_a(memory_controller_size_a_sig) ,	// output [1:0] memory_controller_size_a_sig
	.memory_controller_out_a(memory_controller_out_a_sig) ,	// input [63:0] memory_controller_out_a_sig
	.memory_controller_enable_b(memory_controller_enable_b_sig) ,	// output  memory_controller_enable_b_sig
	.memory_controller_address_b(memory_controller_address_b_sig) ,	// output [31:0] memory_controller_address_b_sig
	.memory_controller_write_enable_b(memory_controller_write_enable_b_sig) ,	// output  memory_controller_write_enable_b_sig
	.memory_controller_in_b(memory_controller_in_b_sig) ,	// output [63:0] memory_controller_in_b_sig
	.memory_controller_size_b(memory_controller_size_b_sig) ,	// output [1:0] memory_controller_size_b_sig
	.memory_controller_out_b(memory_controller_out_b_sig) ,	// input [63:0] memory_controller_out_b_sig
	.eop_88_l_91(eop_88_l_91_sig) ,	// output [31:0] eop_88_l_91_sig
	.eop_88_l_91_valid(eop_88_l_91_valid_sig) ,	// output  eop_88_l_91_valid_sig
	.eop_88_r_92(eop_88_r_92_sig) ,	// output [31:0] eop_88_r_92_sig
	.eop_88_r_92_valid(eop_88_r_92_valid_sig) ,	// output  eop_88_r_92_valid_sig
	.return_val(return_val_sig) ,	// output [31:0] return_val_sig
	.eop_858_main_result_860(eop_858_main_result_860_sig) ,	// output [31:0] eop_858_main_result_860_sig
	.eop_858_main_result_860_valid(eop_858_main_result_860_valid_sig) ,	// output  eop_858_main_result_860_valid_sig
	.eop_823_num_828(eop_823_num_828_sig) ,	// output [31:0] eop_823_num_828_sig
	.eop_823_num_828_valid(eop_823_num_828_valid_sig) ,	// output  eop_823_num_828_valid_sig
	.eop_824_k_829(eop_824_k_829_sig) ,	// output [31:0] eop_824_k_829_sig
	.eop_824_k_829_valid(eop_824_k_829_valid_sig) ,	// output  eop_824_k_829_valid_sig
	.eop_824_l_830(eop_824_l_830_sig) ,	// output [31:0] eop_824_l_830_sig
	.eop_824_l_830_valid(eop_824_l_830_valid_sig) ,	// output  eop_824_l_830_valid_sig
	.eop_825_encordec_831(eop_825_encordec_831_sig) ,	// output [31:0] eop_825_encordec_831_sig
	.eop_825_encordec_831_valid(eop_825_encordec_831_valid_sig) ,	// output  eop_825_encordec_831_valid_sig
	.eop_826_check_832(eop_826_check_832_sig) ,	// output [31:0] eop_826_check_832_sig
	.eop_826_check_832_valid(eop_826_check_832_valid_sig) ,	// output  eop_826_check_832_valid_sig
	.eop_81_n_89(eop_81_n_89_sig) ,	// output [31:0] eop_81_n_89_sig
	.eop_81_n_89_valid(eop_81_n_89_valid_sig) ,	// output  eop_81_n_89_valid_sig
	.eop_101_ri_115(eop_101_ri_115_sig) ,	// output [31:0] eop_101_ri_115_sig
	.eop_101_ri_115_valid(eop_101_ri_115_valid_sig) ,	// output  eop_101_ri_115_valid_sig
	.eop_824_i_839(eop_824_i_839_sig) ,	// output [31:0] eop_824_i_839_sig
	.eop_824_i_839_valid(eop_824_i_839_valid_sig) ,	// output  eop_824_i_839_valid_sig
	.eop_93_n_98(eop_93_n_98_sig) ,	// output [31:0] eop_93_n_98_sig
	.eop_93_n_98_valid(eop_93_n_98_valid_sig) ,	// output  eop_93_n_98_valid_sig
	.eop_94_l_99(eop_94_l_99_sig) ,	// output [31:0] eop_94_l_99_sig
	.eop_94_l_99_valid(eop_94_l_99_valid_sig) ,	// output  eop_94_l_99_valid_sig
	.eop_94_l_103(eop_94_l_103_sig) ,	// output [31:0] eop_94_l_103_sig
	.eop_94_l_103_valid(eop_94_l_103_valid_sig) ,	// output  eop_94_l_103_valid_sig
	.eop_96_cc_151(eop_96_cc_151_sig) ,	// output [7:0] eop_96_cc_151_sig
	.eop_96_cc_151_valid(eop_96_cc_151_valid_sig) ,	// output  eop_96_cc_151_valid_sig
	.eop_96_c_151(eop_96_c_151_sig) ,	// output [7:0] eop_96_c_151_sig
	.eop_96_c_151_valid(eop_96_c_151_valid_sig) ,	// output  eop_96_c_151_valid_sig
	.eop_92_t_151(eop_92_t_151_sig) ,	// output [31:0] eop_92_t_151_sig
	.eop_92_t_151_valid(eop_92_t_151_valid_sig) ,	// output  eop_92_t_151_valid_sig
	.eop_92_v1_151(eop_92_v1_151_sig) ,	// output [31:0] eop_92_v1_151_sig
	.eop_92_v1_151_valid(eop_92_v1_151_valid_sig) ,	// output  eop_92_v1_151_valid_sig
	.eop_92_v0_151(eop_92_v0_151_sig) ,	// output [31:0] eop_92_v0_151_sig
	.eop_92_v0_151_valid(eop_92_v0_151_valid_sig) ,	// output  eop_92_v0_151_valid_sig
	.eop_824_k_843(eop_824_k_843_sig) ,	// output [31:0] eop_824_k_843_sig
	.eop_824_k_843_valid(eop_824_k_843_valid_sig) ,	// output  eop_824_k_843_valid_sig
	.eop_824_i_843(eop_824_i_843_sig) ,	// output [31:0] eop_824_i_843_sig
	.eop_824_i_843_valid(eop_824_i_843_valid_sig) ,	// output  eop_824_i_843_valid_sig
	.eop_92_t_113(eop_92_t_113_sig) ,	// output [31:0] eop_92_t_113_sig
	.eop_92_t_113_valid(eop_92_t_113_valid_sig) ,	// output  eop_92_t_113_valid_sig
	.eop_92_t_115(eop_92_t_115_sig) ,	// output [31:0] eop_92_t_115_sig
	.eop_92_t_115_valid(eop_92_t_115_valid_sig) ,	// output  eop_92_t_115_valid_sig
	.eop_93_n_123(eop_93_n_123_sig) ,	// output [31:0] eop_93_n_123_sig
	.eop_93_n_123_valid(eop_93_n_123_valid_sig) ,	// output  eop_93_n_123_valid_sig
	.eop_96_c_120(eop_96_c_120_sig) ,	// output [7:0] eop_96_c_120_sig
	.eop_96_c_120_valid(eop_96_c_120_valid_sig) ,	// output  eop_96_c_120_valid_sig
	.eop_824_l_848(eop_824_l_848_sig) ,	// output [31:0] eop_824_l_848_sig
	.eop_824_l_848_valid(eop_824_l_848_valid_sig) ,	// output  eop_824_l_848_valid_sig
	.eop_826_check_848(eop_826_check_848_sig) ,	// output [31:0] eop_826_check_848_sig
	.eop_826_check_848_valid(eop_826_check_848_valid_sig) ,	// output  eop_826_check_848_valid_sig
	.eop_824_i_850(eop_824_i_850_sig) ,	// output [31:0] eop_824_i_850_sig
	.eop_824_i_850_valid(eop_824_i_850_valid_sig) ,	// output  eop_824_i_850_valid_sig
	.eop_858_main_result_861(eop_858_main_result_861_sig) ,	// output [31:0] eop_858_main_result_861_sig
	.eop_858_main_result_861_valid(eop_858_main_result_861_valid_sig) 	// output  eop_858_main_result_861_valid_sig
);

defparam main_inst.LEGUP_0 = 'b00000000;
defparam main_inst.LEGUP_F_main_BB__0_1 = 'b00000001;
defparam main_inst.LEGUP_F_main_BB__0_2 = 'b00000010;
defparam main_inst.LEGUP_F_main_BB__0_3 = 'b00000011;
defparam main_inst.LEGUP_F_main_BB__0_4 = 'b00000100;
defparam main_inst.LEGUP_F_main_BB__0_5 = 'b00000101;
defparam main_inst.LEGUP_F_main_BB__0_6 = 'b00000110;
defparam main_inst.LEGUP_F_main_BB__0_7 = 'b00000111;
defparam main_inst.LEGUP_F_main_BB__0_8 = 'b00001000;
defparam main_inst.LEGUP_F_main_BB__0_9 = 'b00001001;
defparam main_inst.LEGUP_F_main_BB__0_10 = 'b00001010;
defparam main_inst.LEGUP_F_main_BB__0_11 = 'b00001011;
defparam main_inst.LEGUP_F_main_BB__0_12 = 'b00001100;
defparam main_inst.LEGUP_F_main_BB__0_13 = 'b00001101;
defparam main_inst.LEGUP_F_main_BB__0_14 = 'b00001110;
defparam main_inst.LEGUP_F_main_BB_lrphi4ii_15 = 'b00001111;
defparam main_inst.LEGUP_F_main_BB_lrphi4ii_16 = 'b00010000;
defparam main_inst.LEGUP_F_main_BB_lrphi4ii_17 = 'b00010001;
defparam main_inst.LEGUP_F_main_BB_memcpyexit5ii_18 = 'b00010010;
defparam main_inst.LEGUP_F_main_BB_memcpyexit5ii_19 = 'b00010011;
defparam main_inst.LEGUP_F_main_BB__19_20 = 'b00010100;
defparam main_inst.LEGUP_F_main_BB__19_21 = 'b00010101;
defparam main_inst.LEGUP_F_main_BB__19_22 = 'b00010110;
defparam main_inst.LEGUP_F_main_BB__19_23 = 'b00010111;
defparam main_inst.LEGUP_F_main_BB__19_24 = 'b00011000;
defparam main_inst.LEGUP_F_main_BB__19_25 = 'b00011001;
defparam main_inst.LEGUP_F_main_BB__19_26 = 'b00011010;
defparam main_inst.LEGUP_F_main_BB__19_27 = 'b00011011;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_28 = 'b00011100;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_29 = 'b00011101;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_31 = 'b00011111;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_32 = 'b00100000;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_33 = 'b00100001;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_34 = 'b00100010;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_36 = 'b00100100;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_37 = 'b00100101;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_38 = 'b00100110;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_39 = 'b00100111;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_41 = 'b00101001;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_42 = 'b00101010;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_43 = 'b00101011;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_44 = 'b00101100;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_46 = 'b00101110;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_47 = 'b00101111;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_48 = 'b00110000;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_49 = 'b00110001;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_51 = 'b00110011;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_52 = 'b00110100;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_53 = 'b00110101;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_54 = 'b00110110;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_56 = 'b00111000;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_57 = 'b00111001;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_58 = 'b00111010;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_59 = 'b00111011;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_61 = 'b00111101;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_62 = 'b00111110;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_63 = 'b00111111;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_64 = 'b01000000;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_66 = 'b01000010;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_67 = 'b01000011;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_68 = 'b01000100;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_69 = 'b01000101;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_71 = 'b01000111;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_72 = 'b01001000;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_73 = 'b01001001;
defparam main_inst.LEGUP_F_main_BB_preheader10ii_74 = 'b01001010;
defparam main_inst.LEGUP_F_main_BB__65_75 = 'b01001011;
defparam main_inst.LEGUP_F_main_BB__65_77 = 'b01001101;
defparam main_inst.LEGUP_F_main_BB__65_78 = 'b01001110;
defparam main_inst.LEGUP_F_main_BB__65_79 = 'b01001111;
defparam main_inst.LEGUP_F_main_BB__65_80 = 'b01010000;
defparam main_inst.LEGUP_F_main_BB_BF_set_keyexiti_81 = 'b01010001;
defparam main_inst.LEGUP_F_main_BB_BF_set_keyexiti_82 = 'b01010010;
defparam main_inst.LEGUP_F_main_BB_preheaderi_83 = 'b01010011;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_84 = 'b01010100;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_85 = 'b01010101;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_86 = 'b01010110;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_87 = 'b01010111;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_88 = 'b01011000;
defparam main_inst.LEGUP_F_main_BB_critedgethreadi_89 = 'b01011001;
defparam main_inst.LEGUP_F_main_BB_lrphi_90 = 'b01011010;
defparam main_inst.LEGUP_F_main_BB__76_91 = 'b01011011;
defparam main_inst.LEGUP_F_main_BB__76_92 = 'b01011100;
defparam main_inst.LEGUP_F_main_BB__76_93 = 'b01011101;
defparam main_inst.LEGUP_F_main_BB__76_94 = 'b01011110;
defparam main_inst.LEGUP_F_main_BB_critedgei_95 = 'b01011111;
defparam main_inst.LEGUP_F_main_BB_critedgei_96 = 'b01100000;
defparam main_inst.LEGUP_F_main_BB_lrph12iipreheader_97 = 'b01100001;
defparam main_inst.LEGUP_F_main_BB_lrph12ii_98 = 'b01100010;
defparam main_inst.LEGUP_F_main_BB__90_99 = 'b01100011;
defparam main_inst.LEGUP_F_main_BB__90_100 = 'b01100100;
defparam main_inst.LEGUP_F_main_BB__90_101 = 'b01100101;
defparam main_inst.LEGUP_F_main_BB__90_102 = 'b01100110;
defparam main_inst.LEGUP_F_main_BB__90_103 = 'b01100111;
defparam main_inst.LEGUP_F_main_BB__90_104 = 'b01101000;
defparam main_inst.LEGUP_F_main_BB__90_106 = 'b01101010;
defparam main_inst.LEGUP_F_main_BB__90_107 = 'b01101011;
defparam main_inst.LEGUP_F_main_BB__90_108 = 'b01101100;
defparam main_inst.LEGUP_F_main_BB__90_109 = 'b01101101;
defparam main_inst.LEGUP_F_main_BB__90_110 = 'b01101110;
defparam main_inst.LEGUP_F_main_BB__90_111 = 'b01101111;
defparam main_inst.LEGUP_F_main_BB__90_112 = 'b01110000;
defparam main_inst.LEGUP_F_main_BB__135_113 = 'b01110001;
defparam main_inst.LEGUP_F_main_BB__135_114 = 'b01110010;
defparam main_inst.LEGUP_F_main_BB__135_115 = 'b01110011;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexitiloopexit_116 = 'b01110100;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexiti_117 = 'b01110101;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexiti_118 = 'b01110110;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexiti_119 = 'b01110111;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexiti_120 = 'b01111000;
defparam main_inst.LEGUP_F_main_BB_BF_cfb64_encryptexiti_121 = 'b01111001;
defparam main_inst.LEGUP_F_main_BB_lrph9ipreheader_122 = 'b01111010;
defparam main_inst.LEGUP_F_main_BB_lrph9i_123 = 'b01111011;
defparam main_inst.LEGUP_F_main_BB_lrph9i_124 = 'b01111100;
defparam main_inst.LEGUP_F_main_BB_lrph9i_125 = 'b01111101;
defparam main_inst.LEGUP_F_main_BB_lrph9i_126 = 'b01111110;
defparam main_inst.LEGUP_F_main_BB__crit_edgei_127 = 'b01111111;
defparam main_inst.LEGUP_F_main_BB__156_128 = 'b10000000;
defparam main_inst.LEGUP_F_main_BB__156_129 = 'b10000001;
defparam main_inst.LEGUP_F_main_BB_blowfish_mainexit_130 = 'b10000010;
defparam main_inst.LEGUP_F_main_BB_blowfish_mainexit_131 = 'b10000011;
defparam main_inst.LEGUP_F_main_BB__160_132 = 'b10000100;
defparam main_inst.LEGUP_F_main_BB__162_133 = 'b10000101;
defparam main_inst.LEGUP_F_main_BB__164_134 = 'b10000110;
defparam main_inst.tag_offset = 'b000000000;
//defparam main_inst.tag_addr_offset = ;
defparam main_inst.LEGUP_function_call_30 = 'b00011110;
defparam main_inst.LEGUP_function_call_35 = 'b00100011;
defparam main_inst.LEGUP_function_call_40 = 'b00101000;
defparam main_inst.LEGUP_function_call_45 = 'b00101101;
defparam main_inst.LEGUP_function_call_50 = 'b00110010;
defparam main_inst.LEGUP_function_call_55 = 'b00110111;
defparam main_inst.LEGUP_function_call_60 = 'b00111100;
defparam main_inst.LEGUP_function_call_65 = 'b01000001;
defparam main_inst.LEGUP_function_call_70 = 'b01000110;
defparam main_inst.LEGUP_function_call_76 = 'b01001100;
defparam main_inst.LEGUP_function_call_105 = 'b01101001;
