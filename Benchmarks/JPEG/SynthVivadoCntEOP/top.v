// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
jpeg_main jpeg_main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_89_read_position_53(eop_89_read_position_53_sig) ,	// output [31:0] eop_89_read_position_53_sig
	.eop_89_read_position_53_ap_vld(eop_89_read_position_53_ap_vld_sig) ,	// output  eop_89_read_position_53_ap_vld_sig
	.eop_70_i_marker_54(eop_70_i_marker_54_sig) ,	// output [31:0] eop_70_i_marker_54_sig
	.eop_70_i_marker_54_ap_vld(eop_70_i_marker_54_ap_vld_sig) ,	// output  eop_70_i_marker_54_ap_vld_sig
	.eop_89_out_length_get_sof_55(eop_89_out_length_get_sof_55_sig) ,	// output [31:0] eop_89_out_length_get_sof_55_sig
	.eop_89_out_length_get_sof_55_ap_vld(eop_89_out_length_get_sof_55_ap_vld_sig) ,	// output  eop_89_out_length_get_sof_55_ap_vld_sig
	.eop_90_out_data_precision_get_s(eop_90_out_data_precision_get_s_sig) ,	// output [31:0] eop_90_out_data_precision_get_s_sig
	.eop_90_out_data_precision_get_s_ap_vld(eop_90_out_data_precision_get_s_ap_vld_sig) ,	// output  eop_90_out_data_precision_get_s_ap_vld_sig
	.eop_91_out_p_jinfo_image_heigh(eop_91_out_p_jinfo_image_heigh_sig) ,	// output [31:0] eop_91_out_p_jinfo_image_heigh_sig
	.eop_91_out_p_jinfo_image_heigh_ap_vld(eop_91_out_p_jinfo_image_heigh_ap_vld_sig) ,	// output  eop_91_out_p_jinfo_image_heigh_ap_vld_sig
	.eop_92_out_p_jinfo_image_width(eop_92_out_p_jinfo_image_width_sig) ,	// output [31:0] eop_92_out_p_jinfo_image_width_sig
	.eop_92_out_p_jinfo_image_width_ap_vld(eop_92_out_p_jinfo_image_width_ap_vld_sig) ,	// output  eop_92_out_p_jinfo_image_width_ap_vld_sig
	.eop_93_out_p_jinfo_num_compone(eop_93_out_p_jinfo_num_compone_sig) ,	// output [31:0] eop_93_out_p_jinfo_num_compone_sig
	.eop_93_out_p_jinfo_num_compone_ap_vld(eop_93_out_p_jinfo_num_compone_ap_vld_sig) ,	// output  eop_93_out_p_jinfo_num_compone_ap_vld_sig
	.eop_111_i_get_sos_60(eop_111_i_get_sos_60_sig) ,	// output [31:0] eop_111_i_get_sos_60_sig
	.eop_111_i_get_sos_60_ap_vld(eop_111_i_get_sos_60_ap_vld_sig) ,	// output  eop_111_i_get_sos_60_ap_vld_sig
	.eop_112_out_length_get_sos_61(eop_112_out_length_get_sos_61_sig) ,	// output [31:0] eop_112_out_length_get_sos_61_sig
	.eop_112_out_length_get_sos_61_ap_vld(eop_112_out_length_get_sos_61_ap_vld_sig) ,	// output  eop_112_out_length_get_sos_61_ap_vld_sig
	.eop_113_out_num_comp_get_sos_6(eop_113_out_num_comp_get_sos_6_sig) ,	// output [31:0] eop_113_out_num_comp_get_sos_6_sig
	.eop_113_out_num_comp_get_sos_6_ap_vld(eop_113_out_num_comp_get_sos_6_ap_vld_sig) ,	// output  eop_113_out_num_comp_get_sos_6_ap_vld_sig
	.eop_127_i_get_dht_63(eop_127_i_get_dht_63_sig) ,	// output [31:0] eop_127_i_get_dht_63_sig
	.eop_127_i_get_dht_63_ap_vld(eop_127_i_get_dht_63_ap_vld_sig) ,	// output  eop_127_i_get_dht_63_ap_vld_sig
	.eop_141_i_get_dqt_64(eop_141_i_get_dqt_64_sig) ,	// output [31:0] eop_141_i_get_dqt_64_sig
	.eop_141_i_get_dqt_64_ap_vld(eop_141_i_get_dqt_64_ap_vld_sig) ,	// output  eop_141_i_get_dqt_64_ap_vld_sig
	.eop_41_main_result_67(eop_41_main_result_67_sig) ,	// output [31:0] eop_41_main_result_67_sig
	.eop_41_main_result_67_ap_vld(eop_41_main_result_67_ap_vld_sig) ,	// output  eop_41_main_result_67_ap_vld_sig
	.eop_56_ci_66(eop_56_ci_66_sig) ,	// output [31:0] eop_56_ci_66_sig
	.eop_56_ci_66_ap_vld(eop_56_ci_66_ap_vld_sig) ,	// output  eop_56_ci_66_ap_vld_sig
	.eop_564_sow_SOI_568(eop_564_sow_SOI_568_sig) ,	// output [31:0] eop_564_sow_SOI_568_sig
	.eop_564_sow_SOI_568_ap_vld(eop_564_sow_SOI_568_ap_vld_sig) ,	// output  eop_564_sow_SOI_568_ap_vld_sig
	.eop_563_unread_marker_570(eop_563_unread_marker_570_sig) ,	// output [31:0] eop_563_unread_marker_570_sig
	.eop_563_unread_marker_570_ap_vld(eop_563_unread_marker_570_ap_vld_sig) ,	// output  eop_563_unread_marker_570_ap_vld_sig
	.eop_206_c1_207(eop_206_c1_207_sig) ,	// output [31:0] eop_206_c1_207_sig
	.eop_206_c1_207_ap_vld(eop_206_c1_207_ap_vld_sig) ,	// output  eop_206_c1_207_ap_vld_sig
	.eop_206_c2_208(eop_206_c2_208_sig) ,	// output [31:0] eop_206_c2_208_sig
	.eop_206_c2_208_ap_vld(eop_206_c2_208_ap_vld_sig) ,	// output  eop_206_c2_208_ap_vld_sig
	.eop_41_main_result_214(eop_41_main_result_214_sig) ,	// output [31:0] eop_41_main_result_214_sig
	.eop_41_main_result_214_ap_vld(eop_41_main_result_214_ap_vld_sig) ,	// output  eop_41_main_result_214_ap_vld_sig
	.eop_563_unread_marker_575(eop_563_unread_marker_575_sig) ,	// output [31:0] eop_563_unread_marker_575_sig
	.eop_563_unread_marker_575_ap_vld(eop_563_unread_marker_575_ap_vld_sig) ,	// output  eop_563_unread_marker_575_ap_vld_sig
	.eop_224_c_227(eop_224_c_227_sig) ,	// output [31:0] eop_224_c_227_sig
	.eop_224_c_227_ap_vld(eop_224_c_227_ap_vld_sig) ,	// output  eop_224_c_227_ap_vld_sig
	.eop_224_c_230(eop_224_c_230_sig) ,	// output [31:0] eop_224_c_230_sig
	.eop_224_c_230_ap_vld(eop_224_c_230_ap_vld_sig) ,	// output  eop_224_c_230_ap_vld_sig
	.eop_224_c_233(eop_224_c_233_sig) ,	// output [31:0] eop_224_c_233_sig
	.eop_224_c_233_ap_vld(eop_224_c_233_ap_vld_sig) ,	// output  eop_224_c_233_ap_vld_sig
	.eop_563_unread_marker_577(eop_563_unread_marker_577_sig) ,	// output [31:0] eop_563_unread_marker_577_sig
	.eop_563_unread_marker_577_ap_vld(eop_563_unread_marker_577_ap_vld_sig) ,	// output  eop_563_unread_marker_577_ap_vld_sig
	.eop_41_main_result_584(eop_41_main_result_584_sig) ,	// output [31:0] eop_41_main_result_584_sig
	.eop_41_main_result_584_ap_vld(eop_41_main_result_584_ap_vld_sig) ,	// output  eop_41_main_result_584_ap_vld_sig
	.eop_70_i_marker_582(eop_70_i_marker_582_sig) ,	// output [31:0] eop_70_i_marker_582_sig
	.eop_70_i_marker_582_ap_vld(eop_70_i_marker_582_ap_vld_sig) ,	// output  eop_70_i_marker_582_ap_vld_sig
	.eop_564_sow_SOI_590(eop_564_sow_SOI_590_sig) ,	// output [31:0] eop_564_sow_SOI_590_sig
	.eop_564_sow_SOI_590_ap_vld(eop_564_sow_SOI_590_ap_vld_sig) ,	// output  eop_564_sow_SOI_590_ap_vld_sig
	.eop_181_c_183(eop_181_c_183_sig) ,	// output [15:0] eop_181_c_183_sig
	.eop_181_c_183_ap_vld(eop_181_c_183_ap_vld_sig) ,	// output  eop_181_c_183_ap_vld_sig
	.eop_249_length_259(eop_249_length_259_sig) ,	// output [31:0] eop_249_length_259_sig
	.eop_249_length_259_ap_vld(eop_249_length_259_ap_vld_sig) ,	// output  eop_249_length_259_ap_vld_sig
	.eop_70_p_jinfo_data_precision_s(eop_70_p_jinfo_data_precision_s_sig) ,	// output [7:0] eop_70_p_jinfo_data_precision_s_sig
	.eop_70_p_jinfo_data_precision_s_ap_vld(eop_70_p_jinfo_data_precision_s_ap_vld_sig) ,	// output  eop_70_p_jinfo_data_precision_s_ap_vld_sig
	.eop_71_p_jinfo_image_height_26(eop_71_p_jinfo_image_height_26_sig) ,	// output [15:0] eop_71_p_jinfo_image_height_26_sig
	.eop_71_p_jinfo_image_height_26_ap_vld(eop_71_p_jinfo_image_height_26_ap_vld_sig) ,	// output  eop_71_p_jinfo_image_height_26_ap_vld_sig
	.eop_72_p_jinfo_image_width_262(eop_72_p_jinfo_image_width_262_sig) ,	// output [15:0] eop_72_p_jinfo_image_width_262_sig
	.eop_72_p_jinfo_image_width_262_ap_vld(eop_72_p_jinfo_image_width_262_ap_vld_sig) ,	// output  eop_72_p_jinfo_image_width_262_ap_vld_sig
	.eop_73_p_jinfo_num_components_s(eop_73_p_jinfo_num_components_s_sig) ,	// output [7:0] eop_73_p_jinfo_num_components_s_sig
	.eop_73_p_jinfo_num_components_s_ap_vld(eop_73_p_jinfo_num_components_s_ap_vld_sig) ,	// output  eop_73_p_jinfo_num_components_s_ap_vld_sig
	.eop_41_main_result_273(eop_41_main_result_273_sig) ,	// output [31:0] eop_41_main_result_273_sig
	.eop_41_main_result_273_ap_vld(eop_41_main_result_273_ap_vld_sig) ,	// output  eop_41_main_result_273_ap_vld_sig
	.eop_41_main_result_277(eop_41_main_result_277_sig) ,	// output [31:0] eop_41_main_result_277_sig
	.eop_41_main_result_277_ap_vld(eop_41_main_result_277_ap_vld_sig) ,	// output  eop_41_main_result_277_ap_vld_sig
	.eop_41_main_result_281(eop_41_main_result_281_sig) ,	// output [31:0] eop_41_main_result_281_sig
	.eop_41_main_result_281_ap_vld(eop_41_main_result_281_ap_vld_sig) ,	// output  eop_41_main_result_281_ap_vld_sig
	.eop_41_main_result_285(eop_41_main_result_285_sig) ,	// output [31:0] eop_41_main_result_285_sig
	.eop_41_main_result_285_ap_vld(eop_41_main_result_285_ap_vld_sig) ,	// output  eop_41_main_result_285_ap_vld_sig
	.eop_41_main_result_289(eop_41_main_result_289_sig) ,	// output [31:0] eop_41_main_result_289_sig
	.eop_41_main_result_289_ap_vld(eop_41_main_result_289_ap_vld_sig) ,	// output  eop_41_main_result_289_ap_vld_sig
	.eop_248_c_306(eop_248_c_306_sig) ,	// output [31:0] eop_248_c_306_sig
	.eop_248_c_306_ap_vld(eop_248_c_306_ap_vld_sig) ,	// output  eop_248_c_306_ap_vld_sig
	.eop_41_main_result_319(eop_41_main_result_319_sig) ,	// output [31:0] eop_41_main_result_319_sig
	.eop_41_main_result_319_ap_vld(eop_41_main_result_319_ap_vld_sig) ,	// output  eop_41_main_result_319_ap_vld_sig
	.eop_41_main_result_323(eop_41_main_result_323_sig) ,	// output [31:0] eop_41_main_result_323_sig
	.eop_41_main_result_323_ap_vld(eop_41_main_result_323_ap_vld_sig) ,	// output  eop_41_main_result_323_ap_vld_sig
	.eop_41_main_result_327(eop_41_main_result_327_sig) ,	// output [31:0] eop_41_main_result_327_sig
	.eop_41_main_result_327_ap_vld(eop_41_main_result_327_ap_vld_sig) ,	// output  eop_41_main_result_327_ap_vld_sig
	.eop_41_main_result_331(eop_41_main_result_331_sig) ,	// output [31:0] eop_41_main_result_331_sig
	.eop_41_main_result_331_ap_vld(eop_41_main_result_331_ap_vld_sig) ,	// output  eop_41_main_result_331_ap_vld_sig
	.eop_41_main_result_335(eop_41_main_result_335_sig) ,	// output [31:0] eop_41_main_result_335_sig
	.eop_41_main_result_335_ap_vld(eop_41_main_result_335_ap_vld_sig) ,	// output  eop_41_main_result_335_ap_vld_sig
	.eop_74_p_jinfo_smp_fact_345(eop_74_p_jinfo_smp_fact_345_sig) ,	// output [31:0] eop_74_p_jinfo_smp_fact_345_sig
	.eop_74_p_jinfo_smp_fact_345_ap_vld(eop_74_p_jinfo_smp_fact_345_ap_vld_sig) ,	// output  eop_74_p_jinfo_smp_fact_345_ap_vld_sig
	.eop_74_p_jinfo_smp_fact_348(eop_74_p_jinfo_smp_fact_348_sig) ,	// output [31:0] eop_74_p_jinfo_smp_fact_348_sig
	.eop_74_p_jinfo_smp_fact_348_ap_vld(eop_74_p_jinfo_smp_fact_348_ap_vld_sig) ,	// output  eop_74_p_jinfo_smp_fact_348_ap_vld_sig
	.eop_357_length_367(eop_357_length_367_sig) ,	// output [31:0] eop_357_length_367_sig
	.eop_357_length_367_ap_vld(eop_357_length_367_ap_vld_sig) ,	// output  eop_357_length_367_ap_vld_sig
	.eop_357_num_comp_368(eop_357_num_comp_368_sig) ,	// output [31:0] eop_357_num_comp_368_sig
	.eop_357_num_comp_368_ap_vld(eop_357_num_comp_368_ap_vld_sig) ,	// output  eop_357_num_comp_368_ap_vld_sig
	.eop_41_main_result_375(eop_41_main_result_375_sig) ,	// output [31:0] eop_41_main_result_375_sig
	.eop_41_main_result_375_ap_vld(eop_41_main_result_375_ap_vld_sig) ,	// output  eop_41_main_result_375_ap_vld_sig
	.eop_41_main_result_379(eop_41_main_result_379_sig) ,	// output [31:0] eop_41_main_result_379_sig
	.eop_41_main_result_379_ap_vld(eop_41_main_result_379_ap_vld_sig) ,	// output  eop_41_main_result_379_ap_vld_sig
	.eop_358_cc_384(eop_358_cc_384_sig) ,	// output [31:0] eop_358_cc_384_sig
	.eop_358_cc_384_ap_vld(eop_358_cc_384_ap_vld_sig) ,	// output  eop_358_cc_384_ap_vld_sig
	.eop_358_c_385(eop_358_c_385_sig) ,	// output [31:0] eop_358_c_385_sig
	.eop_358_c_385_ap_vld(eop_358_c_385_ap_vld_sig) ,	// output  eop_358_c_385_ap_vld_sig
	.eop_41_main_result_399(eop_41_main_result_399_sig) ,	// output [31:0] eop_41_main_result_399_sig
	.eop_41_main_result_399_ap_vld(eop_41_main_result_399_ap_vld_sig) ,	// output  eop_41_main_result_399_ap_vld_sig
	.eop_41_main_result_409(eop_41_main_result_409_sig) ,	// output [31:0] eop_41_main_result_409_sig
	.eop_41_main_result_409_ap_vld(eop_41_main_result_409_ap_vld_sig) ,	// output  eop_41_main_result_409_ap_vld_sig
	.eop_41_main_result_413(eop_41_main_result_413_sig) ,	// output [31:0] eop_41_main_result_413_sig
	.eop_41_main_result_413_ap_vld(eop_41_main_result_413_ap_vld_sig) ,	// output  eop_41_main_result_413_ap_vld_sig
	.eop_41_main_result_417(eop_41_main_result_417_sig) ,	// output [31:0] eop_41_main_result_417_sig
	.eop_41_main_result_417_ap_vld(eop_41_main_result_417_ap_vld_sig) ,	// output  eop_41_main_result_417_ap_vld_sig
	.eop_111_i_get_sos_419(eop_111_i_get_sos_419_sig) ,	// output [31:0] eop_111_i_get_sos_419_sig
	.eop_111_i_get_sos_419_ap_vld(eop_111_i_get_sos_419_ap_vld_sig) ,	// output  eop_111_i_get_sos_419_ap_vld_sig
	.eop_358_j_424(eop_358_j_424_sig) ,	// output [31:0] eop_358_j_424_sig
	.eop_358_j_424_ap_vld(eop_358_j_424_ap_vld_sig) ,	// output  eop_358_j_424_ap_vld_sig
	.eop_358_c_427(eop_358_c_427_sig) ,	// output [31:0] eop_358_c_427_sig
	.eop_358_c_427_ap_vld(eop_358_c_427_ap_vld_sig) ,	// output  eop_358_c_427_ap_vld_sig
	.eop_358_j_425(eop_358_j_425_sig) ,	// output [31:0] eop_358_j_425_sig
	.eop_358_j_425_ap_vld(eop_358_j_425_ap_vld_sig) ,	// output  eop_358_j_425_ap_vld_sig
	.eop_444_length_449(eop_444_length_449_sig) ,	// output [31:0] eop_444_length_449_sig
	.eop_444_length_449_ap_vld(eop_444_length_449_ap_vld_sig) ,	// output  eop_444_length_449_ap_vld_sig
	.eop_41_main_result_456(eop_41_main_result_456_sig) ,	// output [31:0] eop_41_main_result_456_sig
	.eop_41_main_result_456_ap_vld(eop_41_main_result_456_ap_vld_sig) ,	// output  eop_41_main_result_456_ap_vld_sig
	.eop_445_index_460(eop_445_index_460_sig) ,	// output [31:0] eop_445_index_460_sig
	.eop_445_index_460_ap_vld(eop_445_index_460_ap_vld_sig) ,	// output  eop_445_index_460_ap_vld_sig
	.eop_41_main_result_466(eop_41_main_result_466_sig) ,	// output [31:0] eop_41_main_result_466_sig
	.eop_41_main_result_466_ap_vld(eop_41_main_result_466_ap_vld_sig) ,	// output  eop_41_main_result_466_ap_vld_sig
	.eop_445_count_480(eop_445_count_480_sig) ,	// output [31:0] eop_445_count_480_sig
	.eop_445_count_480_ap_vld(eop_445_count_480_ap_vld_sig) ,	// output  eop_445_count_480_ap_vld_sig
	.eop_445_count_484(eop_445_count_484_sig) ,	// output [31:0] eop_445_count_484_sig
	.eop_445_count_484_ap_vld(eop_445_count_484_ap_vld_sig) ,	// output  eop_445_count_484_ap_vld_sig
	.eop_41_main_result_491(eop_41_main_result_491_sig) ,	// output [31:0] eop_41_main_result_491_sig
	.eop_41_main_result_491_ap_vld(eop_41_main_result_491_ap_vld_sig) ,	// output  eop_41_main_result_491_ap_vld_sig
	.eop_127_i_get_dht_493(eop_127_i_get_dht_493_sig) ,	// output [31:0] eop_127_i_get_dht_493_sig
	.eop_127_i_get_dht_493_ap_vld(eop_127_i_get_dht_493_ap_vld_sig) ,	// output  eop_127_i_get_dht_493_ap_vld_sig
	.eop_509_length_514(eop_509_length_514_sig) ,	// output [31:0] eop_509_length_514_sig
	.eop_509_length_514_ap_vld(eop_509_length_514_ap_vld_sig) ,	// output  eop_509_length_514_ap_vld_sig
	.eop_41_main_result_521(eop_41_main_result_521_sig) ,	// output [31:0] eop_41_main_result_521_sig
	.eop_41_main_result_521_ap_vld(eop_41_main_result_521_ap_vld_sig) ,	// output  eop_41_main_result_521_ap_vld_sig
	.eop_510_num_525(eop_510_num_525_sig) ,	// output [31:0] eop_510_num_525_sig
	.eop_510_num_525_ap_vld(eop_510_num_525_ap_vld_sig) ,	// output  eop_510_num_525_ap_vld_sig
	.eop_510_prec_527(eop_510_prec_527_sig) ,	// output [31:0] eop_510_prec_527_sig
	.eop_510_prec_527_ap_vld(eop_510_prec_527_ap_vld_sig) ,	// output  eop_510_prec_527_ap_vld_sig
	.eop_41_main_result_536(eop_41_main_result_536_sig) ,	// output [31:0] eop_41_main_result_536_sig
	.eop_41_main_result_536_ap_vld(eop_41_main_result_536_ap_vld_sig) ,	// output  eop_41_main_result_536_ap_vld_sig
	.eop_41_main_result_540(eop_41_main_result_540_sig) ,	// output [31:0] eop_41_main_result_540_sig
	.eop_41_main_result_540_ap_vld(eop_41_main_result_540_ap_vld_sig) ,	// output  eop_41_main_result_540_ap_vld_sig
	.eop_141_i_get_dqt_542(eop_141_i_get_dqt_542_sig) ,	// output [31:0] eop_141_i_get_dqt_542_sig
	.eop_141_i_get_dqt_542_ap_vld(eop_141_i_get_dqt_542_ap_vld_sig) ,	// output  eop_141_i_get_dqt_542_ap_vld_sig
	.eop_511_tmp_547(eop_511_tmp_547_sig) ,	// output [31:0] eop_511_tmp_547_sig
	.eop_511_tmp_547_ap_vld(eop_511_tmp_547_ap_vld_sig) ,	// output  eop_511_tmp_547_ap_vld_sig
	.eop_511_tmp_549(eop_511_tmp_549_sig) ,	// output [31:0] eop_511_tmp_549_sig
	.eop_511_tmp_549_ap_vld(eop_511_tmp_549_ap_vld_sig) ,	// output  eop_511_tmp_549_ap_vld_sig
	.eop_103_p_jinfo_MCUHeight_56(eop_103_p_jinfo_MCUHeight_56_sig) ,	// output [31:0] eop_103_p_jinfo_MCUHeight_56_sig
	.eop_103_p_jinfo_MCUHeight_56_ap_vld(eop_103_p_jinfo_MCUHeight_56_ap_vld_sig) ,	// output  eop_103_p_jinfo_MCUHeight_56_ap_vld_sig
	.eop_102_p_jinfo_MCUWidth_57(eop_102_p_jinfo_MCUWidth_57_sig) ,	// output [31:0] eop_102_p_jinfo_MCUWidth_57_sig
	.eop_102_p_jinfo_MCUWidth_57_ap_vld(eop_102_p_jinfo_MCUWidth_57_ap_vld_sig) ,	// output  eop_102_p_jinfo_MCUWidth_57_ap_vld_sig
	.eop_104_p_jinfo_NumMCU_58(eop_104_p_jinfo_NumMCU_58_sig) ,	// output [31:0] eop_104_p_jinfo_NumMCU_58_sig
	.eop_104_p_jinfo_NumMCU_58_ap_vld(eop_104_p_jinfo_NumMCU_58_ap_vld_sig) ,	// output  eop_104_p_jinfo_NumMCU_58_ap_vld_sig
	.eop_174_i_183(eop_174_i_183_sig) ,	// output [31:0] eop_174_i_183_sig
	.eop_174_i_183_ap_vld(eop_174_i_183_ap_vld_sig) ,	// output  eop_174_i_183_ap_vld_sig
	.eop_174_p_183(eop_174_p_183_sig) ,	// output [31:0] eop_174_p_183_sig
	.eop_174_p_183_ap_vld(eop_174_p_183_ap_vld_sig) ,	// output  eop_174_p_183_ap_vld_sig
	.eop_174_p_185(eop_174_p_185_sig) ,	// output [31:0] eop_174_p_185_sig
	.eop_174_p_185_ap_vld(eop_174_p_185_ap_vld_sig) ,	// output  eop_174_p_185_ap_vld_sig
	.eop_177_lastp_190(eop_177_lastp_190_sig) ,	// output [31:0] eop_177_lastp_190_sig
	.eop_177_lastp_190_ap_vld(eop_177_lastp_190_ap_vld_sig) ,	// output  eop_177_lastp_190_ap_vld_sig
	.eop_174_p_192(eop_174_p_192_sig) ,	// output [31:0] eop_174_p_192_sig
	.eop_174_p_192_ap_vld(eop_174_p_192_ap_vld_sig) ,	// output  eop_174_p_192_ap_vld_sig
	.eop_174_code_193(eop_174_code_193_sig) ,	// output [31:0] eop_174_code_193_sig
	.eop_174_code_193_ap_vld(eop_174_code_193_ap_vld_sig) ,	// output  eop_174_code_193_ap_vld_sig
	.eop_174_size_194(eop_174_size_194_sig) ,	// output [31:0] eop_174_size_194_sig
	.eop_174_size_194_ap_vld(eop_174_size_194_ap_vld_sig) ,	// output  eop_174_size_194_ap_vld_sig
	.eop_174_size_206(eop_174_size_206_sig) ,	// output [31:0] eop_174_size_206_sig
	.eop_174_size_206_ap_vld(eop_174_size_206_ap_vld_sig) ,	// output  eop_174_size_206_ap_vld_sig
	.eop_174_p_197(eop_174_p_197_sig) ,	// output [31:0] eop_174_p_197_sig
	.eop_174_p_197_ap_vld(eop_174_p_197_ap_vld_sig) ,	// output  eop_174_p_197_ap_vld_sig
	.eop_174_code_197(eop_174_code_197_sig) ,	// output [31:0] eop_174_code_197_sig
	.eop_174_code_197_ap_vld(eop_174_code_197_ap_vld_sig) ,	// output  eop_174_code_197_ap_vld_sig
	.eop_174_l_210(eop_174_l_210_sig) ,	// output [31:0] eop_174_l_210_sig
	.eop_174_l_210_ap_vld(eop_174_l_210_ap_vld_sig) ,	// output  eop_174_l_210_ap_vld_sig
	.eop_174_p_210(eop_174_p_210_sig) ,	// output [31:0] eop_174_p_210_sig
	.eop_174_p_210_ap_vld(eop_174_p_210_ap_vld_sig) ,	// output  eop_174_p_210_ap_vld_sig
	.eop_178_p_dhtbl_ml_210(eop_178_p_dhtbl_ml_210_sig) ,	// output [31:0] eop_178_p_dhtbl_ml_210_sig
	.eop_178_p_dhtbl_ml_210_ap_vld(eop_178_p_dhtbl_ml_210_ap_vld_sig) ,	// output  eop_178_p_dhtbl_ml_210_ap_vld_sig
	.eop_174_p_216(eop_174_p_216_sig) ,	// output [31:0] eop_174_p_216_sig
	.eop_174_p_216_ap_vld(eop_174_p_216_ap_vld_sig) ,	// output  eop_174_p_216_ap_vld_sig
	.eop_178_p_dhtbl_ml_218(eop_178_p_dhtbl_ml_218_sig) ,	// output [31:0] eop_178_p_dhtbl_ml_218_sig
	.eop_178_p_dhtbl_ml_218_ap_vld(eop_178_p_dhtbl_ml_218_ap_vld_sig) ,	// output  eop_178_p_dhtbl_ml_218_ap_vld_sig
	.eop_174_p_219(eop_174_p_219_sig) ,	// output [31:0] eop_174_p_219_sig
	.eop_174_p_219_ap_vld(eop_174_p_219_ap_vld_sig) ,	// output  eop_174_p_219_ap_vld_sig
	.eop_52_tmp_63(eop_52_tmp_63_sig) ,	// output [31:0] eop_52_tmp_63_sig
	.eop_52_tmp_63_ap_vld(eop_52_tmp_63_ap_vld_sig) ,	// output  eop_52_tmp_63_ap_vld_sig
	.eop_52_tmp_69(eop_52_tmp_69_sig) ,	// output [31:0] eop_52_tmp_69_sig
	.eop_52_tmp_69_ap_vld(eop_52_tmp_69_ap_vld_sig) ,	// output  eop_52_tmp_69_ap_vld_sig
	.eop_52_tmp_75(eop_52_tmp_75_sig) ,	// output [31:0] eop_52_tmp_75_sig
	.eop_52_tmp_75_ap_vld(eop_52_tmp_75_ap_vld_sig) ,	// output  eop_52_tmp_75_ap_vld_sig
	.eop_52_tmp_81(eop_52_tmp_81_sig) ,	// output [31:0] eop_52_tmp_81_sig
	.eop_52_tmp_81_ap_vld(eop_52_tmp_81_ap_vld_sig) ,	// output  eop_52_tmp_81_ap_vld_sig
	.eop_339_CurrentMCU_339(eop_339_CurrentMCU_339_sig) ,	// output [31:0] eop_339_CurrentMCU_339_sig
	.eop_339_CurrentMCU_339_ap_vld(eop_339_CurrentMCU_339_ap_vld_sig) ,	// output  eop_339_CurrentMCU_339_ap_vld_sig
	.eop_258_tbl_no_263(eop_258_tbl_no_263_sig) ,	// output [31:0] eop_258_tbl_no_263_sig
	.eop_258_tbl_no_263_ap_vld(eop_258_tbl_no_263_ap_vld_sig) ,	// output  eop_258_tbl_no_263_ap_vld_sig
	.eop_98_temp_101(eop_98_temp_101_sig) ,	// output [31:0] eop_98_temp_101_sig
	.eop_98_temp_101_ap_vld(eop_98_temp_101_ap_vld_sig) ,	// output  eop_98_temp_101_ap_vld_sig
	.eop_98_temp_100(eop_98_temp_100_sig) ,	// output [31:0] eop_98_temp_100_sig
	.eop_98_temp_100_ap_vld(eop_98_temp_100_ap_vld_sig) ,	// output  eop_98_temp_100_ap_vld_sig
	.eop_88_current_read_byte_118(eop_88_current_read_byte_118_sig) ,	// output [31:0] eop_88_current_read_byte_118_sig
	.eop_88_current_read_byte_118_ap_vld(eop_88_current_read_byte_118_ap_vld_sig) ,	// output  eop_88_current_read_byte_118_ap_vld_sig
	.eop_89_read_position_119(eop_89_read_position_119_sig) ,	// output [31:0] eop_89_read_position_119_sig
	.eop_89_read_position_119_ap_vld(eop_89_read_position_119_ap_vld_sig) ,	// output  eop_89_read_position_119_ap_vld_sig
	.eop_89_read_position_122(eop_89_read_position_122_sig) ,	// output [31:0] eop_89_read_position_122_sig
	.eop_89_read_position_122_ap_vld(eop_89_read_position_122_ap_vld_sig) ,	// output  eop_89_read_position_122_ap_vld_sig
	.eop_233_code_235(eop_233_code_235_sig) ,	// output [31:0] eop_233_code_235_sig
	.eop_233_code_235_ap_vld(eop_233_code_235_ap_vld_sig) ,	// output  eop_233_code_235_ap_vld_sig
	.eop_233_code_237(eop_233_code_237_sig) ,	// output [31:0] eop_233_code_237_sig
	.eop_233_code_237_ap_vld(eop_233_code_237_ap_vld_sig) ,	// output  eop_233_code_237_ap_vld_sig
	.eop_41_main_result_241(eop_41_main_result_241_sig) ,	// output [31:0] eop_41_main_result_241_sig
	.eop_41_main_result_241_ap_vld(eop_41_main_result_241_ap_vld_sig) ,	// output  eop_41_main_result_241_ap_vld_sig
	.eop_233_p_242(eop_233_p_242_sig) ,	// output [31:0] eop_233_p_242_sig
	.eop_233_p_242_ap_vld(eop_233_p_242_ap_vld_sig) ,	// output  eop_233_p_242_ap_vld_sig
	.eop_258_s_264(eop_258_s_264_sig) ,	// output [31:0] eop_258_s_264_sig
	.eop_258_s_264_ap_vld(eop_258_s_264_ap_vld_sig) ,	// output  eop_258_s_264_ap_vld_sig
	.eop_134_n_137(eop_134_n_137_sig) ,	// output [31:0] eop_134_n_137_sig
	.eop_134_n_137_ap_vld(eop_134_n_137_ap_vld_sig) ,	// output  eop_134_n_137_ap_vld_sig
	.eop_135_p_138(eop_135_p_138_sig) ,	// output [31:0] eop_135_p_138_sig
	.eop_135_p_138_ap_vld(eop_135_p_138_ap_vld_sig) ,	// output  eop_135_p_138_ap_vld_sig
	.eop_135_rv_141(eop_135_rv_141_sig) ,	// output [31:0] eop_135_rv_141_sig
	.eop_135_rv_141_ap_vld(eop_135_rv_141_ap_vld_sig) ,	// output  eop_135_rv_141_ap_vld_sig
	.eop_88_current_read_byte_142(eop_88_current_read_byte_142_sig) ,	// output [31:0] eop_88_current_read_byte_142_sig
	.eop_88_current_read_byte_142_ap_vld(eop_88_current_read_byte_142_ap_vld_sig) ,	// output  eop_88_current_read_byte_142_ap_vld_sig
	.eop_89_read_position_145(eop_89_read_position_145_sig) ,	// output [31:0] eop_89_read_position_145_sig
	.eop_89_read_position_145_ap_vld(eop_89_read_position_145_ap_vld_sig) ,	// output  eop_89_read_position_145_ap_vld_sig
	.eop_88_current_read_byte_149(eop_88_current_read_byte_149_sig) ,	// output [31:0] eop_88_current_read_byte_149_sig
	.eop_88_current_read_byte_149_ap_vld(eop_88_current_read_byte_149_ap_vld_sig) ,	// output  eop_88_current_read_byte_149_ap_vld_sig
	.eop_89_read_position_150(eop_89_read_position_150_sig) ,	// output [31:0] eop_89_read_position_150_sig
	.eop_89_read_position_150_ap_vld(eop_89_read_position_150_ap_vld_sig) ,	// output  eop_89_read_position_150_ap_vld_sig
	.eop_89_read_position_155(eop_89_read_position_155_sig) ,	// output [31:0] eop_89_read_position_155_sig
	.eop_89_read_position_155_ap_vld(eop_89_read_position_155_ap_vld_sig) ,	// output  eop_89_read_position_155_ap_vld_sig
	.eop_135_p_160(eop_135_p_160_sig) ,	// output [31:0] eop_135_p_160_sig
	.eop_135_p_160_ap_vld(eop_135_p_160_ap_vld_sig) ,	// output  eop_135_p_160_ap_vld_sig
	.eop_89_read_position_162(eop_89_read_position_162_sig) ,	// output [31:0] eop_89_read_position_162_sig
	.eop_89_read_position_162_ap_vld(eop_89_read_position_162_ap_vld_sig) ,	// output  eop_89_read_position_162_ap_vld_sig
	.eop_258_diff_272(eop_258_diff_272_sig) ,	// output [31:0] eop_258_diff_272_sig
	.eop_258_diff_272_ap_vld(eop_258_diff_272_ap_vld_sig) ,	// output  eop_258_diff_272_ap_vld_sig
	.eop_258_s_273(eop_258_s_273_sig) ,	// output [31:0] eop_258_s_273_sig
	.eop_258_s_273_ap_vld(eop_258_s_273_ap_vld_sig) ,	// output  eop_258_s_273_ap_vld_sig
	.eop_258_diff_276(eop_258_diff_276_sig) ,	// output [31:0] eop_258_diff_276_sig
	.eop_258_diff_276_ap_vld(eop_258_diff_276_ap_vld_sig) ,	// output  eop_258_diff_276_ap_vld_sig
	.eop_258_diff_279(eop_258_diff_279_sig) ,	// output [31:0] eop_258_diff_279_sig
	.eop_258_diff_279_ap_vld(eop_258_diff_279_ap_vld_sig) ,	// output  eop_258_diff_279_ap_vld_sig
	.eop_258_r_292(eop_258_r_292_sig) ,	// output [31:0] eop_258_r_292_sig
	.eop_258_r_292_ap_vld(eop_258_r_292_ap_vld_sig) ,	// output  eop_258_r_292_ap_vld_sig
	.eop_258_s_299(eop_258_s_299_sig) ,	// output [31:0] eop_258_s_299_sig
	.eop_258_s_299_ap_vld(eop_258_s_299_ap_vld_sig) ,	// output  eop_258_s_299_ap_vld_sig
	.eop_258_n_300(eop_258_n_300_sig) ,	// output [31:0] eop_258_n_300_sig
	.eop_258_n_300_ap_vld(eop_258_n_300_ap_vld_sig) ,	// output  eop_258_n_300_ap_vld_sig
	.eop_258_k_303(eop_258_k_303_sig) ,	// output [31:0] eop_258_k_303_sig
	.eop_258_k_303_ap_vld(eop_258_k_303_ap_vld_sig) ,	// output  eop_258_k_303_ap_vld_sig
	.eop_258_s_306(eop_258_s_306_sig) ,	// output [31:0] eop_258_s_306_sig
	.eop_258_s_306_ap_vld(eop_258_s_306_ap_vld_sig) ,	// output  eop_258_s_306_ap_vld_sig
	.eop_258_k_311(eop_258_k_311_sig) ,	// output [31:0] eop_258_k_311_sig
	.eop_258_k_311_ap_vld(eop_258_k_311_ap_vld_sig) ,	// output  eop_258_k_311_ap_vld_sig
	.eop_258_k_313(eop_258_k_313_sig) ,	// output [31:0] eop_258_k_313_sig
	.eop_258_k_313_ap_vld(eop_258_k_313_ap_vld_sig) ,	// output  eop_258_k_313_ap_vld_sig
	.eop_83_b0_91(eop_83_b0_91_sig) ,	// output [31:0] eop_83_b0_91_sig
	.eop_83_b0_91_ap_vld(eop_83_b0_91_ap_vld_sig) ,	// output  eop_83_b0_91_ap_vld_sig
	.eop_82_a0_93(eop_82_a0_93_sig) ,	// output [31:0] eop_82_a0_93_sig
	.eop_82_a0_93_ap_vld(eop_82_a0_93_ap_vld_sig) ,	// output  eop_82_a0_93_ap_vld_sig
	.eop_83_b2_95(eop_83_b2_95_sig) ,	// output [31:0] eop_83_b2_95_sig
	.eop_83_b2_95_ap_vld(eop_83_b2_95_ap_vld_sig) ,	// output  eop_83_b2_95_ap_vld_sig
	.eop_82_a1_97(eop_82_a1_97_sig) ,	// output [31:0] eop_82_a1_97_sig
	.eop_82_a1_97_ap_vld(eop_82_a1_97_ap_vld_sig) ,	// output  eop_82_a1_97_ap_vld_sig
	.eop_83_b1_99(eop_83_b1_99_sig) ,	// output [31:0] eop_83_b1_99_sig
	.eop_83_b1_99_ap_vld(eop_83_b1_99_ap_vld_sig) ,	// output  eop_83_b1_99_ap_vld_sig
	.eop_82_a2_101(eop_82_a2_101_sig) ,	// output [31:0] eop_82_a2_101_sig
	.eop_82_a2_101_ap_vld(eop_82_a2_101_ap_vld_sig) ,	// output  eop_82_a2_101_ap_vld_sig
	.eop_83_b3_103(eop_83_b3_103_sig) ,	// output [31:0] eop_83_b3_103_sig
	.eop_83_b3_103_ap_vld(eop_83_b3_103_ap_vld_sig) ,	// output  eop_83_b3_103_ap_vld_sig
	.eop_82_a3_105(eop_82_a3_105_sig) ,	// output [31:0] eop_82_a3_105_sig
	.eop_82_a3_105_ap_vld(eop_82_a3_105_ap_vld_sig) ,	// output  eop_82_a3_105_ap_vld_sig
	.eop_84_c0_111(eop_84_c0_111_sig) ,	// output [31:0] eop_84_c0_111_sig
	.eop_84_c0_111_ap_vld(eop_84_c0_111_ap_vld_sig) ,	// output  eop_84_c0_111_ap_vld_sig
	.eop_84_c1_112(eop_84_c1_112_sig) ,	// output [31:0] eop_84_c1_112_sig
	.eop_84_c1_112_ap_vld(eop_84_c1_112_ap_vld_sig) ,	// output  eop_84_c1_112_ap_vld_sig
	.eop_84_c2_113(eop_84_c2_113_sig) ,	// output [31:0] eop_84_c2_113_sig
	.eop_84_c2_113_ap_vld(eop_84_c2_113_ap_vld_sig) ,	// output  eop_84_c2_113_ap_vld_sig
	.eop_84_c3_114(eop_84_c3_114_sig) ,	// output [31:0] eop_84_c3_114_sig
	.eop_84_c3_114_ap_vld(eop_84_c3_114_ap_vld_sig) ,	// output  eop_84_c3_114_ap_vld_sig
	.eop_82_a0_118(eop_82_a0_118_sig) ,	// output [31:0] eop_82_a0_118_sig
	.eop_82_a0_118_ap_vld(eop_82_a0_118_ap_vld_sig) ,	// output  eop_82_a0_118_ap_vld_sig
	.eop_82_a1_119(eop_82_a1_119_sig) ,	// output [31:0] eop_82_a1_119_sig
	.eop_82_a1_119_ap_vld(eop_82_a1_119_ap_vld_sig) ,	// output  eop_82_a1_119_ap_vld_sig
	.eop_82_a2_121(eop_82_a2_121_sig) ,	// output [31:0] eop_82_a2_121_sig
	.eop_82_a2_121_ap_vld(eop_82_a2_121_ap_vld_sig) ,	// output  eop_82_a2_121_ap_vld_sig
	.eop_82_a3_122(eop_82_a3_122_sig) ,	// output [31:0] eop_82_a3_122_sig
	.eop_82_a3_122_ap_vld(eop_82_a3_122_ap_vld_sig) ,	// output  eop_82_a3_122_ap_vld_sig
	.eop_83_b0_124(eop_83_b0_124_sig) ,	// output [31:0] eop_83_b0_124_sig
	.eop_83_b0_124_ap_vld(eop_83_b0_124_ap_vld_sig) ,	// output  eop_83_b0_124_ap_vld_sig
	.eop_83_b1_125(eop_83_b1_125_sig) ,	// output [31:0] eop_83_b1_125_sig
	.eop_83_b1_125_ap_vld(eop_83_b1_125_ap_vld_sig) ,	// output  eop_83_b1_125_ap_vld_sig
	.eop_83_b2_126(eop_83_b2_126_sig) ,	// output [31:0] eop_83_b2_126_sig
	.eop_83_b2_126_ap_vld(eop_83_b2_126_ap_vld_sig) ,	// output  eop_83_b2_126_ap_vld_sig
	.eop_83_b3_127(eop_83_b3_127_sig) ,	// output [31:0] eop_83_b3_127_sig
	.eop_83_b3_127_ap_vld(eop_83_b3_127_ap_vld_sig) ,	// output  eop_83_b3_127_ap_vld_sig
	.eop_82_a0_131(eop_82_a0_131_sig) ,	// output [31:0] eop_82_a0_131_sig
	.eop_82_a0_131_ap_vld(eop_82_a0_131_ap_vld_sig) ,	// output  eop_82_a0_131_ap_vld_sig
	.eop_82_a1_132(eop_82_a1_132_sig) ,	// output [31:0] eop_82_a1_132_sig
	.eop_82_a1_132_ap_vld(eop_82_a1_132_ap_vld_sig) ,	// output  eop_82_a1_132_ap_vld_sig
	.eop_82_a2_133(eop_82_a2_133_sig) ,	// output [31:0] eop_82_a2_133_sig
	.eop_82_a2_133_ap_vld(eop_82_a2_133_ap_vld_sig) ,	// output  eop_82_a2_133_ap_vld_sig
	.eop_82_a3_134(eop_82_a3_134_sig) ,	// output [31:0] eop_82_a3_134_sig
	.eop_82_a3_134_ap_vld(eop_82_a3_134_ap_vld_sig) ,	// output  eop_82_a3_134_ap_vld_sig
	.eop_84_c0_136(eop_84_c0_136_sig) ,	// output [31:0] eop_84_c0_136_sig
	.eop_84_c0_136_ap_vld(eop_84_c0_136_ap_vld_sig) ,	// output  eop_84_c0_136_ap_vld_sig
	.eop_84_c1_137(eop_84_c1_137_sig) ,	// output [31:0] eop_84_c1_137_sig
	.eop_84_c1_137_ap_vld(eop_84_c1_137_ap_vld_sig) ,	// output  eop_84_c1_137_ap_vld_sig
	.eop_84_c2_138(eop_84_c2_138_sig) ,	// output [31:0] eop_84_c2_138_sig
	.eop_84_c2_138_ap_vld(eop_84_c2_138_ap_vld_sig) ,	// output  eop_84_c2_138_ap_vld_sig
	.eop_84_c3_139(eop_84_c3_139_sig) ,	// output [31:0] eop_84_c3_139_sig
	.eop_84_c3_139_ap_vld(eop_84_c3_139_ap_vld_sig) ,	// output  eop_84_c3_139_ap_vld_sig
	.eop_83_b0_164(eop_83_b0_164_sig) ,	// output [31:0] eop_83_b0_164_sig
	.eop_83_b0_164_ap_vld(eop_83_b0_164_ap_vld_sig) ,	// output  eop_83_b0_164_ap_vld_sig
	.eop_82_a0_165(eop_82_a0_165_sig) ,	// output [31:0] eop_82_a0_165_sig
	.eop_82_a0_165_ap_vld(eop_82_a0_165_ap_vld_sig) ,	// output  eop_82_a0_165_ap_vld_sig
	.eop_83_b2_166(eop_83_b2_166_sig) ,	// output [31:0] eop_83_b2_166_sig
	.eop_83_b2_166_ap_vld(eop_83_b2_166_ap_vld_sig) ,	// output  eop_83_b2_166_ap_vld_sig
	.eop_82_a1_167(eop_82_a1_167_sig) ,	// output [31:0] eop_82_a1_167_sig
	.eop_82_a1_167_ap_vld(eop_82_a1_167_ap_vld_sig) ,	// output  eop_82_a1_167_ap_vld_sig
	.eop_83_b1_168(eop_83_b1_168_sig) ,	// output [31:0] eop_83_b1_168_sig
	.eop_83_b1_168_ap_vld(eop_83_b1_168_ap_vld_sig) ,	// output  eop_83_b1_168_ap_vld_sig
	.eop_82_a2_169(eop_82_a2_169_sig) ,	// output [31:0] eop_82_a2_169_sig
	.eop_82_a2_169_ap_vld(eop_82_a2_169_ap_vld_sig) ,	// output  eop_82_a2_169_ap_vld_sig
	.eop_83_b3_170(eop_83_b3_170_sig) ,	// output [31:0] eop_83_b3_170_sig
	.eop_83_b3_170_ap_vld(eop_83_b3_170_ap_vld_sig) ,	// output  eop_83_b3_170_ap_vld_sig
	.eop_82_a3_171(eop_82_a3_171_sig) ,	// output [31:0] eop_82_a3_171_sig
	.eop_82_a3_171_ap_vld(eop_82_a3_171_ap_vld_sig) ,	// output  eop_82_a3_171_ap_vld_sig
	.eop_84_c0_178(eop_84_c0_178_sig) ,	// output [31:0] eop_84_c0_178_sig
	.eop_84_c0_178_ap_vld(eop_84_c0_178_ap_vld_sig) ,	// output  eop_84_c0_178_ap_vld_sig
	.eop_84_c1_179(eop_84_c1_179_sig) ,	// output [31:0] eop_84_c1_179_sig
	.eop_84_c1_179_ap_vld(eop_84_c1_179_ap_vld_sig) ,	// output  eop_84_c1_179_ap_vld_sig
	.eop_84_c2_180(eop_84_c2_180_sig) ,	// output [31:0] eop_84_c2_180_sig
	.eop_84_c2_180_ap_vld(eop_84_c2_180_ap_vld_sig) ,	// output  eop_84_c2_180_ap_vld_sig
	.eop_84_c3_181(eop_84_c3_181_sig) ,	// output [31:0] eop_84_c3_181_sig
	.eop_84_c3_181_ap_vld(eop_84_c3_181_ap_vld_sig) ,	// output  eop_84_c3_181_ap_vld_sig
	.eop_82_a0_185(eop_82_a0_185_sig) ,	// output [31:0] eop_82_a0_185_sig
	.eop_82_a0_185_ap_vld(eop_82_a0_185_ap_vld_sig) ,	// output  eop_82_a0_185_ap_vld_sig
	.eop_82_a1_186(eop_82_a1_186_sig) ,	// output [31:0] eop_82_a1_186_sig
	.eop_82_a1_186_ap_vld(eop_82_a1_186_ap_vld_sig) ,	// output  eop_82_a1_186_ap_vld_sig
	.eop_82_a2_188(eop_82_a2_188_sig) ,	// output [31:0] eop_82_a2_188_sig
	.eop_82_a2_188_ap_vld(eop_82_a2_188_ap_vld_sig) ,	// output  eop_82_a2_188_ap_vld_sig
	.eop_82_a3_189(eop_82_a3_189_sig) ,	// output [31:0] eop_82_a3_189_sig
	.eop_82_a3_189_ap_vld(eop_82_a3_189_ap_vld_sig) ,	// output  eop_82_a3_189_ap_vld_sig
	.eop_83_b0_193(eop_83_b0_193_sig) ,	// output [31:0] eop_83_b0_193_sig
	.eop_83_b0_193_ap_vld(eop_83_b0_193_ap_vld_sig) ,	// output  eop_83_b0_193_ap_vld_sig
	.eop_83_b1_194(eop_83_b1_194_sig) ,	// output [31:0] eop_83_b1_194_sig
	.eop_83_b1_194_ap_vld(eop_83_b1_194_ap_vld_sig) ,	// output  eop_83_b1_194_ap_vld_sig
	.eop_83_b2_195(eop_83_b2_195_sig) ,	// output [31:0] eop_83_b2_195_sig
	.eop_83_b2_195_ap_vld(eop_83_b2_195_ap_vld_sig) ,	// output  eop_83_b2_195_ap_vld_sig
	.eop_83_b3_196(eop_83_b3_196_sig) ,	// output [31:0] eop_83_b3_196_sig
	.eop_83_b3_196_ap_vld(eop_83_b3_196_ap_vld_sig) ,	// output  eop_83_b3_196_ap_vld_sig
	.eop_82_a0_200(eop_82_a0_200_sig) ,	// output [31:0] eop_82_a0_200_sig
	.eop_82_a0_200_ap_vld(eop_82_a0_200_ap_vld_sig) ,	// output  eop_82_a0_200_ap_vld_sig
	.eop_82_a1_201(eop_82_a1_201_sig) ,	// output [31:0] eop_82_a1_201_sig
	.eop_82_a1_201_ap_vld(eop_82_a1_201_ap_vld_sig) ,	// output  eop_82_a1_201_ap_vld_sig
	.eop_82_a2_202(eop_82_a2_202_sig) ,	// output [31:0] eop_82_a2_202_sig
	.eop_82_a2_202_ap_vld(eop_82_a2_202_ap_vld_sig) ,	// output  eop_82_a2_202_ap_vld_sig
	.eop_82_a3_203(eop_82_a3_203_sig) ,	// output [31:0] eop_82_a3_203_sig
	.eop_82_a3_203_ap_vld(eop_82_a3_203_ap_vld_sig) ,	// output  eop_82_a3_203_ap_vld_sig
	.eop_84_c0_205(eop_84_c0_205_sig) ,	// output [31:0] eop_84_c0_205_sig
	.eop_84_c0_205_ap_vld(eop_84_c0_205_ap_vld_sig) ,	// output  eop_84_c0_205_ap_vld_sig
	.eop_84_c1_206(eop_84_c1_206_sig) ,	// output [31:0] eop_84_c1_206_sig
	.eop_84_c1_206_ap_vld(eop_84_c1_206_ap_vld_sig) ,	// output  eop_84_c1_206_ap_vld_sig
	.eop_84_c2_207(eop_84_c2_207_sig) ,	// output [31:0] eop_84_c2_207_sig
	.eop_84_c2_207_ap_vld(eop_84_c2_207_ap_vld_sig) ,	// output  eop_84_c2_207_ap_vld_sig
	.eop_84_c3_208(eop_84_c3_208_sig) ,	// output [31:0] eop_84_c3_208_sig
	.eop_84_c3_208_ap_vld(eop_84_c3_208_ap_vld_sig) ,	// output  eop_84_c3_208_ap_vld_sig
	.eop_80_i_226(eop_80_i_226_sig) ,	// output [31:0] eop_80_i_226_sig
	.eop_80_i_226_ap_vld(eop_80_i_226_ap_vld_sig) ,	// output  eop_80_i_226_ap_vld_sig
	.eop_272_y_276(eop_272_y_276_sig) ,	// output [31:0] eop_272_y_276_sig
	.eop_272_y_276_ap_vld(eop_272_y_276_ap_vld_sig) ,	// output  eop_272_y_276_ap_vld_sig
	.eop_272_u_277(eop_272_u_277_sig) ,	// output [31:0] eop_272_u_277_sig
	.eop_272_u_277_ap_vld(eop_272_u_277_ap_vld_sig) ,	// output  eop_272_u_277_ap_vld_sig
	.eop_272_v_278(eop_272_v_278_sig) ,	// output [31:0] eop_272_v_278_sig
	.eop_272_v_278_ap_vld(eop_272_v_278_ap_vld_sig) ,	// output  eop_272_v_278_ap_vld_sig
	.eop_271_r_280(eop_271_r_280_sig) ,	// output [31:0] eop_271_r_280_sig
	.eop_271_r_280_ap_vld(eop_271_r_280_ap_vld_sig) ,	// output  eop_271_r_280_ap_vld_sig
	.eop_271_g_281(eop_271_g_281_sig) ,	// output [31:0] eop_271_g_281_sig
	.eop_271_g_281_ap_vld(eop_271_g_281_ap_vld_sig) ,	// output  eop_271_g_281_ap_vld_sig
	.eop_271_b_282(eop_271_b_282_sig) ,	// output [31:0] eop_271_b_282_sig
	.eop_271_b_282_ap_vld(eop_271_b_282_ap_vld_sig) ,	// output  eop_271_b_282_ap_vld_sig
	.eop_271_r_285(eop_271_r_285_sig) ,	// output [31:0] eop_271_r_285_sig
	.eop_271_r_285_ap_vld(eop_271_r_285_ap_vld_sig) ,	// output  eop_271_r_285_ap_vld_sig
	.eop_271_r_287(eop_271_r_287_sig) ,	// output [31:0] eop_271_r_287_sig
	.eop_271_r_287_ap_vld(eop_271_r_287_ap_vld_sig) ,	// output  eop_271_r_287_ap_vld_sig
	.eop_271_g_290(eop_271_g_290_sig) ,	// output [31:0] eop_271_g_290_sig
	.eop_271_g_290_ap_vld(eop_271_g_290_ap_vld_sig) ,	// output  eop_271_g_290_ap_vld_sig
	.eop_271_g_292(eop_271_g_292_sig) ,	// output [31:0] eop_271_g_292_sig
	.eop_271_g_292_ap_vld(eop_271_g_292_ap_vld_sig) ,	// output  eop_271_g_292_ap_vld_sig
	.eop_271_b_295(eop_271_b_295_sig) ,	// output [31:0] eop_271_b_295_sig
	.eop_271_b_295_ap_vld(eop_271_b_295_ap_vld_sig) ,	// output  eop_271_b_295_ap_vld_sig
	.eop_271_b_297(eop_271_b_297_sig) ,	// output [31:0] eop_271_b_297_sig
	.eop_271_b_297_ap_vld(eop_271_b_297_ap_vld_sig) ,	// output  eop_271_b_297_ap_vld_sig
	.eop_172_voffs_177(eop_172_voffs_177_sig) ,	// output [31:0] eop_172_voffs_177_sig
	.eop_172_voffs_177_ap_vld(eop_172_voffs_177_ap_vld_sig) ,	// output  eop_172_voffs_177_ap_vld_sig
	.eop_172_hoffs_178(eop_172_hoffs_178_sig) ,	// output [31:0] eop_172_hoffs_178_sig
	.eop_172_hoffs_178_ap_vld(eop_172_hoffs_178_ap_vld_sig) ,	// output  eop_172_hoffs_178_ap_vld_sig
	.eop_145_diff_146(eop_145_diff_146_sig) ,	// output [31:0] eop_145_diff_146_sig
	.eop_145_diff_146_ap_vld(eop_145_diff_146_ap_vld_sig) ,	// output  eop_145_diff_146_ap_vld_sig
	.eop_339_CurrentMCU_391(eop_339_CurrentMCU_391_sig) ,	// output [31:0] eop_339_CurrentMCU_391_sig
	.eop_339_CurrentMCU_391_ap_vld(eop_339_CurrentMCU_391_ap_vld_sig) ,	// output  eop_339_CurrentMCU_391_ap_vld_sig
	.eop_209_voffs_215(eop_209_voffs_215_sig) ,	// output [31:0] eop_209_voffs_215_sig
	.eop_209_voffs_215_ap_vld(eop_209_voffs_215_ap_vld_sig) ,	// output  eop_209_voffs_215_ap_vld_sig
	.eop_209_hoffs_216(eop_209_hoffs_216_sig) ,	// output [31:0] eop_209_hoffs_216_sig
	.eop_209_hoffs_216_ap_vld(eop_209_hoffs_216_ap_vld_sig) ,	// output  eop_209_hoffs_216_ap_vld_sig
	.eop_209_hoffs_225(eop_209_hoffs_225_sig) ,	// output [31:0] eop_209_hoffs_225_sig
	.eop_209_hoffs_225_ap_vld(eop_209_hoffs_225_ap_vld_sig) ,	// output  eop_209_hoffs_225_ap_vld_sig
	.eop_209_voffs_234(eop_209_voffs_234_sig) ,	// output [31:0] eop_209_voffs_234_sig
	.eop_209_voffs_234_ap_vld(eop_209_voffs_234_ap_vld_sig) ,	// output  eop_209_voffs_234_ap_vld_sig
	.eop_209_hoffs_245(eop_209_hoffs_245_sig) ,	// output [31:0] eop_209_hoffs_245_sig
	.eop_209_hoffs_245_ap_vld(eop_209_hoffs_245_ap_vld_sig) ,	// output  eop_209_hoffs_245_ap_vld_sig
	.eop_339_CurrentMCU_434(eop_339_CurrentMCU_434_sig) ,	// output [31:0] eop_339_CurrentMCU_434_sig
	.eop_339_CurrentMCU_434_ap_vld(eop_339_CurrentMCU_434_ap_vld_sig) ,	// output  eop_339_CurrentMCU_434_ap_vld_sig
	.eop_41_main_result_75(eop_41_main_result_75_sig) ,	// output [31:0] eop_41_main_result_75_sig
	.eop_41_main_result_75_ap_vld(eop_41_main_result_75_ap_vld_sig) ,	// output  eop_41_main_result_75_ap_vld_sig
	.eop_41_main_result_80(eop_41_main_result_80_sig) ,	// output [31:0] eop_41_main_result_80_sig
	.eop_41_main_result_80_ap_vld(eop_41_main_result_80_ap_vld_sig) ,	// output  eop_41_main_result_80_ap_vld_sig
	.eop_41_main_result_83(eop_41_main_result_83_sig) ,	// output [31:0] eop_41_main_result_83_sig
	.eop_41_main_result_83_ap_vld(eop_41_main_result_83_ap_vld_sig) ,	// output  eop_41_main_result_83_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 215;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= eop_89_read_position_53_ap_vld_sig;
valid_regs[1] <= eop_70_i_marker_54_ap_vld_sig;
valid_regs[2] <= eop_89_out_length_get_sof_55_ap_vld_sig;
valid_regs[3] <= eop_90_out_data_precision_get_s_ap_vld_sig;
valid_regs[4] <= eop_91_out_p_jinfo_image_heigh_ap_vld_sig;
valid_regs[5] <= eop_92_out_p_jinfo_image_width_ap_vld_sig;
valid_regs[6] <= eop_93_out_p_jinfo_num_compone_ap_vld_sig;
valid_regs[7] <= eop_111_i_get_sos_60_ap_vld_sig;
valid_regs[8] <= eop_112_out_length_get_sos_61_ap_vld_sig;
valid_regs[9] <= eop_113_out_num_comp_get_sos_6_ap_vld_sig;
valid_regs[10] <= eop_127_i_get_dht_63_ap_vld_sig;
valid_regs[11] <= eop_141_i_get_dqt_64_ap_vld_sig;
valid_regs[12] <= eop_41_main_result_67_ap_vld_sig;
valid_regs[13] <= eop_56_ci_66_ap_vld_sig;
valid_regs[14] <= eop_564_sow_SOI_568_ap_vld_sig;
valid_regs[15] <= eop_563_unread_marker_570_ap_vld_sig;
valid_regs[16] <= eop_206_c1_207_ap_vld_sig;
valid_regs[17] <= eop_206_c2_208_ap_vld_sig;
valid_regs[18] <= eop_41_main_result_214_ap_vld_sig;
valid_regs[19] <= eop_563_unread_marker_575_ap_vld_sig;
valid_regs[20] <= eop_224_c_227_ap_vld_sig;
valid_regs[21] <= eop_224_c_230_ap_vld_sig;
valid_regs[22] <= eop_224_c_233_ap_vld_sig;
valid_regs[23] <= eop_563_unread_marker_577_ap_vld_sig;
valid_regs[24] <= eop_41_main_result_584_ap_vld_sig;
valid_regs[25] <= eop_70_i_marker_582_ap_vld_sig;
valid_regs[26] <= eop_564_sow_SOI_590_ap_vld_sig;
valid_regs[27] <= eop_181_c_183_ap_vld_sig;
valid_regs[28] <= eop_249_length_259_ap_vld_sig;
valid_regs[29] <= eop_70_p_jinfo_data_precision_s_ap_vld_sig;
valid_regs[30] <= eop_71_p_jinfo_image_height_26_ap_vld_sig;
valid_regs[31] <= eop_72_p_jinfo_image_width_262_ap_vld_sig;
valid_regs[32] <= eop_73_p_jinfo_num_components_s_ap_vld_sig;
valid_regs[33] <= eop_41_main_result_273_ap_vld_sig;
valid_regs[34] <= eop_41_main_result_277_ap_vld_sig;
valid_regs[35] <= eop_41_main_result_281_ap_vld_sig;
valid_regs[36] <= eop_41_main_result_285_ap_vld_sig;
valid_regs[37] <= eop_41_main_result_289_ap_vld_sig;
valid_regs[38] <= eop_248_c_306_ap_vld_sig;
valid_regs[39] <= eop_41_main_result_319_ap_vld_sig;
valid_regs[40] <= eop_41_main_result_323_ap_vld_sig;
valid_regs[41] <= eop_41_main_result_327_ap_vld_sig;
valid_regs[42] <= eop_41_main_result_331_ap_vld_sig;
valid_regs[43] <= eop_41_main_result_335_ap_vld_sig;
valid_regs[44] <= eop_74_p_jinfo_smp_fact_345_ap_vld_sig;
valid_regs[45] <= eop_74_p_jinfo_smp_fact_348_ap_vld_sig;
valid_regs[46] <= eop_357_length_367_ap_vld_sig;
valid_regs[47] <= eop_357_num_comp_368_ap_vld_sig;
valid_regs[48] <= eop_41_main_result_375_ap_vld_sig;
valid_regs[49] <= eop_41_main_result_379_ap_vld_sig;
valid_regs[50] <= eop_358_cc_384_ap_vld_sig;
valid_regs[51] <= eop_358_c_385_ap_vld_sig;
valid_regs[52] <= eop_41_main_result_399_ap_vld_sig;
valid_regs[53] <= eop_41_main_result_409_ap_vld_sig;
valid_regs[54] <= eop_41_main_result_413_ap_vld_sig;
valid_regs[55] <= eop_41_main_result_417_ap_vld_sig;
valid_regs[56] <= eop_111_i_get_sos_419_ap_vld_sig;
valid_regs[57] <= eop_358_j_424_ap_vld_sig;
valid_regs[58] <= eop_358_c_427_ap_vld_sig;
valid_regs[59] <= eop_358_j_425_ap_vld_sig;
valid_regs[60] <= eop_444_length_449_ap_vld_sig;
valid_regs[61] <= eop_41_main_result_456_ap_vld_sig;
valid_regs[62] <= eop_445_index_460_ap_vld_sig;
valid_regs[63] <= eop_41_main_result_466_ap_vld_sig;
valid_regs[64] <= eop_445_count_480_ap_vld_sig;
valid_regs[65] <= eop_445_count_484_ap_vld_sig;
valid_regs[66] <= eop_41_main_result_491_ap_vld_sig;
valid_regs[67] <= eop_127_i_get_dht_493_ap_vld_sig;
valid_regs[68] <= eop_509_length_514_ap_vld_sig;
valid_regs[69] <= eop_41_main_result_521_ap_vld_sig;
valid_regs[70] <= eop_510_num_525_ap_vld_sig;
valid_regs[71] <= eop_510_prec_527_ap_vld_sig;
valid_regs[72] <= eop_41_main_result_536_ap_vld_sig;
valid_regs[73] <= eop_41_main_result_540_ap_vld_sig;
valid_regs[74] <= eop_141_i_get_dqt_542_ap_vld_sig;
valid_regs[75] <= eop_511_tmp_547_ap_vld_sig;
valid_regs[76] <= eop_511_tmp_549_ap_vld_sig;
valid_regs[77] <= eop_103_p_jinfo_MCUHeight_56_ap_vld_sig;
valid_regs[78] <= eop_102_p_jinfo_MCUWidth_57_ap_vld_sig;
valid_regs[79] <= eop_104_p_jinfo_NumMCU_58_ap_vld_sig;
valid_regs[80] <= eop_174_i_183_ap_vld_sig;
valid_regs[81] <= eop_174_p_183_ap_vld_sig;
valid_regs[82] <= eop_174_p_185_ap_vld_sig;
valid_regs[83] <= eop_177_lastp_190_ap_vld_sig;
valid_regs[84] <= eop_174_p_192_ap_vld_sig;
valid_regs[85] <= eop_174_code_193_ap_vld_sig;
valid_regs[86] <= eop_174_size_194_ap_vld_sig;
valid_regs[87] <= eop_174_size_206_ap_vld_sig;
valid_regs[88] <= eop_174_p_197_ap_vld_sig;
valid_regs[89] <= eop_174_code_197_ap_vld_sig;
valid_regs[90] <= eop_174_l_210_ap_vld_sig;
valid_regs[91] <= eop_174_p_210_ap_vld_sig;
valid_regs[92] <= eop_178_p_dhtbl_ml_210_ap_vld_sig;
valid_regs[93] <= eop_174_p_216_ap_vld_sig;
valid_regs[94] <= eop_178_p_dhtbl_ml_218_ap_vld_sig;
valid_regs[95] <= eop_174_p_219_ap_vld_sig;
valid_regs[96] <= eop_52_tmp_63_ap_vld_sig;
valid_regs[97] <= eop_52_tmp_69_ap_vld_sig;
valid_regs[98] <= eop_52_tmp_75_ap_vld_sig;
valid_regs[99] <= eop_52_tmp_81_ap_vld_sig;
valid_regs[100] <= eop_339_CurrentMCU_339_ap_vld_sig;
valid_regs[101] <= eop_258_tbl_no_263_ap_vld_sig;
valid_regs[102] <= eop_98_temp_101_ap_vld_sig;
valid_regs[103] <= eop_98_temp_100_ap_vld_sig;
valid_regs[104] <= eop_88_current_read_byte_118_ap_vld_sig;
valid_regs[105] <= eop_89_read_position_119_ap_vld_sig;
valid_regs[106] <= eop_89_read_position_122_ap_vld_sig;
valid_regs[107] <= eop_233_code_235_ap_vld_sig;
valid_regs[108] <= eop_233_code_237_ap_vld_sig;
valid_regs[109] <= eop_41_main_result_241_ap_vld_sig;
valid_regs[110] <= eop_233_p_242_ap_vld_sig;
valid_regs[111] <= eop_258_s_264_ap_vld_sig;
valid_regs[112] <= eop_134_n_137_ap_vld_sig;
valid_regs[113] <= eop_135_p_138_ap_vld_sig;
valid_regs[114] <= eop_135_rv_141_ap_vld_sig;
valid_regs[115] <= eop_88_current_read_byte_142_ap_vld_sig;
valid_regs[116] <= eop_89_read_position_145_ap_vld_sig;
valid_regs[117] <= eop_88_current_read_byte_149_ap_vld_sig;
valid_regs[118] <= eop_89_read_position_150_ap_vld_sig;
valid_regs[119] <= eop_89_read_position_155_ap_vld_sig;
valid_regs[120] <= eop_135_p_160_ap_vld_sig;
valid_regs[121] <= eop_89_read_position_162_ap_vld_sig;
valid_regs[122] <= eop_258_diff_272_ap_vld_sig;
valid_regs[123] <= eop_258_s_273_ap_vld_sig;
valid_regs[124] <= eop_258_diff_276_ap_vld_sig;
valid_regs[125] <= eop_258_diff_279_ap_vld_sig;
valid_regs[126] <= eop_258_r_292_ap_vld_sig;
valid_regs[127] <= eop_258_s_299_ap_vld_sig;
valid_regs[128] <= eop_258_n_300_ap_vld_sig;
valid_regs[129] <= eop_258_k_303_ap_vld_sig;
valid_regs[130] <= eop_258_s_306_ap_vld_sig;
valid_regs[131] <= eop_258_k_311_ap_vld_sig;
valid_regs[132] <= eop_258_k_313_ap_vld_sig;
valid_regs[133] <= eop_83_b0_91_ap_vld_sig;
valid_regs[134] <= eop_82_a0_93_ap_vld_sig;
valid_regs[135] <= eop_83_b2_95_ap_vld_sig;
valid_regs[136] <= eop_82_a1_97_ap_vld_sig;
valid_regs[137] <= eop_83_b1_99_ap_vld_sig;
valid_regs[138] <= eop_82_a2_101_ap_vld_sig;
valid_regs[139] <= eop_83_b3_103_ap_vld_sig;
valid_regs[140] <= eop_82_a3_105_ap_vld_sig;
valid_regs[141] <= eop_84_c0_111_ap_vld_sig;
valid_regs[142] <= eop_84_c1_112_ap_vld_sig;
valid_regs[143] <= eop_84_c2_113_ap_vld_sig;
valid_regs[144] <= eop_84_c3_114_ap_vld_sig;
valid_regs[145] <= eop_82_a0_118_ap_vld_sig;
valid_regs[146] <= eop_82_a1_119_ap_vld_sig;
valid_regs[147] <= eop_82_a2_121_ap_vld_sig;
valid_regs[148] <= eop_82_a3_122_ap_vld_sig;
valid_regs[149] <= eop_83_b0_124_ap_vld_sig;
valid_regs[150] <= eop_83_b1_125_ap_vld_sig;
valid_regs[151] <= eop_83_b2_126_ap_vld_sig;
valid_regs[152] <= eop_83_b3_127_ap_vld_sig;
valid_regs[153] <= eop_82_a0_131_ap_vld_sig;
valid_regs[154] <= eop_82_a1_132_ap_vld_sig;
valid_regs[155] <= eop_82_a2_133_ap_vld_sig;
valid_regs[156] <= eop_82_a3_134_ap_vld_sig;
valid_regs[157] <= eop_84_c0_136_ap_vld_sig;
valid_regs[158] <= eop_84_c1_137_ap_vld_sig;
valid_regs[159] <= eop_84_c2_138_ap_vld_sig;
valid_regs[160] <= eop_84_c3_139_ap_vld_sig;
valid_regs[161] <= eop_83_b0_164_ap_vld_sig;
valid_regs[162] <= eop_82_a0_165_ap_vld_sig;
valid_regs[163] <= eop_83_b2_166_ap_vld_sig;
valid_regs[164] <= eop_82_a1_167_ap_vld_sig;
valid_regs[165] <= eop_83_b1_168_ap_vld_sig;
valid_regs[166] <= eop_82_a2_169_ap_vld_sig;
valid_regs[167] <= eop_83_b3_170_ap_vld_sig;
valid_regs[168] <= eop_82_a3_171_ap_vld_sig;
valid_regs[169] <= eop_84_c0_178_ap_vld_sig;
valid_regs[170] <= eop_84_c1_179_ap_vld_sig;
valid_regs[171] <= eop_84_c2_180_ap_vld_sig;
valid_regs[172] <= eop_84_c3_181_ap_vld_sig;
valid_regs[173] <= eop_82_a0_185_ap_vld_sig;
valid_regs[174] <= eop_82_a1_186_ap_vld_sig;
valid_regs[175] <= eop_82_a2_188_ap_vld_sig;
valid_regs[176] <= eop_82_a3_189_ap_vld_sig;
valid_regs[177] <= eop_83_b0_193_ap_vld_sig;
valid_regs[178] <= eop_83_b1_194_ap_vld_sig;
valid_regs[179] <= eop_83_b2_195_ap_vld_sig;
valid_regs[180] <= eop_83_b3_196_ap_vld_sig;
valid_regs[181] <= eop_82_a0_200_ap_vld_sig;
valid_regs[182] <= eop_82_a1_201_ap_vld_sig;
valid_regs[183] <= eop_82_a2_202_ap_vld_sig;
valid_regs[184] <= eop_82_a3_203_ap_vld_sig;
valid_regs[185] <= eop_84_c0_205_ap_vld_sig;
valid_regs[186] <= eop_84_c1_206_ap_vld_sig;
valid_regs[187] <= eop_84_c2_207_ap_vld_sig;
valid_regs[188] <= eop_84_c3_208_ap_vld_sig;
valid_regs[189] <= eop_80_i_226_ap_vld_sig;
valid_regs[190] <= eop_272_y_276_ap_vld_sig;
valid_regs[191] <= eop_272_u_277_ap_vld_sig;
valid_regs[192] <= eop_272_v_278_ap_vld_sig;
valid_regs[193] <= eop_271_r_280_ap_vld_sig;
valid_regs[194] <= eop_271_g_281_ap_vld_sig;
valid_regs[195] <= eop_271_b_282_ap_vld_sig;
valid_regs[196] <= eop_271_r_285_ap_vld_sig;
valid_regs[197] <= eop_271_r_287_ap_vld_sig;
valid_regs[198] <= eop_271_g_290_ap_vld_sig;
valid_regs[199] <= eop_271_g_292_ap_vld_sig;
valid_regs[200] <= eop_271_b_295_ap_vld_sig;
valid_regs[201] <= eop_271_b_297_ap_vld_sig;
valid_regs[202] <= eop_172_voffs_177_ap_vld_sig;
valid_regs[203] <= eop_172_hoffs_178_ap_vld_sig;
valid_regs[204] <= eop_145_diff_146_ap_vld_sig;
valid_regs[205] <= eop_339_CurrentMCU_391_ap_vld_sig;
valid_regs[206] <= eop_209_voffs_215_ap_vld_sig;
valid_regs[207] <= eop_209_hoffs_216_ap_vld_sig;
valid_regs[208] <= eop_209_hoffs_225_ap_vld_sig;
valid_regs[209] <= eop_209_voffs_234_ap_vld_sig;
valid_regs[210] <= eop_209_hoffs_245_ap_vld_sig;
valid_regs[211] <= eop_339_CurrentMCU_434_ap_vld_sig;
valid_regs[212] <= eop_41_main_result_75_ap_vld_sig;
valid_regs[213] <= eop_41_main_result_80_ap_vld_sig;
valid_regs[214] <= eop_41_main_result_83_ap_vld_sig;

end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
