// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [31:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	reg idle = 1;
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	always @ (posedge start or posedge done) begin
		if (start)
			idle <= 1'b0;
		else
			idle <= 1'b1;
	end
	
main main_inst
(
	.clk(clk) ,	// input  clk_sig
	.clk2x() ,	// input  clk2x_sig
	.clk1x_follower() ,	// input  clk1x_follower_sig
	.reset(reset) ,	// input  reset_sig
	.start(start) ,	// input  start_sig
	.finish(done) ,	// output  finish_sig
	.memory_controller_waitrequest(memory_controller_waitrequest_sig) ,	// input  memory_controller_waitrequest_sig
	.return_val(ap_return_sig) ,	// output [31:0] return_val_sig
	.eop_198_main_result_201(eop_198_main_result_201_sig) ,	// output [31:0] eop_198_main_result_201_sig
	.eop_198_main_result_201_valid(eop_198_main_result_201_valid_sig) ,	// output  eop_198_main_result_201_valid_sig
	.eop_200_x1_205(eop_200_x1_205_sig) ,	// output [63:0] eop_200_x1_205_sig
	.eop_200_x1_205_valid(eop_200_x1_205_valid_sig) ,	// output  eop_200_x1_205_valid_sig
	.eop_200_x2_206(eop_200_x2_206_sig) ,	// output [63:0] eop_200_x2_206_sig
	.eop_200_x2_206_valid(eop_200_x2_206_valid_sig) ,	// output  eop_200_x2_206_valid_sig
	.eop_409_aSign_411(eop_409_aSign_411_sig) ,	// output [31:0] eop_409_aSign_411_sig
	.eop_409_aSign_411_valid(eop_409_aSign_411_valid_sig) ,	// output  eop_409_aSign_411_valid_sig
	.eop_409_bSign_412(eop_409_bSign_412_sig) ,	// output [31:0] eop_409_bSign_412_sig
	.eop_409_bSign_412_valid(eop_409_bSign_412_valid_sig) ,	// output  eop_409_bSign_412_valid_sig
	.eop_248_aSig_251(eop_248_aSig_251_sig) ,	// output [63:0] eop_248_aSig_251_sig
	.eop_248_aSig_251_valid(eop_248_aSig_251_valid_sig) ,	// output  eop_248_aSig_251_valid_sig
	.eop_247_aExp_252(eop_247_aExp_252_sig) ,	// output [31:0] eop_247_aExp_252_sig
	.eop_247_aExp_252_valid(eop_247_aExp_252_valid_sig) ,	// output  eop_247_aExp_252_valid_sig
	.eop_248_bSig_253(eop_248_bSig_253_sig) ,	// output [63:0] eop_248_bSig_253_sig
	.eop_248_bSig_253_valid(eop_248_bSig_253_valid_sig) ,	// output  eop_248_bSig_253_valid_sig
	.eop_247_bExp_254(eop_247_bExp_254_sig) ,	// output [31:0] eop_247_bExp_254_sig
	.eop_247_bExp_254_valid(eop_247_bExp_254_valid_sig) ,	// output  eop_247_bExp_254_valid_sig
	.eop_249_expDiff_255(eop_249_expDiff_255_sig) ,	// output [31:0] eop_249_expDiff_255_sig
	.eop_249_expDiff_255_valid(eop_249_expDiff_255_valid_sig) ,	// output  eop_249_expDiff_255_valid_sig
	.eop_249_expDiff_267(eop_249_expDiff_267_sig) ,	// output [31:0] eop_249_expDiff_267_sig
	.eop_249_expDiff_267_valid(eop_249_expDiff_267_valid_sig) ,	// output  eop_249_expDiff_267_valid_sig
	.eop_247_zExp_271(eop_247_zExp_271_sig) ,	// output [31:0] eop_247_zExp_271_sig
	.eop_247_zExp_271_valid(eop_247_zExp_271_valid_sig) ,	// output  eop_247_zExp_271_valid_sig
	.eop_249_expDiff_282(eop_249_expDiff_282_sig) ,	// output [31:0] eop_249_expDiff_282_sig
	.eop_249_expDiff_282_valid(eop_249_expDiff_282_valid_sig) ,	// output  eop_249_expDiff_282_valid_sig
	.eop_247_zExp_288(eop_247_zExp_288_sig) ,	// output [31:0] eop_247_zExp_288_sig
	.eop_247_zExp_288_valid(eop_247_zExp_288_valid_sig) ,	// output  eop_247_zExp_288_valid_sig
	.eop_248_zSig_300(eop_248_zSig_300_sig) ,	// output [63:0] eop_248_zSig_300_sig
	.eop_248_zSig_300_valid(eop_248_zSig_300_valid_sig) ,	// output  eop_248_zSig_300_valid_sig
	.eop_247_zExp_301(eop_247_zExp_301_sig) ,	// output [31:0] eop_247_zExp_301_sig
	.eop_247_zExp_301_valid(eop_247_zExp_301_valid_sig) ,	// output  eop_247_zExp_301_valid_sig
	.eop_248_zSig_305(eop_248_zSig_305_sig) ,	// output [63:0] eop_248_zSig_305_sig
	.eop_248_zSig_305_valid(eop_248_zSig_305_valid_sig) ,	// output  eop_248_zSig_305_valid_sig
	.eop_247_zExp_306(eop_247_zExp_306_sig) ,	// output [31:0] eop_247_zExp_306_sig
	.eop_247_zExp_306_valid(eop_247_zExp_306_valid_sig) ,	// output  eop_247_zExp_306_valid_sig
	.eop_248_zSig_309(eop_248_zSig_309_sig) ,	// output [63:0] eop_248_zSig_309_sig
	.eop_248_zSig_309_valid(eop_248_zSig_309_valid_sig) ,	// output  eop_248_zSig_309_valid_sig
	.eop_247_zExp_310(eop_247_zExp_310_sig) ,	// output [31:0] eop_247_zExp_310_sig
	.eop_247_zExp_310_valid(eop_247_zExp_310_valid_sig) ,	// output  eop_247_zExp_310_valid_sig
	.eop_158_roundingMode_162(eop_158_roundingMode_162_sig) ,	// output [31:0] eop_158_roundingMode_162_sig
	.eop_158_roundingMode_162_valid(eop_158_roundingMode_162_valid_sig) ,	// output  eop_158_roundingMode_162_valid_sig
	.eop_159_roundNearestEven_163(eop_159_roundNearestEven_163_sig) ,	// output [31:0] eop_159_roundNearestEven_163_sig
	.eop_159_roundNearestEven_163_valid(eop_159_roundNearestEven_163_valid_sig) ,	// output  eop_159_roundNearestEven_163_valid_sig
	.eop_160_roundIncrement_164(eop_160_roundIncrement_164_sig) ,	// output [31:0] eop_160_roundIncrement_164_sig
	.eop_160_roundIncrement_164_valid(eop_160_roundIncrement_164_valid_sig) ,	// output  eop_160_roundIncrement_164_valid_sig
	.eop_160_roundBits_186(eop_160_roundBits_186_sig) ,	// output [31:0] eop_160_roundBits_186_sig
	.eop_160_roundBits_186_valid(eop_160_roundBits_186_valid_sig) ,	// output  eop_160_roundBits_186_valid_sig
	.eop_159_isTiny_197(eop_159_isTiny_197_sig) ,	// output [31:0] eop_159_isTiny_197_sig
	.eop_159_isTiny_197_valid(eop_159_isTiny_197_valid_sig) ,	// output  eop_159_isTiny_197_valid_sig
	.eop_156_zExp_201(eop_156_zExp_201_sig) ,	// output [31:0] eop_156_zExp_201_sig
	.eop_156_zExp_201_valid(eop_156_zExp_201_valid_sig) ,	// output  eop_156_zExp_201_valid_sig
	.eop_160_roundBits_202(eop_160_roundBits_202_sig) ,	// output [31:0] eop_160_roundBits_202_sig
	.eop_160_roundBits_202_valid(eop_160_roundBits_202_valid_sig) ,	// output  eop_160_roundBits_202_valid_sig
	.eop_156_zSig_209(eop_156_zSig_209_sig) ,	// output [63:0] eop_156_zSig_209_sig
	.eop_156_zSig_209_valid(eop_156_zSig_209_valid_sig) ,	// output  eop_156_zSig_209_valid_sig
	.eop_156_zExp_212(eop_156_zExp_212_sig) ,	// output [31:0] eop_156_zExp_212_sig
	.eop_156_zExp_212_valid(eop_156_zExp_212_valid_sig) ,	// output  eop_156_zExp_212_valid_sig
	.eop_329_aSig_332(eop_329_aSig_332_sig) ,	// output [63:0] eop_329_aSig_332_sig
	.eop_329_aSig_332_valid(eop_329_aSig_332_valid_sig) ,	// output  eop_329_aSig_332_valid_sig
	.eop_328_aExp_333(eop_328_aExp_333_sig) ,	// output [31:0] eop_328_aExp_333_sig
	.eop_328_aExp_333_valid(eop_328_aExp_333_valid_sig) ,	// output  eop_328_aExp_333_valid_sig
	.eop_329_bSig_334(eop_329_bSig_334_sig) ,	// output [63:0] eop_329_bSig_334_sig
	.eop_329_bSig_334_valid(eop_329_bSig_334_valid_sig) ,	// output  eop_329_bSig_334_valid_sig
	.eop_328_bExp_335(eop_328_bExp_335_sig) ,	// output [31:0] eop_328_bExp_335_sig
	.eop_328_bExp_335_valid(eop_328_bExp_335_valid_sig) ,	// output  eop_328_bExp_335_valid_sig
	.eop_330_expDiff_336(eop_330_expDiff_336_sig) ,	// output [31:0] eop_330_expDiff_336_sig
	.eop_330_expDiff_336_valid(eop_330_expDiff_336_valid_sig) ,	// output  eop_330_expDiff_336_valid_sig
	.eop_328_aExp_352(eop_328_aExp_352_sig) ,	// output [31:0] eop_328_aExp_352_sig
	.eop_328_aExp_352_valid(eop_328_aExp_352_valid_sig) ,	// output  eop_328_aExp_352_valid_sig
	.eop_328_bExp_353(eop_328_bExp_353_sig) ,	// output [31:0] eop_328_bExp_353_sig
	.eop_328_bExp_353_valid(eop_328_bExp_353_valid_sig) ,	// output  eop_328_bExp_353_valid_sig
	.eop_330_expDiff_368(eop_330_expDiff_368_sig) ,	// output [31:0] eop_330_expDiff_368_sig
	.eop_330_expDiff_368_valid(eop_330_expDiff_368_valid_sig) ,	// output  eop_330_expDiff_368_valid_sig
	.eop_329_zSig_374(eop_329_zSig_374_sig) ,	// output [63:0] eop_329_zSig_374_sig
	.eop_329_zSig_374_valid(eop_329_zSig_374_valid_sig) ,	// output  eop_329_zSig_374_valid_sig
	.eop_328_zExp_375(eop_328_zExp_375_sig) ,	// output [31:0] eop_328_zExp_375_sig
	.eop_328_zExp_375_valid(eop_328_zExp_375_valid_sig) ,	// output  eop_328_zExp_375_valid_sig
	.eop_330_expDiff_386(eop_330_expDiff_386_sig) ,	// output [31:0] eop_330_expDiff_386_sig
	.eop_330_expDiff_386_valid(eop_330_expDiff_386_valid_sig) ,	// output  eop_330_expDiff_386_valid_sig
	.eop_329_zSig_392(eop_329_zSig_392_sig) ,	// output [63:0] eop_329_zSig_392_sig
	.eop_329_zSig_392_valid(eop_329_zSig_392_valid_sig) ,	// output  eop_329_zSig_392_valid_sig
	.eop_328_zExp_393(eop_328_zExp_393_sig) ,	// output [31:0] eop_328_zExp_393_sig
	.eop_328_zExp_393_valid(eop_328_zExp_393_valid_sig) ,	// output  eop_328_zExp_393_valid_sig
	.eop_328_zExp_395(eop_328_zExp_395_sig) ,	// output [31:0] eop_328_zExp_395_sig
	.eop_328_zExp_395_valid(eop_328_zExp_395_valid_sig) ,	// output  eop_328_zExp_395_valid_sig
	.eop_229_shiftCount_231(eop_229_shiftCount_231_sig) ,	// output [31:0] eop_229_shiftCount_231_sig
	.eop_229_shiftCount_231_valid(eop_229_shiftCount_231_valid_sig) ,	// output  eop_229_shiftCount_231_valid_sig
	.eop_204_result_207(eop_204_result_207_sig) ,	// output [63:0] eop_204_result_207_sig
	.eop_204_result_207_valid(eop_204_result_207_valid_sig) ,	// output  eop_204_result_207_valid_sig
	.eop_198_main_result_208(eop_198_main_result_208_sig) ,	// output [31:0] eop_198_main_result_208_sig
	.eop_198_main_result_208_valid(eop_198_main_result_208_valid_sig) 	// output  eop_198_main_result_208_valid_sig
);



parameter N = 46;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
	valid_regs[0] <= eop_198_main_result_201_valid_sig;
	valid_regs[1] <= eop_200_x1_205_valid_sig;
	valid_regs[2] <= eop_200_x2_206_valid_sig;
	valid_regs[3] <= eop_409_aSign_411_valid_sig;
	valid_regs[4] <= eop_409_bSign_412_valid_sig;
	valid_regs[5] <= eop_248_aSig_251_valid_sig;
	valid_regs[6] <= eop_247_aExp_252_valid_sig;
	valid_regs[7] <= eop_248_bSig_253_valid_sig;
	valid_regs[8] <= eop_247_bExp_254_valid_sig;
	valid_regs[9] <= eop_249_expDiff_255_valid_sig;
	valid_regs[10] <= eop_249_expDiff_267_valid_sig;
	valid_regs[11] <= eop_247_zExp_271_valid_sig;
	valid_regs[12] <= eop_249_expDiff_282_valid_sig;
	valid_regs[13] <= eop_247_zExp_288_valid_sig;
	valid_regs[14] <= eop_248_zSig_300_valid_sig;
	valid_regs[15] <= eop_247_zExp_301_valid_sig;
	valid_regs[16] <= eop_248_zSig_305_valid_sig;
	valid_regs[17] <= eop_247_zExp_306_valid_sig;
	valid_regs[18] <= eop_248_zSig_309_valid_sig;
	valid_regs[19] <= eop_247_zExp_310_valid_sig;
	valid_regs[20] <= eop_158_roundingMode_162_valid_sig;
	valid_regs[21] <= eop_159_roundNearestEven_163_valid_sig;
	valid_regs[22] <= eop_160_roundIncrement_164_valid_sig;
	valid_regs[23] <= eop_160_roundBits_186_valid_sig;
	valid_regs[24] <= eop_159_isTiny_197_valid_sig;
	valid_regs[25] <= eop_156_zExp_201_valid_sig;
	valid_regs[26] <= eop_160_roundBits_202_valid_sig;
	valid_regs[27] <= eop_156_zSig_209_valid_sig;
	valid_regs[28] <= eop_156_zExp_212_valid_sig;
	valid_regs[29] <= eop_329_aSig_332_valid_sig;
	valid_regs[30] <= eop_328_aExp_333_valid_sig;
	valid_regs[31] <= eop_329_bSig_334_valid_sig;
	valid_regs[32] <= eop_328_bExp_335_valid_sig;
	valid_regs[33] <= eop_330_expDiff_336_valid_sig;
	valid_regs[34] <= eop_328_aExp_352_valid_sig;
	valid_regs[35] <= eop_328_bExp_353_valid_sig;
	valid_regs[36] <= eop_330_expDiff_368_valid_sig;
	valid_regs[37] <= eop_329_zSig_374_valid_sig;
	valid_regs[38] <= eop_328_zExp_375_valid_sig;
	valid_regs[39] <= eop_330_expDiff_386_valid_sig;
	valid_regs[40] <= eop_329_zSig_392_valid_sig;
	valid_regs[41] <= eop_328_zExp_393_valid_sig;
	valid_regs[42] <= eop_328_zExp_395_valid_sig;
	valid_regs[43] <= eop_229_shiftCount_231_valid_sig;
	valid_regs[44] <= eop_204_result_207_valid_sig;
	valid_regs[45] <= eop_198_main_result_208_valid_sig;

end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
