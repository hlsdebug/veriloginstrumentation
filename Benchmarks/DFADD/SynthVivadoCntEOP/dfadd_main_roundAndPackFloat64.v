// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.3
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module dfadd_main_roundAndPackFloat64 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        zSign,
        zExp,
        zSig,
        eop_158_roundingMode_162,
        eop_158_roundingMode_162_ap_vld,
        eop_159_roundNearestEven_163,
        eop_159_roundNearestEven_163_ap_vld,
        eop_160_roundIncrement_164,
        eop_160_roundIncrement_164_ap_vld,
        eop_160_roundBits_186,
        eop_160_roundBits_186_ap_vld,
        eop_159_isTiny_197,
        eop_159_isTiny_197_ap_vld,
        eop_156_zExp_201,
        eop_156_zExp_201_ap_vld,
        eop_160_roundBits_202,
        eop_160_roundBits_202_ap_vld,
        eop_156_zSig_209,
        eop_156_zSig_209_ap_vld,
        eop_156_zExp_212,
        eop_156_zExp_212_ap_vld,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 3'b1;
parameter    ap_ST_st2_fsm_1 = 3'b10;
parameter    ap_ST_st3_fsm_2 = 3'b100;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv12_0 = 12'b000000000000;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_200 = 32'b1000000000;
parameter    ap_const_lv12_7FC = 12'b11111111100;
parameter    ap_const_lv12_7FD = 12'b11111111101;
parameter    ap_const_lv64_200 = 64'b1000000000;
parameter    ap_const_lv32_3F = 32'b111111;
parameter    ap_const_lv32_B = 32'b1011;
parameter    ap_const_lv7_0 = 7'b0000000;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv64_0 = 64'b0000000000000000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv63_0 = 63'b000000000000000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv64_7FF0000000000000 = 64'b111111111110000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv32_A = 32'b1010;
parameter    ap_const_lv10_200 = 10'b1000000000;
parameter    ap_const_lv32_FFFFFFFF = 32'b11111111111111111111111111111111;
parameter    ap_const_lv54_0 = 54'b000000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv52_0 = 52'b0000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv9_0 = 9'b000000000;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [0:0] zSign;
input  [11:0] zExp;
input  [63:0] zSig;
output  [31:0] eop_158_roundingMode_162;
output   eop_158_roundingMode_162_ap_vld;
output  [31:0] eop_159_roundNearestEven_163;
output   eop_159_roundNearestEven_163_ap_vld;
output  [31:0] eop_160_roundIncrement_164;
output   eop_160_roundIncrement_164_ap_vld;
output  [31:0] eop_160_roundBits_186;
output   eop_160_roundBits_186_ap_vld;
output  [31:0] eop_159_isTiny_197;
output   eop_159_isTiny_197_ap_vld;
output  [31:0] eop_156_zExp_201;
output   eop_156_zExp_201_ap_vld;
output  [31:0] eop_160_roundBits_202;
output   eop_160_roundBits_202_ap_vld;
output  [63:0] eop_156_zSig_209;
output   eop_156_zSig_209_ap_vld;
output  [31:0] eop_156_zExp_212;
output   eop_156_zExp_212_ap_vld;
output  [63:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg eop_158_roundingMode_162_ap_vld;
reg eop_159_roundNearestEven_163_ap_vld;
reg eop_160_roundIncrement_164_ap_vld;
reg eop_160_roundBits_186_ap_vld;
reg eop_159_isTiny_197_ap_vld;
reg eop_156_zExp_201_ap_vld;
reg eop_160_roundBits_202_ap_vld;
reg eop_156_zSig_209_ap_vld;
reg eop_156_zExp_212_ap_vld;
reg[63:0] ap_return;
(* fsm_encoding = "none" *) reg   [2:0] ap_CS_fsm = 3'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_21;
wire   [9:0] roundBits_fu_234_p1;
wire   [0:0] tmp_s_fu_243_p2;
reg   [0:0] tmp_s_reg_496;
wire   [0:0] tmp_20_fu_249_p2;
reg   [0:0] tmp_20_reg_500;
wire   [0:0] tmp_21_fu_255_p2;
reg   [0:0] tmp_21_reg_504;
wire   [0:0] tmp_52_fu_267_p3;
reg   [0:0] tmp_52_reg_508;
wire   [0:0] tmp_53_fu_275_p3;
reg   [0:0] tmp_53_reg_512;
reg   [0:0] tmp_55_reg_516;
wire   [0:0] tmp_2_i_fu_335_p2;
reg   [0:0] tmp_2_i_reg_521;
reg   [62:0] tmp_3_i_reg_526;
wire   [0:0] tmp_32_i_fu_351_p2;
reg   [0:0] tmp_32_i_reg_531;
wire   [63:0] tmp_55_i7_fu_365_p2;
wire   [53:0] zSig_assign_2_fu_439_p2;
reg   [53:0] zSig_assign_2_reg_541;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_115;
wire   [63:0] z_6_fu_380_p3;
reg   [63:0] tmp_25_phi_fu_182_p6;
reg   [63:0] tmp_25_reg_179;
wire   [9:0] roundBits_2_fu_388_p1;
reg   [9:0] roundBits_1_phi_fu_193_p6;
reg   [9:0] roundBits_1_reg_190;
reg   [11:0] zExp_assign_1_reg_201;
reg   [11:0] zExp_assign_phi_fu_217_p4;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_147;
wire   [0:0] tmp_30_fu_445_p2;
reg   [63:0] p_0_phi_fu_228_p4;
reg   [63:0] p_0_reg_225;
wire   [63:0] tmp_55_i6_fu_466_p2;
wire   [63:0] tmp_22_fu_261_p2;
wire   [6:0] tmp_54_fu_283_p1;
wire   [6:0] count_assign_fu_287_p2;
wire   [63:0] tmp_25_i_fu_301_p1;
wire   [5:0] tmp_56_fu_311_p1;
wire   [63:0] tmp_29_i_fu_315_p1;
wire   [63:0] tmp_30_i_fu_319_p2;
wire   [63:0] tmp_26_i_fu_305_p2;
wire   [0:0] tmp_57_fu_331_p1;
wire   [0:0] tmp_31_i_fu_325_p2;
wire   [63:0] tmp_i_fu_357_p3;
wire   [63:0] z_2_fu_377_p1;
wire   [63:0] z_fu_371_p3;
wire   [63:0] tmp_26_fu_398_p2;
wire   [53:0] zSig_assign_1_fu_404_p4;
wire   [0:0] tmp_27_fu_419_p2;
wire   [31:0] tmp_28_fu_425_p1;
wire   [31:0] tmp_29_fu_429_p2;
wire  signed [53:0] tmp_43_cast_fu_435_p1;
wire   [63:0] tmp_53_i_fu_450_p3;
wire   [63:0] tmp_54_i_fu_458_p4;
reg   [63:0] ap_return_preg = 64'b0000000000000000000000000000000000000000000000000000000000000000;
reg   [2:0] ap_NS_fsm;
reg    ap_sig_bdd_162;
reg    ap_sig_bdd_169;




/// the current state (ap_CS_fsm) of the state machine. ///
always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

/// ap_return_preg assign process. ///
always @ (posedge ap_clk) begin : ap_ret_ap_return_preg
    if (ap_rst == 1'b1) begin
        ap_return_preg <= ap_const_lv64_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
            ap_return_preg <= p_0_phi_fu_228_p4;
        end
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ((tmp_s_reg_496 == ap_const_lv1_0) | ((tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_reg_504)) | ((tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_reg_508))))) begin
        p_0_reg_225 <= tmp_55_i6_fu_466_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & ~(tmp_20_fu_249_p2 == ap_const_lv1_0)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_21_fu_255_p2) & ~(ap_const_lv1_0 == tmp_52_fu_267_p3))))) begin
        p_0_reg_225 <= tmp_55_i7_fu_365_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (tmp_s_fu_243_p2 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2) & (ap_const_lv1_0 == tmp_53_fu_275_p3)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3) & (ap_const_lv1_0 == tmp_53_fu_275_p3)))))) begin
        roundBits_1_reg_190 <= roundBits_fu_234_p1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        roundBits_1_reg_190 <= roundBits_2_fu_388_p1;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (tmp_s_fu_243_p2 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2) & (ap_const_lv1_0 == tmp_53_fu_275_p3)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3) & (ap_const_lv1_0 == tmp_53_fu_275_p3)))))) begin
        tmp_25_reg_179 <= zSig;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        tmp_25_reg_179 <= z_6_fu_380_p3;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & (tmp_s_fu_243_p2 == ap_const_lv1_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2) & (ap_const_lv1_0 == tmp_53_fu_275_p3)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3) & (ap_const_lv1_0 == tmp_53_fu_275_p3)))))) begin
        zExp_assign_1_reg_201 <= zExp;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        zExp_assign_1_reg_201 <= ap_const_lv12_0;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_s_fu_243_p2 == ap_const_lv1_0))) begin
        tmp_20_reg_500 <= tmp_20_fu_249_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0))) begin
        tmp_21_reg_504 <= tmp_21_fu_255_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2) & ~(ap_const_lv1_0 == tmp_53_fu_275_p3)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3) & ~(ap_const_lv1_0 == tmp_53_fu_275_p3))))) begin
        tmp_2_i_reg_521 <= tmp_2_i_fu_335_p2;
        tmp_32_i_reg_531 <= tmp_32_i_fu_351_p2;
        tmp_3_i_reg_526 <= {{tmp_26_i_fu_305_p2[ap_const_lv32_3F : ap_const_lv32_1]}};
        tmp_55_reg_516 <= count_assign_fu_287_p2[ap_const_lv32_6];
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_21_fu_255_p2))) begin
        tmp_52_reg_508 <= tmp_22_fu_261_p2[ap_const_lv32_3F];
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & (tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3))))) begin
        tmp_53_reg_512 <= zExp[ap_const_lv32_B];
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        tmp_s_reg_496 <= tmp_s_fu_243_p2;
    end
end

/// assign process. ///
always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        zSig_assign_2_reg_541 <= zSig_assign_2_fu_439_p2;
    end
end

/// ap_done assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st3_fsm_2) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

/// ap_idle assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

/// ap_ready assign process. ///
always @ (ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

/// ap_return assign process. ///
always @ (ap_sig_cseq_ST_st3_fsm_2 or p_0_phi_fu_228_p4 or ap_return_preg) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        ap_return = p_0_phi_fu_228_p4;
    end else begin
        ap_return = ap_return_preg;
    end
end

/// ap_sig_cseq_ST_st1_fsm_0 assign process. ///
always @ (ap_sig_bdd_21) begin
    if (ap_sig_bdd_21) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st2_fsm_1 assign process. ///
always @ (ap_sig_bdd_115) begin
    if (ap_sig_bdd_115) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

/// ap_sig_cseq_ST_st3_fsm_2 assign process. ///
always @ (ap_sig_bdd_147) begin
    if (ap_sig_bdd_147) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

/// eop_156_zExp_201_ap_vld assign process. ///
always @ (tmp_s_reg_496 or tmp_53_reg_512 or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        eop_156_zExp_201_ap_vld = ap_const_logic_1;
    end else begin
        eop_156_zExp_201_ap_vld = ap_const_logic_0;
    end
end

/// eop_156_zExp_212_ap_vld assign process. ///
always @ (tmp_s_reg_496 or tmp_20_reg_500 or tmp_21_reg_504 or tmp_52_reg_508 or ap_sig_cseq_ST_st3_fsm_2 or tmp_30_fu_445_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & (((tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_30_fu_445_p2)) | (~(ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_reg_504)) | (~(ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_reg_508))))) begin
        eop_156_zExp_212_ap_vld = ap_const_logic_1;
    end else begin
        eop_156_zExp_212_ap_vld = ap_const_logic_0;
    end
end

/// eop_156_zSig_209_ap_vld assign process. ///
always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        eop_156_zSig_209_ap_vld = ap_const_logic_1;
    end else begin
        eop_156_zSig_209_ap_vld = ap_const_logic_0;
    end
end

/// eop_158_roundingMode_162_ap_vld assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        eop_158_roundingMode_162_ap_vld = ap_const_logic_1;
    end else begin
        eop_158_roundingMode_162_ap_vld = ap_const_logic_0;
    end
end

/// eop_159_isTiny_197_ap_vld assign process. ///
always @ (tmp_s_reg_496 or tmp_53_reg_512 or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        eop_159_isTiny_197_ap_vld = ap_const_logic_1;
    end else begin
        eop_159_isTiny_197_ap_vld = ap_const_logic_0;
    end
end

/// eop_159_roundNearestEven_163_ap_vld assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        eop_159_roundNearestEven_163_ap_vld = ap_const_logic_1;
    end else begin
        eop_159_roundNearestEven_163_ap_vld = ap_const_logic_0;
    end
end

/// eop_160_roundBits_186_ap_vld assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        eop_160_roundBits_186_ap_vld = ap_const_logic_1;
    end else begin
        eop_160_roundBits_186_ap_vld = ap_const_logic_0;
    end
end

/// eop_160_roundBits_202_ap_vld assign process. ///
always @ (tmp_s_reg_496 or tmp_53_reg_512 or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        eop_160_roundBits_202_ap_vld = ap_const_logic_1;
    end else begin
        eop_160_roundBits_202_ap_vld = ap_const_logic_0;
    end
end

/// eop_160_roundIncrement_164_ap_vld assign process. ///
always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        eop_160_roundIncrement_164_ap_vld = ap_const_logic_1;
    end else begin
        eop_160_roundIncrement_164_ap_vld = ap_const_logic_0;
    end
end

/// p_0_phi_fu_228_p4 assign process. ///
always @ (tmp_s_reg_496 or tmp_20_reg_500 or tmp_21_reg_504 or tmp_52_reg_508 or ap_sig_cseq_ST_st3_fsm_2 or p_0_reg_225 or tmp_55_i6_fu_466_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ((tmp_s_reg_496 == ap_const_lv1_0) | ((tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_reg_504)) | ((tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_reg_508))))) begin
        p_0_phi_fu_228_p4 = tmp_55_i6_fu_466_p2;
    end else begin
        p_0_phi_fu_228_p4 = p_0_reg_225;
    end
end

/// roundBits_1_phi_fu_193_p6 assign process. ///
always @ (tmp_s_reg_496 or tmp_53_reg_512 or ap_sig_cseq_ST_st2_fsm_1 or roundBits_2_fu_388_p1 or roundBits_1_reg_190) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        roundBits_1_phi_fu_193_p6 = roundBits_2_fu_388_p1;
    end else begin
        roundBits_1_phi_fu_193_p6 = roundBits_1_reg_190;
    end
end

/// tmp_25_phi_fu_182_p6 assign process. ///
always @ (tmp_s_reg_496 or tmp_53_reg_512 or ap_sig_cseq_ST_st2_fsm_1 or z_6_fu_380_p3 or tmp_25_reg_179) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_53_reg_512))) begin
        tmp_25_phi_fu_182_p6 = z_6_fu_380_p3;
    end else begin
        tmp_25_phi_fu_182_p6 = tmp_25_reg_179;
    end
end

/// zExp_assign_phi_fu_217_p4 assign process. ///
always @ (zExp_assign_1_reg_201 or ap_sig_cseq_ST_st3_fsm_2 or ap_sig_bdd_162 or ap_sig_bdd_169) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        if (ap_sig_bdd_169) begin
            zExp_assign_phi_fu_217_p4 = zExp_assign_1_reg_201;
        end else if (ap_sig_bdd_162) begin
            zExp_assign_phi_fu_217_p4 = ap_const_lv12_0;
        end else begin
            zExp_assign_phi_fu_217_p4 = 'bx;
        end
    end else begin
        zExp_assign_phi_fu_217_p4 = 'bx;
    end
end
/// the next state (ap_NS_fsm) of the state machine. ///
always @ (ap_start or ap_CS_fsm or tmp_s_fu_243_p2 or tmp_20_fu_249_p2 or tmp_21_fu_255_p2 or tmp_52_fu_267_p3) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if ((~(ap_start == ap_const_logic_0) & ((~(tmp_s_fu_243_p2 == ap_const_lv1_0) & ~(tmp_20_fu_249_p2 == ap_const_lv1_0)) | (~(tmp_s_fu_243_p2 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_21_fu_255_p2) & ~(ap_const_lv1_0 == tmp_52_fu_267_p3))))) begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end else if ((~(ap_start == ap_const_logic_0) & ((tmp_s_fu_243_p2 == ap_const_lv1_0) | ((tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_fu_255_p2)) | ((tmp_20_fu_249_p2 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_fu_267_p3))))) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            ap_NS_fsm = ap_ST_st3_fsm_2;
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


/// ap_sig_bdd_115 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_115 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

/// ap_sig_bdd_147 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_147 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end

/// ap_sig_bdd_162 assign process. ///
always @ (tmp_s_reg_496 or tmp_20_reg_500 or tmp_21_reg_504 or tmp_52_reg_508 or tmp_30_fu_445_p2) begin
    ap_sig_bdd_162 = (((tmp_s_reg_496 == ap_const_lv1_0) & ~(ap_const_lv1_0 == tmp_30_fu_445_p2)) | (~(ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_reg_504)) | (~(ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_reg_508)));
end

/// ap_sig_bdd_169 assign process. ///
always @ (tmp_s_reg_496 or tmp_20_reg_500 or tmp_21_reg_504 or tmp_52_reg_508 or tmp_30_fu_445_p2) begin
    ap_sig_bdd_169 = (((tmp_s_reg_496 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_30_fu_445_p2)) | ((ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_21_reg_504)) | ((ap_const_lv1_0 == tmp_30_fu_445_p2) & (tmp_20_reg_500 == ap_const_lv1_0) & (ap_const_lv1_0 == tmp_52_reg_508)));
end

/// ap_sig_bdd_21 assign process. ///
always @ (ap_CS_fsm) begin
    ap_sig_bdd_21 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end
assign count_assign_fu_287_p2 = (ap_const_lv7_0 - tmp_54_fu_283_p1);
assign eop_156_zExp_201 = ap_const_lv32_0;
assign eop_156_zExp_212 = ap_const_lv32_0;
assign eop_156_zSig_209 = zSig_assign_1_fu_404_p4;
assign eop_158_roundingMode_162 = ap_const_lv32_0;
assign eop_159_isTiny_197 = ap_const_lv32_1;
assign eop_159_roundNearestEven_163 = ap_const_lv32_1;
assign eop_160_roundBits_186 = roundBits_fu_234_p1;
assign eop_160_roundBits_202 = roundBits_2_fu_388_p1;
assign eop_160_roundIncrement_164 = ap_const_lv32_200;
assign roundBits_2_fu_388_p1 = z_6_fu_380_p3[9:0];
assign roundBits_fu_234_p1 = zSig[9:0];
assign tmp_20_fu_249_p2 = ($signed(zExp) > $signed(12'b11111111101)? 1'b1: 1'b0);
assign tmp_21_fu_255_p2 = (zExp == ap_const_lv12_7FD? 1'b1: 1'b0);
assign tmp_22_fu_261_p2 = (zSig + ap_const_lv64_200);
assign tmp_25_i_fu_301_p1 = count_assign_fu_287_p2;
assign tmp_26_fu_398_p2 = (tmp_25_phi_fu_182_p6 + ap_const_lv64_200);
assign tmp_26_i_fu_305_p2 = zSig >> tmp_25_i_fu_301_p1;
assign tmp_27_fu_419_p2 = (roundBits_1_phi_fu_193_p6 == ap_const_lv10_200? 1'b1: 1'b0);
assign tmp_28_fu_425_p1 = tmp_27_fu_419_p2;
assign tmp_29_fu_429_p2 = (tmp_28_fu_425_p1 ^ ap_const_lv32_FFFFFFFF);
assign tmp_29_i_fu_315_p1 = tmp_56_fu_311_p1;
assign tmp_2_i_fu_335_p2 = (tmp_57_fu_331_p1 | tmp_31_i_fu_325_p2);
assign tmp_30_fu_445_p2 = (zSig_assign_2_reg_541 == ap_const_lv54_0? 1'b1: 1'b0);
assign tmp_30_i_fu_319_p2 = zSig << tmp_29_i_fu_315_p1;
assign tmp_31_i_fu_325_p2 = (tmp_30_i_fu_319_p2 != ap_const_lv64_0? 1'b1: 1'b0);
assign tmp_32_i_fu_351_p2 = (zSig != ap_const_lv64_0? 1'b1: 1'b0);
assign tmp_43_cast_fu_435_p1 = $signed(tmp_29_fu_429_p2);
assign tmp_52_fu_267_p3 = tmp_22_fu_261_p2[ap_const_lv32_3F];
assign tmp_53_fu_275_p3 = zExp[ap_const_lv32_B];
assign tmp_53_i_fu_450_p3 = {{zExp_assign_phi_fu_217_p4}, {ap_const_lv52_0}};
assign tmp_54_fu_283_p1 = zExp[6:0];
assign tmp_54_i_fu_458_p4 = {{{zSign}, {ap_const_lv9_0}}, {zSig_assign_2_reg_541}};
assign tmp_55_i6_fu_466_p2 = (tmp_53_i_fu_450_p3 + tmp_54_i_fu_458_p4);
assign tmp_55_i7_fu_365_p2 = (tmp_i_fu_357_p3 | ap_const_lv64_7FF0000000000000);
assign tmp_56_fu_311_p1 = zExp[5:0];
assign tmp_57_fu_331_p1 = tmp_26_i_fu_305_p2[0:0];
assign tmp_i_fu_357_p3 = {{zSign}, {ap_const_lv63_0}};
assign tmp_s_fu_243_p2 = (zExp > ap_const_lv12_7FC? 1'b1: 1'b0);
assign zSig_assign_1_fu_404_p4 = {{tmp_26_fu_398_p2[ap_const_lv32_3F : ap_const_lv32_A]}};
assign zSig_assign_2_fu_439_p2 = (zSig_assign_1_fu_404_p4 & tmp_43_cast_fu_435_p1);
assign z_2_fu_377_p1 = tmp_32_i_reg_531;
assign z_6_fu_380_p3 = ((tmp_55_reg_516[0:0] === 1'b1) ? z_2_fu_377_p1 : z_fu_371_p3);
assign z_fu_371_p3 = {{tmp_3_i_reg_526}, {tmp_2_i_reg_521}};


endmodule //dfadd_main_roundAndPackFloat64

