// Adding code from verilog file: ../../../boards/CycloneII/DE2/top.v

module hex_digits(x, hex_LEDs);
    input [3:0] x;
    output [6:0] hex_LEDs;
    
    assign hex_LEDs[0] = (~x[3] & ~x[2] & ~x[1] & x[0]) |
                            (~x[3] & x[2] & ~x[1] & ~x[0]) |
                            (x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & ~x[2] & x[1] & x[0]);
    assign hex_LEDs[1] = (~x[3] & x[2] & ~x[1] & x[0]) |
                            (x[3] & x[1] & x[0]) |
                            (x[3] & x[2] & ~x[0]) |
                            (x[2] & x[1] & ~x[0]);
    assign hex_LEDs[2] = (x[3] & x[2] & ~x[0]) |
                            (x[3] & x[2] & x[1]) |
                            (~x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[3] = (~x[3] & ~x[2] & ~x[1] & x[0]) | 
                            (~x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (x[2] & x[1] & x[0]) | 
                            (x[3] & ~x[2] & x[1] & ~x[0]);
    assign hex_LEDs[4] = (~x[3] & x[0]) |
                            (~x[3] & x[2] & ~x[1]) |
                            (~x[2] & ~x[1] & x[0]);
    assign hex_LEDs[5] = (~x[3] & ~x[2] & x[0]) | 
                            (~x[3] & ~x[2] & x[1]) | 
                            (~x[3] & x[1] & x[0]) | 
                            (x[3] & x[2] & ~x[1] & x[0]);
    assign hex_LEDs[6] = (~x[3] & ~x[2] & ~x[1]) | 
                            (x[3] & x[2] & ~x[1] & ~x[0]) | 
                            (~x[3] & x[2] & x[1] & x[0]);
    
endmodule

module de2 (
	    CLOCK_50,
	    KEY,
	    SW,
	    HEX0,
	    HEX1,
	    HEX2,
	    HEX3,
	    HEX4,
	    HEX5,
	    HEX6,
	    HEX7,
	    LEDG,
		LEDR,
		UART_RXD,
		UART_TXD

	    );

   input CLOCK_50;
   input [3:0] KEY;
   input [17:0] SW;
   output [6:0] HEX0, HEX1,  HEX2,  HEX3,  HEX4,  HEX5,  HEX6,  HEX7;
   reg [6:0] 	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;
   
   output [17:0] LEDR;
   output [7:0] LEDG;
   input UART_RXD;
   output UART_TXD;    
	
	wire 	reset = ~KEY[0];
	wire 	start = ~KEY[1];
	
	reg [31:0] clk_counter = 0;
	reg out_clk=0;
	
	wire div_tick = (clk_counter>=32'd25000);
	
	always @ (negedge CLOCK_50 or posedge reset) begin	
		if (reset) begin
			clk_counter <= 0;
		end		
		else if (div_tick) begin	
			clk_counter <= 0;
		end
		else begin
			clk_counter <= clk_counter + 32'd1;
		end
	end
	
	always @ (posedge CLOCK_50 or posedge reset) begin
		if (reset)
			out_clk <= 0;
		else if (div_tick)
			out_clk <= ~out_clk;
		else	
			out_clk <= out_clk;
	end
	
	wire clk = out_clk;
   
	wire [31:0] 	ap_return_sig;
	reg  [31:0] 	return_val_reg;

	reg  [31:0]	signal_count_reg=0;
	
	reg  [15:0]		parallel_wr_cnt [32:0];
	reg [31:0]sel_parallel_wr_count;
	wire 			done;
   
	//========================================CYCLE COUNTER
	
	
	reg [31:0]cycle_cnt = 0;
	reg [31:0]cycle_cnt_reg = 0;
	reg done_reg;
	always @ (posedge clk or posedge reset) begin
		if (reset)
			cycle_cnt <= 0;
		else if (!idle)
			cycle_cnt <= cycle_cnt + 32'd1;
		else
			cycle_cnt <= cycle_cnt;
	end
	
	always @ (posedge done_reg or posedge reset) begin
		if (reset) begin
			cycle_cnt_reg <= 0;
		end
		else if (done_reg) begin
			cycle_cnt_reg <= cycle_cnt;
		end
		else begin
			cycle_cnt_reg <= cycle_cnt_reg;
		end
	end
	
	always @ (negedge clk or posedge reset) begin
		if (reset)
			done_reg <= 0;
		else if (done)
			done_reg <= 1;
		else
			done_reg <=done_reg;
	end
	
	//========================================PARALLEL WRITE COUNTER
	
	always @ (*) begin
		sel_parallel_wr_count[31:28]	<= 0; //parallel_wr_cnt[SW[6:1]][31:28];
		sel_parallel_wr_count[27:24]	<= 0; //parallel_wr_cnt[SW[6:1]][27:24];
		sel_parallel_wr_count[23:20]	<= 0; //parallel_wr_cnt[SW[6:1]][23:20];
		sel_parallel_wr_count[19:16]	<= 0; //parallel_wr_cnt[SW[6:1]][19:16];
		sel_parallel_wr_count[15:12]	<= parallel_wr_cnt[SW[6:1]][15:12];
		sel_parallel_wr_count[11:8]	<= parallel_wr_cnt[SW[6:1]][11:8];
		sel_parallel_wr_count[7:4]		<= parallel_wr_cnt[SW[6:1]][7:4];
		sel_parallel_wr_count[3:0]		<= parallel_wr_cnt[SW[6:1]][3:0];   
	end
		
	hex_digits h7( .x(hex7), .hex_LEDs(HEX7));
	hex_digits h6( .x(hex6), .hex_LEDs(HEX6));
	hex_digits h5( .x(hex5), .hex_LEDs(HEX5));
	hex_digits h4( .x(hex4), .hex_LEDs(HEX4));
	hex_digits h3( .x(hex3), .hex_LEDs(HEX3));
	hex_digits h2( .x(hex2), .hex_LEDs(HEX2));
	hex_digits h1( .x(hex1), .hex_LEDs(HEX1));
	hex_digits h0( .x(hex0), .hex_LEDs(HEX0));
    
	
	always @ (*) begin
	case (SW)
	0: begin
		hex7 <= KEY[2]?0:return_val_reg[31:28];	//total_signal_count_reg[31:28]:return_val_reg[31:28];
		hex6 <= KEY[2]?0:return_val_reg[27:24];	//total_signal_count_reg[27:24]:return_val_reg[27:24];
		hex5 <= KEY[2]?0:return_val_reg[23:20];	//total_signal_count_reg[23:20]:return_val_reg[23:20];
		hex4 <= KEY[2]?0:return_val_reg[19:16];	//total_signal_count_reg[19:16]:return_val_reg[19:16];
		hex3 <= KEY[2]?signal_count_reg[15:12]:return_val_reg[15:12];
		hex2 <= KEY[2]?signal_count_reg[11:8] :return_val_reg[11:8];
		hex1 <= KEY[2]?signal_count_reg[7:4]  :return_val_reg[7:4];
		hex0 <= KEY[2]?signal_count_reg[3:0]  :return_val_reg[3:0];   
	end
	1: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	
	128: begin
		hex7 <= cycle_cnt_reg[31:28];
		hex6 <= cycle_cnt_reg[27:24];
		hex5 <= cycle_cnt_reg[23:20];
		hex4 <= cycle_cnt_reg[19:16];
		hex3 <= cycle_cnt_reg[15:12];
		hex2 <= cycle_cnt_reg[11:8]; 
		hex1 <= cycle_cnt_reg[7:4];  
		hex0 <= cycle_cnt_reg[3:0];  
	
	end
	
	default: begin
		hex7 <= sel_parallel_wr_count[31:28];
		hex6 <= sel_parallel_wr_count[27:24];
		hex5 <= sel_parallel_wr_count[23:20];
		hex4 <= sel_parallel_wr_count[19:16];
		hex3 <= sel_parallel_wr_count[15:12];
		hex2 <= sel_parallel_wr_count[11:8]; 
		hex1 <= sel_parallel_wr_count[7:4];  
		hex0 <= sel_parallel_wr_count[3:0];  
	end
	
	endcase
	end
	
	always @(posedge clk or posedge reset) begin
		if (reset)
			return_val_reg <= 0;
		else if (done) begin
			return_val_reg <= ap_return_sig;
		end
	end
	
	
main main_inst
(
	.ap_clk(clk) ,	// input  ap_clk_sig
	.ap_rst(reset) ,	// input  ap_rst_sig
	.ap_start(start) ,	// input  ap_start_sig
	.ap_done(done) ,	// output  ap_done_sig
	.ap_idle(idle) ,	// output  ap_idle_sig
	.ap_ready(ready) ,	// output  ap_ready_sig
	.eop_858_main_result_860(eop_858_main_result_860_sig) ,	// output [31:0] eop_858_main_result_860_sig
	.eop_858_main_result_860_ap_vld(eop_858_main_result_860_ap_vld_sig) ,	// output  eop_858_main_result_860_ap_vld_sig
	.eop_823_num_828(eop_823_num_828_sig) ,	// output [31:0] eop_823_num_828_sig
	.eop_823_num_828_ap_vld(eop_823_num_828_ap_vld_sig) ,	// output  eop_823_num_828_ap_vld_sig
	.eop_824_k_829(eop_824_k_829_sig) ,	// output [31:0] eop_824_k_829_sig
	.eop_824_k_829_ap_vld(eop_824_k_829_ap_vld_sig) ,	// output  eop_824_k_829_ap_vld_sig
	.eop_824_l_830(eop_824_l_830_sig) ,	// output [31:0] eop_824_l_830_sig
	.eop_824_l_830_ap_vld(eop_824_l_830_ap_vld_sig) ,	// output  eop_824_l_830_ap_vld_sig
	.eop_825_encordec_831(eop_825_encordec_831_sig) ,	// output [31:0] eop_825_encordec_831_sig
	.eop_825_encordec_831_ap_vld(eop_825_encordec_831_ap_vld_sig) ,	// output  eop_825_encordec_831_ap_vld_sig
	.eop_826_check_832(eop_826_check_832_sig) ,	// output [31:0] eop_826_check_832_sig
	.eop_826_check_832_ap_vld(eop_826_check_832_ap_vld_sig) ,	// output  eop_826_check_832_ap_vld_sig
	.eop_81_n_89(eop_81_n_89_sig) ,	// output [31:0] eop_81_n_89_sig
	.eop_81_n_89_ap_vld(eop_81_n_89_ap_vld_sig) ,	// output  eop_81_n_89_ap_vld_sig
	.eop_101_ri_115(eop_101_ri_115_sig) ,	// output [31:0] eop_101_ri_115_sig
	.eop_101_ri_115_ap_vld(eop_101_ri_115_ap_vld_sig) ,	// output  eop_101_ri_115_ap_vld_sig
	.eop_88_l_91(eop_88_l_91_sig) ,	// output [31:0] eop_88_l_91_sig
	.eop_88_l_91_ap_vld(eop_88_l_91_ap_vld_sig) ,	// output  eop_88_l_91_ap_vld_sig
	.eop_88_r_92(eop_88_r_92_sig) ,	// output [31:0] eop_88_r_92_sig
	.eop_88_r_92_ap_vld(eop_88_r_92_ap_vld_sig) ,	// output  eop_88_r_92_ap_vld_sig
	.eop_824_i_839(eop_824_i_839_sig) ,	// output [31:0] eop_824_i_839_sig
	.eop_824_i_839_ap_vld(eop_824_i_839_ap_vld_sig) ,	// output  eop_824_i_839_ap_vld_sig
	.eop_824_k_843(eop_824_k_843_sig) ,	// output [31:0] eop_824_k_843_sig
	.eop_824_k_843_ap_vld(eop_824_k_843_ap_vld_sig) ,	// output  eop_824_k_843_ap_vld_sig
	.eop_824_i_843(eop_824_i_843_sig) ,	// output [31:0] eop_824_i_843_sig
	.eop_824_i_843_ap_vld(eop_824_i_843_ap_vld_sig) ,	// output  eop_824_i_843_ap_vld_sig
	.eop_93_n_98(eop_93_n_98_sig) ,	// output [31:0] eop_93_n_98_sig
	.eop_93_n_98_ap_vld(eop_93_n_98_ap_vld_sig) ,	// output  eop_93_n_98_ap_vld_sig
	.eop_94_l_99(eop_94_l_99_sig) ,	// output [31:0] eop_94_l_99_sig
	.eop_94_l_99_ap_vld(eop_94_l_99_ap_vld_sig) ,	// output  eop_94_l_99_ap_vld_sig
	.eop_92_t_113(eop_92_t_113_sig) ,	// output [31:0] eop_92_t_113_sig
	.eop_92_t_113_ap_vld(eop_92_t_113_ap_vld_sig) ,	// output  eop_92_t_113_ap_vld_sig
	.eop_92_t_115(eop_92_t_115_sig) ,	// output [31:0] eop_92_t_115_sig
	.eop_92_t_115_ap_vld(eop_92_t_115_ap_vld_sig) ,	// output  eop_92_t_115_ap_vld_sig
	.eop_96_c_120(eop_96_c_120_sig) ,	// output [7:0] eop_96_c_120_sig
	.eop_96_c_120_ap_vld(eop_96_c_120_ap_vld_sig) ,	// output  eop_96_c_120_ap_vld_sig
	.eop_93_n_123(eop_93_n_123_sig) ,	// output [31:0] eop_93_n_123_sig
	.eop_93_n_123_ap_vld(eop_93_n_123_ap_vld_sig) ,	// output  eop_93_n_123_ap_vld_sig
	.eop_94_l_103(eop_94_l_103_sig) ,	// output [31:0] eop_94_l_103_sig
	.eop_94_l_103_ap_vld(eop_94_l_103_ap_vld_sig) ,	// output  eop_94_l_103_ap_vld_sig
	.eop_96_cc_151(eop_96_cc_151_sig) ,	// output [7:0] eop_96_cc_151_sig
	.eop_96_cc_151_ap_vld(eop_96_cc_151_ap_vld_sig) ,	// output  eop_96_cc_151_ap_vld_sig
	.eop_96_c_151(eop_96_c_151_sig) ,	// output [7:0] eop_96_c_151_sig
	.eop_96_c_151_ap_vld(eop_96_c_151_ap_vld_sig) ,	// output  eop_96_c_151_ap_vld_sig
	.eop_92_t_151(eop_92_t_151_sig) ,	// output [31:0] eop_92_t_151_sig
	.eop_92_t_151_ap_vld(eop_92_t_151_ap_vld_sig) ,	// output  eop_92_t_151_ap_vld_sig
	.eop_92_v1_151(eop_92_v1_151_sig) ,	// output [31:0] eop_92_v1_151_sig
	.eop_92_v1_151_ap_vld(eop_92_v1_151_ap_vld_sig) ,	// output  eop_92_v1_151_ap_vld_sig
	.eop_92_v0_151(eop_92_v0_151_sig) ,	// output [31:0] eop_92_v0_151_sig
	.eop_92_v0_151_ap_vld(eop_92_v0_151_ap_vld_sig) ,	// output  eop_92_v0_151_ap_vld_sig
	.eop_824_l_848(eop_824_l_848_sig) ,	// output [31:0] eop_824_l_848_sig
	.eop_824_l_848_ap_vld(eop_824_l_848_ap_vld_sig) ,	// output  eop_824_l_848_ap_vld_sig
	.eop_826_check_848(eop_826_check_848_sig) ,	// output [31:0] eop_826_check_848_sig
	.eop_826_check_848_ap_vld(eop_826_check_848_ap_vld_sig) ,	// output  eop_826_check_848_ap_vld_sig
	.eop_824_i_850(eop_824_i_850_sig) ,	// output [31:0] eop_824_i_850_sig
	.eop_824_i_850_ap_vld(eop_824_i_850_ap_vld_sig) ,	// output  eop_824_i_850_ap_vld_sig
	.eop_858_main_result_861(eop_858_main_result_861_sig) ,	// output [31:0] eop_858_main_result_861_sig
	.eop_858_main_result_861_ap_vld(eop_858_main_result_861_ap_vld_sig) ,	// output  eop_858_main_result_861_ap_vld_sig
	.ap_return(ap_return_sig) 	// output [31:0] ap_return_sig
);

parameter N = 29;
reg [N-1:0] valid_regs;

always @ (negedge clk) begin
	
valid_regs[0] <= valid_regs[0] + eop_858_main_result_860_ap_vld_sig;
valid_regs[1] <= valid_regs[1] + eop_823_num_828_ap_vld_sig;
valid_regs[2] <= valid_regs[2] + eop_824_k_829_ap_vld_sig;
valid_regs[3] <= valid_regs[3] + eop_824_l_830_ap_vld_sig;
valid_regs[4] <= valid_regs[4] + eop_825_encordec_831_ap_vld_sig;
valid_regs[5] <= valid_regs[5] + eop_826_check_832_ap_vld_sig;
valid_regs[6] <= valid_regs[6] + eop_81_n_89_ap_vld_sig;
valid_regs[7] <= valid_regs[7] + eop_101_ri_115_ap_vld_sig;
valid_regs[8] <= valid_regs[8] + eop_88_l_91_ap_vld_sig;
valid_regs[9] <= valid_regs[9] + eop_88_r_92_ap_vld_sig;
valid_regs[10] <= valid_regs[10] + eop_824_i_839_ap_vld_sig;
valid_regs[11] <= valid_regs[11] + eop_824_k_843_ap_vld_sig;
valid_regs[12] <= valid_regs[12] + eop_824_i_843_ap_vld_sig;
valid_regs[13] <= valid_regs[13] + eop_93_n_98_ap_vld_sig;
valid_regs[14] <= valid_regs[14] + eop_94_l_99_ap_vld_sig;
valid_regs[15] <= valid_regs[15] + eop_92_t_113_ap_vld_sig;
valid_regs[16] <= valid_regs[16] + eop_92_t_115_ap_vld_sig;
valid_regs[17] <= valid_regs[17] + eop_96_c_120_ap_vld_sig;
valid_regs[18] <= valid_regs[18] + eop_93_n_123_ap_vld_sig;
valid_regs[19] <= valid_regs[19] + eop_94_l_103_ap_vld_sig;
valid_regs[20] <= valid_regs[20] + eop_96_cc_151_ap_vld_sig;
valid_regs[21] <= valid_regs[21] + eop_96_c_151_ap_vld_sig;
valid_regs[22] <= valid_regs[22] + eop_92_t_151_ap_vld_sig;
valid_regs[23] <= valid_regs[23] + eop_92_v1_151_ap_vld_sig;
valid_regs[24] <= valid_regs[24] + eop_92_v0_151_ap_vld_sig;
valid_regs[25] <= valid_regs[25] + eop_824_l_848_ap_vld_sig;
valid_regs[26] <= valid_regs[26] + eop_826_check_848_ap_vld_sig;
valid_regs[27] <= valid_regs[27] + eop_824_i_850_ap_vld_sig;
valid_regs[28] <= valid_regs[28] + eop_858_main_result_861_ap_vld_sig;


end


integer i;

reg [31:0] sum_valids;

always @*
begin
  sum_valids = 32'b0;
  for(i = 0; i < N; i=i+1)
    sum_valids = sum_valids + valid_regs[i];
end

always @ (posedge clk or posedge reset) begin
	if (reset)
		signal_count_reg <= 0;
	else if (!idle)
		signal_count_reg <= signal_count_reg + sum_valids;
end

always @(posedge clk or posedge reset) begin
	if (reset) begin
		parallel_wr_cnt[0] <=0;
		parallel_wr_cnt[1] <=0;
		parallel_wr_cnt[2] <=0;
		parallel_wr_cnt[3] <=0;
		parallel_wr_cnt[4] <=0;
		parallel_wr_cnt[5] <=0;
		parallel_wr_cnt[6] <=0;
		parallel_wr_cnt[7] <=0;
		parallel_wr_cnt[8] <=0;
		parallel_wr_cnt[9] <=0;
		parallel_wr_cnt[10]<=0;
		parallel_wr_cnt[11]<=0;
		parallel_wr_cnt[12]<=0;
		parallel_wr_cnt[13]<=0;
		parallel_wr_cnt[14]<=0;
		parallel_wr_cnt[15]<=0;
		parallel_wr_cnt[16]<=0;
		parallel_wr_cnt[17]<=0;
		parallel_wr_cnt[18]<=0;
		parallel_wr_cnt[19]<=0;
		parallel_wr_cnt[20]<=0;
		parallel_wr_cnt[21]<=0;
		parallel_wr_cnt[22]<=0;
		parallel_wr_cnt[23]<=0;
		parallel_wr_cnt[24]<=0;
		parallel_wr_cnt[25]<=0;
		parallel_wr_cnt[26]<=0;
		parallel_wr_cnt[27]<=0;
		parallel_wr_cnt[28]<=0;
		parallel_wr_cnt[29]<=0;
		parallel_wr_cnt[30]<=0;
		parallel_wr_cnt[31]<=0;
		parallel_wr_cnt[32]<=0;
	end
	else if (!idle) begin
		if (sum_valids>32)
			parallel_wr_cnt[0] <= parallel_wr_cnt[0] + 1;
		else
			parallel_wr_cnt[sum_valids]<= parallel_wr_cnt[sum_valids] + 1;
	end
end

assign LEDR = sum_valids;

endmodule
